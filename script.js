﻿! function(e, t) {
  "use strict";
  "object" == typeof module && "object" == typeof module.exports ? module.exports = e.document ? t(e, !0) : function(e) {
    if (!e.document) throw new Error("jQuery requires a window with a document");
    return t(e)
  } : t(e)
}("undefined" != typeof window ? window : this, function(k, e) {
  "use strict";
  var t = [],
    C = k.document,
    i = Object.getPrototypeOf,
    a = t.slice,
    g = t.concat,
    l = t.push,
    o = t.indexOf,
    n = {},
    r = n.toString,
    m = n.hasOwnProperty,
    s = m.toString,
    c = s.call(Object),
    v = {},
    y = function(e) {
      return "function" == typeof e && "number" != typeof e.nodeType
    },
    b = function(e) {
      return null != e && e === e.window
    },
    u = {
      type: !0,
      src: !0,
      noModule: !0
    };

  function x(e, t, n) {
    var i, o = (t = t || C).createElement("script");
    if (o.text = e, n)
      for (i in u) n[i] && (o[i] = n[i]);
    t.head.appendChild(o).parentNode.removeChild(o)
  }

  function w(e) {
    return null == e ? e + "" : "object" == typeof e || "function" == typeof e ? n[r.call(e)] || "object" : typeof e
  }
  var _ = function(e, t) {
      return new _.fn.init(e, t)
    },
    d = /^[\s\uFEFF\xA0]+|[\s\uFEFF\xA0]+$/g;

  function f(e) {
    var t = !!e && "length" in e && e.length,
      n = w(e);
    return !y(e) && !b(e) && ("array" === n || 0 === t || "number" == typeof t && 0 < t && t - 1 in e)
  }
  _.fn = _.prototype = {
    jquery: "3.3.1",
    constructor: _,
    length: 0,
    toArray: function() {
      return a.call(this)
    },
    get: function(e) {
      return null == e ? a.call(this) : e < 0 ? this[e + this.length] : this[e]
    },
    pushStack: function(e) {
      var t = _.merge(this.constructor(), e);
      return t.prevObject = this, t
    },
    each: function(e) {
      return _.each(this, e)
    },
    map: function(n) {
      return this.pushStack(_.map(this, function(e, t) {
        return n.call(e, t, e)
      }))
    },
    slice: function() {
      return this.pushStack(a.apply(this, arguments))
    },
    first: function() {
      return this.eq(0)
    },
    last: function() {
      return this.eq(-1)
    },
    eq: function(e) {
      var t = this.length,
        n = +e + (e < 0 ? t : 0);
      return this.pushStack(0 <= n && n < t ? [this[n]] : [])
    },
    end: function() {
      return this.prevObject || this.constructor()
    },
    push: l,
    sort: t.sort,
    splice: t.splice
  }, _.extend = _.fn.extend = function() {
    var e, t, n, i, o, r, s = arguments[0] || {},
      a = 1,
      l = arguments.length,
      c = !1;
    for ("boolean" == typeof s && (c = s, s = arguments[a] || {}, a++), "object" == typeof s || y(s) || (s = {}), a === l && (s = this, a--); a < l; a++)
      if (null != (e = arguments[a]))
        for (t in e) n = s[t], s !== (i = e[t]) && (c && i && (_.isPlainObject(i) || (o = Array.isArray(i))) ? (o ? (o = !1, r = n && Array.isArray(n) ? n : []) : r = n && _.isPlainObject(n) ? n : {}, s[t] = _.extend(c, r, i)) : void 0 !== i && (s[t] = i));
    return s
  }, _.extend({
    expando: "jQuery" + ("3.3.1" + Math.random()).replace(/\D/g, ""),
    isReady: !0,
    error: function(e) {
      throw new Error(e)
    },
    noop: function() {},
    isPlainObject: function(e) {
      var t, n;
      return !(!e || "[object Object]" !== r.call(e) || (t = i(e)) && ("function" != typeof(n = m.call(t, "constructor") && t.constructor) || s.call(n) !== c))
    },
    isEmptyObject: function(e) {
      var t;
      for (t in e) return !1;
      return !0
    },
    globalEval: function(e) {
      x(e)
    },
    each: function(e, t) {
      var n, i = 0;
      if (f(e))
        for (n = e.length; i < n && !1 !== t.call(e[i], i, e[i]); i++);
      else
        for (i in e)
          if (!1 === t.call(e[i], i, e[i])) break;
      return e
    },
    trim: function(e) {
      return null == e ? "" : (e + "").replace(d, "")
    },
    makeArray: function(e, t) {
      var n = t || [];
      return null != e && (f(Object(e)) ? _.merge(n, "string" == typeof e ? [e] : e) : l.call(n, e)), n
    },
    inArray: function(e, t, n) {
      return null == t ? -1 : o.call(t, e, n)
    },
    merge: function(e, t) {
      for (var n = +t.length, i = 0, o = e.length; i < n; i++) e[o++] = t[i];
      return e.length = o, e
    },
    grep: function(e, t, n) {
      for (var i = [], o = 0, r = e.length, s = !n; o < r; o++) !t(e[o], o) !== s && i.push(e[o]);
      return i
    },
    map: function(e, t, n) {
      var i, o, r = 0,
        s = [];
      if (f(e))
        for (i = e.length; r < i; r++) null != (o = t(e[r], r, n)) && s.push(o);
      else
        for (r in e) null != (o = t(e[r], r, n)) && s.push(o);
      return g.apply([], s)
    },
    guid: 1,
    support: v
  }), "function" == typeof Symbol && (_.fn[Symbol.iterator] = t[Symbol.iterator]), _.each("Boolean Number String Function Array Date RegExp Object Error Symbol".split(" "), function(e, t) {
    n["[object " + t + "]"] = t.toLowerCase()
  });
  var h = function(n) {
    var e, h, x, r, o, p, d, g, w, l, c, S, k, s, C, m, a, u, v, _ = "sizzle" + 1 * new Date,
      y = n.document,
      T = 0,
      i = 0,
      f = se(),
      b = se(),
      $ = se(),
      A = function(e, t) {
        return e === t && (c = !0), 0
      },
      j = {}.hasOwnProperty,
      t = [],
      P = t.pop,
      E = t.push,
      D = t.push,
      O = t.slice,
      M = function(e, t) {
        for (var n = 0, i = e.length; n < i; n++)
          if (e[n] === t) return n;
        return -1
      },
      I = "checked|selected|async|autofocus|autoplay|controls|defer|disabled|hidden|ismap|loop|multiple|open|readonly|required|scoped",
      H = "[\\x20\\t\\r\\n\\f]",
      L = "(?:\\\\.|[\\w-]|[^\0-\\xa0])+",
      R = "\\[" + H + "*(" + L + ")(?:" + H + "*([*^$|!~]?=)" + H + "*(?:'((?:\\\\.|[^\\\\'])*)'|\"((?:\\\\.|[^\\\\\"])*)\"|(" + L + "))|)" + H + "*\\]",
      N = ":(" + L + ")(?:\\((('((?:\\\\.|[^\\\\'])*)'|\"((?:\\\\.|[^\\\\\"])*)\")|((?:\\\\.|[^\\\\()[\\]]|" + R + ")*)|.*)\\)|)",
      F = new RegExp(H + "+", "g"),
      B = new RegExp("^" + H + "+|((?:^|[^\\\\])(?:\\\\.)*)" + H + "+$", "g"),
      z = new RegExp("^" + H + "*," + H + "*"),
      q = new RegExp("^" + H + "*([>+~]|" + H + ")" + H + "*"),
      W = new RegExp("=" + H + "*([^\\]'\"]*?)" + H + "*\\]", "g"),
      Y = new RegExp(N),
      V = new RegExp("^" + L + "$"),
      U = {
        ID: new RegExp("^#(" + L + ")"),
        CLASS: new RegExp("^\\.(" + L + ")"),
        TAG: new RegExp("^(" + L + "|[*])"),
        ATTR: new RegExp("^" + R),
        PSEUDO: new RegExp("^" + N),
        CHILD: new RegExp("^:(only|first|last|nth|nth-last)-(child|of-type)(?:\\(" + H + "*(even|odd|(([+-]|)(\\d*)n|)" + H + "*(?:([+-]|)" + H + "*(\\d+)|))" + H + "*\\)|)", "i"),
        bool: new RegExp("^(?:" + I + ")$", "i"),
        needsContext: new RegExp("^" + H + "*[>+~]|:(even|odd|eq|gt|lt|nth|first|last)(?:\\(" + H + "*((?:-\\d)?\\d*)" + H + "*\\)|)(?=[^-]|$)", "i")
      },
      G = /^(?:input|select|textarea|button)$/i,
      X = /^h\d$/i,
      Q = /^[^{]+\{\s*\[native \w/,
      Z = /^(?:#([\w-]+)|(\w+)|\.([\w-]+))$/,
      K = /[+~]/,
      J = new RegExp("\\\\([\\da-f]{1,6}" + H + "?|(" + H + ")|.)", "ig"),
      ee = function(e, t, n) {
        var i = "0x" + t - 65536;
        return i != i || n ? t : i < 0 ? String.fromCharCode(i + 65536) : String.fromCharCode(i >> 10 | 55296, 1023 & i | 56320)
      },
      te = /([\0-\x1f\x7f]|^-?\d)|^-$|[^\0-\x1f\x7f-\uFFFF\w-]/g,
      ne = function(e, t) {
        return t ? "\0" === e ? "�" : e.slice(0, -1) + "\\" + e.charCodeAt(e.length - 1).toString(16) + " " : "\\" + e
      },
      ie = function() {
        S()
      },
      oe = ye(function(e) {
        return !0 === e.disabled && ("form" in e || "label" in e)
      }, {
        dir: "parentNode",
        next: "legend"
      });
    try {
      D.apply(t = O.call(y.childNodes), y.childNodes), t[y.childNodes.length].nodeType
    } catch (n) {
      D = {
        apply: t.length ? function(e, t) {
          E.apply(e, O.call(t))
        } : function(e, t) {
          for (var n = e.length, i = 0; e[n++] = t[i++];);
          e.length = n - 1
        }
      }
    }

    function re(e, t, n, i) {
      var o, r, s, a, l, c, u, d = t && t.ownerDocument,
        f = t ? t.nodeType : 9;
      if (n = n || [], "string" != typeof e || !e || 1 !== f && 9 !== f && 11 !== f) return n;
      if (!i && ((t ? t.ownerDocument || t : y) !== k && S(t), t = t || k, C)) {
        if (11 !== f && (l = Z.exec(e)))
          if (o = l[1]) {
            if (9 === f) {
              if (!(s = t.getElementById(o))) return n;
              if (s.id === o) return n.push(s), n
            } else if (d && (s = d.getElementById(o)) && v(t, s) && s.id === o) return n.push(s), n
          } else {
            if (l[2]) return D.apply(n, t.getElementsByTagName(e)), n;
            if ((o = l[3]) && h.getElementsByClassName && t.getElementsByClassName) return D.apply(n, t.getElementsByClassName(o)), n
          }
        if (h.qsa && !$[e + " "] && (!m || !m.test(e))) {
          if (1 !== f) d = t, u = e;
          else if ("object" !== t.nodeName.toLowerCase()) {
            for ((a = t.getAttribute("id")) ? a = a.replace(te, ne) : t.setAttribute("id", a = _), r = (c = p(e)).length; r--;) c[r] = "#" + a + " " + ve(c[r]);
            u = c.join(","), d = K.test(e) && ge(t.parentNode) || t
          }
          if (u) try {
            return D.apply(n, d.querySelectorAll(u)), n
          } catch (e) {} finally {
            a === _ && t.removeAttribute("id")
          }
        }
      }
      return g(e.replace(B, "$1"), t, n, i)
    }

    function se() {
      var i = [];
      return function e(t, n) {
        return i.push(t + " ") > x.cacheLength && delete e[i.shift()], e[t + " "] = n
      }
    }

    function ae(e) {
      return e[_] = !0, e
    }

    function le(e) {
      var t = k.createElement("fieldset");
      try {
        return !!e(t)
      } catch (e) {
        return !1
      } finally {
        t.parentNode && t.parentNode.removeChild(t), t = null
      }
    }

    function ce(e, t) {
      for (var n = e.split("|"), i = n.length; i--;) x.attrHandle[n[i]] = t
    }

    function ue(e, t) {
      var n = t && e,
        i = n && 1 === e.nodeType && 1 === t.nodeType && e.sourceIndex - t.sourceIndex;
      if (i) return i;
      if (n)
        for (; n = n.nextSibling;)
          if (n === t) return -1;
      return e ? 1 : -1
    }

    function de(t) {
      return function(e) {
        return "input" === e.nodeName.toLowerCase() && e.type === t
      }
    }

    function fe(n) {
      return function(e) {
        var t = e.nodeName.toLowerCase();
        return ("input" === t || "button" === t) && e.type === n
      }
    }

    function he(t) {
      return function(e) {
        return "form" in e ? e.parentNode && !1 === e.disabled ? "label" in e ? "label" in e.parentNode ? e.parentNode.disabled === t : e.disabled === t : e.isDisabled === t || e.isDisabled !== !t && oe(e) === t : e.disabled === t : "label" in e && e.disabled === t
      }
    }

    function pe(s) {
      return ae(function(r) {
        return r = +r, ae(function(e, t) {
          for (var n, i = s([], e.length, r), o = i.length; o--;) e[n = i[o]] && (e[n] = !(t[n] = e[n]))
        })
      })
    }

    function ge(e) {
      return e && void 0 !== e.getElementsByTagName && e
    }
    for (e in h = re.support = {}, o = re.isXML = function(e) {
        var t = e && (e.ownerDocument || e).documentElement;
        return !!t && "HTML" !== t.nodeName
      }, S = re.setDocument = function(e) {
        var t, n, i = e ? e.ownerDocument || e : y;
        return i !== k && 9 === i.nodeType && i.documentElement && (s = (k = i).documentElement, C = !o(k), y !== k && (n = k.defaultView) && n.top !== n && (n.addEventListener ? n.addEventListener("unload", ie, !1) : n.attachEvent && n.attachEvent("onunload", ie)), h.attributes = le(function(e) {
          return e.className = "i", !e.getAttribute("className")
        }), h.getElementsByTagName = le(function(e) {
          return e.appendChild(k.createComment("")), !e.getElementsByTagName("*").length
        }), h.getElementsByClassName = Q.test(k.getElementsByClassName), h.getById = le(function(e) {
          return s.appendChild(e).id = _, !k.getElementsByName || !k.getElementsByName(_).length
        }), h.getById ? (x.filter.ID = function(e) {
          var t = e.replace(J, ee);
          return function(e) {
            return e.getAttribute("id") === t
          }
        }, x.find.ID = function(e, t) {
          if (void 0 !== t.getElementById && C) {
            var n = t.getElementById(e);
            return n ? [n] : []
          }
        }) : (x.filter.ID = function(e) {
          var n = e.replace(J, ee);
          return function(e) {
            var t = void 0 !== e.getAttributeNode && e.getAttributeNode("id");
            return t && t.value === n
          }
        }, x.find.ID = function(e, t) {
          if (void 0 !== t.getElementById && C) {
            var n, i, o, r = t.getElementById(e);
            if (r) {
              if ((n = r.getAttributeNode("id")) && n.value === e) return [r];
              for (o = t.getElementsByName(e), i = 0; r = o[i++];)
                if ((n = r.getAttributeNode("id")) && n.value === e) return [r]
            }
            return []
          }
        }), x.find.TAG = h.getElementsByTagName ? function(e, t) {
          return void 0 !== t.getElementsByTagName ? t.getElementsByTagName(e) : h.qsa ? t.querySelectorAll(e) : void 0
        } : function(e, t) {
          var n, i = [],
            o = 0,
            r = t.getElementsByTagName(e);
          if ("*" === e) {
            for (; n = r[o++];) 1 === n.nodeType && i.push(n);
            return i
          }
          return r
        }, x.find.CLASS = h.getElementsByClassName && function(e, t) {
          if (void 0 !== t.getElementsByClassName && C) return t.getElementsByClassName(e)
        }, a = [], m = [], (h.qsa = Q.test(k.querySelectorAll)) && (le(function(e) {
          s.appendChild(e).innerHTML = "<a id='" + _ + "'></a><select id='" + _ + "-\r\\' msallowcapture=''><option selected=''></option></select>", e.querySelectorAll("[msallowcapture^='']").length && m.push("[*^$]=" + H + "*(?:''|\"\")"), e.querySelectorAll("[selected]").length || m.push("\\[" + H + "*(?:value|" + I + ")"), e.querySelectorAll("[id~=" + _ + "-]").length || m.push("~="), e.querySelectorAll(":checked").length || m.push(":checked"), e.querySelectorAll("a#" + _ + "+*").length || m.push(".#.+[+~]")
        }), le(function(e) {
          e.innerHTML = "<a href='' disabled='disabled'></a><select disabled='disabled'><option/></select>";
          var t = k.createElement("input");
          t.setAttribute("type", "hidden"), e.appendChild(t).setAttribute("name", "D"), e.querySelectorAll("[name=d]").length && m.push("name" + H + "*[*^$|!~]?="), 2 !== e.querySelectorAll(":enabled").length && m.push(":enabled", ":disabled"), s.appendChild(e).disabled = !0, 2 !== e.querySelectorAll(":disabled").length && m.push(":enabled", ":disabled"), e.querySelectorAll("*,:x"), m.push(",.*:")
        })), (h.matchesSelector = Q.test(u = s.matches || s.webkitMatchesSelector || s.mozMatchesSelector || s.oMatchesSelector || s.msMatchesSelector)) && le(function(e) {
          h.disconnectedMatch = u.call(e, "*"), u.call(e, "[s!='']:x"), a.push("!=", N)
        }), m = m.length && new RegExp(m.join("|")), a = a.length && new RegExp(a.join("|")), t = Q.test(s.compareDocumentPosition), v = t || Q.test(s.contains) ? function(e, t) {
          var n = 9 === e.nodeType ? e.documentElement : e,
            i = t && t.parentNode;
          return e === i || !(!i || 1 !== i.nodeType || !(n.contains ? n.contains(i) : e.compareDocumentPosition && 16 & e.compareDocumentPosition(i)))
        } : function(e, t) {
          if (t)
            for (; t = t.parentNode;)
              if (t === e) return !0;
          return !1
        }, A = t ? function(e, t) {
          if (e === t) return c = !0, 0;
          var n = !e.compareDocumentPosition - !t.compareDocumentPosition;
          return n || (1 & (n = (e.ownerDocument || e) === (t.ownerDocument || t) ? e.compareDocumentPosition(t) : 1) || !h.sortDetached && t.compareDocumentPosition(e) === n ? e === k || e.ownerDocument === y && v(y, e) ? -1 : t === k || t.ownerDocument === y && v(y, t) ? 1 : l ? M(l, e) - M(l, t) : 0 : 4 & n ? -1 : 1)
        } : function(e, t) {
          if (e === t) return c = !0, 0;
          var n, i = 0,
            o = e.parentNode,
            r = t.parentNode,
            s = [e],
            a = [t];
          if (!o || !r) return e === k ? -1 : t === k ? 1 : o ? -1 : r ? 1 : l ? M(l, e) - M(l, t) : 0;
          if (o === r) return ue(e, t);
          for (n = e; n = n.parentNode;) s.unshift(n);
          for (n = t; n = n.parentNode;) a.unshift(n);
          for (; s[i] === a[i];) i++;
          return i ? ue(s[i], a[i]) : s[i] === y ? -1 : a[i] === y ? 1 : 0
        }), k
      }, re.matches = function(e, t) {
        return re(e, null, null, t)
      }, re.matchesSelector = function(e, t) {
        if ((e.ownerDocument || e) !== k && S(e), t = t.replace(W, "='$1']"), h.matchesSelector && C && !$[t + " "] && (!a || !a.test(t)) && (!m || !m.test(t))) try {
          var n = u.call(e, t);
          if (n || h.disconnectedMatch || e.document && 11 !== e.document.nodeType) return n
        } catch (e) {}
        return 0 < re(t, k, null, [e]).length
      }, re.contains = function(e, t) {
        return (e.ownerDocument || e) !== k && S(e), v(e, t)
      }, re.attr = function(e, t) {
        (e.ownerDocument || e) !== k && S(e);
        var n = x.attrHandle[t.toLowerCase()],
          i = n && j.call(x.attrHandle, t.toLowerCase()) ? n(e, t, !C) : void 0;
        return void 0 !== i ? i : h.attributes || !C ? e.getAttribute(t) : (i = e.getAttributeNode(t)) && i.specified ? i.value : null
      }, re.escape = function(e) {
        return (e + "").replace(te, ne)
      }, re.error = function(e) {
        throw new Error("Syntax error, unrecognized expression: " + e)
      }, re.uniqueSort = function(e) {
        var t, n = [],
          i = 0,
          o = 0;
        if (c = !h.detectDuplicates, l = !h.sortStable && e.slice(0), e.sort(A), c) {
          for (; t = e[o++];) t === e[o] && (i = n.push(o));
          for (; i--;) e.splice(n[i], 1)
        }
        return l = null, e
      }, r = re.getText = function(e) {
        var t, n = "",
          i = 0,
          o = e.nodeType;
        if (o) {
          if (1 === o || 9 === o || 11 === o) {
            if ("string" == typeof e.textContent) return e.textContent;
            for (e = e.firstChild; e; e = e.nextSibling) n += r(e)
          } else if (3 === o || 4 === o) return e.nodeValue
        } else
          for (; t = e[i++];) n += r(t);
        return n
      }, (x = re.selectors = {
        cacheLength: 50,
        createPseudo: ae,
        match: U,
        attrHandle: {},
        find: {},
        relative: {
          ">": {
            dir: "parentNode",
            first: !0
          },
          " ": {
            dir: "parentNode"
          },
          "+": {
            dir: "previousSibling",
            first: !0
          },
          "~": {
            dir: "previousSibling"
          }
        },
        preFilter: {
          ATTR: function(e) {
            return e[1] = e[1].replace(J, ee), e[3] = (e[3] || e[4] || e[5] || "").replace(J, ee), "~=" === e[2] && (e[3] = " " + e[3] + " "), e.slice(0, 4)
          },
          CHILD: function(e) {
            return e[1] = e[1].toLowerCase(), "nth" === e[1].slice(0, 3) ? (e[3] || re.error(e[0]), e[4] = +(e[4] ? e[5] + (e[6] || 1) : 2 * ("even" === e[3] || "odd" === e[3])), e[5] = +(e[7] + e[8] || "odd" === e[3])) : e[3] && re.error(e[0]), e
          },
          PSEUDO: function(e) {
            var t, n = !e[6] && e[2];
            return U.CHILD.test(e[0]) ? null : (e[3] ? e[2] = e[4] || e[5] || "" : n && Y.test(n) && (t = p(n, !0)) && (t = n.indexOf(")", n.length - t) - n.length) && (e[0] = e[0].slice(0, t), e[2] = n.slice(0, t)), e.slice(0, 3))
          }
        },
        filter: {
          TAG: function(e) {
            var t = e.replace(J, ee).toLowerCase();
            return "*" === e ? function() {
              return !0
            } : function(e) {
              return e.nodeName && e.nodeName.toLowerCase() === t
            }
          },
          CLASS: function(e) {
            var t = f[e + " "];
            return t || (t = new RegExp("(^|" + H + ")" + e + "(" + H + "|$)")) && f(e, function(e) {
              return t.test("string" == typeof e.className && e.className || void 0 !== e.getAttribute && e.getAttribute("class") || "")
            })
          },
          ATTR: function(n, i, o) {
            return function(e) {
              var t = re.attr(e, n);
              return null == t ? "!=" === i : !i || (t += "", "=" === i ? t === o : "!=" === i ? t !== o : "^=" === i ? o && 0 === t.indexOf(o) : "*=" === i ? o && -1 < t.indexOf(o) : "$=" === i ? o && t.slice(-o.length) === o : "~=" === i ? -1 < (" " + t.replace(F, " ") + " ").indexOf(o) : "|=" === i && (t === o || t.slice(0, o.length + 1) === o + "-"))
            }
          },
          CHILD: function(p, e, t, g, m) {
            var v = "nth" !== p.slice(0, 3),
              y = "last" !== p.slice(-4),
              b = "of-type" === e;
            return 1 === g && 0 === m ? function(e) {
              return !!e.parentNode
            } : function(e, t, n) {
              var i, o, r, s, a, l, c = v !== y ? "nextSibling" : "previousSibling",
                u = e.parentNode,
                d = b && e.nodeName.toLowerCase(),
                f = !n && !b,
                h = !1;
              if (u) {
                if (v) {
                  for (; c;) {
                    for (s = e; s = s[c];)
                      if (b ? s.nodeName.toLowerCase() === d : 1 === s.nodeType) return !1;
                    l = c = "only" === p && !l && "nextSibling"
                  }
                  return !0
                }
                if (l = [y ? u.firstChild : u.lastChild], y && f) {
                  for (h = (a = (i = (o = (r = (s = u)[_] || (s[_] = {}))[s.uniqueID] || (r[s.uniqueID] = {}))[p] || [])[0] === T && i[1]) && i[2], s = a && u.childNodes[a]; s = ++a && s && s[c] || (h = a = 0) || l.pop();)
                    if (1 === s.nodeType && ++h && s === e) {
                      o[p] = [T, a, h];
                      break
                    }
                } else if (f && (h = a = (i = (o = (r = (s = e)[_] || (s[_] = {}))[s.uniqueID] || (r[s.uniqueID] = {}))[p] || [])[0] === T && i[1]), !1 === h)
                  for (;
                    (s = ++a && s && s[c] || (h = a = 0) || l.pop()) && ((b ? s.nodeName.toLowerCase() !== d : 1 !== s.nodeType) || !++h || (f && ((o = (r = s[_] || (s[_] = {}))[s.uniqueID] || (r[s.uniqueID] = {}))[p] = [T, h]), s !== e)););
                return (h -= m) === g || h % g == 0 && 0 <= h / g
              }
            }
          },
          PSEUDO: function(e, r) {
            var t, s = x.pseudos[e] || x.setFilters[e.toLowerCase()] || re.error("unsupported pseudo: " + e);
            return s[_] ? s(r) : 1 < s.length ? (t = [e, e, "", r], x.setFilters.hasOwnProperty(e.toLowerCase()) ? ae(function(e, t) {
              for (var n, i = s(e, r), o = i.length; o--;) e[n = M(e, i[o])] = !(t[n] = i[o])
            }) : function(e) {
              return s(e, 0, t)
            }) : s
          }
        },
        pseudos: {
          not: ae(function(e) {
            var i = [],
              o = [],
              a = d(e.replace(B, "$1"));
            return a[_] ? ae(function(e, t, n, i) {
              for (var o, r = a(e, null, i, []), s = e.length; s--;)(o = r[s]) && (e[s] = !(t[s] = o))
            }) : function(e, t, n) {
              return i[0] = e, a(i, null, n, o), i[0] = null, !o.pop()
            }
          }),
          has: ae(function(t) {
            return function(e) {
              return 0 < re(t, e).length
            }
          }),
          contains: ae(function(t) {
            return t = t.replace(J, ee),
              function(e) {
                return -1 < (e.textContent || e.innerText || r(e)).indexOf(t)
              }
          }),
          lang: ae(function(n) {
            return V.test(n || "") || re.error("unsupported lang: " + n), n = n.replace(J, ee).toLowerCase(),
              function(e) {
                var t;
                do {
                  if (t = C ? e.lang : e.getAttribute("xml:lang") || e.getAttribute("lang")) return (t = t.toLowerCase()) === n || 0 === t.indexOf(n + "-")
                } while ((e = e.parentNode) && 1 === e.nodeType);
                return !1
              }
          }),
          target: function(e) {
            var t = n.location && n.location.hash;
            return t && t.slice(1) === e.id
          },
          root: function(e) {
            return e === s
          },
          focus: function(e) {
            return e === k.activeElement && (!k.hasFocus || k.hasFocus()) && !!(e.type || e.href || ~e.tabIndex)
          },
          enabled: he(!1),
          disabled: he(!0),
          checked: function(e) {
            var t = e.nodeName.toLowerCase();
            return "input" === t && !!e.checked || "option" === t && !!e.selected
          },
          selected: function(e) {
            return e.parentNode && e.parentNode.selectedIndex, !0 === e.selected
          },
          empty: function(e) {
            for (e = e.firstChild; e; e = e.nextSibling)
              if (e.nodeType < 6) return !1;
            return !0
          },
          parent: function(e) {
            return !x.pseudos.empty(e)
          },
          header: function(e) {
            return X.test(e.nodeName)
          },
          input: function(e) {
            return G.test(e.nodeName)
          },
          button: function(e) {
            var t = e.nodeName.toLowerCase();
            return "input" === t && "button" === e.type || "button" === t
          },
          text: function(e) {
            var t;
            return "input" === e.nodeName.toLowerCase() && "text" === e.type && (null == (t = e.getAttribute("type")) || "text" === t.toLowerCase())
          },
          first: pe(function() {
            return [0]
          }),
          last: pe(function(e, t) {
            return [t - 1]
          }),
          eq: pe(function(e, t, n) {
            return [n < 0 ? n + t : n]
          }),
          even: pe(function(e, t) {
            for (var n = 0; n < t; n += 2) e.push(n);
            return e
          }),
          odd: pe(function(e, t) {
            for (var n = 1; n < t; n += 2) e.push(n);
            return e
          }),
          lt: pe(function(e, t, n) {
            for (var i = n < 0 ? n + t : n; 0 <= --i;) e.push(i);
            return e
          }),
          gt: pe(function(e, t, n) {
            for (var i = n < 0 ? n + t : n; ++i < t;) e.push(i);
            return e
          })
        }
      }).pseudos.nth = x.pseudos.eq, {
        radio: !0,
        checkbox: !0,
        file: !0,
        password: !0,
        image: !0
      }) x.pseudos[e] = de(e);
    for (e in {
        submit: !0,
        reset: !0
      }) x.pseudos[e] = fe(e);

    function me() {}

    function ve(e) {
      for (var t = 0, n = e.length, i = ""; t < n; t++) i += e[t].value;
      return i
    }

    function ye(a, e, t) {
      var l = e.dir,
        c = e.next,
        u = c || l,
        d = t && "parentNode" === u,
        f = i++;
      return e.first ? function(e, t, n) {
        for (; e = e[l];)
          if (1 === e.nodeType || d) return a(e, t, n);
        return !1
      } : function(e, t, n) {
        var i, o, r, s = [T, f];
        if (n) {
          for (; e = e[l];)
            if ((1 === e.nodeType || d) && a(e, t, n)) return !0
        } else
          for (; e = e[l];)
            if (1 === e.nodeType || d)
              if (o = (r = e[_] || (e[_] = {}))[e.uniqueID] || (r[e.uniqueID] = {}), c && c === e.nodeName.toLowerCase()) e = e[l] || e;
              else {
                if ((i = o[u]) && i[0] === T && i[1] === f) return s[2] = i[2];
                if ((o[u] = s)[2] = a(e, t, n)) return !0
              } return !1
      }
    }

    function be(o) {
      return 1 < o.length ? function(e, t, n) {
        for (var i = o.length; i--;)
          if (!o[i](e, t, n)) return !1;
        return !0
      } : o[0]
    }

    function xe(e, t, n, i, o) {
      for (var r, s = [], a = 0, l = e.length, c = null != t; a < l; a++)(r = e[a]) && (n && !n(r, i, o) || (s.push(r), c && t.push(a)));
      return s
    }

    function we(h, p, g, m, v, e) {
      return m && !m[_] && (m = we(m)), v && !v[_] && (v = we(v, e)), ae(function(e, t, n, i) {
        var o, r, s, a = [],
          l = [],
          c = t.length,
          u = e || function(e, t, n) {
            for (var i = 0, o = t.length; i < o; i++) re(e, t[i], n);
            return n
          }(p || "*", n.nodeType ? [n] : n, []),
          d = !h || !e && p ? u : xe(u, a, h, n, i),
          f = g ? v || (e ? h : c || m) ? [] : t : d;
        if (g && g(d, f, n, i), m)
          for (o = xe(f, l), m(o, [], n, i), r = o.length; r--;)(s = o[r]) && (f[l[r]] = !(d[l[r]] = s));
        if (e) {
          if (v || h) {
            if (v) {
              for (o = [], r = f.length; r--;)(s = f[r]) && o.push(d[r] = s);
              v(null, f = [], o, i)
            }
            for (r = f.length; r--;)(s = f[r]) && -1 < (o = v ? M(e, s) : a[r]) && (e[o] = !(t[o] = s))
          }
        } else f = xe(f === t ? f.splice(c, f.length) : f), v ? v(null, t, f, i) : D.apply(t, f)
      })
    }

    function Se(e) {
      for (var o, t, n, i = e.length, r = x.relative[e[0].type], s = r || x.relative[" "], a = r ? 1 : 0, l = ye(function(e) {
          return e === o
        }, s, !0), c = ye(function(e) {
          return -1 < M(o, e)
        }, s, !0), u = [function(e, t, n) {
          var i = !r && (n || t !== w) || ((o = t).nodeType ? l(e, t, n) : c(e, t, n));
          return o = null, i
        }]; a < i; a++)
        if (t = x.relative[e[a].type]) u = [ye(be(u), t)];
        else {
          if ((t = x.filter[e[a].type].apply(null, e[a].matches))[_]) {
            for (n = ++a; n < i && !x.relative[e[n].type]; n++);
            return we(1 < a && be(u), 1 < a && ve(e.slice(0, a - 1).concat({
              value: " " === e[a - 2].type ? "*" : ""
            })).replace(B, "$1"), t, a < n && Se(e.slice(a, n)), n < i && Se(e = e.slice(n)), n < i && ve(e))
          }
          u.push(t)
        }
      return be(u)
    }
    return me.prototype = x.filters = x.pseudos, x.setFilters = new me, p = re.tokenize = function(e, t) {
      var n, i, o, r, s, a, l, c = b[e + " "];
      if (c) return t ? 0 : c.slice(0);
      for (s = e, a = [], l = x.preFilter; s;) {
        for (r in n && !(i = z.exec(s)) || (i && (s = s.slice(i[0].length) || s), a.push(o = [])), n = !1, (i = q.exec(s)) && (n = i.shift(), o.push({
            value: n,
            type: i[0].replace(B, " ")
          }), s = s.slice(n.length)), x.filter) !(i = U[r].exec(s)) || l[r] && !(i = l[r](i)) || (n = i.shift(), o.push({
          value: n,
          type: r,
          matches: i
        }), s = s.slice(n.length));
        if (!n) break
      }
      return t ? s.length : s ? re.error(e) : b(e, a).slice(0)
    }, d = re.compile = function(e, t) {
      var n, m, v, y, b, i, o = [],
        r = [],
        s = $[e + " "];
      if (!s) {
        for (t || (t = p(e)), n = t.length; n--;)(s = Se(t[n]))[_] ? o.push(s) : r.push(s);
        (s = $(e, (m = r, v = o, y = 0 < v.length, b = 0 < m.length, i = function(e, t, n, i, o) {
          var r, s, a, l = 0,
            c = "0",
            u = e && [],
            d = [],
            f = w,
            h = e || b && x.find.TAG("*", o),
            p = T += null == f ? 1 : Math.random() || .1,
            g = h.length;
          for (o && (w = t === k || t || o); c !== g && null != (r = h[c]); c++) {
            if (b && r) {
              for (s = 0, t || r.ownerDocument === k || (S(r), n = !C); a = m[s++];)
                if (a(r, t || k, n)) {
                  i.push(r);
                  break
                }
              o && (T = p)
            }
            y && ((r = !a && r) && l--, e && u.push(r))
          }
          if (l += c, y && c !== l) {
            for (s = 0; a = v[s++];) a(u, d, t, n);
            if (e) {
              if (0 < l)
                for (; c--;) u[c] || d[c] || (d[c] = P.call(i));
              d = xe(d)
            }
            D.apply(i, d), o && !e && 0 < d.length && 1 < l + v.length && re.uniqueSort(i)
          }
          return o && (T = p, w = f), u
        }, y ? ae(i) : i))).selector = e
      }
      return s
    }, g = re.select = function(e, t, n, i) {
      var o, r, s, a, l, c = "function" == typeof e && e,
        u = !i && p(e = c.selector || e);
      if (n = n || [], 1 === u.length) {
        if (2 < (r = u[0] = u[0].slice(0)).length && "ID" === (s = r[0]).type && 9 === t.nodeType && C && x.relative[r[1].type]) {
          if (!(t = (x.find.ID(s.matches[0].replace(J, ee), t) || [])[0])) return n;
          c && (t = t.parentNode), e = e.slice(r.shift().value.length)
        }
        for (o = U.needsContext.test(e) ? 0 : r.length; o-- && (s = r[o], !x.relative[a = s.type]);)
          if ((l = x.find[a]) && (i = l(s.matches[0].replace(J, ee), K.test(r[0].type) && ge(t.parentNode) || t))) {
            if (r.splice(o, 1), !(e = i.length && ve(r))) return D.apply(n, i), n;
            break
          }
      }
      return (c || d(e, u))(i, t, !C, n, !t || K.test(e) && ge(t.parentNode) || t), n
    }, h.sortStable = _.split("").sort(A).join("") === _, h.detectDuplicates = !!c, S(), h.sortDetached = le(function(e) {
      return 1 & e.compareDocumentPosition(k.createElement("fieldset"))
    }), le(function(e) {
      return e.innerHTML = "<a href='#'></a>", "#" === e.firstChild.getAttribute("href")
    }) || ce("type|href|height|width", function(e, t, n) {
      if (!n) return e.getAttribute(t, "type" === t.toLowerCase() ? 1 : 2)
    }), h.attributes && le(function(e) {
      return e.innerHTML = "<input/>", e.firstChild.setAttribute("value", ""), "" === e.firstChild.getAttribute("value")
    }) || ce("value", function(e, t, n) {
      if (!n && "input" === e.nodeName.toLowerCase()) return e.defaultValue
    }), le(function(e) {
      return null == e.getAttribute("disabled")
    }) || ce(I, function(e, t, n) {
      var i;
      if (!n) return !0 === e[t] ? t.toLowerCase() : (i = e.getAttributeNode(t)) && i.specified ? i.value : null
    }), re
  }(k);
  _.find = h, _.expr = h.selectors, _.expr[":"] = _.expr.pseudos, _.uniqueSort = _.unique = h.uniqueSort, _.text = h.getText, _.isXMLDoc = h.isXML, _.contains = h.contains, _.escapeSelector = h.escape;
  var p = function(e, t, n) {
      for (var i = [], o = void 0 !== n;
        (e = e[t]) && 9 !== e.nodeType;)
        if (1 === e.nodeType) {
          if (o && _(e).is(n)) break;
          i.push(e)
        }
      return i
    },
    S = function(e, t) {
      for (var n = []; e; e = e.nextSibling) 1 === e.nodeType && e !== t && n.push(e);
      return n
    },
    T = _.expr.match.needsContext;

  function $(e, t) {
    return e.nodeName && e.nodeName.toLowerCase() === t.toLowerCase()
  }
  var A = /^<([a-z][^\/\0>:\x20\t\r\n\f]*)[\x20\t\r\n\f]*\/?>(?:<\/\1>|)$/i;

  function j(e, n, i) {
    return y(n) ? _.grep(e, function(e, t) {
      return !!n.call(e, t, e) !== i
    }) : n.nodeType ? _.grep(e, function(e) {
      return e === n !== i
    }) : "string" != typeof n ? _.grep(e, function(e) {
      return -1 < o.call(n, e) !== i
    }) : _.filter(n, e, i)
  }
  _.filter = function(e, t, n) {
    var i = t[0];
    return n && (e = ":not(" + e + ")"), 1 === t.length && 1 === i.nodeType ? _.find.matchesSelector(i, e) ? [i] : [] : _.find.matches(e, _.grep(t, function(e) {
      return 1 === e.nodeType
    }))
  }, _.fn.extend({
    find: function(e) {
      var t, n, i = this.length,
        o = this;
      if ("string" != typeof e) return this.pushStack(_(e).filter(function() {
        for (t = 0; t < i; t++)
          if (_.contains(o[t], this)) return !0
      }));
      for (n = this.pushStack([]), t = 0; t < i; t++) _.find(e, o[t], n);
      return 1 < i ? _.uniqueSort(n) : n
    },
    filter: function(e) {
      return this.pushStack(j(this, e || [], !1))
    },
    not: function(e) {
      return this.pushStack(j(this, e || [], !0))
    },
    is: function(e) {
      return !!j(this, "string" == typeof e && T.test(e) ? _(e) : e || [], !1).length
    }
  });
  var P, E = /^(?:\s*(<[\w\W]+>)[^>]*|#([\w-]+))$/;
  (_.fn.init = function(e, t, n) {
    var i, o;
    if (!e) return this;
    if (n = n || P, "string" == typeof e) {
      if (!(i = "<" === e[0] && ">" === e[e.length - 1] && 3 <= e.length ? [null, e, null] : E.exec(e)) || !i[1] && t) return !t || t.jquery ? (t || n).find(e) : this.constructor(t).find(e);
      if (i[1]) {
        if (t = t instanceof _ ? t[0] : t, _.merge(this, _.parseHTML(i[1], t && t.nodeType ? t.ownerDocument || t : C, !0)), A.test(i[1]) && _.isPlainObject(t))
          for (i in t) y(this[i]) ? this[i](t[i]) : this.attr(i, t[i]);
        return this
      }
      return (o = C.getElementById(i[2])) && (this[0] = o, this.length = 1), this
    }
    return e.nodeType ? (this[0] = e, this.length = 1, this) : y(e) ? void 0 !== n.ready ? n.ready(e) : e(_) : _.makeArray(e, this)
  }).prototype = _.fn, P = _(C);
  var D = /^(?:parents|prev(?:Until|All))/,
    O = {
      children: !0,
      contents: !0,
      next: !0,
      prev: !0
    };

  function M(e, t) {
    for (;
      (e = e[t]) && 1 !== e.nodeType;);
    return e
  }
  _.fn.extend({
    has: function(e) {
      var t = _(e, this),
        n = t.length;
      return this.filter(function() {
        for (var e = 0; e < n; e++)
          if (_.contains(this, t[e])) return !0
      })
    },
    closest: function(e, t) {
      var n, i = 0,
        o = this.length,
        r = [],
        s = "string" != typeof e && _(e);
      if (!T.test(e))
        for (; i < o; i++)
          for (n = this[i]; n && n !== t; n = n.parentNode)
            if (n.nodeType < 11 && (s ? -1 < s.index(n) : 1 === n.nodeType && _.find.matchesSelector(n, e))) {
              r.push(n);
              break
            }
      return this.pushStack(1 < r.length ? _.uniqueSort(r) : r)
    },
    index: function(e) {
      return e ? "string" == typeof e ? o.call(_(e), this[0]) : o.call(this, e.jquery ? e[0] : e) : this[0] && this[0].parentNode ? this.first().prevAll().length : -1
    },
    add: function(e, t) {
      return this.pushStack(_.uniqueSort(_.merge(this.get(), _(e, t))))
    },
    addBack: function(e) {
      return this.add(null == e ? this.prevObject : this.prevObject.filter(e))
    }
  }), _.each({
    parent: function(e) {
      var t = e.parentNode;
      return t && 11 !== t.nodeType ? t : null
    },
    parents: function(e) {
      return p(e, "parentNode")
    },
    parentsUntil: function(e, t, n) {
      return p(e, "parentNode", n)
    },
    next: function(e) {
      return M(e, "nextSibling")
    },
    prev: function(e) {
      return M(e, "previousSibling")
    },
    nextAll: function(e) {
      return p(e, "nextSibling")
    },
    prevAll: function(e) {
      return p(e, "previousSibling")
    },
    nextUntil: function(e, t, n) {
      return p(e, "nextSibling", n)
    },
    prevUntil: function(e, t, n) {
      return p(e, "previousSibling", n)
    },
    siblings: function(e) {
      return S((e.parentNode || {}).firstChild, e)
    },
    children: function(e) {
      return S(e.firstChild)
    },
    contents: function(e) {
      return $(e, "iframe") ? e.contentDocument : ($(e, "template") && (e = e.content || e), _.merge([], e.childNodes))
    }
  }, function(i, o) {
    _.fn[i] = function(e, t) {
      var n = _.map(this, o, e);
      return "Until" !== i.slice(-5) && (t = e), t && "string" == typeof t && (n = _.filter(t, n)), 1 < this.length && (O[i] || _.uniqueSort(n), D.test(i) && n.reverse()), this.pushStack(n)
    }
  });
  var I = /[^\x20\t\r\n\f]+/g;

  function H(e) {
    return e
  }

  function L(e) {
    throw e
  }

  function R(e, t, n, i) {
    var o;
    try {
      e && y(o = e.promise) ? o.call(e).done(t).fail(n) : e && y(o = e.then) ? o.call(e, t, n) : t.apply(void 0, [e].slice(i))
    } catch (e) {
      n.apply(void 0, [e])
    }
  }
  _.Callbacks = function(i) {
    var e, n;
    i = "string" == typeof i ? (e = i, n = {}, _.each(e.match(I) || [], function(e, t) {
      n[t] = !0
    }), n) : _.extend({}, i);
    var o, t, r, s, a = [],
      l = [],
      c = -1,
      u = function() {
        for (s = s || i.once, r = o = !0; l.length; c = -1)
          for (t = l.shift(); ++c < a.length;) !1 === a[c].apply(t[0], t[1]) && i.stopOnFalse && (c = a.length, t = !1);
        i.memory || (t = !1), o = !1, s && (a = t ? [] : "")
      },
      d = {
        add: function() {
          return a && (t && !o && (c = a.length - 1, l.push(t)), function n(e) {
            _.each(e, function(e, t) {
              y(t) ? i.unique && d.has(t) || a.push(t) : t && t.length && "string" !== w(t) && n(t)
            })
          }(arguments), t && !o && u()), this
        },
        remove: function() {
          return _.each(arguments, function(e, t) {
            for (var n; - 1 < (n = _.inArray(t, a, n));) a.splice(n, 1), n <= c && c--
          }), this
        },
        has: function(e) {
          return e ? -1 < _.inArray(e, a) : 0 < a.length
        },
        empty: function() {
          return a && (a = []), this
        },
        disable: function() {
          return s = l = [], a = t = "", this
        },
        disabled: function() {
          return !a
        },
        lock: function() {
          return s = l = [], t || o || (a = t = ""), this
        },
        locked: function() {
          return !!s
        },
        fireWith: function(e, t) {
          return s || (t = [e, (t = t || []).slice ? t.slice() : t], l.push(t), o || u()), this
        },
        fire: function() {
          return d.fireWith(this, arguments), this
        },
        fired: function() {
          return !!r
        }
      };
    return d
  }, _.extend({
    Deferred: function(e) {
      var r = [
          ["notify", "progress", _.Callbacks("memory"), _.Callbacks("memory"), 2],
          ["resolve", "done", _.Callbacks("once memory"), _.Callbacks("once memory"), 0, "resolved"],
          ["reject", "fail", _.Callbacks("once memory"), _.Callbacks("once memory"), 1, "rejected"]
        ],
        o = "pending",
        s = {
          state: function() {
            return o
          },
          always: function() {
            return a.done(arguments).fail(arguments), this
          },
          catch: function(e) {
            return s.then(null, e)
          },
          pipe: function() {
            var o = arguments;
            return _.Deferred(function(i) {
              _.each(r, function(e, t) {
                var n = y(o[t[4]]) && o[t[4]];
                a[t[1]](function() {
                  var e = n && n.apply(this, arguments);
                  e && y(e.promise) ? e.promise().progress(i.notify).done(i.resolve).fail(i.reject) : i[t[0] + "With"](this, n ? [e] : arguments)
                })
              }), o = null
            }).promise()
          },
          then: function(t, n, i) {
            var l = 0;

            function c(o, r, s, a) {
              return function() {
                var n = this,
                  i = arguments,
                  e = function() {
                    var e, t;
                    if (!(o < l)) {
                      if ((e = s.apply(n, i)) === r.promise()) throw new TypeError("Thenable self-resolution");
                      t = e && ("object" == typeof e || "function" == typeof e) && e.then, y(t) ? a ? t.call(e, c(l, r, H, a), c(l, r, L, a)) : (l++, t.call(e, c(l, r, H, a), c(l, r, L, a), c(l, r, H, r.notifyWith))) : (s !== H && (n = void 0, i = [e]), (a || r.resolveWith)(n, i))
                    }
                  },
                  t = a ? e : function() {
                    try {
                      e()
                    } catch (e) {
                      _.Deferred.exceptionHook && _.Deferred.exceptionHook(e, t.stackTrace), l <= o + 1 && (s !== L && (n = void 0, i = [e]), r.rejectWith(n, i))
                    }
                  };
                o ? t() : (_.Deferred.getStackHook && (t.stackTrace = _.Deferred.getStackHook()), k.setTimeout(t))
              }
            }
            return _.Deferred(function(e) {
              r[0][3].add(c(0, e, y(i) ? i : H, e.notifyWith)), r[1][3].add(c(0, e, y(t) ? t : H)), r[2][3].add(c(0, e, y(n) ? n : L))
            }).promise()
          },
          promise: function(e) {
            return null != e ? _.extend(e, s) : s
          }
        },
        a = {};
      return _.each(r, function(e, t) {
        var n = t[2],
          i = t[5];
        s[t[1]] = n.add, i && n.add(function() {
          o = i
        }, r[3 - e][2].disable, r[3 - e][3].disable, r[0][2].lock, r[0][3].lock), n.add(t[3].fire), a[t[0]] = function() {
          return a[t[0] + "With"](this === a ? void 0 : this, arguments), this
        }, a[t[0] + "With"] = n.fireWith
      }), s.promise(a), e && e.call(a, a), a
    },
    when: function(e) {
      var n = arguments.length,
        t = n,
        i = Array(t),
        o = a.call(arguments),
        r = _.Deferred(),
        s = function(t) {
          return function(e) {
            i[t] = this, o[t] = 1 < arguments.length ? a.call(arguments) : e, --n || r.resolveWith(i, o)
          }
        };
      if (n <= 1 && (R(e, r.done(s(t)).resolve, r.reject, !n), "pending" === r.state() || y(o[t] && o[t].then))) return r.then();
      for (; t--;) R(o[t], s(t), r.reject);
      return r.promise()
    }
  });
  var N = /^(Eval|Internal|Range|Reference|Syntax|Type|URI)Error$/;
  _.Deferred.exceptionHook = function(e, t) {
    k.console && k.console.warn && e && N.test(e.name) && k.console.warn("jQuery.Deferred exception: " + e.message, e.stack, t)
  }, _.readyException = function(e) {
    k.setTimeout(function() {
      throw e
    })
  };
  var F = _.Deferred();

  function B() {
    C.removeEventListener("DOMContentLoaded", B), k.removeEventListener("load", B), _.ready()
  }
  _.fn.ready = function(e) {
    return F.then(e).catch(function(e) {
      _.readyException(e)
    }), this
  }, _.extend({
    isReady: !1,
    readyWait: 1,
    ready: function(e) {
      (!0 === e ? --_.readyWait : _.isReady) || ((_.isReady = !0) !== e && 0 < --_.readyWait || F.resolveWith(C, [_]))
    }
  }), _.ready.then = F.then, "complete" === C.readyState || "loading" !== C.readyState && !C.documentElement.doScroll ? k.setTimeout(_.ready) : (C.addEventListener("DOMContentLoaded", B), k.addEventListener("load", B));
  var z = function(e, t, n, i, o, r, s) {
      var a = 0,
        l = e.length,
        c = null == n;
      if ("object" === w(n))
        for (a in o = !0, n) z(e, t, a, n[a], !0, r, s);
      else if (void 0 !== i && (o = !0, y(i) || (s = !0), c && (s ? (t.call(e, i), t = null) : (c = t, t = function(e, t, n) {
          return c.call(_(e), n)
        })), t))
        for (; a < l; a++) t(e[a], n, s ? i : i.call(e[a], a, t(e[a], n)));
      return o ? e : c ? t.call(e) : l ? t(e[0], n) : r
    },
    q = /^-ms-/,
    W = /-([a-z])/g;

  function Y(e, t) {
    return t.toUpperCase()
  }

  function V(e) {
    return e.replace(q, "ms-").replace(W, Y)
  }
  var U = function(e) {
    return 1 === e.nodeType || 9 === e.nodeType || !+e.nodeType
  };

  function G() {
    this.expando = _.expando + G.uid++
  }
  G.uid = 1, G.prototype = {
    cache: function(e) {
      var t = e[this.expando];
      return t || (t = {}, U(e) && (e.nodeType ? e[this.expando] = t : Object.defineProperty(e, this.expando, {
        value: t,
        configurable: !0
      }))), t
    },
    set: function(e, t, n) {
      var i, o = this.cache(e);
      if ("string" == typeof t) o[V(t)] = n;
      else
        for (i in t) o[V(i)] = t[i];
      return o
    },
    get: function(e, t) {
      return void 0 === t ? this.cache(e) : e[this.expando] && e[this.expando][V(t)]
    },
    access: function(e, t, n) {
      return void 0 === t || t && "string" == typeof t && void 0 === n ? this.get(e, t) : (this.set(e, t, n), void 0 !== n ? n : t)
    },
    remove: function(e, t) {
      var n, i = e[this.expando];
      if (void 0 !== i) {
        if (void 0 !== t) {
          n = (t = Array.isArray(t) ? t.map(V) : (t = V(t)) in i ? [t] : t.match(I) || []).length;
          for (; n--;) delete i[t[n]]
        }(void 0 === t || _.isEmptyObject(i)) && (e.nodeType ? e[this.expando] = void 0 : delete e[this.expando])
      }
    },
    hasData: function(e) {
      var t = e[this.expando];
      return void 0 !== t && !_.isEmptyObject(t)
    }
  };
  var X = new G,
    Q = new G,
    Z = /^(?:\{[\w\W]*\}|\[[\w\W]*\])$/,
    K = /[A-Z]/g;

  function J(e, t, n) {
    var i, o;
    if (void 0 === n && 1 === e.nodeType)
      if (i = "data-" + t.replace(K, "-$&").toLowerCase(), "string" == typeof(n = e.getAttribute(i))) {
        try {
          n = "true" === (o = n) || "false" !== o && ("null" === o ? null : o === +o + "" ? +o : Z.test(o) ? JSON.parse(o) : o)
        } catch (e) {}
        Q.set(e, t, n)
      } else n = void 0;
    return n
  }
  _.extend({
    hasData: function(e) {
      return Q.hasData(e) || X.hasData(e)
    },
    data: function(e, t, n) {
      return Q.access(e, t, n)
    },
    removeData: function(e, t) {
      Q.remove(e, t)
    },
    _data: function(e, t, n) {
      return X.access(e, t, n)
    },
    _removeData: function(e, t) {
      X.remove(e, t)
    }
  }), _.fn.extend({
    data: function(n, e) {
      var t, i, o, r = this[0],
        s = r && r.attributes;
      if (void 0 === n) {
        if (this.length && (o = Q.get(r), 1 === r.nodeType && !X.get(r, "hasDataAttrs"))) {
          for (t = s.length; t--;) s[t] && 0 === (i = s[t].name).indexOf("data-") && (i = V(i.slice(5)), J(r, i, o[i]));
          X.set(r, "hasDataAttrs", !0)
        }
        return o
      }
      return "object" == typeof n ? this.each(function() {
        Q.set(this, n)
      }) : z(this, function(e) {
        var t;
        if (r && void 0 === e) {
          if (void 0 !== (t = Q.get(r, n))) return t;
          if (void 0 !== (t = J(r, n))) return t
        } else this.each(function() {
          Q.set(this, n, e)
        })
      }, null, e, 1 < arguments.length, null, !0)
    },
    removeData: function(e) {
      return this.each(function() {
        Q.remove(this, e)
      })
    }
  }), _.extend({
    queue: function(e, t, n) {
      var i;
      if (e) return t = (t || "fx") + "queue", i = X.get(e, t), n && (!i || Array.isArray(n) ? i = X.access(e, t, _.makeArray(n)) : i.push(n)), i || []
    },
    dequeue: function(e, t) {
      t = t || "fx";
      var n = _.queue(e, t),
        i = n.length,
        o = n.shift(),
        r = _._queueHooks(e, t);
      "inprogress" === o && (o = n.shift(), i--), o && ("fx" === t && n.unshift("inprogress"), delete r.stop, o.call(e, function() {
        _.dequeue(e, t)
      }, r)), !i && r && r.empty.fire()
    },
    _queueHooks: function(e, t) {
      var n = t + "queueHooks";
      return X.get(e, n) || X.access(e, n, {
        empty: _.Callbacks("once memory").add(function() {
          X.remove(e, [t + "queue", n])
        })
      })
    }
  }), _.fn.extend({
    queue: function(t, n) {
      var e = 2;
      return "string" != typeof t && (n = t, t = "fx", e--), arguments.length < e ? _.queue(this[0], t) : void 0 === n ? this : this.each(function() {
        var e = _.queue(this, t, n);
        _._queueHooks(this, t), "fx" === t && "inprogress" !== e[0] && _.dequeue(this, t)
      })
    },
    dequeue: function(e) {
      return this.each(function() {
        _.dequeue(this, e)
      })
    },
    clearQueue: function(e) {
      return this.queue(e || "fx", [])
    },
    promise: function(e, t) {
      var n, i = 1,
        o = _.Deferred(),
        r = this,
        s = this.length,
        a = function() {
          --i || o.resolveWith(r, [r])
        };
      for ("string" != typeof e && (t = e, e = void 0), e = e || "fx"; s--;)(n = X.get(r[s], e + "queueHooks")) && n.empty && (i++, n.empty.add(a));
      return a(), o.promise(t)
    }
  });
  var ee = /[+-]?(?:\d*\.|)\d+(?:[eE][+-]?\d+|)/.source,
    te = new RegExp("^(?:([+-])=|)(" + ee + ")([a-z%]*)$", "i"),
    ne = ["Top", "Right", "Bottom", "Left"],
    ie = function(e, t) {
      return "none" === (e = t || e).style.display || "" === e.style.display && _.contains(e.ownerDocument, e) && "none" === _.css(e, "display")
    },
    oe = function(e, t, n, i) {
      var o, r, s = {};
      for (r in t) s[r] = e.style[r], e.style[r] = t[r];
      for (r in o = n.apply(e, i || []), t) e.style[r] = s[r];
      return o
    };

  function re(e, t, n, i) {
    var o, r, s = 20,
      a = i ? function() {
        return i.cur()
      } : function() {
        return _.css(e, t, "")
      },
      l = a(),
      c = n && n[3] || (_.cssNumber[t] ? "" : "px"),
      u = (_.cssNumber[t] || "px" !== c && +l) && te.exec(_.css(e, t));
    if (u && u[3] !== c) {
      for (l /= 2, c = c || u[3], u = +l || 1; s--;) _.style(e, t, u + c), (1 - r) * (1 - (r = a() / l || .5)) <= 0 && (s = 0), u /= r;
      u *= 2, _.style(e, t, u + c), n = n || []
    }
    return n && (u = +u || +l || 0, o = n[1] ? u + (n[1] + 1) * n[2] : +n[2], i && (i.unit = c, i.start = u, i.end = o)), o
  }
  var se = {};

  function ae(e, t) {
    for (var n, i, o = [], r = 0, s = e.length; r < s; r++)(i = e[r]).style && (n = i.style.display, t ? ("none" === n && (o[r] = X.get(i, "display") || null, o[r] || (i.style.display = "")), "" === i.style.display && ie(i) && (o[r] = (d = c = l = void 0, c = (a = i).ownerDocument, u = a.nodeName, (d = se[u]) || (l = c.body.appendChild(c.createElement(u)), d = _.css(l, "display"), l.parentNode.removeChild(l), "none" === d && (d = "block"), se[u] = d)))) : "none" !== n && (o[r] = "none", X.set(i, "display", n)));
    var a, l, c, u, d;
    for (r = 0; r < s; r++) null != o[r] && (e[r].style.display = o[r]);
    return e
  }
  _.fn.extend({
    show: function() {
      return ae(this, !0)
    },
    hide: function() {
      return ae(this)
    },
    toggle: function(e) {
      return "boolean" == typeof e ? e ? this.show() : this.hide() : this.each(function() {
        ie(this) ? _(this).show() : _(this).hide()
      })
    }
  });
  var le = /^(?:checkbox|radio)$/i,
    ce = /<([a-z][^\/\0>\x20\t\r\n\f]+)/i,
    ue = /^$|^module$|\/(?:java|ecma)script/i,
    de = {
      option: [1, "<select multiple='multiple'>", "</select>"],
      thead: [1, "<table>", "</table>"],
      col: [2, "<table><colgroup>", "</colgroup></table>"],
      tr: [2, "<table><tbody>", "</tbody></table>"],
      td: [3, "<table><tbody><tr>", "</tr></tbody></table>"],
      _default: [0, "", ""]
    };

  function fe(e, t) {
    var n;
    return n = void 0 !== e.getElementsByTagName ? e.getElementsByTagName(t || "*") : void 0 !== e.querySelectorAll ? e.querySelectorAll(t || "*") : [], void 0 === t || t && $(e, t) ? _.merge([e], n) : n
  }

  function he(e, t) {
    for (var n = 0, i = e.length; n < i; n++) X.set(e[n], "globalEval", !t || X.get(t[n], "globalEval"))
  }
  de.optgroup = de.option, de.tbody = de.tfoot = de.colgroup = de.caption = de.thead, de.th = de.td;
  var pe, ge, me = /<|&#?\w+;/;

  function ve(e, t, n, i, o) {
    for (var r, s, a, l, c, u, d = t.createDocumentFragment(), f = [], h = 0, p = e.length; h < p; h++)
      if ((r = e[h]) || 0 === r)
        if ("object" === w(r)) _.merge(f, r.nodeType ? [r] : r);
        else if (me.test(r)) {
      for (s = s || d.appendChild(t.createElement("div")), a = (ce.exec(r) || ["", ""])[1].toLowerCase(), l = de[a] || de._default, s.innerHTML = l[1] + _.htmlPrefilter(r) + l[2], u = l[0]; u--;) s = s.lastChild;
      _.merge(f, s.childNodes), (s = d.firstChild).textContent = ""
    } else f.push(t.createTextNode(r));
    for (d.textContent = "", h = 0; r = f[h++];)
      if (i && -1 < _.inArray(r, i)) o && o.push(r);
      else if (c = _.contains(r.ownerDocument, r), s = fe(d.appendChild(r), "script"), c && he(s), n)
      for (u = 0; r = s[u++];) ue.test(r.type || "") && n.push(r);
    return d
  }
  pe = C.createDocumentFragment().appendChild(C.createElement("div")), (ge = C.createElement("input")).setAttribute("type", "radio"), ge.setAttribute("checked", "checked"), ge.setAttribute("name", "t"), pe.appendChild(ge), v.checkClone = pe.cloneNode(!0).cloneNode(!0).lastChild.checked, pe.innerHTML = "<textarea>x</textarea>", v.noCloneChecked = !!pe.cloneNode(!0).lastChild.defaultValue;
  var ye = C.documentElement,
    be = /^key/,
    xe = /^(?:mouse|pointer|contextmenu|drag|drop)|click/,
    we = /^([^.]*)(?:\.(.+)|)/;

  function Se() {
    return !0
  }

  function ke() {
    return !1
  }

  function Ce() {
    try {
      return C.activeElement
    } catch (e) {}
  }

  function _e(e, t, n, i, o, r) {
    var s, a;
    if ("object" == typeof t) {
      for (a in "string" != typeof n && (i = i || n, n = void 0), t) _e(e, a, n, i, t[a], r);
      return e
    }
    if (null == i && null == o ? (o = n, i = n = void 0) : null == o && ("string" == typeof n ? (o = i, i = void 0) : (o = i, i = n, n = void 0)), !1 === o) o = ke;
    else if (!o) return e;
    return 1 === r && (s = o, (o = function(e) {
      return _().off(e), s.apply(this, arguments)
    }).guid = s.guid || (s.guid = _.guid++)), e.each(function() {
      _.event.add(this, t, o, i, n)
    })
  }
  _.event = {
    global: {},
    add: function(t, e, n, i, o) {
      var r, s, a, l, c, u, d, f, h, p, g, m = X.get(t);
      if (m)
        for (n.handler && (n = (r = n).handler, o = r.selector), o && _.find.matchesSelector(ye, o), n.guid || (n.guid = _.guid++), (l = m.events) || (l = m.events = {}), (s = m.handle) || (s = m.handle = function(e) {
            return void 0 !== _ && _.event.triggered !== e.type ? _.event.dispatch.apply(t, arguments) : void 0
          }), c = (e = (e || "").match(I) || [""]).length; c--;) h = g = (a = we.exec(e[c]) || [])[1], p = (a[2] || "").split(".").sort(), h && (d = _.event.special[h] || {}, h = (o ? d.delegateType : d.bindType) || h, d = _.event.special[h] || {}, u = _.extend({
          type: h,
          origType: g,
          data: i,
          handler: n,
          guid: n.guid,
          selector: o,
          needsContext: o && _.expr.match.needsContext.test(o),
          namespace: p.join(".")
        }, r), (f = l[h]) || ((f = l[h] = []).delegateCount = 0, d.setup && !1 !== d.setup.call(t, i, p, s) || t.addEventListener && t.addEventListener(h, s)), d.add && (d.add.call(t, u), u.handler.guid || (u.handler.guid = n.guid)), o ? f.splice(f.delegateCount++, 0, u) : f.push(u), _.event.global[h] = !0)
    },
    remove: function(e, t, n, i, o) {
      var r, s, a, l, c, u, d, f, h, p, g, m = X.hasData(e) && X.get(e);
      if (m && (l = m.events)) {
        for (c = (t = (t || "").match(I) || [""]).length; c--;)
          if (h = g = (a = we.exec(t[c]) || [])[1], p = (a[2] || "").split(".").sort(), h) {
            for (d = _.event.special[h] || {}, f = l[h = (i ? d.delegateType : d.bindType) || h] || [], a = a[2] && new RegExp("(^|\\.)" + p.join("\\.(?:.*\\.|)") + "(\\.|$)"), s = r = f.length; r--;) u = f[r], !o && g !== u.origType || n && n.guid !== u.guid || a && !a.test(u.namespace) || i && i !== u.selector && ("**" !== i || !u.selector) || (f.splice(r, 1), u.selector && f.delegateCount--, d.remove && d.remove.call(e, u));
            s && !f.length && (d.teardown && !1 !== d.teardown.call(e, p, m.handle) || _.removeEvent(e, h, m.handle), delete l[h])
          } else
            for (h in l) _.event.remove(e, h + t[c], n, i, !0);
        _.isEmptyObject(l) && X.remove(e, "handle events")
      }
    },
    dispatch: function(e) {
      var t, n, i, o, r, s, a = _.event.fix(e),
        l = new Array(arguments.length),
        c = (X.get(this, "events") || {})[a.type] || [],
        u = _.event.special[a.type] || {};
      for (l[0] = a, t = 1; t < arguments.length; t++) l[t] = arguments[t];
      if (a.delegateTarget = this, !u.preDispatch || !1 !== u.preDispatch.call(this, a)) {
        for (s = _.event.handlers.call(this, a, c), t = 0;
          (o = s[t++]) && !a.isPropagationStopped();)
          for (a.currentTarget = o.elem, n = 0;
            (r = o.handlers[n++]) && !a.isImmediatePropagationStopped();) a.rnamespace && !a.rnamespace.test(r.namespace) || (a.handleObj = r, a.data = r.data, void 0 !== (i = ((_.event.special[r.origType] || {}).handle || r.handler).apply(o.elem, l)) && !1 === (a.result = i) && (a.preventDefault(), a.stopPropagation()));
        return u.postDispatch && u.postDispatch.call(this, a), a.result
      }
    },
    handlers: function(e, t) {
      var n, i, o, r, s, a = [],
        l = t.delegateCount,
        c = e.target;
      if (l && c.nodeType && !("click" === e.type && 1 <= e.button))
        for (; c !== this; c = c.parentNode || this)
          if (1 === c.nodeType && ("click" !== e.type || !0 !== c.disabled)) {
            for (r = [], s = {}, n = 0; n < l; n++) void 0 === s[o = (i = t[n]).selector + " "] && (s[o] = i.needsContext ? -1 < _(o, this).index(c) : _.find(o, this, null, [c]).length), s[o] && r.push(i);
            r.length && a.push({
              elem: c,
              handlers: r
            })
          }
      return c = this, l < t.length && a.push({
        elem: c,
        handlers: t.slice(l)
      }), a
    },
    addProp: function(t, e) {
      Object.defineProperty(_.Event.prototype, t, {
        enumerable: !0,
        configurable: !0,
        get: y(e) ? function() {
          if (this.originalEvent) return e(this.originalEvent)
        } : function() {
          if (this.originalEvent) return this.originalEvent[t]
        },
        set: function(e) {
          Object.defineProperty(this, t, {
            enumerable: !0,
            configurable: !0,
            writable: !0,
            value: e
          })
        }
      })
    },
    fix: function(e) {
      return e[_.expando] ? e : new _.Event(e)
    },
    special: {
      load: {
        noBubble: !0
      },
      focus: {
        trigger: function() {
          if (this !== Ce() && this.focus) return this.focus(), !1
        },
        delegateType: "focusin"
      },
      blur: {
        trigger: function() {
          if (this === Ce() && this.blur) return this.blur(), !1
        },
        delegateType: "focusout"
      },
      click: {
        trigger: function() {
          if ("checkbox" === this.type && this.click && $(this, "input")) return this.click(), !1
        },
        _default: function(e) {
          return $(e.target, "a")
        }
      },
      beforeunload: {
        postDispatch: function(e) {
          void 0 !== e.result && e.originalEvent && (e.originalEvent.returnValue = e.result)
        }
      }
    }
  }, _.removeEvent = function(e, t, n) {
    e.removeEventListener && e.removeEventListener(t, n)
  }, _.Event = function(e, t) {
    if (!(this instanceof _.Event)) return new _.Event(e, t);
    e && e.type ? (this.originalEvent = e, this.type = e.type, this.isDefaultPrevented = e.defaultPrevented || void 0 === e.defaultPrevented && !1 === e.returnValue ? Se : ke, this.target = e.target && 3 === e.target.nodeType ? e.target.parentNode : e.target, this.currentTarget = e.currentTarget, this.relatedTarget = e.relatedTarget) : this.type = e, t && _.extend(this, t), this.timeStamp = e && e.timeStamp || Date.now(), this[_.expando] = !0
  }, _.Event.prototype = {
    constructor: _.Event,
    isDefaultPrevented: ke,
    isPropagationStopped: ke,
    isImmediatePropagationStopped: ke,
    isSimulated: !1,
    preventDefault: function() {
      var e = this.originalEvent;
      this.isDefaultPrevented = Se, e && !this.isSimulated && e.preventDefault()
    },
    stopPropagation: function() {
      var e = this.originalEvent;
      this.isPropagationStopped = Se, e && !this.isSimulated && e.stopPropagation()
    },
    stopImmediatePropagation: function() {
      var e = this.originalEvent;
      this.isImmediatePropagationStopped = Se, e && !this.isSimulated && e.stopImmediatePropagation(), this.stopPropagation()
    }
  }, _.each({
    altKey: !0,
    bubbles: !0,
    cancelable: !0,
    changedTouches: !0,
    ctrlKey: !0,
    detail: !0,
    eventPhase: !0,
    metaKey: !0,
    pageX: !0,
    pageY: !0,
    shiftKey: !0,
    view: !0,
    char: !0,
    charCode: !0,
    key: !0,
    keyCode: !0,
    button: !0,
    buttons: !0,
    clientX: !0,
    clientY: !0,
    offsetX: !0,
    offsetY: !0,
    pointerId: !0,
    pointerType: !0,
    screenX: !0,
    screenY: !0,
    targetTouches: !0,
    toElement: !0,
    touches: !0,
    which: function(e) {
      var t = e.button;
      return null == e.which && be.test(e.type) ? null != e.charCode ? e.charCode : e.keyCode : !e.which && void 0 !== t && xe.test(e.type) ? 1 & t ? 1 : 2 & t ? 3 : 4 & t ? 2 : 0 : e.which
    }
  }, _.event.addProp), _.each({
    mouseenter: "mouseover",
    mouseleave: "mouseout",
    pointerenter: "pointerover",
    pointerleave: "pointerout"
  }, function(e, o) {
    _.event.special[e] = {
      delegateType: o,
      bindType: o,
      handle: function(e) {
        var t, n = e.relatedTarget,
          i = e.handleObj;
        return n && (n === this || _.contains(this, n)) || (e.type = i.origType, t = i.handler.apply(this, arguments), e.type = o), t
      }
    }
  }), _.fn.extend({
    on: function(e, t, n, i) {
      return _e(this, e, t, n, i)
    },
    one: function(e, t, n, i) {
      return _e(this, e, t, n, i, 1)
    },
    off: function(e, t, n) {
      var i, o;
      if (e && e.preventDefault && e.handleObj) return i = e.handleObj, _(e.delegateTarget).off(i.namespace ? i.origType + "." + i.namespace : i.origType, i.selector, i.handler), this;
      if ("object" == typeof e) {
        for (o in e) this.off(o, t, e[o]);
        return this
      }
      return !1 !== t && "function" != typeof t || (n = t, t = void 0), !1 === n && (n = ke), this.each(function() {
        _.event.remove(this, e, n, t)
      })
    }
  });
  var Te = /<(?!area|br|col|embed|hr|img|input|link|meta|param)(([a-z][^\/\0>\x20\t\r\n\f]*)[^>]*)\/>/gi,
    $e = /<script|<style|<link/i,
    Ae = /checked\s*(?:[^=]|=\s*.checked.)/i,
    je = /^\s*<!(?:\[CDATA\[|--)|(?:\]\]|--)>\s*$/g;

  function Pe(e, t) {
    return $(e, "table") && $(11 !== t.nodeType ? t : t.firstChild, "tr") && _(e).children("tbody")[0] || e
  }

  function Ee(e) {
    return e.type = (null !== e.getAttribute("type")) + "/" + e.type, e
  }

  function De(e) {
    return "true/" === (e.type || "").slice(0, 5) ? e.type = e.type.slice(5) : e.removeAttribute("type"), e
  }

  function Oe(e, t) {
    var n, i, o, r, s, a, l, c;
    if (1 === t.nodeType) {
      if (X.hasData(e) && (r = X.access(e), s = X.set(t, r), c = r.events))
        for (o in delete s.handle, s.events = {}, c)
          for (n = 0, i = c[o].length; n < i; n++) _.event.add(t, o, c[o][n]);
      Q.hasData(e) && (a = Q.access(e), l = _.extend({}, a), Q.set(t, l))
    }
  }

  function Me(n, i, o, r) {
    i = g.apply([], i);
    var e, t, s, a, l, c, u = 0,
      d = n.length,
      f = d - 1,
      h = i[0],
      p = y(h);
    if (p || 1 < d && "string" == typeof h && !v.checkClone && Ae.test(h)) return n.each(function(e) {
      var t = n.eq(e);
      p && (i[0] = h.call(this, e, t.html())), Me(t, i, o, r)
    });
    if (d && (t = (e = ve(i, n[0].ownerDocument, !1, n, r)).firstChild, 1 === e.childNodes.length && (e = t), t || r)) {
      for (a = (s = _.map(fe(e, "script"), Ee)).length; u < d; u++) l = e, u !== f && (l = _.clone(l, !0, !0), a && _.merge(s, fe(l, "script"))), o.call(n[u], l, u);
      if (a)
        for (c = s[s.length - 1].ownerDocument, _.map(s, De), u = 0; u < a; u++) l = s[u], ue.test(l.type || "") && !X.access(l, "globalEval") && _.contains(c, l) && (l.src && "module" !== (l.type || "").toLowerCase() ? _._evalUrl && _._evalUrl(l.src) : x(l.textContent.replace(je, ""), c, l))
    }
    return n
  }

  function Ie(e, t, n) {
    for (var i, o = t ? _.filter(t, e) : e, r = 0; null != (i = o[r]); r++) n || 1 !== i.nodeType || _.cleanData(fe(i)), i.parentNode && (n && _.contains(i.ownerDocument, i) && he(fe(i, "script")), i.parentNode.removeChild(i));
    return e
  }
  _.extend({
    htmlPrefilter: function(e) {
      return e.replace(Te, "<$1></$2>")
    },
    clone: function(e, t, n) {
      var i, o, r, s, a, l, c, u = e.cloneNode(!0),
        d = _.contains(e.ownerDocument, e);
      if (!(v.noCloneChecked || 1 !== e.nodeType && 11 !== e.nodeType || _.isXMLDoc(e)))
        for (s = fe(u), i = 0, o = (r = fe(e)).length; i < o; i++) a = r[i], l = s[i], void 0, "input" === (c = l.nodeName.toLowerCase()) && le.test(a.type) ? l.checked = a.checked : "input" !== c && "textarea" !== c || (l.defaultValue = a.defaultValue);
      if (t)
        if (n)
          for (r = r || fe(e), s = s || fe(u), i = 0, o = r.length; i < o; i++) Oe(r[i], s[i]);
        else Oe(e, u);
      return 0 < (s = fe(u, "script")).length && he(s, !d && fe(e, "script")), u
    },
    cleanData: function(e) {
      for (var t, n, i, o = _.event.special, r = 0; void 0 !== (n = e[r]); r++)
        if (U(n)) {
          if (t = n[X.expando]) {
            if (t.events)
              for (i in t.events) o[i] ? _.event.remove(n, i) : _.removeEvent(n, i, t.handle);
            n[X.expando] = void 0
          }
          n[Q.expando] && (n[Q.expando] = void 0)
        }
    }
  }), _.fn.extend({
    detach: function(e) {
      return Ie(this, e, !0)
    },
    remove: function(e) {
      return Ie(this, e)
    },
    text: function(e) {
      return z(this, function(e) {
        return void 0 === e ? _.text(this) : this.empty().each(function() {
          1 !== this.nodeType && 11 !== this.nodeType && 9 !== this.nodeType || (this.textContent = e)
        })
      }, null, e, arguments.length)
    },
    append: function() {
      return Me(this, arguments, function(e) {
        1 !== this.nodeType && 11 !== this.nodeType && 9 !== this.nodeType || Pe(this, e).appendChild(e)
      })
    },
    prepend: function() {
      return Me(this, arguments, function(e) {
        if (1 === this.nodeType || 11 === this.nodeType || 9 === this.nodeType) {
          var t = Pe(this, e);
          t.insertBefore(e, t.firstChild)
        }
      })
    },
    before: function() {
      return Me(this, arguments, function(e) {
        this.parentNode && this.parentNode.insertBefore(e, this)
      })
    },
    after: function() {
      return Me(this, arguments, function(e) {
        this.parentNode && this.parentNode.insertBefore(e, this.nextSibling)
      })
    },
    empty: function() {
      for (var e, t = 0; null != (e = this[t]); t++) 1 === e.nodeType && (_.cleanData(fe(e, !1)), e.textContent = "");
      return this
    },
    clone: function(e, t) {
      return e = null != e && e, t = null == t ? e : t, this.map(function() {
        return _.clone(this, e, t)
      })
    },
    html: function(e) {
      return z(this, function(e) {
        var t = this[0] || {},
          n = 0,
          i = this.length;
        if (void 0 === e && 1 === t.nodeType) return t.innerHTML;
        if ("string" == typeof e && !$e.test(e) && !de[(ce.exec(e) || ["", ""])[1].toLowerCase()]) {
          e = _.htmlPrefilter(e);
          try {
            for (; n < i; n++) 1 === (t = this[n] || {}).nodeType && (_.cleanData(fe(t, !1)), t.innerHTML = e);
            t = 0
          } catch (e) {}
        }
        t && this.empty().append(e)
      }, null, e, arguments.length)
    },
    replaceWith: function() {
      var n = [];
      return Me(this, arguments, function(e) {
        var t = this.parentNode;
        _.inArray(this, n) < 0 && (_.cleanData(fe(this)), t && t.replaceChild(e, this))
      }, n)
    }
  }), _.each({
    appendTo: "append",
    prependTo: "prepend",
    insertBefore: "before",
    insertAfter: "after",
    replaceAll: "replaceWith"
  }, function(e, s) {
    _.fn[e] = function(e) {
      for (var t, n = [], i = _(e), o = i.length - 1, r = 0; r <= o; r++) t = r === o ? this : this.clone(!0), _(i[r])[s](t), l.apply(n, t.get());
      return this.pushStack(n)
    }
  });
  var He = new RegExp("^(" + ee + ")(?!px)[a-z%]+$", "i"),
    Le = function(e) {
      var t = e.ownerDocument.defaultView;
      return t && t.opener || (t = k), t.getComputedStyle(e)
    },
    Re = new RegExp(ne.join("|"), "i");

  function Ne(e, t, n) {
    var i, o, r, s, a = e.style;
    return (n = n || Le(e)) && ("" !== (s = n.getPropertyValue(t) || n[t]) || _.contains(e.ownerDocument, e) || (s = _.style(e, t)), !v.pixelBoxStyles() && He.test(s) && Re.test(t) && (i = a.width, o = a.minWidth, r = a.maxWidth, a.minWidth = a.maxWidth = a.width = s, s = n.width, a.width = i, a.minWidth = o, a.maxWidth = r)), void 0 !== s ? s + "" : s
  }

  function Fe(e, t) {
    return {
      get: function() {
        if (!e()) return (this.get = t).apply(this, arguments);
        delete this.get
      }
    }
  }! function() {
    function e() {
      if (l) {
        a.style.cssText = "position:absolute;left:-11111px;width:60px;margin-top:1px;padding:0;border:0", l.style.cssText = "position:relative;display:block;box-sizing:border-box;overflow:scroll;margin:auto;border:1px;padding:1px;width:60%;top:1%", ye.appendChild(a).appendChild(l);
        var e = k.getComputedStyle(l);
        n = "1%" !== e.top, s = 12 === t(e.marginLeft), l.style.right = "60%", r = 36 === t(e.right), i = 36 === t(e.width), l.style.position = "absolute", o = 36 === l.offsetWidth || "absolute", ye.removeChild(a), l = null
      }
    }

    function t(e) {
      return Math.round(parseFloat(e))
    }
    var n, i, o, r, s, a = C.createElement("div"),
      l = C.createElement("div");
    l.style && (l.style.backgroundClip = "content-box", l.cloneNode(!0).style.backgroundClip = "", v.clearCloneStyle = "content-box" === l.style.backgroundClip, _.extend(v, {
      boxSizingReliable: function() {
        return e(), i
      },
      pixelBoxStyles: function() {
        return e(), r
      },
      pixelPosition: function() {
        return e(), n
      },
      reliableMarginLeft: function() {
        return e(), s
      },
      scrollboxSize: function() {
        return e(), o
      }
    }))
  }();
  var Be = /^(none|table(?!-c[ea]).+)/,
    ze = /^--/,
    qe = {
      position: "absolute",
      visibility: "hidden",
      display: "block"
    },
    We = {
      letterSpacing: "0",
      fontWeight: "400"
    },
    Ye = ["Webkit", "Moz", "ms"],
    Ve = C.createElement("div").style;

  function Ue(e) {
    var t = _.cssProps[e];
    return t || (t = _.cssProps[e] = function(e) {
      if (e in Ve) return e;
      for (var t = e[0].toUpperCase() + e.slice(1), n = Ye.length; n--;)
        if ((e = Ye[n] + t) in Ve) return e
    }(e) || e), t
  }

  function Ge(e, t, n) {
    var i = te.exec(t);
    return i ? Math.max(0, i[2] - (n || 0)) + (i[3] || "px") : t
  }

  function Xe(e, t, n, i, o, r) {
    var s = "width" === t ? 1 : 0,
      a = 0,
      l = 0;
    if (n === (i ? "border" : "content")) return 0;
    for (; s < 4; s += 2) "margin" === n && (l += _.css(e, n + ne[s], !0, o)), i ? ("content" === n && (l -= _.css(e, "padding" + ne[s], !0, o)), "margin" !== n && (l -= _.css(e, "border" + ne[s] + "Width", !0, o))) : (l += _.css(e, "padding" + ne[s], !0, o), "padding" !== n ? l += _.css(e, "border" + ne[s] + "Width", !0, o) : a += _.css(e, "border" + ne[s] + "Width", !0, o));
    return !i && 0 <= r && (l += Math.max(0, Math.ceil(e["offset" + t[0].toUpperCase() + t.slice(1)] - r - l - a - .5))), l
  }

  function Qe(e, t, n) {
    var i = Le(e),
      o = Ne(e, t, i),
      r = "border-box" === _.css(e, "boxSizing", !1, i),
      s = r;
    if (He.test(o)) {
      if (!n) return o;
      o = "auto"
    }
    return s = s && (v.boxSizingReliable() || o === e.style[t]), ("auto" === o || !parseFloat(o) && "inline" === _.css(e, "display", !1, i)) && (o = e["offset" + t[0].toUpperCase() + t.slice(1)], s = !0), (o = parseFloat(o) || 0) + Xe(e, t, n || (r ? "border" : "content"), s, i, o) + "px"
  }

  function Ze(e, t, n, i, o) {
    return new Ze.prototype.init(e, t, n, i, o)
  }
  _.extend({
    cssHooks: {
      opacity: {
        get: function(e, t) {
          if (t) {
            var n = Ne(e, "opacity");
            return "" === n ? "1" : n
          }
        }
      }
    },
    cssNumber: {
      animationIterationCount: !0,
      columnCount: !0,
      fillOpacity: !0,
      flexGrow: !0,
      flexShrink: !0,
      fontWeight: !0,
      lineHeight: !0,
      opacity: !0,
      order: !0,
      orphans: !0,
      widows: !0,
      zIndex: !0,
      zoom: !0
    },
    cssProps: {},
    style: function(e, t, n, i) {
      if (e && 3 !== e.nodeType && 8 !== e.nodeType && e.style) {
        var o, r, s, a = V(t),
          l = ze.test(t),
          c = e.style;
        if (l || (t = Ue(a)), s = _.cssHooks[t] || _.cssHooks[a], void 0 === n) return s && "get" in s && void 0 !== (o = s.get(e, !1, i)) ? o : c[t];
        "string" == (r = typeof n) && (o = te.exec(n)) && o[1] && (n = re(e, t, o), r = "number"), null != n && n == n && ("number" === r && (n += o && o[3] || (_.cssNumber[a] ? "" : "px")), v.clearCloneStyle || "" !== n || 0 !== t.indexOf("background") || (c[t] = "inherit"), s && "set" in s && void 0 === (n = s.set(e, n, i)) || (l ? c.setProperty(t, n) : c[t] = n))
      }
    },
    css: function(e, t, n, i) {
      var o, r, s, a = V(t);
      return ze.test(t) || (t = Ue(a)), (s = _.cssHooks[t] || _.cssHooks[a]) && "get" in s && (o = s.get(e, !0, n)), void 0 === o && (o = Ne(e, t, i)), "normal" === o && t in We && (o = We[t]), "" === n || n ? (r = parseFloat(o), !0 === n || isFinite(r) ? r || 0 : o) : o
    }
  }), _.each(["height", "width"], function(e, a) {
    _.cssHooks[a] = {
      get: function(e, t, n) {
        if (t) return !Be.test(_.css(e, "display")) || e.getClientRects().length && e.getBoundingClientRect().width ? Qe(e, a, n) : oe(e, qe, function() {
          return Qe(e, a, n)
        })
      },
      set: function(e, t, n) {
        var i, o = Le(e),
          r = "border-box" === _.css(e, "boxSizing", !1, o),
          s = n && Xe(e, a, n, r, o);
        return r && v.scrollboxSize() === o.position && (s -= Math.ceil(e["offset" + a[0].toUpperCase() + a.slice(1)] - parseFloat(o[a]) - Xe(e, a, "border", !1, o) - .5)), s && (i = te.exec(t)) && "px" !== (i[3] || "px") && (e.style[a] = t, t = _.css(e, a)), Ge(0, t, s)
      }
    }
  }), _.cssHooks.marginLeft = Fe(v.reliableMarginLeft, function(e, t) {
    if (t) return (parseFloat(Ne(e, "marginLeft")) || e.getBoundingClientRect().left - oe(e, {
      marginLeft: 0
    }, function() {
      return e.getBoundingClientRect().left
    })) + "px"
  }), _.each({
    margin: "",
    padding: "",
    border: "Width"
  }, function(o, r) {
    _.cssHooks[o + r] = {
      expand: function(e) {
        for (var t = 0, n = {}, i = "string" == typeof e ? e.split(" ") : [e]; t < 4; t++) n[o + ne[t] + r] = i[t] || i[t - 2] || i[0];
        return n
      }
    }, "margin" !== o && (_.cssHooks[o + r].set = Ge)
  }), _.fn.extend({
    css: function(e, t) {
      return z(this, function(e, t, n) {
        var i, o, r = {},
          s = 0;
        if (Array.isArray(t)) {
          for (i = Le(e), o = t.length; s < o; s++) r[t[s]] = _.css(e, t[s], !1, i);
          return r
        }
        return void 0 !== n ? _.style(e, t, n) : _.css(e, t)
      }, e, t, 1 < arguments.length)
    }
  }), ((_.Tween = Ze).prototype = {
    constructor: Ze,
    init: function(e, t, n, i, o, r) {
      this.elem = e, this.prop = n, this.easing = o || _.easing._default, this.options = t, this.start = this.now = this.cur(), this.end = i, this.unit = r || (_.cssNumber[n] ? "" : "px")
    },
    cur: function() {
      var e = Ze.propHooks[this.prop];
      return e && e.get ? e.get(this) : Ze.propHooks._default.get(this)
    },
    run: function(e) {
      var t, n = Ze.propHooks[this.prop];
      return this.options.duration ? this.pos = t = _.easing[this.easing](e, this.options.duration * e, 0, 1, this.options.duration) : this.pos = t = e, this.now = (this.end - this.start) * t + this.start, this.options.step && this.options.step.call(this.elem, this.now, this), n && n.set ? n.set(this) : Ze.propHooks._default.set(this), this
    }
  }).init.prototype = Ze.prototype, (Ze.propHooks = {
    _default: {
      get: function(e) {
        var t;
        return 1 !== e.elem.nodeType || null != e.elem[e.prop] && null == e.elem.style[e.prop] ? e.elem[e.prop] : (t = _.css(e.elem, e.prop, "")) && "auto" !== t ? t : 0
      },
      set: function(e) {
        _.fx.step[e.prop] ? _.fx.step[e.prop](e) : 1 !== e.elem.nodeType || null == e.elem.style[_.cssProps[e.prop]] && !_.cssHooks[e.prop] ? e.elem[e.prop] = e.now : _.style(e.elem, e.prop, e.now + e.unit)
      }
    }
  }).scrollTop = Ze.propHooks.scrollLeft = {
    set: function(e) {
      e.elem.nodeType && e.elem.parentNode && (e.elem[e.prop] = e.now)
    }
  }, _.easing = {
    linear: function(e) {
      return e
    },
    swing: function(e) {
      return .5 - Math.cos(e * Math.PI) / 2
    },
    _default: "swing"
  }, _.fx = Ze.prototype.init, _.fx.step = {};
  var Ke, Je, et, tt, nt = /^(?:toggle|show|hide)$/,
    it = /queueHooks$/;

  function ot() {
    Je && (!1 === C.hidden && k.requestAnimationFrame ? k.requestAnimationFrame(ot) : k.setTimeout(ot, _.fx.interval), _.fx.tick())
  }

  function rt() {
    return k.setTimeout(function() {
      Ke = void 0
    }), Ke = Date.now()
  }

  function st(e, t) {
    var n, i = 0,
      o = {
        height: e
      };
    for (t = t ? 1 : 0; i < 4; i += 2 - t) o["margin" + (n = ne[i])] = o["padding" + n] = e;
    return t && (o.opacity = o.width = e), o
  }

  function at(e, t, n) {
    for (var i, o = (lt.tweeners[t] || []).concat(lt.tweeners["*"]), r = 0, s = o.length; r < s; r++)
      if (i = o[r].call(n, t, e)) return i
  }

  function lt(r, e, t) {
    var n, s, i = 0,
      o = lt.prefilters.length,
      a = _.Deferred().always(function() {
        delete l.elem
      }),
      l = function() {
        if (s) return !1;
        for (var e = Ke || rt(), t = Math.max(0, c.startTime + c.duration - e), n = 1 - (t / c.duration || 0), i = 0, o = c.tweens.length; i < o; i++) c.tweens[i].run(n);
        return a.notifyWith(r, [c, n, t]), n < 1 && o ? t : (o || a.notifyWith(r, [c, 1, 0]), a.resolveWith(r, [c]), !1)
      },
      c = a.promise({
        elem: r,
        props: _.extend({}, e),
        opts: _.extend(!0, {
          specialEasing: {},
          easing: _.easing._default
        }, t),
        originalProperties: e,
        originalOptions: t,
        startTime: Ke || rt(),
        duration: t.duration,
        tweens: [],
        createTween: function(e, t) {
          var n = _.Tween(r, c.opts, e, t, c.opts.specialEasing[e] || c.opts.easing);
          return c.tweens.push(n), n
        },
        stop: function(e) {
          var t = 0,
            n = e ? c.tweens.length : 0;
          if (s) return this;
          for (s = !0; t < n; t++) c.tweens[t].run(1);
          return e ? (a.notifyWith(r, [c, 1, 0]), a.resolveWith(r, [c, e])) : a.rejectWith(r, [c, e]), this
        }
      }),
      u = c.props;
    for (function(e, t) {
        var n, i, o, r, s;
        for (n in e)
          if (o = t[i = V(n)], r = e[n], Array.isArray(r) && (o = r[1], r = e[n] = r[0]), n !== i && (e[i] = r, delete e[n]), (s = _.cssHooks[i]) && "expand" in s)
            for (n in r = s.expand(r), delete e[i], r) n in e || (e[n] = r[n], t[n] = o);
          else t[i] = o
      }(u, c.opts.specialEasing); i < o; i++)
      if (n = lt.prefilters[i].call(c, r, u, c.opts)) return y(n.stop) && (_._queueHooks(c.elem, c.opts.queue).stop = n.stop.bind(n)), n;
    return _.map(u, at, c), y(c.opts.start) && c.opts.start.call(r, c), c.progress(c.opts.progress).done(c.opts.done, c.opts.complete).fail(c.opts.fail).always(c.opts.always), _.fx.timer(_.extend(l, {
      elem: r,
      anim: c,
      queue: c.opts.queue
    })), c
  }
  _.Animation = _.extend(lt, {
    tweeners: {
      "*": [function(e, t) {
        var n = this.createTween(e, t);
        return re(n.elem, e, te.exec(t), n), n
      }]
    },
    tweener: function(e, t) {
      y(e) ? (t = e, e = ["*"]) : e = e.match(I);
      for (var n, i = 0, o = e.length; i < o; i++) n = e[i], lt.tweeners[n] = lt.tweeners[n] || [], lt.tweeners[n].unshift(t)
    },
    prefilters: [function(e, t, n) {
      var i, o, r, s, a, l, c, u, d = "width" in t || "height" in t,
        f = this,
        h = {},
        p = e.style,
        g = e.nodeType && ie(e),
        m = X.get(e, "fxshow");
      for (i in n.queue || (null == (s = _._queueHooks(e, "fx")).unqueued && (s.unqueued = 0, a = s.empty.fire, s.empty.fire = function() {
          s.unqueued || a()
        }), s.unqueued++, f.always(function() {
          f.always(function() {
            s.unqueued--, _.queue(e, "fx").length || s.empty.fire()
          })
        })), t)
        if (o = t[i], nt.test(o)) {
          if (delete t[i], r = r || "toggle" === o, o === (g ? "hide" : "show")) {
            if ("show" !== o || !m || void 0 === m[i]) continue;
            g = !0
          }
          h[i] = m && m[i] || _.style(e, i)
        }
      if ((l = !_.isEmptyObject(t)) || !_.isEmptyObject(h))
        for (i in d && 1 === e.nodeType && (n.overflow = [p.overflow, p.overflowX, p.overflowY], null == (c = m && m.display) && (c = X.get(e, "display")), "none" === (u = _.css(e, "display")) && (c ? u = c : (ae([e], !0), c = e.style.display || c, u = _.css(e, "display"), ae([e]))), ("inline" === u || "inline-block" === u && null != c) && "none" === _.css(e, "float") && (l || (f.done(function() {
            p.display = c
          }), null == c && (u = p.display, c = "none" === u ? "" : u)), p.display = "inline-block")), n.overflow && (p.overflow = "hidden", f.always(function() {
            p.overflow = n.overflow[0], p.overflowX = n.overflow[1], p.overflowY = n.overflow[2]
          })), l = !1, h) l || (m ? "hidden" in m && (g = m.hidden) : m = X.access(e, "fxshow", {
          display: c
        }), r && (m.hidden = !g), g && ae([e], !0), f.done(function() {
          for (i in g || ae([e]), X.remove(e, "fxshow"), h) _.style(e, i, h[i])
        })), l = at(g ? m[i] : 0, i, f), i in m || (m[i] = l.start, g && (l.end = l.start, l.start = 0))
    }],
    prefilter: function(e, t) {
      t ? lt.prefilters.unshift(e) : lt.prefilters.push(e)
    }
  }), _.speed = function(e, t, n) {
    var i = e && "object" == typeof e ? _.extend({}, e) : {
      complete: n || !n && t || y(e) && e,
      duration: e,
      easing: n && t || t && !y(t) && t
    };
    return _.fx.off ? i.duration = 0 : "number" != typeof i.duration && (i.duration in _.fx.speeds ? i.duration = _.fx.speeds[i.duration] : i.duration = _.fx.speeds._default), null != i.queue && !0 !== i.queue || (i.queue = "fx"), i.old = i.complete, i.complete = function() {
      y(i.old) && i.old.call(this), i.queue && _.dequeue(this, i.queue)
    }, i
  }, _.fn.extend({
    fadeTo: function(e, t, n, i) {
      return this.filter(ie).css("opacity", 0).show().end().animate({
        opacity: t
      }, e, n, i)
    },
    animate: function(t, e, n, i) {
      var o = _.isEmptyObject(t),
        r = _.speed(e, n, i),
        s = function() {
          var e = lt(this, _.extend({}, t), r);
          (o || X.get(this, "finish")) && e.stop(!0)
        };
      return s.finish = s, o || !1 === r.queue ? this.each(s) : this.queue(r.queue, s)
    },
    stop: function(o, e, r) {
      var s = function(e) {
        var t = e.stop;
        delete e.stop, t(r)
      };
      return "string" != typeof o && (r = e, e = o, o = void 0), e && !1 !== o && this.queue(o || "fx", []), this.each(function() {
        var e = !0,
          t = null != o && o + "queueHooks",
          n = _.timers,
          i = X.get(this);
        if (t) i[t] && i[t].stop && s(i[t]);
        else
          for (t in i) i[t] && i[t].stop && it.test(t) && s(i[t]);
        for (t = n.length; t--;) n[t].elem !== this || null != o && n[t].queue !== o || (n[t].anim.stop(r), e = !1, n.splice(t, 1));
        !e && r || _.dequeue(this, o)
      })
    },
    finish: function(s) {
      return !1 !== s && (s = s || "fx"), this.each(function() {
        var e, t = X.get(this),
          n = t[s + "queue"],
          i = t[s + "queueHooks"],
          o = _.timers,
          r = n ? n.length : 0;
        for (t.finish = !0, _.queue(this, s, []), i && i.stop && i.stop.call(this, !0), e = o.length; e--;) o[e].elem === this && o[e].queue === s && (o[e].anim.stop(!0), o.splice(e, 1));
        for (e = 0; e < r; e++) n[e] && n[e].finish && n[e].finish.call(this);
        delete t.finish
      })
    }
  }), _.each(["toggle", "show", "hide"], function(e, i) {
    var o = _.fn[i];
    _.fn[i] = function(e, t, n) {
      return null == e || "boolean" == typeof e ? o.apply(this, arguments) : this.animate(st(i, !0), e, t, n)
    }
  }), _.each({
    slideDown: st("show"),
    slideUp: st("hide"),
    slideToggle: st("toggle"),
    fadeIn: {
      opacity: "show"
    },
    fadeOut: {
      opacity: "hide"
    },
    fadeToggle: {
      opacity: "toggle"
    }
  }, function(e, i) {
    _.fn[e] = function(e, t, n) {
      return this.animate(i, e, t, n)
    }
  }), _.timers = [], _.fx.tick = function() {
    var e, t = 0,
      n = _.timers;
    for (Ke = Date.now(); t < n.length; t++)(e = n[t])() || n[t] !== e || n.splice(t--, 1);
    n.length || _.fx.stop(), Ke = void 0
  }, _.fx.timer = function(e) {
    _.timers.push(e), _.fx.start()
  }, _.fx.interval = 13, _.fx.start = function() {
    Je || (Je = !0, ot())
  }, _.fx.stop = function() {
    Je = null
  }, _.fx.speeds = {
    slow: 600,
    fast: 200,
    _default: 400
  }, _.fn.delay = function(i, e) {
    return i = _.fx && _.fx.speeds[i] || i, e = e || "fx", this.queue(e, function(e, t) {
      var n = k.setTimeout(e, i);
      t.stop = function() {
        k.clearTimeout(n)
      }
    })
  }, et = C.createElement("input"), tt = C.createElement("select").appendChild(C.createElement("option")), et.type = "checkbox", v.checkOn = "" !== et.value, v.optSelected = tt.selected, (et = C.createElement("input")).value = "t", et.type = "radio", v.radioValue = "t" === et.value;
  var ct, ut = _.expr.attrHandle;
  _.fn.extend({
    attr: function(e, t) {
      return z(this, _.attr, e, t, 1 < arguments.length)
    },
    removeAttr: function(e) {
      return this.each(function() {
        _.removeAttr(this, e)
      })
    }
  }), _.extend({
    attr: function(e, t, n) {
      var i, o, r = e.nodeType;
      if (3 !== r && 8 !== r && 2 !== r) return void 0 === e.getAttribute ? _.prop(e, t, n) : (1 === r && _.isXMLDoc(e) || (o = _.attrHooks[t.toLowerCase()] || (_.expr.match.bool.test(t) ? ct : void 0)), void 0 !== n ? null === n ? void _.removeAttr(e, t) : o && "set" in o && void 0 !== (i = o.set(e, n, t)) ? i : (e.setAttribute(t, n + ""), n) : o && "get" in o && null !== (i = o.get(e, t)) ? i : null == (i = _.find.attr(e, t)) ? void 0 : i)
    },
    attrHooks: {
      type: {
        set: function(e, t) {
          if (!v.radioValue && "radio" === t && $(e, "input")) {
            var n = e.value;
            return e.setAttribute("type", t), n && (e.value = n), t
          }
        }
      }
    },
    removeAttr: function(e, t) {
      var n, i = 0,
        o = t && t.match(I);
      if (o && 1 === e.nodeType)
        for (; n = o[i++];) e.removeAttribute(n)
    }
  }), ct = {
    set: function(e, t, n) {
      return !1 === t ? _.removeAttr(e, n) : e.setAttribute(n, n), n
    }
  }, _.each(_.expr.match.bool.source.match(/\w+/g), function(e, t) {
    var s = ut[t] || _.find.attr;
    ut[t] = function(e, t, n) {
      var i, o, r = t.toLowerCase();
      return n || (o = ut[r], ut[r] = i, i = null != s(e, t, n) ? r : null, ut[r] = o), i
    }
  });
  var dt = /^(?:input|select|textarea|button)$/i,
    ft = /^(?:a|area)$/i;

  function ht(e) {
    return (e.match(I) || []).join(" ")
  }

  function pt(e) {
    return e.getAttribute && e.getAttribute("class") || ""
  }

  function gt(e) {
    return Array.isArray(e) ? e : "string" == typeof e && e.match(I) || []
  }
  _.fn.extend({
    prop: function(e, t) {
      return z(this, _.prop, e, t, 1 < arguments.length)
    },
    removeProp: function(e) {
      return this.each(function() {
        delete this[_.propFix[e] || e]
      })
    }
  }), _.extend({
    prop: function(e, t, n) {
      var i, o, r = e.nodeType;
      if (3 !== r && 8 !== r && 2 !== r) return 1 === r && _.isXMLDoc(e) || (t = _.propFix[t] || t, o = _.propHooks[t]), void 0 !== n ? o && "set" in o && void 0 !== (i = o.set(e, n, t)) ? i : e[t] = n : o && "get" in o && null !== (i = o.get(e, t)) ? i : e[t]
    },
    propHooks: {
      tabIndex: {
        get: function(e) {
          var t = _.find.attr(e, "tabindex");
          return t ? parseInt(t, 10) : dt.test(e.nodeName) || ft.test(e.nodeName) && e.href ? 0 : -1
        }
      }
    },
    propFix: {
      for: "htmlFor",
      class: "className"
    }
  }), v.optSelected || (_.propHooks.selected = {
    get: function(e) {
      var t = e.parentNode;
      return t && t.parentNode && t.parentNode.selectedIndex, null
    },
    set: function(e) {
      var t = e.parentNode;
      t && (t.selectedIndex, t.parentNode && t.parentNode.selectedIndex)
    }
  }), _.each(["tabIndex", "readOnly", "maxLength", "cellSpacing", "cellPadding", "rowSpan", "colSpan", "useMap", "frameBorder", "contentEditable"], function() {
    _.propFix[this.toLowerCase()] = this
  }), _.fn.extend({
    addClass: function(t) {
      var e, n, i, o, r, s, a, l = 0;
      if (y(t)) return this.each(function(e) {
        _(this).addClass(t.call(this, e, pt(this)))
      });
      if ((e = gt(t)).length)
        for (; n = this[l++];)
          if (o = pt(n), i = 1 === n.nodeType && " " + ht(o) + " ") {
            for (s = 0; r = e[s++];) i.indexOf(" " + r + " ") < 0 && (i += r + " ");
            o !== (a = ht(i)) && n.setAttribute("class", a)
          }
      return this
    },
    removeClass: function(t) {
      var e, n, i, o, r, s, a, l = 0;
      if (y(t)) return this.each(function(e) {
        _(this).removeClass(t.call(this, e, pt(this)))
      });
      if (!arguments.length) return this.attr("class", "");
      if ((e = gt(t)).length)
        for (; n = this[l++];)
          if (o = pt(n), i = 1 === n.nodeType && " " + ht(o) + " ") {
            for (s = 0; r = e[s++];)
              for (; - 1 < i.indexOf(" " + r + " ");) i = i.replace(" " + r + " ", " ");
            o !== (a = ht(i)) && n.setAttribute("class", a)
          }
      return this
    },
    toggleClass: function(o, t) {
      var r = typeof o,
        s = "string" === r || Array.isArray(o);
      return "boolean" == typeof t && s ? t ? this.addClass(o) : this.removeClass(o) : y(o) ? this.each(function(e) {
        _(this).toggleClass(o.call(this, e, pt(this), t), t)
      }) : this.each(function() {
        var e, t, n, i;
        if (s)
          for (t = 0, n = _(this), i = gt(o); e = i[t++];) n.hasClass(e) ? n.removeClass(e) : n.addClass(e);
        else void 0 !== o && "boolean" !== r || ((e = pt(this)) && X.set(this, "__className__", e), this.setAttribute && this.setAttribute("class", e || !1 === o ? "" : X.get(this, "__className__") || ""))
      })
    },
    hasClass: function(e) {
      var t, n, i = 0;
      for (t = " " + e + " "; n = this[i++];)
        if (1 === n.nodeType && -1 < (" " + ht(pt(n)) + " ").indexOf(t)) return !0;
      return !1
    }
  });
  var mt = /\r/g;
  _.fn.extend({
    val: function(n) {
      var i, e, o, t = this[0];
      return arguments.length ? (o = y(n), this.each(function(e) {
        var t;
        1 === this.nodeType && (null == (t = o ? n.call(this, e, _(this).val()) : n) ? t = "" : "number" == typeof t ? t += "" : Array.isArray(t) && (t = _.map(t, function(e) {
          return null == e ? "" : e + ""
        })), (i = _.valHooks[this.type] || _.valHooks[this.nodeName.toLowerCase()]) && "set" in i && void 0 !== i.set(this, t, "value") || (this.value = t))
      })) : t ? (i = _.valHooks[t.type] || _.valHooks[t.nodeName.toLowerCase()]) && "get" in i && void 0 !== (e = i.get(t, "value")) ? e : "string" == typeof(e = t.value) ? e.replace(mt, "") : null == e ? "" : e : void 0
    }
  }), _.extend({
    valHooks: {
      option: {
        get: function(e) {
          var t = _.find.attr(e, "value");
          return null != t ? t : ht(_.text(e))
        }
      },
      select: {
        get: function(e) {
          var t, n, i, o = e.options,
            r = e.selectedIndex,
            s = "select-one" === e.type,
            a = s ? null : [],
            l = s ? r + 1 : o.length;
          for (i = r < 0 ? l : s ? r : 0; i < l; i++)
            if (((n = o[i]).selected || i === r) && !n.disabled && (!n.parentNode.disabled || !$(n.parentNode, "optgroup"))) {
              if (t = _(n).val(), s) return t;
              a.push(t)
            }
          return a
        },
        set: function(e, t) {
          for (var n, i, o = e.options, r = _.makeArray(t), s = o.length; s--;)((i = o[s]).selected = -1 < _.inArray(_.valHooks.option.get(i), r)) && (n = !0);
          return n || (e.selectedIndex = -1), r
        }
      }
    }
  }), _.each(["radio", "checkbox"], function() {
    _.valHooks[this] = {
      set: function(e, t) {
        if (Array.isArray(t)) return e.checked = -1 < _.inArray(_(e).val(), t)
      }
    }, v.checkOn || (_.valHooks[this].get = function(e) {
      return null === e.getAttribute("value") ? "on" : e.value
    })
  }), v.focusin = "onfocusin" in k;
  var vt = /^(?:focusinfocus|focusoutblur)$/,
    yt = function(e) {
      e.stopPropagation()
    };
  _.extend(_.event, {
    trigger: function(e, t, n, i) {
      var o, r, s, a, l, c, u, d, f = [n || C],
        h = m.call(e, "type") ? e.type : e,
        p = m.call(e, "namespace") ? e.namespace.split(".") : [];
      if (r = d = s = n = n || C, 3 !== n.nodeType && 8 !== n.nodeType && !vt.test(h + _.event.triggered) && (-1 < h.indexOf(".") && (h = (p = h.split(".")).shift(), p.sort()), l = h.indexOf(":") < 0 && "on" + h, (e = e[_.expando] ? e : new _.Event(h, "object" == typeof e && e)).isTrigger = i ? 2 : 3, e.namespace = p.join("."), e.rnamespace = e.namespace ? new RegExp("(^|\\.)" + p.join("\\.(?:.*\\.|)") + "(\\.|$)") : null, e.result = void 0, e.target || (e.target = n), t = null == t ? [e] : _.makeArray(t, [e]), u = _.event.special[h] || {}, i || !u.trigger || !1 !== u.trigger.apply(n, t))) {
        if (!i && !u.noBubble && !b(n)) {
          for (a = u.delegateType || h, vt.test(a + h) || (r = r.parentNode); r; r = r.parentNode) f.push(r), s = r;
          s === (n.ownerDocument || C) && f.push(s.defaultView || s.parentWindow || k)
        }
        for (o = 0;
          (r = f[o++]) && !e.isPropagationStopped();) d = r, e.type = 1 < o ? a : u.bindType || h, (c = (X.get(r, "events") || {})[e.type] && X.get(r, "handle")) && c.apply(r, t), (c = l && r[l]) && c.apply && U(r) && (e.result = c.apply(r, t), !1 === e.result && e.preventDefault());
        return e.type = h, i || e.isDefaultPrevented() || u._default && !1 !== u._default.apply(f.pop(), t) || !U(n) || l && y(n[h]) && !b(n) && ((s = n[l]) && (n[l] = null), _.event.triggered = h, e.isPropagationStopped() && d.addEventListener(h, yt), n[h](), e.isPropagationStopped() && d.removeEventListener(h, yt), _.event.triggered = void 0, s && (n[l] = s)), e.result
      }
    },
    simulate: function(e, t, n) {
      var i = _.extend(new _.Event, n, {
        type: e,
        isSimulated: !0
      });
      _.event.trigger(i, null, t)
    }
  }), _.fn.extend({
    trigger: function(e, t) {
      return this.each(function() {
        _.event.trigger(e, t, this)
      })
    },
    triggerHandler: function(e, t) {
      var n = this[0];
      if (n) return _.event.trigger(e, t, n, !0)
    }
  }), v.focusin || _.each({
    focus: "focusin",
    blur: "focusout"
  }, function(n, i) {
    var o = function(e) {
      _.event.simulate(i, e.target, _.event.fix(e))
    };
    _.event.special[i] = {
      setup: function() {
        var e = this.ownerDocument || this,
          t = X.access(e, i);
        t || e.addEventListener(n, o, !0), X.access(e, i, (t || 0) + 1)
      },
      teardown: function() {
        var e = this.ownerDocument || this,
          t = X.access(e, i) - 1;
        t ? X.access(e, i, t) : (e.removeEventListener(n, o, !0), X.remove(e, i))
      }
    }
  });
  var bt = k.location,
    xt = Date.now(),
    wt = /\?/;
  _.parseXML = function(e) {
    var t;
    if (!e || "string" != typeof e) return null;
    try {
      t = (new k.DOMParser).parseFromString(e, "text/xml")
    } catch (e) {
      t = void 0
    }
    return t && !t.getElementsByTagName("parsererror").length || _.error("Invalid XML: " + e), t
  };
  var St = /\[\]$/,
    kt = /\r?\n/g,
    Ct = /^(?:submit|button|image|reset|file)$/i,
    _t = /^(?:input|select|textarea|keygen)/i;

  function Tt(n, e, i, o) {
    var t;
    if (Array.isArray(e)) _.each(e, function(e, t) {
      i || St.test(n) ? o(n, t) : Tt(n + "[" + ("object" == typeof t && null != t ? e : "") + "]", t, i, o)
    });
    else if (i || "object" !== w(e)) o(n, e);
    else
      for (t in e) Tt(n + "[" + t + "]", e[t], i, o)
  }
  _.param = function(e, t) {
    var n, i = [],
      o = function(e, t) {
        var n = y(t) ? t() : t;
        i[i.length] = encodeURIComponent(e) + "=" + encodeURIComponent(null == n ? "" : n)
      };
    if (Array.isArray(e) || e.jquery && !_.isPlainObject(e)) _.each(e, function() {
      o(this.name, this.value)
    });
    else
      for (n in e) Tt(n, e[n], t, o);
    return i.join("&")
  }, _.fn.extend({
    serialize: function() {
      return _.param(this.serializeArray())
    },
    serializeArray: function() {
      return this.map(function() {
        var e = _.prop(this, "elements");
        return e ? _.makeArray(e) : this
      }).filter(function() {
        var e = this.type;
        return this.name && !_(this).is(":disabled") && _t.test(this.nodeName) && !Ct.test(e) && (this.checked || !le.test(e))
      }).map(function(e, t) {
        var n = _(this).val();
        return null == n ? null : Array.isArray(n) ? _.map(n, function(e) {
          return {
            name: t.name,
            value: e.replace(kt, "\r\n")
          }
        }) : {
          name: t.name,
          value: n.replace(kt, "\r\n")
        }
      }).get()
    }
  });
  var $t = /%20/g,
    At = /#.*$/,
    jt = /([?&])_=[^&]*/,
    Pt = /^(.*?):[ \t]*([^\r\n]*)$/gm,
    Et = /^(?:GET|HEAD)$/,
    Dt = /^\/\//,
    Ot = {},
    Mt = {},
    It = "*/".concat("*"),
    Ht = C.createElement("a");

  function Lt(r) {
    return function(e, t) {
      "string" != typeof e && (t = e, e = "*");
      var n, i = 0,
        o = e.toLowerCase().match(I) || [];
      if (y(t))
        for (; n = o[i++];) "+" === n[0] ? (n = n.slice(1) || "*", (r[n] = r[n] || []).unshift(t)) : (r[n] = r[n] || []).push(t)
    }
  }

  function Rt(t, o, r, s) {
    var a = {},
      l = t === Mt;

    function c(e) {
      var i;
      return a[e] = !0, _.each(t[e] || [], function(e, t) {
        var n = t(o, r, s);
        return "string" != typeof n || l || a[n] ? l ? !(i = n) : void 0 : (o.dataTypes.unshift(n), c(n), !1)
      }), i
    }
    return c(o.dataTypes[0]) || !a["*"] && c("*")
  }

  function Nt(e, t) {
    var n, i, o = _.ajaxSettings.flatOptions || {};
    for (n in t) void 0 !== t[n] && ((o[n] ? e : i || (i = {}))[n] = t[n]);
    return i && _.extend(!0, e, i), e
  }
  Ht.href = bt.href, _.extend({
    active: 0,
    lastModified: {},
    etag: {},
    ajaxSettings: {
      url: bt.href,
      type: "GET",
      isLocal: /^(?:about|app|app-storage|.+-extension|file|res|widget):$/.test(bt.protocol),
      global: !0,
      processData: !0,
      async: !0,
      contentType: "application/x-www-form-urlencoded; charset=UTF-8",
      accepts: {
        "*": It,
        text: "text/plain",
        html: "text/html",
        xml: "application/xml, text/xml",
        json: "application/json, text/javascript"
      },
      contents: {
        xml: /\bxml\b/,
        html: /\bhtml/,
        json: /\bjson\b/
      },
      responseFields: {
        xml: "responseXML",
        text: "responseText",
        json: "responseJSON"
      },
      converters: {
        "* text": String,
        "text html": !0,
        "text json": JSON.parse,
        "text xml": _.parseXML
      },
      flatOptions: {
        url: !0,
        context: !0
      }
    },
    ajaxSetup: function(e, t) {
      return t ? Nt(Nt(e, _.ajaxSettings), t) : Nt(_.ajaxSettings, e)
    },
    ajaxPrefilter: Lt(Ot),
    ajaxTransport: Lt(Mt),
    ajax: function(e, t) {
      "object" == typeof e && (t = e, e = void 0), t = t || {};
      var u, d, f, n, h, i, p, g, o, r, m = _.ajaxSetup({}, t),
        v = m.context || m,
        y = m.context && (v.nodeType || v.jquery) ? _(v) : _.event,
        b = _.Deferred(),
        x = _.Callbacks("once memory"),
        w = m.statusCode || {},
        s = {},
        a = {},
        l = "canceled",
        S = {
          readyState: 0,
          getResponseHeader: function(e) {
            var t;
            if (p) {
              if (!n)
                for (n = {}; t = Pt.exec(f);) n[t[1].toLowerCase()] = t[2];
              t = n[e.toLowerCase()]
            }
            return null == t ? null : t
          },
          getAllResponseHeaders: function() {
            return p ? f : null
          },
          setRequestHeader: function(e, t) {
            return null == p && (e = a[e.toLowerCase()] = a[e.toLowerCase()] || e, s[e] = t), this
          },
          overrideMimeType: function(e) {
            return null == p && (m.mimeType = e), this
          },
          statusCode: function(e) {
            var t;
            if (e)
              if (p) S.always(e[S.status]);
              else
                for (t in e) w[t] = [w[t], e[t]];
            return this
          },
          abort: function(e) {
            var t = e || l;
            return u && u.abort(t), c(0, t), this
          }
        };
      if (b.promise(S), m.url = ((e || m.url || bt.href) + "").replace(Dt, bt.protocol + "//"), m.type = t.method || t.type || m.method || m.type, m.dataTypes = (m.dataType || "*").toLowerCase().match(I) || [""], null == m.crossDomain) {
        i = C.createElement("a");
        try {
          i.href = m.url, i.href = i.href, m.crossDomain = Ht.protocol + "//" + Ht.host != i.protocol + "//" + i.host
        } catch (e) {
          m.crossDomain = !0
        }
      }
      if (m.data && m.processData && "string" != typeof m.data && (m.data = _.param(m.data, m.traditional)), Rt(Ot, m, t, S), p) return S;
      for (o in (g = _.event && m.global) && 0 == _.active++ && _.event.trigger("ajaxStart"), m.type = m.type.toUpperCase(), m.hasContent = !Et.test(m.type), d = m.url.replace(At, ""), m.hasContent ? m.data && m.processData && 0 === (m.contentType || "").indexOf("application/x-www-form-urlencoded") && (m.data = m.data.replace($t, "+")) : (r = m.url.slice(d.length), m.data && (m.processData || "string" == typeof m.data) && (d += (wt.test(d) ? "&" : "?") + m.data, delete m.data), !1 === m.cache && (d = d.replace(jt, "$1"), r = (wt.test(d) ? "&" : "?") + "_=" + xt++ + r), m.url = d + r), m.ifModified && (_.lastModified[d] && S.setRequestHeader("If-Modified-Since", _.lastModified[d]), _.etag[d] && S.setRequestHeader("If-None-Match", _.etag[d])), (m.data && m.hasContent && !1 !== m.contentType || t.contentType) && S.setRequestHeader("Content-Type", m.contentType), S.setRequestHeader("Accept", m.dataTypes[0] && m.accepts[m.dataTypes[0]] ? m.accepts[m.dataTypes[0]] + ("*" !== m.dataTypes[0] ? ", " + It + "; q=0.01" : "") : m.accepts["*"]), m.headers) S.setRequestHeader(o, m.headers[o]);
      if (m.beforeSend && (!1 === m.beforeSend.call(v, S, m) || p)) return S.abort();
      if (l = "abort", x.add(m.complete), S.done(m.success), S.fail(m.error), u = Rt(Mt, m, t, S)) {
        if (S.readyState = 1, g && y.trigger("ajaxSend", [S, m]), p) return S;
        m.async && 0 < m.timeout && (h = k.setTimeout(function() {
          S.abort("timeout")
        }, m.timeout));
        try {
          p = !1, u.send(s, c)
        } catch (e) {
          if (p) throw e;
          c(-1, e)
        }
      } else c(-1, "No Transport");

      function c(e, t, n, i) {
        var o, r, s, a, l, c = t;
        p || (p = !0, h && k.clearTimeout(h), u = void 0, f = i || "", S.readyState = 0 < e ? 4 : 0, o = 200 <= e && e < 300 || 304 === e, n && (a = function(e, t, n) {
          for (var i, o, r, s, a = e.contents, l = e.dataTypes;
            "*" === l[0];) l.shift(), void 0 === i && (i = e.mimeType || t.getResponseHeader("Content-Type"));
          if (i)
            for (o in a)
              if (a[o] && a[o].test(i)) {
                l.unshift(o);
                break
              }
          if (l[0] in n) r = l[0];
          else {
            for (o in n) {
              if (!l[0] || e.converters[o + " " + l[0]]) {
                r = o;
                break
              }
              s || (s = o)
            }
            r = r || s
          }
          if (r) return r !== l[0] && l.unshift(r), n[r]
        }(m, S, n)), a = function(e, t, n, i) {
          var o, r, s, a, l, c = {},
            u = e.dataTypes.slice();
          if (u[1])
            for (s in e.converters) c[s.toLowerCase()] = e.converters[s];
          for (r = u.shift(); r;)
            if (e.responseFields[r] && (n[e.responseFields[r]] = t), !l && i && e.dataFilter && (t = e.dataFilter(t, e.dataType)), l = r, r = u.shift())
              if ("*" === r) r = l;
              else if ("*" !== l && l !== r) {
            if (!(s = c[l + " " + r] || c["* " + r]))
              for (o in c)
                if ((a = o.split(" "))[1] === r && (s = c[l + " " + a[0]] || c["* " + a[0]])) {
                  !0 === s ? s = c[o] : !0 !== c[o] && (r = a[0], u.unshift(a[1]));
                  break
                }
            if (!0 !== s)
              if (s && e.throws) t = s(t);
              else try {
                t = s(t)
              } catch (e) {
                return {
                  state: "parsererror",
                  error: s ? e : "No conversion from " + l + " to " + r
                }
              }
          }
          return {
            state: "success",
            data: t
          }
        }(m, a, S, o), o ? (m.ifModified && ((l = S.getResponseHeader("Last-Modified")) && (_.lastModified[d] = l), (l = S.getResponseHeader("etag")) && (_.etag[d] = l)), 204 === e || "HEAD" === m.type ? c = "nocontent" : 304 === e ? c = "notmodified" : (c = a.state, r = a.data, o = !(s = a.error))) : (s = c, !e && c || (c = "error", e < 0 && (e = 0))), S.status = e, S.statusText = (t || c) + "", o ? b.resolveWith(v, [r, c, S]) : b.rejectWith(v, [S, c, s]), S.statusCode(w), w = void 0, g && y.trigger(o ? "ajaxSuccess" : "ajaxError", [S, m, o ? r : s]), x.fireWith(v, [S, c]), g && (y.trigger("ajaxComplete", [S, m]), --_.active || _.event.trigger("ajaxStop")))
      }
      return S
    },
    getJSON: function(e, t, n) {
      return _.get(e, t, n, "json")
    },
    getScript: function(e, t) {
      return _.get(e, void 0, t, "script")
    }
  }), _.each(["get", "post"], function(e, o) {
    _[o] = function(e, t, n, i) {
      return y(t) && (i = i || n, n = t, t = void 0), _.ajax(_.extend({
        url: e,
        type: o,
        dataType: i,
        data: t,
        success: n
      }, _.isPlainObject(e) && e))
    }
  }), _._evalUrl = function(e) {
    return _.ajax({
      url: e,
      type: "GET",
      dataType: "script",
      cache: !0,
      async: !1,
      global: !1,
      throws: !0
    })
  }, _.fn.extend({
    wrapAll: function(e) {
      var t;
      return this[0] && (y(e) && (e = e.call(this[0])), t = _(e, this[0].ownerDocument).eq(0).clone(!0), this[0].parentNode && t.insertBefore(this[0]), t.map(function() {
        for (var e = this; e.firstElementChild;) e = e.firstElementChild;
        return e
      }).append(this)), this
    },
    wrapInner: function(n) {
      return y(n) ? this.each(function(e) {
        _(this).wrapInner(n.call(this, e))
      }) : this.each(function() {
        var e = _(this),
          t = e.contents();
        t.length ? t.wrapAll(n) : e.append(n)
      })
    },
    wrap: function(t) {
      var n = y(t);
      return this.each(function(e) {
        _(this).wrapAll(n ? t.call(this, e) : t)
      })
    },
    unwrap: function(e) {
      return this.parent(e).not("body").each(function() {
        _(this).replaceWith(this.childNodes)
      }), this
    }
  }), _.expr.pseudos.hidden = function(e) {
    return !_.expr.pseudos.visible(e)
  }, _.expr.pseudos.visible = function(e) {
    return !!(e.offsetWidth || e.offsetHeight || e.getClientRects().length)
  }, _.ajaxSettings.xhr = function() {
    try {
      return new k.XMLHttpRequest
    } catch (e) {}
  };
  var Ft = {
      0: 200,
      1223: 204
    },
    Bt = _.ajaxSettings.xhr();
  v.cors = !!Bt && "withCredentials" in Bt, v.ajax = Bt = !!Bt, _.ajaxTransport(function(o) {
    var r, s;
    if (v.cors || Bt && !o.crossDomain) return {
      send: function(e, t) {
        var n, i = o.xhr();
        if (i.open(o.type, o.url, o.async, o.username, o.password), o.xhrFields)
          for (n in o.xhrFields) i[n] = o.xhrFields[n];
        for (n in o.mimeType && i.overrideMimeType && i.overrideMimeType(o.mimeType), o.crossDomain || e["X-Requested-With"] || (e["X-Requested-With"] = "XMLHttpRequest"), e) i.setRequestHeader(n, e[n]);
        r = function(e) {
          return function() {
            r && (r = s = i.onload = i.onerror = i.onabort = i.ontimeout = i.onreadystatechange = null, "abort" === e ? i.abort() : "error" === e ? "number" != typeof i.status ? t(0, "error") : t(i.status, i.statusText) : t(Ft[i.status] || i.status, i.statusText, "text" !== (i.responseType || "text") || "string" != typeof i.responseText ? {
              binary: i.response
            } : {
              text: i.responseText
            }, i.getAllResponseHeaders()))
          }
        }, i.onload = r(), s = i.onerror = i.ontimeout = r("error"), void 0 !== i.onabort ? i.onabort = s : i.onreadystatechange = function() {
          4 === i.readyState && k.setTimeout(function() {
            r && s()
          })
        }, r = r("abort");
        try {
          i.send(o.hasContent && o.data || null)
        } catch (e) {
          if (r) throw e
        }
      },
      abort: function() {
        r && r()
      }
    }
  }), _.ajaxPrefilter(function(e) {
    e.crossDomain && (e.contents.script = !1)
  }), _.ajaxSetup({
    accepts: {
      script: "text/javascript, application/javascript, application/ecmascript, application/x-ecmascript"
    },
    contents: {
      script: /\b(?:java|ecma)script\b/
    },
    converters: {
      "text script": function(e) {
        return _.globalEval(e), e
      }
    }
  }), _.ajaxPrefilter("script", function(e) {
    void 0 === e.cache && (e.cache = !1), e.crossDomain && (e.type = "GET")
  }), _.ajaxTransport("script", function(n) {
    var i, o;
    if (n.crossDomain) return {
      send: function(e, t) {
        i = _("<script>").prop({
          charset: n.scriptCharset,
          src: n.url
        }).on("load error", o = function(e) {
          i.remove(), o = null, e && t("error" === e.type ? 404 : 200, e.type)
        }), C.head.appendChild(i[0])
      },
      abort: function() {
        o && o()
      }
    }
  });
  var zt, qt = [],
    Wt = /(=)\?(?=&|$)|\?\?/;
  _.ajaxSetup({
    jsonp: "callback",
    jsonpCallback: function() {
      var e = qt.pop() || _.expando + "_" + xt++;
      return this[e] = !0, e
    }
  }), _.ajaxPrefilter("json jsonp", function(e, t, n) {
    var i, o, r, s = !1 !== e.jsonp && (Wt.test(e.url) ? "url" : "string" == typeof e.data && 0 === (e.contentType || "").indexOf("application/x-www-form-urlencoded") && Wt.test(e.data) && "data");
    if (s || "jsonp" === e.dataTypes[0]) return i = e.jsonpCallback = y(e.jsonpCallback) ? e.jsonpCallback() : e.jsonpCallback, s ? e[s] = e[s].replace(Wt, "$1" + i) : !1 !== e.jsonp && (e.url += (wt.test(e.url) ? "&" : "?") + e.jsonp + "=" + i), e.converters["script json"] = function() {
      return r || _.error(i + " was not called"), r[0]
    }, e.dataTypes[0] = "json", o = k[i], k[i] = function() {
      r = arguments
    }, n.always(function() {
      void 0 === o ? _(k).removeProp(i) : k[i] = o, e[i] && (e.jsonpCallback = t.jsonpCallback, qt.push(i)), r && y(o) && o(r[0]), r = o = void 0
    }), "script"
  }), v.createHTMLDocument = ((zt = C.implementation.createHTMLDocument("").body).innerHTML = "<form></form><form></form>", 2 === zt.childNodes.length), _.parseHTML = function(e, t, n) {
    return "string" != typeof e ? [] : ("boolean" == typeof t && (n = t, t = !1), t || (v.createHTMLDocument ? ((i = (t = C.implementation.createHTMLDocument("")).createElement("base")).href = C.location.href, t.head.appendChild(i)) : t = C), r = !n && [], (o = A.exec(e)) ? [t.createElement(o[1])] : (o = ve([e], t, r), r && r.length && _(r).remove(), _.merge([], o.childNodes)));
    var i, o, r
  }, _.fn.load = function(e, t, n) {
    var i, o, r, s = this,
      a = e.indexOf(" ");
    return -1 < a && (i = ht(e.slice(a)), e = e.slice(0, a)), y(t) ? (n = t, t = void 0) : t && "object" == typeof t && (o = "POST"), 0 < s.length && _.ajax({
      url: e,
      type: o || "GET",
      dataType: "html",
      data: t
    }).done(function(e) {
      r = arguments, s.html(i ? _("<div>").append(_.parseHTML(e)).find(i) : e)
    }).always(n && function(e, t) {
      s.each(function() {
        n.apply(this, r || [e.responseText, t, e])
      })
    }), this
  }, _.each(["ajaxStart", "ajaxStop", "ajaxComplete", "ajaxError", "ajaxSuccess", "ajaxSend"], function(e, t) {
    _.fn[t] = function(e) {
      return this.on(t, e)
    }
  }), _.expr.pseudos.animated = function(t) {
    return _.grep(_.timers, function(e) {
      return t === e.elem
    }).length
  }, _.offset = {
    setOffset: function(e, t, n) {
      var i, o, r, s, a, l, c = _.css(e, "position"),
        u = _(e),
        d = {};
      "static" === c && (e.style.position = "relative"), a = u.offset(), r = _.css(e, "top"), l = _.css(e, "left"), ("absolute" === c || "fixed" === c) && -1 < (r + l).indexOf("auto") ? (s = (i = u.position()).top, o = i.left) : (s = parseFloat(r) || 0, o = parseFloat(l) || 0), y(t) && (t = t.call(e, n, _.extend({}, a))), null != t.top && (d.top = t.top - a.top + s), null != t.left && (d.left = t.left - a.left + o), "using" in t ? t.using.call(e, d) : u.css(d)
    }
  }, _.fn.extend({
    offset: function(t) {
      if (arguments.length) return void 0 === t ? this : this.each(function(e) {
        _.offset.setOffset(this, t, e)
      });
      var e, n, i = this[0];
      return i ? i.getClientRects().length ? (e = i.getBoundingClientRect(), n = i.ownerDocument.defaultView, {
        top: e.top + n.pageYOffset,
        left: e.left + n.pageXOffset
      }) : {
        top: 0,
        left: 0
      } : void 0
    },
    position: function() {
      if (this[0]) {
        var e, t, n, i = this[0],
          o = {
            top: 0,
            left: 0
          };
        if ("fixed" === _.css(i, "position")) t = i.getBoundingClientRect();
        else {
          for (t = this.offset(), n = i.ownerDocument, e = i.offsetParent || n.documentElement; e && (e === n.body || e === n.documentElement) && "static" === _.css(e, "position");) e = e.parentNode;
          e && e !== i && 1 === e.nodeType && ((o = _(e).offset()).top += _.css(e, "borderTopWidth", !0), o.left += _.css(e, "borderLeftWidth", !0))
        }
        return {
          top: t.top - o.top - _.css(i, "marginTop", !0),
          left: t.left - o.left - _.css(i, "marginLeft", !0)
        }
      }
    },
    offsetParent: function() {
      return this.map(function() {
        for (var e = this.offsetParent; e && "static" === _.css(e, "position");) e = e.offsetParent;
        return e || ye
      })
    }
  }), _.each({
    scrollLeft: "pageXOffset",
    scrollTop: "pageYOffset"
  }, function(t, o) {
    var r = "pageYOffset" === o;
    _.fn[t] = function(e) {
      return z(this, function(e, t, n) {
        var i;
        if (b(e) ? i = e : 9 === e.nodeType && (i = e.defaultView), void 0 === n) return i ? i[o] : e[t];
        i ? i.scrollTo(r ? i.pageXOffset : n, r ? n : i.pageYOffset) : e[t] = n
      }, t, e, arguments.length)
    }
  }), _.each(["top", "left"], function(e, n) {
    _.cssHooks[n] = Fe(v.pixelPosition, function(e, t) {
      if (t) return t = Ne(e, n), He.test(t) ? _(e).position()[n] + "px" : t
    })
  }), _.each({
    Height: "height",
    Width: "width"
  }, function(s, a) {
    _.each({
      padding: "inner" + s,
      content: a,
      "": "outer" + s
    }, function(i, r) {
      _.fn[r] = function(e, t) {
        var n = arguments.length && (i || "boolean" != typeof e),
          o = i || (!0 === e || !0 === t ? "margin" : "border");
        return z(this, function(e, t, n) {
          var i;
          return b(e) ? 0 === r.indexOf("outer") ? e["inner" + s] : e.document.documentElement["client" + s] : 9 === e.nodeType ? (i = e.documentElement, Math.max(e.body["scroll" + s], i["scroll" + s], e.body["offset" + s], i["offset" + s], i["client" + s])) : void 0 === n ? _.css(e, t, o) : _.style(e, t, n, o)
        }, a, n ? e : void 0, n)
      }
    })
  }), _.each("blur focus focusin focusout resize scroll click dblclick mousedown mouseup mousemove mouseover mouseout mouseenter mouseleave change select submit keydown keypress keyup contextmenu".split(" "), function(e, n) {
    _.fn[n] = function(e, t) {
      return 0 < arguments.length ? this.on(n, null, e, t) : this.trigger(n)
    }
  }), _.fn.extend({
    hover: function(e, t) {
      return this.mouseenter(e).mouseleave(t || e)
    }
  }), _.fn.extend({
    bind: function(e, t, n) {
      return this.on(e, null, t, n)
    },
    unbind: function(e, t) {
      return this.off(e, null, t)
    },
    delegate: function(e, t, n, i) {
      return this.on(t, e, n, i)
    },
    undelegate: function(e, t, n) {
      return 1 === arguments.length ? this.off(e, "**") : this.off(t, e || "**", n)
    }
  }), _.proxy = function(e, t) {
    var n, i, o;
    if ("string" == typeof t && (n = e[t], t = e, e = n), y(e)) return i = a.call(arguments, 2), (o = function() {
      return e.apply(t || this, i.concat(a.call(arguments)))
    }).guid = e.guid = e.guid || _.guid++, o
  }, _.holdReady = function(e) {
    e ? _.readyWait++ : _.ready(!0)
  }, _.isArray = Array.isArray, _.parseJSON = JSON.parse, _.nodeName = $, _.isFunction = y, _.isWindow = b, _.camelCase = V, _.type = w, _.now = Date.now, _.isNumeric = function(e) {
    var t = _.type(e);
    return ("number" === t || "string" === t) && !isNaN(e - parseFloat(e))
  }, "function" == typeof define && define.amd && define("jquery", [], function() {
    return _
  });
  var Yt = k.jQuery,
    Vt = k.$;
  return _.noConflict = function(e) {
    return k.$ === _ && (k.$ = Vt), e && k.jQuery === _ && (k.jQuery = Yt), _
  }, e || (k.jQuery = k.$ = _), _
}),
function(l, n, o, a) {
  function c(e, t) {
    this.settings = null, this.options = l.extend({}, c.Defaults, t), this.$element = l(e), this._handlers = {}, this._plugins = {}, this._supress = {}, this._current = null, this._speed = null, this._coordinates = [], this._breakpoint = null, this._width = null, this._items = [], this._clones = [], this._mergers = [], this._widths = [], this._invalidated = {}, this._pipe = [], this._drag = {
      time: null,
      target: null,
      pointer: null,
      stage: {
        start: null,
        current: null
      },
      direction: null
    }, this._states = {
      current: {},
      tags: {
        initializing: ["busy"],
        animating: ["busy"],
        dragging: ["interacting"]
      }
    }, l.each(["onResize", "onThrottledResize"], l.proxy(function(e, t) {
      this._handlers[t] = l.proxy(this[t], this)
    }, this)), l.each(c.Plugins, l.proxy(function(e, t) {
      this._plugins[e.charAt(0).toLowerCase() + e.slice(1)] = new t(this)
    }, this)), l.each(c.Workers, l.proxy(function(e, t) {
      this._pipe.push({
        filter: t.filter,
        run: l.proxy(t.run, this)
      })
    }, this)), this.setup(), this.initialize()
  }
  c.Defaults = {
    items: 3,
    loop: !1,
    center: !1,
    rewind: !1,
    checkVisibility: !0,
    mouseDrag: !0,
    touchDrag: !0,
    pullDrag: !0,
    freeDrag: !1,
    margin: 0,
    stagePadding: 0,
    merge: !1,
    mergeFit: !0,
    autoWidth: !1,
    startPosition: 0,
    rtl: !1,
    smartSpeed: 250,
    fluidSpeed: !1,
    dragEndSpeed: !1,
    responsive: {},
    responsiveRefreshRate: 200,
    responsiveBaseElement: n,
    fallbackEasing: "swing",
    info: !1,
    nestedItemSelector: !1,
    itemElement: "div",
    stageElement: "div",
    refreshClass: "owl-refresh",
    loadedClass: "owl-loaded",
    loadingClass: "owl-loading",
    rtlClass: "owl-rtl",
    responsiveClass: "owl-responsive",
    dragClass: "owl-drag",
    itemClass: "owl-item",
    stageClass: "owl-stage",
    stageOuterClass: "owl-stage-outer",
    grabClass: "owl-grab"
  }, c.Width = {
    Default: "default",
    Inner: "inner",
    Outer: "outer"
  }, c.Type = {
    Event: "event",
    State: "state"
  }, c.Plugins = {}, c.Workers = [{
    filter: ["width", "settings"],
    run: function() {
      this._width = this.$element.width()
    }
  }, {
    filter: ["width", "items", "settings"],
    run: function(e) {
      e.current = this._items && this._items[this.relative(this._current)]
    }
  }, {
    filter: ["items", "settings"],
    run: function() {
      this.$stage.children(".cloned").remove()
    }
  }, {
    filter: ["width", "items", "settings"],
    run: function(e) {
      var t = this.settings.margin || "",
        n = !this.settings.autoWidth,
        i = this.settings.rtl,
        o = {
          width: "auto",
          "margin-left": i ? t : "",
          "margin-right": i ? "" : t
        };
      !n && this.$stage.children().css(o), e.css = o
    }
  }, {
    filter: ["width", "items", "settings"],
    run: function(e) {
      var t = (this.width() / this.settings.items).toFixed(3) - this.settings.margin,
        n = null,
        i = this._items.length,
        o = !this.settings.autoWidth,
        r = [];
      for (e.items = {
          merge: !1,
          width: t
        }; i--;) n = this._mergers[i], n = this.settings.mergeFit && Math.min(n, this.settings.items) || n, e.items.merge = 1 < n || e.items.merge, r[i] = o ? t * n : this._items[i].width();
      this._widths = r
    }
  }, {
    filter: ["items", "settings"],
    run: function() {
      var e = [],
        t = this._items,
        n = this.settings,
        i = Math.max(2 * n.items, 4),
        o = 2 * Math.ceil(t.length / 2),
        r = n.loop && t.length ? n.rewind ? i : Math.max(i, o) : 0,
        s = "",
        a = "";
      for (r /= 2; 0 < r;) e.push(this.normalize(e.length / 2, !0)), s += t[e[e.length - 1]][0].outerHTML, e.push(this.normalize(t.length - 1 - (e.length - 1) / 2, !0)), a = t[e[e.length - 1]][0].outerHTML + a, r -= 1;
      this._clones = e, l(s).addClass("cloned").appendTo(this.$stage), l(a).addClass("cloned").prependTo(this.$stage)
    }
  }, {
    filter: ["width", "items", "settings"],
    run: function() {
      for (var e = this.settings.rtl ? 1 : -1, t = this._clones.length + this._items.length, n = -1, i = 0, o = 0, r = []; ++n < t;) i = r[n - 1] || 0, o = this._widths[this.relative(n)] + this.settings.margin, r.push(i + o * e);
      this._coordinates = r
    }
  }, {
    filter: ["width", "items", "settings"],
    run: function() {
      var e = this.settings.stagePadding,
        t = this._coordinates,
        n = {
          width: Math.ceil(Math.abs(t[t.length - 1])) + 2 * e,
          "padding-left": e || "",
          "padding-right": e || ""
        };
      this.$stage.css(n)
    }
  }, {
    filter: ["width", "items", "settings"],
    run: function(e) {
      var t = this._coordinates.length,
        n = !this.settings.autoWidth,
        i = this.$stage.children();
      if (n && e.items.merge)
        for (; t--;) e.css.width = this._widths[this.relative(t)], i.eq(t).css(e.css);
      else n && (e.css.width = e.items.width, i.css(e.css))
    }
  }, {
    filter: ["items"],
    run: function() {
      this._coordinates.length < 1 && this.$stage.removeAttr("style")
    }
  }, {
    filter: ["width", "items", "settings"],
    run: function(e) {
      e.current = e.current ? this.$stage.children().index(e.current) : 0, e.current = Math.max(this.minimum(), Math.min(this.maximum(), e.current)), this.reset(e.current)
    }
  }, {
    filter: ["position"],
    run: function() {
      this.animate(this.coordinates(this._current))
    }
  }, {
    filter: ["width", "position", "items", "settings"],
    run: function() {
      var e, t, n, i, o = this.settings.rtl ? 1 : -1,
        r = 2 * this.settings.stagePadding,
        s = this.coordinates(this.current()) + r,
        a = s + this.width() * o,
        l = [];
      for (n = 0, i = this._coordinates.length; n < i; n++) e = this._coordinates[n - 1] || 0, t = Math.abs(this._coordinates[n]) + r * o, (this.op(e, "<=", s) && this.op(e, ">", a) || this.op(t, "<", s) && this.op(t, ">", a)) && l.push(n);
      this.$stage.children(".active").removeClass("active"), this.$stage.children(":eq(" + l.join("), :eq(") + ")").addClass("active"), this.$stage.children(".center").removeClass("center"), this.settings.center && this.$stage.children().eq(this.current()).addClass("center")
    }
  }], c.prototype.initializeStage = function() {
    this.$stage = this.$element.find("." + this.settings.stageClass), this.$stage.length || (this.$element.addClass(this.options.loadingClass), this.$stage = l("<" + this.settings.stageElement + ' class="' + this.settings.stageClass + '"/>').wrap('<div class="' + this.settings.stageOuterClass + '"/>'), this.$element.append(this.$stage.parent()))
  }, c.prototype.initializeItems = function() {
    var e = this.$element.find(".owl-item");
    if (e.length) return this._items = e.get().map(function(e) {
      return l(e)
    }), this._mergers = this._items.map(function() {
      return 1
    }), void this.refresh();
    this.replace(this.$element.children().not(this.$stage.parent())), this.isVisible() ? this.refresh() : this.invalidate("width"), this.$element.removeClass(this.options.loadingClass).addClass(this.options.loadedClass)
  }, c.prototype.initialize = function() {
    var e, t, n;
    (this.enter("initializing"), this.trigger("initialize"), this.$element.toggleClass(this.settings.rtlClass, this.settings.rtl), this.settings.autoWidth && !this.is("pre-loading")) && (e = this.$element.find("img"), t = this.settings.nestedItemSelector ? "." + this.settings.nestedItemSelector : a, n = this.$element.children(t).width(), e.length && n <= 0 && this.preloadAutoWidthImages(e));
    this.initializeStage(), this.initializeItems(), this.registerEventHandlers(), this.leave("initializing"), this.trigger("initialized")
  }, c.prototype.isVisible = function() {
    return !this.settings.checkVisibility || this.$element.is(":visible")
  }, c.prototype.setup = function() {
    var t = this.viewport(),
      e = this.options.responsive,
      n = -1,
      i = null;
    e ? (l.each(e, function(e) {
      e <= t && n < e && (n = Number(e))
    }), "function" == typeof(i = l.extend({}, this.options, e[n])).stagePadding && (i.stagePadding = i.stagePadding()), delete i.responsive, i.responsiveClass && this.$element.attr("class", this.$element.attr("class").replace(new RegExp("(" + this.options.responsiveClass + "-)\\S+\\s", "g"), "$1" + n))) : i = l.extend({}, this.options), this.trigger("change", {
      property: {
        name: "settings",
        value: i
      }
    }), this._breakpoint = n, this.settings = i, this.invalidate("settings"), this.trigger("changed", {
      property: {
        name: "settings",
        value: this.settings
      }
    })
  }, c.prototype.optionsLogic = function() {
    this.settings.autoWidth && (this.settings.stagePadding = !1, this.settings.merge = !1)
  }, c.prototype.prepare = function(e) {
    var t = this.trigger("prepare", {
      content: e
    });
    return t.data || (t.data = l("<" + this.settings.itemElement + "/>").addClass(this.options.itemClass).append(e)), this.trigger("prepared", {
      content: t.data
    }), t.data
  }, c.prototype.update = function() {
    for (var e = 0, t = this._pipe.length, n = l.proxy(function(e) {
        return this[e]
      }, this._invalidated), i = {}; e < t;)(this._invalidated.all || 0 < l.grep(this._pipe[e].filter, n).length) && this._pipe[e].run(i), e++;
    this._invalidated = {}, !this.is("valid") && this.enter("valid")
  }, c.prototype.width = function(e) {
    switch (e = e || c.Width.Default) {
      case c.Width.Inner:
      case c.Width.Outer:
        return this._width;
      default:
        return this._width - 2 * this.settings.stagePadding + this.settings.margin
    }
  }, c.prototype.refresh = function() {
    this.enter("refreshing"), this.trigger("refresh"), this.setup(), this.optionsLogic(), this.$element.addClass(this.options.refreshClass), this.update(), this.$element.removeClass(this.options.refreshClass), this.leave("refreshing"), this.trigger("refreshed")
  }, c.prototype.onThrottledResize = function() {
    n.clearTimeout(this.resizeTimer), this.resizeTimer = n.setTimeout(this._handlers.onResize, this.settings.responsiveRefreshRate)
  }, c.prototype.onResize = function() {
    return !!this._items.length && this._width !== this.$element.width() && !!this.isVisible() && (this.enter("resizing"), this.trigger("resize").isDefaultPrevented() ? (this.leave("resizing"), !1) : (this.invalidate("width"), this.refresh(), this.leave("resizing"), void this.trigger("resized")))
  }, c.prototype.registerEventHandlers = function() {
    l.support.transition && this.$stage.on(l.support.transition.end + ".owl.core", l.proxy(this.onTransitionEnd, this)), !1 !== this.settings.responsive && this.on(n, "resize", this._handlers.onThrottledResize), this.settings.mouseDrag && (this.$element.addClass(this.options.dragClass), this.$stage.on("mousedown.owl.core", l.proxy(this.onDragStart, this)), this.$stage.on("dragstart.owl.core selectstart.owl.core", function() {
      return !1
    })), this.settings.touchDrag && (this.$stage.on("touchstart.owl.core", l.proxy(this.onDragStart, this)), this.$stage.on("touchcancel.owl.core", l.proxy(this.onDragEnd, this)))
  }, c.prototype.onDragStart = function(e) {
    var t = null;
    3 !== e.which && (l.support.transform ? t = {
      x: (t = this.$stage.css("transform").replace(/.*\(|\)| /g, "").split(","))[16 === t.length ? 12 : 4],
      y: t[16 === t.length ? 13 : 5]
    } : (t = this.$stage.position(), t = {
      x: this.settings.rtl ? t.left + this.$stage.width() - this.width() + this.settings.margin : t.left,
      y: t.top
    }), this.is("animating") && (l.support.transform ? this.animate(t.x) : this.$stage.stop(), this.invalidate("position")), this.$element.toggleClass(this.options.grabClass, "mousedown" === e.type), this.speed(0), this._drag.time = (new Date).getTime(), this._drag.target = l(e.target), this._drag.stage.start = t, this._drag.stage.current = t, this._drag.pointer = this.pointer(e), l(o).on("mouseup.owl.core touchend.owl.core", l.proxy(this.onDragEnd, this)), l(o).one("mousemove.owl.core touchmove.owl.core", l.proxy(function(e) {
      var t = this.difference(this._drag.pointer, this.pointer(e));
      l(o).on("mousemove.owl.core touchmove.owl.core", l.proxy(this.onDragMove, this)), Math.abs(t.x) < Math.abs(t.y) && this.is("valid") || (e.preventDefault(), this.enter("dragging"), this.trigger("drag"))
    }, this)))
  }, c.prototype.onDragMove = function(e) {
    var t = null,
      n = null,
      i = null,
      o = this.difference(this._drag.pointer, this.pointer(e)),
      r = this.difference(this._drag.stage.start, o);
    this.is("dragging") && (e.preventDefault(), this.settings.loop ? (t = this.coordinates(this.minimum()), n = this.coordinates(this.maximum() + 1) - t, r.x = ((r.x - t) % n + n) % n + t) : (t = this.settings.rtl ? this.coordinates(this.maximum()) : this.coordinates(this.minimum()), n = this.settings.rtl ? this.coordinates(this.minimum()) : this.coordinates(this.maximum()), i = this.settings.pullDrag ? -1 * o.x / 5 : 0, r.x = Math.max(Math.min(r.x, t + i), n + i)), this._drag.stage.current = r, this.animate(r.x))
  }, c.prototype.onDragEnd = function(e) {
    var t = this.difference(this._drag.pointer, this.pointer(e)),
      n = this._drag.stage.current,
      i = 0 < t.x ^ this.settings.rtl ? "left" : "right";
    l(o).off(".owl.core"), this.$element.removeClass(this.options.grabClass), (0 !== t.x && this.is("dragging") || !this.is("valid")) && (this.speed(this.settings.dragEndSpeed || this.settings.smartSpeed), this.current(this.closest(n.x, 0 !== t.x ? i : this._drag.direction)), this.invalidate("position"), this.update(), this._drag.direction = i, (3 < Math.abs(t.x) || 300 < (new Date).getTime() - this._drag.time) && this._drag.target.one("click.owl.core", function() {
      return !1
    })), this.is("dragging") && (this.leave("dragging"), this.trigger("dragged"))
  }, c.prototype.closest = function(n, i) {
    var o = -1,
      r = this.width(),
      s = this.coordinates();
    return this.settings.freeDrag || l.each(s, l.proxy(function(e, t) {
      return "left" === i && t - 30 < n && n < t + 30 ? o = e : "right" === i && t - r - 30 < n && n < t - r + 30 ? o = e + 1 : this.op(n, "<", t) && this.op(n, ">", s[e + 1] !== a ? s[e + 1] : t - r) && (o = "left" === i ? e + 1 : e), -1 === o
    }, this)), this.settings.loop || (this.op(n, ">", s[this.minimum()]) ? o = n = this.minimum() : this.op(n, "<", s[this.maximum()]) && (o = n = this.maximum())), o
  }, c.prototype.animate = function(e) {
    var t = 0 < this.speed();
    this.is("animating") && this.onTransitionEnd(), t && (this.enter("animating"), this.trigger("translate")), l.support.transform3d && l.support.transition ? this.$stage.css({
      transform: "translate3d(" + e + "px,0px,0px)",
      transition: this.speed() / 1e3 + "s"
    }) : t ? this.$stage.animate({
      left: e + "px"
    }, this.speed(), this.settings.fallbackEasing, l.proxy(this.onTransitionEnd, this)) : this.$stage.css({
      left: e + "px"
    })
  }, c.prototype.is = function(e) {
    return this._states.current[e] && 0 < this._states.current[e]
  }, c.prototype.current = function(e) {
    if (e === a) return this._current;
    if (0 === this._items.length) return a;
    if (e = this.normalize(e), this._current !== e) {
      var t = this.trigger("change", {
        property: {
          name: "position",
          value: e
        }
      });
      t.data !== a && (e = this.normalize(t.data)), this._current = e, this.invalidate("position"), this.trigger("changed", {
        property: {
          name: "position",
          value: this._current
        }
      })
    }
    return this._current
  }, c.prototype.invalidate = function(e) {
    return "string" === l.type(e) && (this._invalidated[e] = !0, this.is("valid") && this.leave("valid")), l.map(this._invalidated, function(e, t) {
      return t
    })
  }, c.prototype.reset = function(e) {
    (e = this.normalize(e)) !== a && (this._speed = 0, this._current = e, this.suppress(["translate", "translated"]), this.animate(this.coordinates(e)), this.release(["translate", "translated"]))
  }, c.prototype.normalize = function(e, t) {
    var n = this._items.length,
      i = t ? 0 : this._clones.length;
    return !this.isNumeric(e) || n < 1 ? e = a : (e < 0 || n + i <= e) && (e = ((e - i / 2) % n + n) % n + i / 2), e
  }, c.prototype.relative = function(e) {
    return e -= this._clones.length / 2, this.normalize(e, !0)
  }, c.prototype.maximum = function(e) {
    var t, n, i, o = this.settings,
      r = this._coordinates.length;
    if (o.loop) r = this._clones.length / 2 + this._items.length - 1;
    else if (o.autoWidth || o.merge) {
      if (t = this._items.length)
        for (n = this._items[--t].width(), i = this.$element.width(); t-- && !((n += this._items[t].width() + this.settings.margin) > i););
      r = t + 1
    } else r = o.center ? this._items.length - 1 : this._items.length - o.items;
    return e && (r -= this._clones.length / 2), Math.max(r, 0)
  }, c.prototype.minimum = function(e) {
    return e ? 0 : this._clones.length / 2
  }, c.prototype.items = function(e) {
    return e === a ? this._items.slice() : (e = this.normalize(e, !0), this._items[e])
  }, c.prototype.mergers = function(e) {
    return e === a ? this._mergers.slice() : (e = this.normalize(e, !0), this._mergers[e])
  }, c.prototype.clones = function(n) {
    var t = this._clones.length / 2,
      i = t + this._items.length,
      o = function(e) {
        return e % 2 == 0 ? i + e / 2 : t - (e + 1) / 2
      };
    return n === a ? l.map(this._clones, function(e, t) {
      return o(t)
    }) : l.map(this._clones, function(e, t) {
      return e === n ? o(t) : null
    })
  }, c.prototype.speed = function(e) {
    return e !== a && (this._speed = e), this._speed
  }, c.prototype.coordinates = function(e) {
    var t, n = 1,
      i = e - 1;
    return e === a ? l.map(this._coordinates, l.proxy(function(e, t) {
      return this.coordinates(t)
    }, this)) : (this.settings.center ? (this.settings.rtl && (n = -1, i = e + 1), t = this._coordinates[e], t += (this.width() - t + (this._coordinates[i] || 0)) / 2 * n) : t = this._coordinates[i] || 0, t = Math.ceil(t))
  }, c.prototype.duration = function(e, t, n) {
    return 0 === n ? 0 : Math.min(Math.max(Math.abs(t - e), 1), 6) * Math.abs(n || this.settings.smartSpeed)
  }, c.prototype.to = function(e, t) {
    var n = this.current(),
      i = null,
      o = e - this.relative(n),
      r = (0 < o) - (o < 0),
      s = this._items.length,
      a = this.minimum(),
      l = this.maximum();
    this.settings.loop ? (!this.settings.rewind && Math.abs(o) > s / 2 && (o += -1 * r * s), (i = (((e = n + o) - a) % s + s) % s + a) !== e && i - o <= l && 0 < i - o && (n = i - o, e = i, this.reset(n))) : this.settings.rewind ? e = (e % (l += 1) + l) % l : e = Math.max(a, Math.min(l, e)), this.speed(this.duration(n, e, t)), this.current(e), this.isVisible() && this.update()
  }, c.prototype.next = function(e) {
    e = e || !1, this.to(this.relative(this.current()) + 1, e)
  }, c.prototype.prev = function(e) {
    e = e || !1, this.to(this.relative(this.current()) - 1, e)
  }, c.prototype.onTransitionEnd = function(e) {
    if (e !== a && (e.stopPropagation(), (e.target || e.srcElement || e.originalTarget) !== this.$stage.get(0))) return !1;
    this.leave("animating"), this.trigger("translated")
  }, c.prototype.viewport = function() {
    var e;
    return this.options.responsiveBaseElement !== n ? e = l(this.options.responsiveBaseElement).width() : n.innerWidth ? e = n.innerWidth : o.documentElement && o.documentElement.clientWidth ? e = o.documentElement.clientWidth : console.warn("Can not detect viewport width."), e
  }, c.prototype.replace = function(e) {
    this.$stage.empty(), this._items = [], e && (e = e instanceof jQuery ? e : l(e)), this.settings.nestedItemSelector && (e = e.find("." + this.settings.nestedItemSelector)), e.filter(function() {
      return 1 === this.nodeType
    }).each(l.proxy(function(e, t) {
      t = this.prepare(t), this.$stage.append(t), this._items.push(t), this._mergers.push(1 * t.find("[data-merge]").addBack("[data-merge]").attr("data-merge") || 1)
    }, this)), this.reset(this.isNumeric(this.settings.startPosition) ? this.settings.startPosition : 0), this.invalidate("items")
  }, c.prototype.add = function(e, t) {
    var n = this.relative(this._current);
    t = t === a ? this._items.length : this.normalize(t, !0), e = e instanceof jQuery ? e : l(e), this.trigger("add", {
      content: e,
      position: t
    }), e = this.prepare(e), 0 === this._items.length || t === this._items.length ? (0 === this._items.length && this.$stage.append(e), 0 !== this._items.length && this._items[t - 1].after(e), this._items.push(e), this._mergers.push(1 * e.find("[data-merge]").addBack("[data-merge]").attr("data-merge") || 1)) : (this._items[t].before(e), this._items.splice(t, 0, e), this._mergers.splice(t, 0, 1 * e.find("[data-merge]").addBack("[data-merge]").attr("data-merge") || 1)), this._items[n] && this.reset(this._items[n].index()), this.invalidate("items"), this.trigger("added", {
      content: e,
      position: t
    })
  }, c.prototype.remove = function(e) {
    (e = this.normalize(e, !0)) !== a && (this.trigger("remove", {
      content: this._items[e],
      position: e
    }), this._items[e].remove(), this._items.splice(e, 1), this._mergers.splice(e, 1), this.invalidate("items"), this.trigger("removed", {
      content: null,
      position: e
    }))
  }, c.prototype.preloadAutoWidthImages = function(e) {
    e.each(l.proxy(function(e, t) {
      this.enter("pre-loading"), t = l(t), l(new Image).one("load", l.proxy(function(e) {
        t.attr("src", e.target.src), t.css("opacity", 1), this.leave("pre-loading"), !this.is("pre-loading") && !this.is("initializing") && this.refresh()
      }, this)).attr("src", t.attr("src") || t.attr("data-src") || t.attr("data-src-retina"))
    }, this))
  }, c.prototype.destroy = function() {
    for (var e in this.$element.off(".owl.core"), this.$stage.off(".owl.core"), l(o).off(".owl.core"), !1 !== this.settings.responsive && (n.clearTimeout(this.resizeTimer), this.off(n, "resize", this._handlers.onThrottledResize)), this._plugins) this._plugins[e].destroy();
    this.$stage.children(".cloned").remove(), this.$stage.unwrap(), this.$stage.children().contents().unwrap(), this.$stage.children().unwrap(), this.$stage.remove(), this.$element.removeClass(this.options.refreshClass).removeClass(this.options.loadingClass).removeClass(this.options.loadedClass).removeClass(this.options.rtlClass).removeClass(this.options.dragClass).removeClass(this.options.grabClass).attr("class", this.$element.attr("class").replace(new RegExp(this.options.responsiveClass + "-\\S+\\s", "g"), "")).removeData("owl.carousel")
  }, c.prototype.op = function(e, t, n) {
    var i = this.settings.rtl;
    switch (t) {
      case "<":
        return i ? n < e : e < n;
      case ">":
        return i ? e < n : n < e;
      case ">=":
        return i ? e <= n : n <= e;
      case "<=":
        return i ? n <= e : e <= n
    }
  }, c.prototype.on = function(e, t, n, i) {
    e.addEventListener ? e.addEventListener(t, n, i) : e.attachEvent && e.attachEvent("on" + t, n)
  }, c.prototype.off = function(e, t, n, i) {
    e.removeEventListener ? e.removeEventListener(t, n, i) : e.detachEvent && e.detachEvent("on" + t, n)
  }, c.prototype.trigger = function(e, t, n, i, o) {
    var r = {
        item: {
          count: this._items.length,
          index: this.current()
        }
      },
      s = l.camelCase(l.grep(["on", e, n], function(e) {
        return e
      }).join("-").toLowerCase()),
      a = l.Event([e, "owl", n || "carousel"].join(".").toLowerCase(), l.extend({
        relatedTarget: this
      }, r, t));
    return this._supress[e] || (l.each(this._plugins, function(e, t) {
      t.onTrigger && t.onTrigger(a)
    }), this.register({
      type: c.Type.Event,
      name: e
    }), this.$element.trigger(a), this.settings && "function" == typeof this.settings[s] && this.settings[s].call(this, a)), a
  }, c.prototype.enter = function(e) {
    l.each([e].concat(this._states.tags[e] || []), l.proxy(function(e, t) {
      this._states.current[t] === a && (this._states.current[t] = 0), this._states.current[t]++
    }, this))
  }, c.prototype.leave = function(e) {
    l.each([e].concat(this._states.tags[e] || []), l.proxy(function(e, t) {
      this._states.current[t]--
    }, this))
  }, c.prototype.register = function(n) {
    if (n.type === c.Type.Event) {
      if (l.event.special[n.name] || (l.event.special[n.name] = {}), !l.event.special[n.name].owl) {
        var t = l.event.special[n.name]._default;
        l.event.special[n.name]._default = function(e) {
          return !t || !t.apply || e.namespace && -1 !== e.namespace.indexOf("owl") ? e.namespace && -1 < e.namespace.indexOf("owl") : t.apply(this, arguments)
        }, l.event.special[n.name].owl = !0
      }
    } else n.type === c.Type.State && (this._states.tags[n.name] ? this._states.tags[n.name] = this._states.tags[n.name].concat(n.tags) : this._states.tags[n.name] = n.tags, this._states.tags[n.name] = l.grep(this._states.tags[n.name], l.proxy(function(e, t) {
      return l.inArray(e, this._states.tags[n.name]) === t
    }, this)))
  }, c.prototype.suppress = function(e) {
    l.each(e, l.proxy(function(e, t) {
      this._supress[t] = !0
    }, this))
  }, c.prototype.release = function(e) {
    l.each(e, l.proxy(function(e, t) {
      delete this._supress[t]
    }, this))
  }, c.prototype.pointer = function(e) {
    var t = {
      x: null,
      y: null
    };
    return (e = (e = e.originalEvent || e || n.event).touches && e.touches.length ? e.touches[0] : e.changedTouches && e.changedTouches.length ? e.changedTouches[0] : e).pageX ? (t.x = e.pageX, t.y = e.pageY) : (t.x = e.clientX, t.y = e.clientY), t
  }, c.prototype.isNumeric = function(e) {
    return !isNaN(parseFloat(e))
  }, c.prototype.difference = function(e, t) {
    return {
      x: e.x - t.x,
      y: e.y - t.y
    }
  }, l.fn.owlCarousel = function(t) {
    var i = Array.prototype.slice.call(arguments, 1);
    return this.each(function() {
      var e = l(this),
        n = e.data("owl.carousel");
      n || (n = new c(this, "object" == typeof t && t), e.data("owl.carousel", n), l.each(["next", "prev", "to", "destroy", "refresh", "replace", "add", "remove"], function(e, t) {
        n.register({
          type: c.Type.Event,
          name: t
        }), n.$element.on(t + ".owl.carousel.core", l.proxy(function(e) {
          e.namespace && e.relatedTarget !== this && (this.suppress([t]), n[t].apply(this, [].slice.call(arguments, 1)), this.release([t]))
        }, n))
      })), "string" == typeof t && "_" !== t.charAt(0) && n[t].apply(n, i)
    })
  }, l.fn.owlCarousel.Constructor = c
}(window.Zepto || window.jQuery, window, document),
function(t, n, e, i) {
  var o = function(e) {
    this._core = e, this._interval = null, this._visible = null, this._handlers = {
      "initialized.owl.carousel": t.proxy(function(e) {
        e.namespace && this._core.settings.autoRefresh && this.watch()
      }, this)
    }, this._core.options = t.extend({}, o.Defaults, this._core.options), this._core.$element.on(this._handlers)
  };
  o.Defaults = {
    autoRefresh: !0,
    autoRefreshInterval: 500
  }, o.prototype.watch = function() {
    this._interval || (this._visible = this._core.isVisible(), this._interval = n.setInterval(t.proxy(this.refresh, this), this._core.settings.autoRefreshInterval))
  }, o.prototype.refresh = function() {
    this._core.isVisible() !== this._visible && (this._visible = !this._visible, this._core.$element.toggleClass("owl-hidden", !this._visible), this._visible && this._core.invalidate("width") && this._core.refresh())
  }, o.prototype.destroy = function() {
    var e, t;
    for (e in n.clearInterval(this._interval), this._handlers) this._core.$element.off(e, this._handlers[e]);
    for (t in Object.getOwnPropertyNames(this)) "function" != typeof this[t] && (this[t] = null)
  }, t.fn.owlCarousel.Constructor.Plugins.AutoRefresh = o
}(window.Zepto || window.jQuery, window, document),
function(a, r, e, t) {
  var n = function(e) {
    this._core = e, this._loaded = [], this._handlers = {
      "initialized.owl.carousel change.owl.carousel resized.owl.carousel": a.proxy(function(e) {
        if (e.namespace && this._core.settings && this._core.settings.lazyLoad && (e.property && "position" == e.property.name || "initialized" == e.type))
          for (var t = this._core.settings, n = t.center && Math.ceil(t.items / 2) || t.items, i = t.center && -1 * n || 0, o = (e.property && void 0 !== e.property.value ? e.property.value : this._core.current()) + i, r = this._core.clones().length, s = a.proxy(function(e, t) {
              this.load(t)
            }, this); i++ < n;) this.load(r / 2 + this._core.relative(o)), r && a.each(this._core.clones(this._core.relative(o)), s), o++
      }, this)
    }, this._core.options = a.extend({}, n.Defaults, this._core.options), this._core.$element.on(this._handlers)
  };
  n.Defaults = {
    lazyLoad: !1
  }, n.prototype.load = function(e) {
    var t = this._core.$stage.children().eq(e),
      n = t && t.find(".owl-lazy");
    !n || -1 < a.inArray(t.get(0), this._loaded) || (n.each(a.proxy(function(e, t) {
      var n, i = a(t),
        o = 1 < r.devicePixelRatio && i.attr("data-src-retina") || i.attr("data-src") || i.attr("data-srcset");
      this._core.trigger("load", {
        element: i,
        url: o
      }, "lazy"), i.is("img") ? i.one("load.owl.lazy", a.proxy(function() {
        i.css("opacity", 1), this._core.trigger("loaded", {
          element: i,
          url: o
        }, "lazy")
      }, this)).attr("src", o) : i.is("source") ? i.one("load.owl.lazy", a.proxy(function() {
        this._core.trigger("loaded", {
          element: i,
          url: o
        }, "lazy")
      }, this)).attr("srcset", o) : ((n = new Image).onload = a.proxy(function() {
        i.css({
          "background-image": 'url("' + o + '")',
          opacity: "1"
        }), this._core.trigger("loaded", {
          element: i,
          url: o
        }, "lazy")
      }, this), n.src = o)
    }, this)), this._loaded.push(t.get(0)))
  }, n.prototype.destroy = function() {
    var e, t;
    for (e in this.handlers) this._core.$element.off(e, this.handlers[e]);
    for (t in Object.getOwnPropertyNames(this)) "function" != typeof this[t] && (this[t] = null)
  }, a.fn.owlCarousel.Constructor.Plugins.Lazy = n
}(window.Zepto || window.jQuery, window, document),
function(r, n, e, t) {
  var i = function(e) {
    this._core = e, this._handlers = {
      "initialized.owl.carousel refreshed.owl.carousel": r.proxy(function(e) {
        e.namespace && this._core.settings.autoHeight && this.update()
      }, this),
      "changed.owl.carousel": r.proxy(function(e) {
        e.namespace && this._core.settings.autoHeight && "position" === e.property.name && (console.log("update called"), this.update())
      }, this),
      "loaded.owl.lazy": r.proxy(function(e) {
        e.namespace && this._core.settings.autoHeight && e.element.closest("." + this._core.settings.itemClass).index() === this._core.current() && this.update()
      }, this)
    }, this._core.options = r.extend({}, i.Defaults, this._core.options), this._core.$element.on(this._handlers), this._intervalId = null;
    var t = this;
    r(n).on("load", function() {
      t._core.settings.autoHeight && t.update()
    }), r(n).resize(function() {
      t._core.settings.autoHeight && (null != t._intervalId && clearTimeout(t._intervalId), t._intervalId = setTimeout(function() {
        t.update()
      }, 250))
    })
  };
  i.Defaults = {
    autoHeight: !1,
    autoHeightClass: "owl-height"
  }, i.prototype.update = function() {
    var e, t = this._core._current,
      n = t + this._core.settings.items,
      i = this._core.$stage.children().toArray().slice(t, n),
      o = [];
    r.each(i, function(e, t) {
      o.push(r(t).height())
    }), e = Math.max.apply(null, o), this._core.$stage.parent().height(e).addClass(this._core.settings.autoHeightClass)
  }, i.prototype.destroy = function() {
    var e, t;
    for (e in this._handlers) this._core.$element.off(e, this._handlers[e]);
    for (t in Object.getOwnPropertyNames(this)) "function" != typeof this[t] && (this[t] = null)
  }, r.fn.owlCarousel.Constructor.Plugins.AutoHeight = i
}(window.Zepto || window.jQuery, window, document),
function(u, e, t, n) {
  var i = function(e) {
    this._core = e, this._videos = {}, this._playing = null, this._handlers = {
      "initialized.owl.carousel": u.proxy(function(e) {
        e.namespace && this._core.register({
          type: "state",
          name: "playing",
          tags: ["interacting"]
        })
      }, this),
      "resize.owl.carousel": u.proxy(function(e) {
        e.namespace && this._core.settings.video && this.isInFullScreen() && e.preventDefault()
      }, this),
      "refreshed.owl.carousel": u.proxy(function(e) {
        e.namespace && this._core.is("resizing") && this._core.$stage.find(".cloned .owl-video-frame").remove()
      }, this),
      "changed.owl.carousel": u.proxy(function(e) {
        e.namespace && "position" === e.property.name && this._playing && this.stop()
      }, this),
      "prepared.owl.carousel": u.proxy(function(e) {
        if (e.namespace) {
          var t = u(e.content).find(".owl-video");
          t.length && (t.css("display", "none"), this.fetch(t, u(e.content)))
        }
      }, this)
    }, this._core.options = u.extend({}, i.Defaults, this._core.options), this._core.$element.on(this._handlers), this._core.$element.on("click.owl.video", ".owl-video-play-icon", u.proxy(function(e) {
      this.play(e)
    }, this))
  };
  i.Defaults = {
    video: !1,
    videoHeight: !1,
    videoWidth: !1
  }, i.prototype.fetch = function(e, t) {
    var n = e.attr("data-vimeo-id") ? "vimeo" : e.attr("data-vzaar-id") ? "vzaar" : "youtube",
      i = e.attr("data-vimeo-id") || e.attr("data-youtube-id") || e.attr("data-vzaar-id"),
      o = e.attr("data-width") || this._core.settings.videoWidth,
      r = e.attr("data-height") || this._core.settings.videoHeight,
      s = e.attr("href");
    if (!s) throw new Error("Missing video URL.");
    if (-1 < (i = s.match(/(http:|https:|)\/\/(player.|www.|app.)?(vimeo\.com|youtu(be\.com|\.be|be\.googleapis\.com)|vzaar\.com)\/(video\/|videos\/|embed\/|channels\/.+\/|groups\/.+\/|watch\?v=|v\/)?([A-Za-z0-9._%-]*)(\&\S+)?/))[3].indexOf("youtu")) n = "youtube";
    else if (-1 < i[3].indexOf("vimeo")) n = "vimeo";
    else {
      if (!(-1 < i[3].indexOf("vzaar"))) throw new Error("Video URL not supported.");
      n = "vzaar"
    }
    i = i[6], this._videos[s] = {
      type: n,
      id: i,
      width: o,
      height: r
    }, t.attr("data-video", s), this.thumbnail(e, this._videos[s])
  }, i.prototype.thumbnail = function(t, e) {
    var n, i, o = e.width && e.height ? 'style="width:' + e.width + "px;height:" + e.height + 'px;"' : "",
      r = t.find("img"),
      s = "src",
      a = "",
      l = this._core.settings,
      c = function(e) {
        '<div class="owl-video-play-icon"></div>',
        n = l.lazyLoad ? '<div class="owl-video-tn ' + a + '" ' + s + '="' + e + '"></div>' : '<div class="owl-video-tn" style="opacity:1;background-image:url(' + e + ')"></div>',
        t.after(n),
        t.after('<div class="owl-video-play-icon"></div>')
      };
    if (t.wrap('<div class="owl-video-wrapper"' + o + "></div>"), this._core.settings.lazyLoad && (s = "data-src", a = "owl-lazy"), r.length) return c(r.attr(s)), r.remove(), !1;
    "youtube" === e.type ? (i = "//img.youtube.com/vi/" + e.id + "/hqdefault.jpg", c(i)) : "vimeo" === e.type ? u.ajax({
      type: "GET",
      url: "//vimeo.com/api/v2/video/" + e.id + ".json",
      jsonp: "callback",
      dataType: "jsonp",
      success: function(e) {
        i = e[0].thumbnail_large, c(i)
      }
    }) : "vzaar" === e.type && u.ajax({
      type: "GET",
      url: "//vzaar.com/api/videos/" + e.id + ".json",
      jsonp: "callback",
      dataType: "jsonp",
      success: function(e) {
        i = e.framegrab_url, c(i)
      }
    })
  }, i.prototype.stop = function() {
    this._core.trigger("stop", null, "video"), this._playing.find(".owl-video-frame").remove(), this._playing.removeClass("owl-video-playing"), this._playing = null, this._core.leave("playing"), this._core.trigger("stopped", null, "video")
  }, i.prototype.play = function(e) {
    var t, n = u(e.target).closest("." + this._core.settings.itemClass),
      i = this._videos[n.attr("data-video")],
      o = i.width || "100%",
      r = i.height || this._core.$stage.height();
    this._playing || (this._core.enter("playing"), this._core.trigger("play", null, "video"), n = this._core.items(this._core.relative(n.index())), this._core.reset(n.index()), "youtube" === i.type ? t = '<iframe width="' + o + '" height="' + r + '" src="//www.youtube.com/embed/' + i.id + "?autoplay=1&rel=0&v=" + i.id + '" frameborder="0" allowfullscreen></iframe>' : "vimeo" === i.type ? t = '<iframe src="//player.vimeo.com/video/' + i.id + '?autoplay=1" width="' + o + '" height="' + r + '" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>' : "vzaar" === i.type && (t = '<iframe frameborder="0"height="' + r + '"width="' + o + '" allowfullscreen mozallowfullscreen webkitAllowFullScreen src="//view.vzaar.com/' + i.id + '/player?autoplay=true"></iframe>'), u('<div class="owl-video-frame">' + t + "</div>").insertAfter(n.find(".owl-video")), this._playing = n.addClass("owl-video-playing"))
  }, i.prototype.isInFullScreen = function() {
    var e = t.fullscreenElement || t.mozFullScreenElement || t.webkitFullscreenElement;
    return e && u(e).parent().hasClass("owl-video-frame")
  }, i.prototype.destroy = function() {
    var e, t;
    for (e in this._core.$element.off("click.owl.video"), this._handlers) this._core.$element.off(e, this._handlers[e]);
    for (t in Object.getOwnPropertyNames(this)) "function" != typeof this[t] && (this[t] = null)
  }, u.fn.owlCarousel.Constructor.Plugins.Video = i
}(window.Zepto || window.jQuery, window, document),
function(s, e, t, n) {
  var i = function(e) {
    this.core = e, this.core.options = s.extend({}, i.Defaults, this.core.options), this.swapping = !0, this.previous = void 0, this.next = void 0, this.handlers = {
      "change.owl.carousel": s.proxy(function(e) {
        e.namespace && "position" == e.property.name && (this.previous = this.core.current(), this.next = e.property.value)
      }, this),
      "drag.owl.carousel dragged.owl.carousel translated.owl.carousel": s.proxy(function(e) {
        e.namespace && (this.swapping = "translated" == e.type)
      }, this),
      "translate.owl.carousel": s.proxy(function(e) {
        e.namespace && this.swapping && (this.core.options.animateOut || this.core.options.animateIn) && this.swap()
      }, this)
    }, this.core.$element.on(this.handlers)
  };
  i.Defaults = {
    animateOut: !1,
    animateIn: !1
  }, i.prototype.swap = function() {
    if (1 === this.core.settings.items && s.support.animation && s.support.transition) {
      this.core.speed(0);
      var e, t = s.proxy(this.clear, this),
        n = this.core.$stage.children().eq(this.previous),
        i = this.core.$stage.children().eq(this.next),
        o = this.core.settings.animateIn,
        r = this.core.settings.animateOut;
      this.core.current() !== this.previous && (r && (e = this.core.coordinates(this.previous) - this.core.coordinates(this.next), n.one(s.support.animation.end, t).css({
        left: e + "px"
      }).addClass("animated owl-animated-out").addClass(r)), o && i.one(s.support.animation.end, t).addClass("animated owl-animated-in").addClass(o))
    }
  }, i.prototype.clear = function(e) {
    s(e.target).css({
      left: ""
    }).removeClass("animated owl-animated-out owl-animated-in").removeClass(this.core.settings.animateIn).removeClass(this.core.settings.animateOut), this.core.onTransitionEnd()
  }, i.prototype.destroy = function() {
    var e, t;
    for (e in this.handlers) this.core.$element.off(e, this.handlers[e]);
    for (t in Object.getOwnPropertyNames(this)) "function" != typeof this[t] && (this[t] = null)
  }, s.fn.owlCarousel.Constructor.Plugins.Animate = i
}(window.Zepto || window.jQuery, window, document),
function(i, o, t, e) {
  var n = function(e) {
    this._core = e, this._call = null, this._time = 0, this._timeout = 0, this._paused = !0, this._handlers = {
      "changed.owl.carousel": i.proxy(function(e) {
        e.namespace && "settings" === e.property.name ? this._core.settings.autoplay ? this.play() : this.stop() : e.namespace && "position" === e.property.name && this._paused && (this._time = 0)
      }, this),
      "initialized.owl.carousel": i.proxy(function(e) {
        e.namespace && this._core.settings.autoplay && this.play()
      }, this),
      "play.owl.autoplay": i.proxy(function(e, t, n) {
        e.namespace && this.play(t, n)
      }, this),
      "stop.owl.autoplay": i.proxy(function(e) {
        e.namespace && this.stop()
      }, this),
      "mouseover.owl.autoplay": i.proxy(function() {
        this._core.settings.autoplayHoverPause && this._core.is("rotating") && this.pause()
      }, this),
      "mouseleave.owl.autoplay": i.proxy(function() {
        this._core.settings.autoplayHoverPause && this._core.is("rotating") && this.play()
      }, this),
      "touchstart.owl.core": i.proxy(function() {
        this._core.settings.autoplayHoverPause && this._core.is("rotating") && this.pause()
      }, this),
      "touchend.owl.core": i.proxy(function() {
        this._core.settings.autoplayHoverPause && this.play()
      }, this)
    }, this._core.$element.on(this._handlers), this._core.options = i.extend({}, n.Defaults, this._core.options)
  };
  n.Defaults = {
    autoplay: !1,
    autoplayTimeout: 5e3,
    autoplayHoverPause: !1,
    autoplaySpeed: !1
  }, n.prototype._next = function(e) {
    this._call = o.setTimeout(i.proxy(this._next, this, e), this._timeout * (Math.round(this.read() / this._timeout) + 1) - this.read()), this._core.is("interacting") || t.hidden || this._core.next(e || this._core.settings.autoplaySpeed)
  }, n.prototype.read = function() {
    return (new Date).getTime() - this._time
  }, n.prototype.play = function(e, t) {
    var n;
    this._core.is("rotating") || this._core.enter("rotating"), e = e || this._core.settings.autoplayTimeout, n = Math.min(this._time % (this._timeout || e), e), this._paused ? (this._time = this.read(), this._paused = !1) : o.clearTimeout(this._call), this._time += this.read() % e - n, this._timeout = e, this._call = o.setTimeout(i.proxy(this._next, this, t), e - n)
  }, n.prototype.stop = function() {
    this._core.is("rotating") && (this._time = 0, this._paused = !0, o.clearTimeout(this._call), this._core.leave("rotating"))
  }, n.prototype.pause = function() {
    this._core.is("rotating") && !this._paused && (this._time = this.read(), this._paused = !0, o.clearTimeout(this._call))
  }, n.prototype.destroy = function() {
    var e, t;
    for (e in this.stop(), this._handlers) this._core.$element.off(e, this._handlers[e]);
    for (t in Object.getOwnPropertyNames(this)) "function" != typeof this[t] && (this[t] = null)
  }, i.fn.owlCarousel.Constructor.Plugins.autoplay = n
}(window.Zepto || window.jQuery, window, document),
function(r, e, t, n) {
  "use strict";
  var i = function(e) {
    this._core = e, this._initialized = !1, this._pages = [], this._controls = {}, this._templates = [], this.$element = this._core.$element, this._overrides = {
      next: this._core.next,
      prev: this._core.prev,
      to: this._core.to
    }, this._handlers = {
      "prepared.owl.carousel": r.proxy(function(e) {
        e.namespace && this._core.settings.dotsData && this._templates.push('<div class="' + this._core.settings.dotClass + '">' + r(e.content).find("[data-dot]").addBack("[data-dot]").attr("data-dot") + "</div>")
      }, this),
      "added.owl.carousel": r.proxy(function(e) {
        e.namespace && this._core.settings.dotsData && this._templates.splice(e.position, 0, this._templates.pop())
      }, this),
      "remove.owl.carousel": r.proxy(function(e) {
        e.namespace && this._core.settings.dotsData && this._templates.splice(e.position, 1)
      }, this),
      "changed.owl.carousel": r.proxy(function(e) {
        e.namespace && "position" == e.property.name && this.draw()
      }, this),
      "initialized.owl.carousel": r.proxy(function(e) {
        e.namespace && !this._initialized && (this._core.trigger("initialize", null, "navigation"), this.initialize(), this.update(), this.draw(), this._initialized = !0, this._core.trigger("initialized", null, "navigation"))
      }, this),
      "refreshed.owl.carousel": r.proxy(function(e) {
        e.namespace && this._initialized && (this._core.trigger("refresh", null, "navigation"), this.update(), this.draw(), this._core.trigger("refreshed", null, "navigation"))
      }, this)
    }, this._core.options = r.extend({}, i.Defaults, this._core.options), this.$element.on(this._handlers)
  };
  i.Defaults = {
    nav: !1,
    navText: ['<span aria-label="Previous">&#x2039;</span>', '<span aria-label="Next">&#x203a;</span>'],
    navSpeed: !1,
    navElement: 'button type="button" role="presentation"',
    navContainer: !1,
    navContainerClass: "owl-nav",
    navClass: ["owl-prev", "owl-next"],
    slideBy: 1,
    dotClass: "owl-dot",
    dotsClass: "owl-dots",
    dots: !0,
    dotsEach: !1,
    dotsData: !1,
    dotsSpeed: !1,
    dotsContainer: !1
  }, i.prototype.initialize = function() {
    var e, n = this._core.settings;
    for (e in this._controls.$relative = (n.navContainer ? r(n.navContainer) : r("<div>").addClass(n.navContainerClass).appendTo(this.$element)).addClass("disabled"), this._controls.$previous = r("<" + n.navElement + ">").addClass(n.navClass[0]).html(n.navText[0]).prependTo(this._controls.$relative).on("click", r.proxy(function(e) {
        this.prev(n.navSpeed)
      }, this)), this._controls.$next = r("<" + n.navElement + ">").addClass(n.navClass[1]).html(n.navText[1]).appendTo(this._controls.$relative).on("click", r.proxy(function(e) {
        this.next(n.navSpeed)
      }, this)), n.dotsData || (this._templates = [r('<button role="button">').addClass(n.dotClass).append(r("<span>")).prop("outerHTML")]), this._controls.$absolute = (n.dotsContainer ? r(n.dotsContainer) : r("<div>").addClass(n.dotsClass).appendTo(this.$element)).addClass("disabled"), this._controls.$absolute.on("click", "button", r.proxy(function(e) {
        var t = r(e.target).parent().is(this._controls.$absolute) ? r(e.target).index() : r(e.target).parent().index();
        e.preventDefault(), this.to(t, n.dotsSpeed)
      }, this)), this._overrides) this._core[e] = r.proxy(this[e], this)
  }, i.prototype.destroy = function() {
    var e, t, n, i, o;
    for (e in o = this._core.settings, this._handlers) this.$element.off(e, this._handlers[e]);
    for (t in this._controls) "$relative" === t && o.navContainer ? this._controls[t].html("") : this._controls[t].remove();
    for (i in this.overides) this._core[i] = this._overrides[i];
    for (n in Object.getOwnPropertyNames(this)) "function" != typeof this[n] && (this[n] = null)
  }, i.prototype.update = function() {
    var e, t, n = this._core.clones().length / 2,
      i = n + this._core.items().length,
      o = this._core.maximum(!0),
      r = this._core.settings,
      s = r.center || r.autoWidth || r.dotsData ? 1 : r.dotsEach || r.items;
    if ("page" !== r.slideBy && (r.slideBy = Math.min(r.slideBy, r.items)), r.dots || "page" == r.slideBy)
      for (this._pages = [], e = n, t = 0; e < i; e++) {
        if (s <= t || 0 === t) {
          if (this._pages.push({
              start: Math.min(o, e - n),
              end: e - n + s - 1
            }), Math.min(o, e - n) === o) break;
          t = 0, 0
        }
        t += this._core.mergers(this._core.relative(e))
      }
  }, i.prototype.draw = function() {
    var e, t = this._core.settings,
      n = this._core.items().length <= t.items,
      i = this._core.relative(this._core.current()),
      o = t.loop || t.rewind;
    this._controls.$relative.toggleClass("disabled", !t.nav || n), t.nav && (this._controls.$previous.toggleClass("disabled", !o && i <= this._core.minimum(!0)), this._controls.$next.toggleClass("disabled", !o && i >= this._core.maximum(!0))), this._controls.$absolute.toggleClass("disabled", !t.dots || n), t.dots && (e = this._pages.length - this._controls.$absolute.children().length, t.dotsData && 0 !== e ? this._controls.$absolute.html(this._templates.join("")) : 0 < e ? this._controls.$absolute.append(new Array(e + 1).join(this._templates[0])) : e < 0 && this._controls.$absolute.children().slice(e).remove(), this._controls.$absolute.find(".active").removeClass("active"), this._controls.$absolute.children().eq(r.inArray(this.current(), this._pages)).addClass("active"))
  }, i.prototype.onTrigger = function(e) {
    var t = this._core.settings;
    e.page = {
      index: r.inArray(this.current(), this._pages),
      count: this._pages.length,
      size: t && (t.center || t.autoWidth || t.dotsData ? 1 : t.dotsEach || t.items)
    }
  }, i.prototype.current = function() {
    var n = this._core.relative(this._core.current());
    return r.grep(this._pages, r.proxy(function(e, t) {
      return e.start <= n && e.end >= n
    }, this)).pop()
  }, i.prototype.getPosition = function(e) {
    var t, n, i = this._core.settings;
    return "page" == i.slideBy ? (t = r.inArray(this.current(), this._pages), n = this._pages.length, e ? ++t : --t, t = this._pages[(t % n + n) % n].start) : (t = this._core.relative(this._core.current()), n = this._core.items().length, e ? t += i.slideBy : t -= i.slideBy), t
  }, i.prototype.next = function(e) {
    r.proxy(this._overrides.to, this._core)(this.getPosition(!0), e)
  }, i.prototype.prev = function(e) {
    r.proxy(this._overrides.to, this._core)(this.getPosition(!1), e)
  }, i.prototype.to = function(e, t, n) {
    var i;
    !n && this._pages.length ? (i = this._pages.length, r.proxy(this._overrides.to, this._core)(this._pages[(e % i + i) % i].start, t)) : r.proxy(this._overrides.to, this._core)(e, t)
  }, r.fn.owlCarousel.Constructor.Plugins.Navigation = i
}(window.Zepto || window.jQuery, window, document),
function(i, o, e, t) {
  "use strict";
  var n = function(e) {
    this._core = e, this._hashes = {}, this.$element = this._core.$element, this._handlers = {
      "initialized.owl.carousel": i.proxy(function(e) {
        e.namespace && "URLHash" === this._core.settings.startPosition && i(o).trigger("hashchange.owl.navigation")
      }, this),
      "prepared.owl.carousel": i.proxy(function(e) {
        if (e.namespace) {
          var t = i(e.content).find("[data-hash]").addBack("[data-hash]").attr("data-hash");
          if (!t) return;
          this._hashes[t] = e.content
        }
      }, this),
      "changed.owl.carousel": i.proxy(function(e) {
        if (e.namespace && "position" === e.property.name) {
          var n = this._core.items(this._core.relative(this._core.current())),
            t = i.map(this._hashes, function(e, t) {
              return e === n ? t : null
            }).join();
          if (!t || o.location.hash.slice(1) === t) return;
          o.location.hash = t
        }
      }, this)
    }, this._core.options = i.extend({}, n.Defaults, this._core.options), this.$element.on(this._handlers), i(o).on("hashchange.owl.navigation", i.proxy(function(e) {
      var t = o.location.hash.substring(1),
        n = this._core.$stage.children(),
        i = this._hashes[t] && n.index(this._hashes[t]);
      void 0 !== i && i !== this._core.current() && this._core.to(this._core.relative(i), !1, !0)
    }, this))
  };
  n.Defaults = {
    URLhashListener: !1
  }, n.prototype.destroy = function() {
    var e, t;
    for (e in i(o).off("hashchange.owl.navigation"), this._handlers) this._core.$element.off(e, this._handlers[e]);
    for (t in Object.getOwnPropertyNames(this)) "function" != typeof this[t] && (this[t] = null)
  }, i.fn.owlCarousel.Constructor.Plugins.Hash = n
}(window.Zepto || window.jQuery, window, document),
function(o, e, t, r) {
  function n(e, n) {
    var i = !1,
      t = e.charAt(0).toUpperCase() + e.slice(1);
    return o.each((e + " " + a.join(t + " ") + t).split(" "), function(e, t) {
      if (s[t] !== r) return i = !n || t, !1
    }), i
  }

  function i(e) {
    return n(e, !0)
  }
  var s = o("<support>").get(0).style,
    a = "Webkit Moz O ms".split(" "),
    l = {
      transition: {
        end: {
          WebkitTransition: "webkitTransitionEnd",
          MozTransition: "transitionend",
          OTransition: "oTransitionEnd",
          transition: "transitionend"
        }
      },
      animation: {
        end: {
          WebkitAnimation: "webkitAnimationEnd",
          MozAnimation: "animationend",
          OAnimation: "oAnimationEnd",
          animation: "animationend"
        }
      }
    },
    c = function() {
      return !!n("transform")
    },
    u = function() {
      return !!n("perspective")
    },
    d = function() {
      return !!n("animation")
    };
  (function() {
    return !!n("transition")
  })() && (o.support.transition = new String(i("transition")), o.support.transition.end = l.transition.end[o.support.transition]), d() && (o.support.animation = new String(i("animation")), o.support.animation.end = l.animation.end[o.support.animation]), c() && (o.support.transform = new String(i("transform")), o.support.transform3d = u())
}(window.Zepto || window.jQuery, window, document), (function(factory) {
  if (typeof define === "function" && define.amd) {
    define(["jquery"], factory)
  } else if (typeof module !== "undefined" && module.exports) {
    module.exports = factory
  } else {
    factory(jQuery, window, document)
  }
}(function($) {
  (function(init) {
    var _rjs = typeof define === "function" && define.amd,
      _njs = typeof module !== "undefined" && module.exports,
      _dlp = ("https:" == document.location.protocol) ? "https:" : "http:",
      _url = "cdnjs.cloudflare.com/ajax/libs/jquery-mousewheel/3.1.13/jquery.mousewheel.min.js";
    if (!_rjs) {
      if (_njs) {
        require("jquery-mousewheel")($)
      } else {
        $.event.special.mousewheel || $("head").append(decodeURI("%3Cscript src=" + _dlp + "//" + _url + "%3E%3C/script%3E"))
      }
    }
    init()
  }(function() {
    var pluginNS = "mCustomScrollbar",
      pluginPfx = "mCS",
      defaultSelector = ".mCustomScrollbar",
      defaults = {
        setTop: 0,
        setLeft: 0,
        axis: "y",
        scrollbarPosition: "inside",
        scrollInertia: 950,
        autoDraggerLength: !0,
        alwaysShowScrollbar: 0,
        snapOffset: 0,
        mouseWheel: {
          enable: !0,
          scrollAmount: "auto",
          axis: "y",
          deltaFactor: "auto",
          disableOver: ["select", "option", "keygen", "datalist", "textarea"]
        },
        scrollButtons: {
          scrollType: "stepless",
          scrollAmount: "auto"
        },
        keyboard: {
          enable: !0,
          scrollType: "stepless",
          scrollAmount: "auto"
        },
        contentTouchScroll: 25,
        documentTouchScroll: !0,
        advanced: {
          autoScrollOnFocus: "input,textarea,select,button,datalist,keygen,a[tabindex],area,object,[contenteditable='true']",
          updateOnContentResize: !0,
          updateOnImageLoad: "auto",
          autoUpdateTimeout: 60
        },
        theme: "light",
        callbacks: {
          onTotalScrollOffset: 0,
          onTotalScrollBackOffset: 0,
          alwaysTriggerOffsets: !0
        }
      },
      totalInstances = 0,
      liveTimers = {},
      oldIE = (window.attachEvent && !window.addEventListener) ? 1 : 0,
      touchActive = !1,
      touchable, classes = ["mCSB_dragger_onDrag", "mCSB_scrollTools_onDrag", "mCS_img_loaded", "mCS_disabled", "mCS_destroyed", "mCS_no_scrollbar", "mCS-autoHide", "mCS-dir-rtl", "mCS_no_scrollbar_y", "mCS_no_scrollbar_x", "mCS_y_hidden", "mCS_x_hidden", "mCSB_draggerContainer", "mCSB_buttonUp", "mCSB_buttonDown", "mCSB_buttonLeft", "mCSB_buttonRight"],
      methods = {
        init: function(options) {
          var options = $.extend(!0, {}, defaults, options),
            selector = _selector.call(this);
          if (options.live) {
            var liveSelector = options.liveSelector || this.selector || defaultSelector,
              $liveSelector = $(liveSelector);
            if (options.live === "off") {
              removeLiveTimers(liveSelector);
              return
            }
            liveTimers[liveSelector] = setTimeout(function() {
              $liveSelector.mCustomScrollbar(options);
              if (options.live === "once" && $liveSelector.length) {
                removeLiveTimers(liveSelector)
              }
            }, 500)
          } else {
            removeLiveTimers(liveSelector)
          }
          options.setWidth = (options.set_width) ? options.set_width : options.setWidth;
          options.setHeight = (options.set_height) ? options.set_height : options.setHeight;
          options.axis = (options.horizontalScroll) ? "x" : _findAxis(options.axis);
          options.scrollInertia = options.scrollInertia > 0 && options.scrollInertia < 17 ? 17 : options.scrollInertia;
          if (typeof options.mouseWheel !== "object" && options.mouseWheel == !0) {
            options.mouseWheel = {
              enable: !0,
              scrollAmount: "auto",
              axis: "y",
              preventDefault: !1,
              deltaFactor: "auto",
              normalizeDelta: !1,
              invert: !1
            }
          }
          options.mouseWheel.scrollAmount = !options.mouseWheelPixels ? options.mouseWheel.scrollAmount : options.mouseWheelPixels;
          options.mouseWheel.normalizeDelta = !options.advanced.normalizeMouseWheelDelta ? options.mouseWheel.normalizeDelta : options.advanced.normalizeMouseWheelDelta;
          options.scrollButtons.scrollType = _findScrollButtonsType(options.scrollButtons.scrollType);
          _theme(options);
          return $(selector).each(function() {
            var $this = $(this);
            if (!$this.data(pluginPfx)) {
              $this.data(pluginPfx, {
                idx: ++totalInstances,
                opt: options,
                scrollRatio: {
                  y: null,
                  x: null
                },
                overflowed: null,
                contentReset: {
                  y: null,
                  x: null
                },
                bindEvents: !1,
                tweenRunning: !1,
                sequential: {},
                langDir: $this.css("direction"),
                cbOffsets: null,
                trigger: null,
                poll: {
                  size: {
                    o: 0,
                    n: 0
                  },
                  img: {
                    o: 0,
                    n: 0
                  },
                  change: {
                    o: 0,
                    n: 0
                  }
                }
              });
              var d = $this.data(pluginPfx),
                o = d.opt,
                htmlDataAxis = $this.data("mcs-axis"),
                htmlDataSbPos = $this.data("mcs-scrollbar-position"),
                htmlDataTheme = $this.data("mcs-theme");
              if (htmlDataAxis) {
                o.axis = htmlDataAxis
              }
              if (htmlDataSbPos) {
                o.scrollbarPosition = htmlDataSbPos
              }
              if (htmlDataTheme) {
                o.theme = htmlDataTheme;
                _theme(o)
              }
              _pluginMarkup.call(this);
              if (d && o.callbacks.onCreate && typeof o.callbacks.onCreate === "function") {
                o.callbacks.onCreate.call(this)
              }
              $("#mCSB_" + d.idx + "_container img:not(." + classes[2] + ")").addClass(classes[2]);
              methods.update.call(null, $this)
            }
          })
        },
        update: function(el, cb) {
          var selector = el || _selector.call(this);
          return $(selector).each(function() {
            var $this = $(this);
            if ($this.data(pluginPfx)) {
              var d = $this.data(pluginPfx),
                o = d.opt,
                mCSB_container = $("#mCSB_" + d.idx + "_container"),
                mCustomScrollBox = $("#mCSB_" + d.idx),
                mCSB_dragger = [$("#mCSB_" + d.idx + "_dragger_vertical"), $("#mCSB_" + d.idx + "_dragger_horizontal")];
              if (!mCSB_container.length) {
                return
              }
              if (d.tweenRunning) {
                _stop($this)
              }
              if (cb && d && o.callbacks.onBeforeUpdate && typeof o.callbacks.onBeforeUpdate === "function") {
                o.callbacks.onBeforeUpdate.call(this)
              }
              if ($this.hasClass(classes[3])) {
                $this.removeClass(classes[3])
              }
              if ($this.hasClass(classes[4])) {
                $this.removeClass(classes[4])
              }
              mCustomScrollBox.css("max-height", "none");
              if (mCustomScrollBox.height() !== $this.height()) {
                mCustomScrollBox.css("max-height", $this.height())
              }
              _expandContentHorizontally.call(this);
              if (o.axis !== "y" && !o.advanced.autoExpandHorizontalScroll) {
                mCSB_container.css("width", _contentWidth(mCSB_container))
              }
              d.overflowed = _overflowed.call(this);
              _scrollbarVisibility.call(this);
              if (o.autoDraggerLength) {
                _setDraggerLength.call(this)
              }
              _scrollRatio.call(this);
              _bindEvents.call(this);
              var to = [Math.abs(mCSB_container[0].offsetTop), Math.abs(mCSB_container[0].offsetLeft)];
              if (o.axis !== "x") {
                if (!d.overflowed[0]) {
                  _resetContentPosition.call(this);
                  if (o.axis === "y") {
                    _unbindEvents.call(this)
                  } else if (o.axis === "yx" && d.overflowed[1]) {
                    _scrollTo($this, to[1].toString(), {
                      dir: "x",
                      dur: 0,
                      overwrite: "none"
                    })
                  }
                } else if (mCSB_dragger[0].height() > mCSB_dragger[0].parent().height()) {
                  _resetContentPosition.call(this)
                } else {
                  _scrollTo($this, to[0].toString(), {
                    dir: "y",
                    dur: 0,
                    overwrite: "none"
                  });
                  d.contentReset.y = null
                }
              }
              if (o.axis !== "y") {
                if (!d.overflowed[1]) {
                  _resetContentPosition.call(this);
                  if (o.axis === "x") {
                    _unbindEvents.call(this)
                  } else if (o.axis === "yx" && d.overflowed[0]) {
                    _scrollTo($this, to[0].toString(), {
                      dir: "y",
                      dur: 0,
                      overwrite: "none"
                    })
                  }
                } else if (mCSB_dragger[1].width() > mCSB_dragger[1].parent().width()) {
                  _resetContentPosition.call(this)
                } else {
                  _scrollTo($this, to[1].toString(), {
                    dir: "x",
                    dur: 0,
                    overwrite: "none"
                  });
                  d.contentReset.x = null
                }
              }
              if (cb && d) {
                if (cb === 2 && o.callbacks.onImageLoad && typeof o.callbacks.onImageLoad === "function") {
                  o.callbacks.onImageLoad.call(this)
                } else if (cb === 3 && o.callbacks.onSelectorChange && typeof o.callbacks.onSelectorChange === "function") {
                  o.callbacks.onSelectorChange.call(this)
                } else if (o.callbacks.onUpdate && typeof o.callbacks.onUpdate === "function") {
                  o.callbacks.onUpdate.call(this)
                }
              }
              _autoUpdate.call(this)
            }
          })
        },
        scrollTo: function(val, options) {
          if (typeof val == "undefined" || val == null) {
            return
          }
          var selector = _selector.call(this);
          return $(selector).each(function() {
            var $this = $(this);
            if ($this.data(pluginPfx)) {
              var d = $this.data(pluginPfx),
                o = d.opt,
                methodDefaults = {
                  trigger: "external",
                  scrollInertia: o.scrollInertia,
                  scrollEasing: "mcsEaseInOut",
                  moveDragger: !1,
                  timeout: 60,
                  callbacks: !0,
                  onStart: !0,
                  onUpdate: !0,
                  onComplete: !0
                },
                methodOptions = $.extend(!0, {}, methodDefaults, options),
                to = _arr.call(this, val),
                dur = methodOptions.scrollInertia > 0 && methodOptions.scrollInertia < 17 ? 17 : methodOptions.scrollInertia;
              to[0] = _to.call(this, to[0], "y");
              to[1] = _to.call(this, to[1], "x");
              if (methodOptions.moveDragger) {
                to[0] *= d.scrollRatio.y;
                to[1] *= d.scrollRatio.x
              }
              methodOptions.dur = _isTabHidden() ? 0 : dur;
              setTimeout(function() {
                if (to[0] !== null && typeof to[0] !== "undefined" && o.axis !== "x" && d.overflowed[0]) {
                  methodOptions.dir = "y";
                  methodOptions.overwrite = "all";
                  _scrollTo($this, to[0].toString(), methodOptions)
                }
                if (to[1] !== null && typeof to[1] !== "undefined" && o.axis !== "y" && d.overflowed[1]) {
                  methodOptions.dir = "x";
                  methodOptions.overwrite = "none";
                  _scrollTo($this, to[1].toString(), methodOptions)
                }
              }, methodOptions.timeout)
            }
          })
        },
        stop: function() {
          var selector = _selector.call(this);
          return $(selector).each(function() {
            var $this = $(this);
            if ($this.data(pluginPfx)) {
              _stop($this)
            }
          })
        },
        disable: function(r) {
          var selector = _selector.call(this);
          return $(selector).each(function() {
            var $this = $(this);
            if ($this.data(pluginPfx)) {
              var d = $this.data(pluginPfx);
              _autoUpdate.call(this, "remove");
              _unbindEvents.call(this);
              if (r) {
                _resetContentPosition.call(this)
              }
              _scrollbarVisibility.call(this, !0);
              $this.addClass(classes[3])
            }
          })
        },
        destroy: function() {
          var selector = _selector.call(this);
          return $(selector).each(function() {
            var $this = $(this);
            if ($this.data(pluginPfx)) {
              var d = $this.data(pluginPfx),
                o = d.opt,
                mCustomScrollBox = $("#mCSB_" + d.idx),
                mCSB_container = $("#mCSB_" + d.idx + "_container"),
                scrollbar = $(".mCSB_" + d.idx + "_scrollbar");
              if (o.live) {
                removeLiveTimers(o.liveSelector || $(selector).selector)
              }
              _autoUpdate.call(this, "remove");
              _unbindEvents.call(this);
              _resetContentPosition.call(this);
              $this.removeData(pluginPfx);
              _delete(this, "mcs");
              scrollbar.remove();
              mCSB_container.find("img." + classes[2]).removeClass(classes[2]);
              mCustomScrollBox.replaceWith(mCSB_container.contents());
              $this.removeClass(pluginNS + " _" + pluginPfx + "_" + d.idx + " " + classes[6] + " " + classes[7] + " " + classes[5] + " " + classes[3]).addClass(classes[4])
            }
          })
        }
      },
      _selector = function() {
        return (typeof $(this) !== "object" || $(this).length < 1) ? defaultSelector : this
      },
      _theme = function(obj) {
        var fixedSizeScrollbarThemes = ["rounded", "rounded-dark", "rounded-dots", "rounded-dots-dark"],
          nonExpandedScrollbarThemes = ["rounded-dots", "rounded-dots-dark", "3d", "3d-dark", "3d-thick", "3d-thick-dark", "inset", "inset-dark", "inset-2", "inset-2-dark", "inset-3", "inset-3-dark"],
          disabledScrollButtonsThemes = ["minimal", "minimal-dark"],
          enabledAutoHideScrollbarThemes = ["minimal", "minimal-dark"],
          scrollbarPositionOutsideThemes = ["minimal", "minimal-dark"];
        obj.autoDraggerLength = $.inArray(obj.theme, fixedSizeScrollbarThemes) > -1 ? !1 : obj.autoDraggerLength;
        obj.autoExpandScrollbar = $.inArray(obj.theme, nonExpandedScrollbarThemes) > -1 ? !1 : obj.autoExpandScrollbar;
        obj.scrollButtons.enable = $.inArray(obj.theme, disabledScrollButtonsThemes) > -1 ? !1 : obj.scrollButtons.enable;
        obj.autoHideScrollbar = $.inArray(obj.theme, enabledAutoHideScrollbarThemes) > -1 ? !0 : obj.autoHideScrollbar;
        obj.scrollbarPosition = $.inArray(obj.theme, scrollbarPositionOutsideThemes) > -1 ? "outside" : obj.scrollbarPosition
      },
      removeLiveTimers = function(selector) {
        if (liveTimers[selector]) {
          clearTimeout(liveTimers[selector]);
          _delete(liveTimers, selector)
        }
      },
      _findAxis = function(val) {
        return (val === "yx" || val === "xy" || val === "auto") ? "yx" : (val === "x" || val === "horizontal") ? "x" : "y"
      },
      _findScrollButtonsType = function(val) {
        return (val === "stepped" || val === "pixels" || val === "step" || val === "click") ? "stepped" : "stepless"
      },
      _pluginMarkup = function() {
        var $this = $(this),
          d = $this.data(pluginPfx),
          o = d.opt,
          expandClass = o.autoExpandScrollbar ? " " + classes[1] + "_expand" : "",
          scrollbar = ["<div id='mCSB_" + d.idx + "_scrollbar_vertical' class='mCSB_scrollTools mCSB_" + d.idx + "_scrollbar mCS-" + o.theme + " mCSB_scrollTools_vertical" + expandClass + "'><div class='" + classes[12] + "'><div id='mCSB_" + d.idx + "_dragger_vertical' class='mCSB_dragger' style='position:absolute;'><div class='mCSB_dragger_bar' /></div><div class='mCSB_draggerRail' /></div></div>", "<div id='mCSB_" + d.idx + "_scrollbar_horizontal' class='mCSB_scrollTools mCSB_" + d.idx + "_scrollbar mCS-" + o.theme + " mCSB_scrollTools_horizontal" + expandClass + "'><div class='" + classes[12] + "'><div id='mCSB_" + d.idx + "_dragger_horizontal' class='mCSB_dragger' style='position:absolute;'><div class='mCSB_dragger_bar' /></div><div class='mCSB_draggerRail' /></div></div>"],
          wrapperClass = o.axis === "yx" ? "mCSB_vertical_horizontal" : o.axis === "x" ? "mCSB_horizontal" : "mCSB_vertical",
          scrollbars = o.axis === "yx" ? scrollbar[0] + scrollbar[1] : o.axis === "x" ? scrollbar[1] : scrollbar[0],
          contentWrapper = o.axis === "yx" ? "<div id='mCSB_" + d.idx + "_container_wrapper' class='mCSB_container_wrapper' />" : "",
          autoHideClass = o.autoHideScrollbar ? " " + classes[6] : "",
          scrollbarDirClass = (o.axis !== "x" && d.langDir === "rtl") ? " " + classes[7] : "";
        if (o.setWidth) {
          $this.css("width", o.setWidth)
        }
        if (o.setHeight) {
          $this.css("height", o.setHeight)
        }
        o.setLeft = (o.axis !== "y" && d.langDir === "rtl") ? "989999px" : o.setLeft;
        $this.addClass(pluginNS + " _" + pluginPfx + "_" + d.idx + autoHideClass + scrollbarDirClass).wrapInner("<div id='mCSB_" + d.idx + "' class='mCustomScrollBox mCS-" + o.theme + " " + wrapperClass + "'><div id='mCSB_" + d.idx + "_container' class='mCSB_container' style='position:relative; top:" + o.setTop + "; left:" + o.setLeft + ";' dir='" + d.langDir + "' /></div>");
        var mCustomScrollBox = $("#mCSB_" + d.idx),
          mCSB_container = $("#mCSB_" + d.idx + "_container");
        if (o.axis !== "y" && !o.advanced.autoExpandHorizontalScroll) {
          mCSB_container.css("width", _contentWidth(mCSB_container))
        }
        if (o.scrollbarPosition === "outside") {
          if ($this.css("position") === "static") {
            $this.css("position", "relative")
          }
          $this.css("overflow", "visible");
          mCustomScrollBox.addClass("mCSB_outside").after(scrollbars)
        } else {
          mCustomScrollBox.addClass("mCSB_inside").append(scrollbars);
          mCSB_container.wrap(contentWrapper)
        }
        _scrollButtons.call(this);
        var mCSB_dragger = [$("#mCSB_" + d.idx + "_dragger_vertical"), $("#mCSB_" + d.idx + "_dragger_horizontal")];
        mCSB_dragger[0].css("min-height", mCSB_dragger[0].height());
        mCSB_dragger[1].css("min-width", mCSB_dragger[1].width())
      },
      _contentWidth = function(el) {
        var val = [el[0].scrollWidth, Math.max.apply(Math, el.children().map(function() {
            return $(this).outerWidth(!0)
          }).get())],
          w = el.parent().width();
        return val[0] > w ? val[0] : val[1] > w ? val[1] : "100%"
      },
      _expandContentHorizontally = function() {
        var $this = $(this),
          d = $this.data(pluginPfx),
          o = d.opt,
          mCSB_container = $("#mCSB_" + d.idx + "_container");
        if (o.advanced.autoExpandHorizontalScroll && o.axis !== "y") {
          mCSB_container.css({
            "width": "auto",
            "min-width": 0,
            "overflow-x": "scroll"
          });
          var w = Math.ceil(mCSB_container[0].scrollWidth);
          if (o.advanced.autoExpandHorizontalScroll === 3 || (o.advanced.autoExpandHorizontalScroll !== 2 && w > mCSB_container.parent().width())) {
            mCSB_container.css({
              "width": w,
              "min-width": "100%",
              "overflow-x": "inherit"
            })
          } else {
            mCSB_container.css({
              "overflow-x": "inherit",
              "position": "absolute"
            }).wrap("<div class='mCSB_h_wrapper' style='position:relative; left:0; width:999999px;' />").css({
              "width": (Math.ceil(mCSB_container[0].getBoundingClientRect().right + 0.4) - Math.floor(mCSB_container[0].getBoundingClientRect().left)),
              "min-width": "100%",
              "position": "relative"
            }).unwrap()
          }
        }
      },
      _scrollButtons = function() {
        var $this = $(this),
          d = $this.data(pluginPfx),
          o = d.opt,
          mCSB_scrollTools = $(".mCSB_" + d.idx + "_scrollbar:first"),
          tabindex = !_isNumeric(o.scrollButtons.tabindex) ? "" : "tabindex='" + o.scrollButtons.tabindex + "'",
          btnHTML = ["<a href='#' class='" + classes[13] + "' " + tabindex + " />", "<a href='#' class='" + classes[14] + "' " + tabindex + " />", "<a href='#' class='" + classes[15] + "' " + tabindex + " />", "<a href='#' class='" + classes[16] + "' " + tabindex + " />"],
          btn = [(o.axis === "x" ? btnHTML[2] : btnHTML[0]), (o.axis === "x" ? btnHTML[3] : btnHTML[1]), btnHTML[2], btnHTML[3]];
        if (o.scrollButtons.enable) {
          mCSB_scrollTools.prepend(btn[0]).append(btn[1]).next(".mCSB_scrollTools").prepend(btn[2]).append(btn[3])
        }
      },
      _setDraggerLength = function() {
        var $this = $(this),
          d = $this.data(pluginPfx),
          mCustomScrollBox = $("#mCSB_" + d.idx),
          mCSB_container = $("#mCSB_" + d.idx + "_container"),
          mCSB_dragger = [$("#mCSB_" + d.idx + "_dragger_vertical"), $("#mCSB_" + d.idx + "_dragger_horizontal")],
          ratio = [mCustomScrollBox.height() / mCSB_container.outerHeight(!1), mCustomScrollBox.width() / mCSB_container.outerWidth(!1)],
          l = [parseInt(mCSB_dragger[0].css("min-height")), Math.round(ratio[0] * mCSB_dragger[0].parent().height()), parseInt(mCSB_dragger[1].css("min-width")), Math.round(ratio[1] * mCSB_dragger[1].parent().width())],
          h = oldIE && (l[1] < l[0]) ? l[0] : l[1],
          w = oldIE && (l[3] < l[2]) ? l[2] : l[3];
        mCSB_dragger[0].css({
          "height": h,
          "max-height": (mCSB_dragger[0].parent().height() - 10)
        }).find(".mCSB_dragger_bar").css({
          "line-height": l[0] + "px"
        });
        mCSB_dragger[1].css({
          "width": w,
          "max-width": (mCSB_dragger[1].parent().width() - 10)
        })
      },
      _scrollRatio = function() {
        var $this = $(this),
          d = $this.data(pluginPfx),
          mCustomScrollBox = $("#mCSB_" + d.idx),
          mCSB_container = $("#mCSB_" + d.idx + "_container"),
          mCSB_dragger = [$("#mCSB_" + d.idx + "_dragger_vertical"), $("#mCSB_" + d.idx + "_dragger_horizontal")],
          scrollAmount = [mCSB_container.outerHeight(!1) - mCustomScrollBox.height(), mCSB_container.outerWidth(!1) - mCustomScrollBox.width()],
          ratio = [scrollAmount[0] / (mCSB_dragger[0].parent().height() - mCSB_dragger[0].height()), scrollAmount[1] / (mCSB_dragger[1].parent().width() - mCSB_dragger[1].width())];
        d.scrollRatio = {
          y: ratio[0],
          x: ratio[1]
        }
      },
      _onDragClasses = function(el, action, xpnd) {
        var expandClass = xpnd ? classes[0] + "_expanded" : "",
          scrollbar = el.closest(".mCSB_scrollTools");
        if (action === "active") {
          el.toggleClass(classes[0] + " " + expandClass);
          scrollbar.toggleClass(classes[1]);
          el[0]._draggable = el[0]._draggable ? 0 : 1
        } else {
          if (!el[0]._draggable) {
            if (action === "hide") {
              el.removeClass(classes[0]);
              scrollbar.removeClass(classes[1])
            } else {
              el.addClass(classes[0]);
              scrollbar.addClass(classes[1])
            }
          }
        }
      },
      _overflowed = function() {
        var $this = $(this),
          d = $this.data(pluginPfx),
          mCustomScrollBox = $("#mCSB_" + d.idx),
          mCSB_container = $("#mCSB_" + d.idx + "_container"),
          contentHeight = d.overflowed == null ? mCSB_container.height() : mCSB_container.outerHeight(!1),
          contentWidth = d.overflowed == null ? mCSB_container.width() : mCSB_container.outerWidth(!1),
          h = mCSB_container[0].scrollHeight,
          w = mCSB_container[0].scrollWidth;
        if (h > contentHeight) {
          contentHeight = h
        }
        if (w > contentWidth) {
          contentWidth = w
        }
        return [contentHeight > mCustomScrollBox.height(), contentWidth > mCustomScrollBox.width()]
      },
      _resetContentPosition = function() {
        var $this = $(this),
          d = $this.data(pluginPfx),
          o = d.opt,
          mCustomScrollBox = $("#mCSB_" + d.idx),
          mCSB_container = $("#mCSB_" + d.idx + "_container"),
          mCSB_dragger = [$("#mCSB_" + d.idx + "_dragger_vertical"), $("#mCSB_" + d.idx + "_dragger_horizontal")];
        _stop($this);
        if ((o.axis !== "x" && !d.overflowed[0]) || (o.axis === "y" && d.overflowed[0])) {
          mCSB_dragger[0].add(mCSB_container).css("top", 0);
          _scrollTo($this, "_resetY")
        }
        if ((o.axis !== "y" && !d.overflowed[1]) || (o.axis === "x" && d.overflowed[1])) {
          var cx = dx = 0;
          if (d.langDir === "rtl") {
            cx = mCustomScrollBox.width() - mCSB_container.outerWidth(!1);
            dx = Math.abs(cx / d.scrollRatio.x)
          }
          mCSB_container.css("left", cx);
          mCSB_dragger[1].css("left", dx);
          _scrollTo($this, "_resetX")
        }
      },
      _bindEvents = function() {
        var $this = $(this),
          d = $this.data(pluginPfx),
          o = d.opt;
        if (!d.bindEvents) {
          _draggable.call(this);
          if (o.contentTouchScroll) {
            _contentDraggable.call(this)
          }
          _selectable.call(this);
          if (o.mouseWheel.enable) {
            function _mwt() {
              mousewheelTimeout = setTimeout(function() {
                if (!$.event.special.mousewheel) {
                  _mwt()
                } else {
                  clearTimeout(mousewheelTimeout);
                  _mousewheel.call($this[0])
                }
              }, 100)
            }
            var mousewheelTimeout;
            _mwt()
          }
          _draggerRail.call(this);
          _wrapperScroll.call(this);
          if (o.advanced.autoScrollOnFocus) {
            _focus.call(this)
          }
          if (o.scrollButtons.enable) {
            _buttons.call(this)
          }
          if (o.keyboard.enable) {
            _keyboard.call(this)
          }
          d.bindEvents = !0
        }
      },
      _unbindEvents = function() {
        var $this = $(this),
          d = $this.data(pluginPfx),
          o = d.opt,
          namespace = pluginPfx + "_" + d.idx,
          sb = ".mCSB_" + d.idx + "_scrollbar",
          sel = $("#mCSB_" + d.idx + ",#mCSB_" + d.idx + "_container,#mCSB_" + d.idx + "_container_wrapper," + sb + " ." + classes[12] + ",#mCSB_" + d.idx + "_dragger_vertical,#mCSB_" + d.idx + "_dragger_horizontal," + sb + ">a"),
          mCSB_container = $("#mCSB_" + d.idx + "_container");
        if (o.advanced.releaseDraggableSelectors) {
          sel.add($(o.advanced.releaseDraggableSelectors))
        }
        if (o.advanced.extraDraggableSelectors) {
          sel.add($(o.advanced.extraDraggableSelectors))
        }
        if (d.bindEvents) {
          $(document).add($(!_canAccessIFrame() || top.document)).unbind("." + namespace);
          sel.each(function() {
            $(this).unbind("." + namespace)
          });
          clearTimeout($this[0]._focusTimeout);
          _delete($this[0], "_focusTimeout");
          clearTimeout(d.sequential.step);
          _delete(d.sequential, "step");
          clearTimeout(mCSB_container[0].onCompleteTimeout);
          _delete(mCSB_container[0], "onCompleteTimeout");
          d.bindEvents = !1
        }
      },
      _scrollbarVisibility = function(disabled) {
        var $this = $(this),
          d = $this.data(pluginPfx),
          o = d.opt,
          contentWrapper = $("#mCSB_" + d.idx + "_container_wrapper"),
          content = contentWrapper.length ? contentWrapper : $("#mCSB_" + d.idx + "_container"),
          scrollbar = [$("#mCSB_" + d.idx + "_scrollbar_vertical"), $("#mCSB_" + d.idx + "_scrollbar_horizontal")],
          mCSB_dragger = [scrollbar[0].find(".mCSB_dragger"), scrollbar[1].find(".mCSB_dragger")];
        if (o.axis !== "x") {
          if (d.overflowed[0] && !disabled) {
            scrollbar[0].add(mCSB_dragger[0]).add(scrollbar[0].children("a")).css("display", "block");
            content.removeClass(classes[8] + " " + classes[10])
          } else {
            if (o.alwaysShowScrollbar) {
              if (o.alwaysShowScrollbar !== 2) {
                mCSB_dragger[0].css("display", "none")
              }
              content.removeClass(classes[10])
            } else {
              scrollbar[0].css("display", "none");
              content.addClass(classes[10])
            }
            content.addClass(classes[8])
          }
        }
        if (o.axis !== "y") {
          if (d.overflowed[1] && !disabled) {
            scrollbar[1].add(mCSB_dragger[1]).add(scrollbar[1].children("a")).css("display", "block");
            content.removeClass(classes[9] + " " + classes[11])
          } else {
            if (o.alwaysShowScrollbar) {
              if (o.alwaysShowScrollbar !== 2) {
                mCSB_dragger[1].css("display", "none")
              }
              content.removeClass(classes[11])
            } else {
              scrollbar[1].css("display", "none");
              content.addClass(classes[11])
            }
            content.addClass(classes[9])
          }
        }
        if (!d.overflowed[0] && !d.overflowed[1]) {
          $this.addClass(classes[5])
        } else {
          $this.removeClass(classes[5])
        }
      },
      _coordinates = function(e) {
        var t = e.type,
          o = e.target.ownerDocument !== document && frameElement !== null ? [$(frameElement).offset().top, $(frameElement).offset().left] : null,
          io = _canAccessIFrame() && e.target.ownerDocument !== top.document && frameElement !== null ? [$(e.view.frameElement).offset().top, $(e.view.frameElement).offset().left] : [0, 0];
        switch (t) {
          case "pointerdown":
          case "MSPointerDown":
          case "pointermove":
          case "MSPointerMove":
          case "pointerup":
          case "MSPointerUp":
            return o ? [e.originalEvent.pageY - o[0] + io[0], e.originalEvent.pageX - o[1] + io[1], !1] : [e.originalEvent.pageY, e.originalEvent.pageX, !1];
            break;
          case "touchstart":
          case "touchmove":
          case "touchend":
            var touch = e.originalEvent.touches[0] || e.originalEvent.changedTouches[0],
              touches = e.originalEvent.touches.length || e.originalEvent.changedTouches.length;
            return e.target.ownerDocument !== document ? [touch.screenY, touch.screenX, touches > 1] : [touch.pageY, touch.pageX, touches > 1];
            break;
          default:
            return o ? [e.pageY - o[0] + io[0], e.pageX - o[1] + io[1], !1] : [e.pageY, e.pageX, !1]
        }
      },
      _draggable = function() {
        var $this = $(this),
          d = $this.data(pluginPfx),
          o = d.opt,
          namespace = pluginPfx + "_" + d.idx,
          draggerId = ["mCSB_" + d.idx + "_dragger_vertical", "mCSB_" + d.idx + "_dragger_horizontal"],
          mCSB_container = $("#mCSB_" + d.idx + "_container"),
          mCSB_dragger = $("#" + draggerId[0] + ",#" + draggerId[1]),
          draggable, dragY, dragX, rds = o.advanced.releaseDraggableSelectors ? mCSB_dragger.add($(o.advanced.releaseDraggableSelectors)) : mCSB_dragger,
          eds = o.advanced.extraDraggableSelectors ? $(!_canAccessIFrame() || top.document).add($(o.advanced.extraDraggableSelectors)) : $(!_canAccessIFrame() || top.document);
        mCSB_dragger.bind("contextmenu." + namespace, function(e) {
          e.preventDefault()
        }).bind("mousedown." + namespace + " touchstart." + namespace + " pointerdown." + namespace + " MSPointerDown." + namespace, function(e) {
          e.stopImmediatePropagation();
          e.preventDefault();
          if (!_mouseBtnLeft(e)) {
            return
          }
          touchActive = !0;
          if (oldIE) {
            document.onselectstart = function() {
              return !1
            }
          }
          _iframe.call(mCSB_container, !1);
          _stop($this);
          draggable = $(this);
          var offset = draggable.offset(),
            y = _coordinates(e)[0] - offset.top,
            x = _coordinates(e)[1] - offset.left,
            h = draggable.height() + offset.top,
            w = draggable.width() + offset.left;
          if (y < h && y > 0 && x < w && x > 0) {
            dragY = y;
            dragX = x
          }
          _onDragClasses(draggable, "active", o.autoExpandScrollbar)
        }).bind("touchmove." + namespace, function(e) {
          e.stopImmediatePropagation();
          e.preventDefault();
          var offset = draggable.offset(),
            y = _coordinates(e)[0] - offset.top,
            x = _coordinates(e)[1] - offset.left;
          _drag(dragY, dragX, y, x)
        });
        $(document).add(eds).bind("mousemove." + namespace + " pointermove." + namespace + " MSPointerMove." + namespace, function(e) {
          if (draggable) {
            var offset = draggable.offset(),
              y = _coordinates(e)[0] - offset.top,
              x = _coordinates(e)[1] - offset.left;
            if (dragY === y && dragX === x) {
              return
            }
            _drag(dragY, dragX, y, x)
          }
        }).add(rds).bind("mouseup." + namespace + " touchend." + namespace + " pointerup." + namespace + " MSPointerUp." + namespace, function(e) {
          if (draggable) {
            _onDragClasses(draggable, "active", o.autoExpandScrollbar);
            draggable = null
          }
          touchActive = !1;
          if (oldIE) {
            document.onselectstart = null
          }
          _iframe.call(mCSB_container, !0)
        });

        function _drag(dragY, dragX, y, x) {
          mCSB_container[0].idleTimer = o.scrollInertia < 233 ? 250 : 0;
          if (draggable.attr("id") === draggerId[1]) {
            var dir = "x",
              to = ((draggable[0].offsetLeft - dragX) + x) * d.scrollRatio.x
          } else {
            var dir = "y",
              to = ((draggable[0].offsetTop - dragY) + y) * d.scrollRatio.y
          }
          _scrollTo($this, to.toString(), {
            dir: dir,
            drag: !0
          })
        }
      },
      _contentDraggable = function() {
        var $this = $(this),
          d = $this.data(pluginPfx),
          o = d.opt,
          namespace = pluginPfx + "_" + d.idx,
          mCustomScrollBox = $("#mCSB_" + d.idx),
          mCSB_container = $("#mCSB_" + d.idx + "_container"),
          mCSB_dragger = [$("#mCSB_" + d.idx + "_dragger_vertical"), $("#mCSB_" + d.idx + "_dragger_horizontal")],
          draggable, dragY, dragX, touchStartY, touchStartX, touchMoveY = [],
          touchMoveX = [],
          startTime, runningTime, endTime, distance, speed, amount, durA = 0,
          durB, overwrite = o.axis === "yx" ? "none" : "all",
          touchIntent = [],
          touchDrag, docDrag, iframe = mCSB_container.find("iframe"),
          events = ["touchstart." + namespace + " pointerdown." + namespace + " MSPointerDown." + namespace, "touchmove." + namespace + " pointermove." + namespace + " MSPointerMove." + namespace, "touchend." + namespace + " pointerup." + namespace + " MSPointerUp." + namespace],
          touchAction = document.body.style.touchAction !== undefined && document.body.style.touchAction !== "";
        mCSB_container.bind(events[0], function(e) {
          _onTouchstart(e)
        }).bind(events[1], function(e) {
          _onTouchmove(e)
        });
        mCustomScrollBox.bind(events[0], function(e) {
          _onTouchstart2(e)
        }).bind(events[2], function(e) {
          _onTouchend(e)
        });
        if (iframe.length) {
          iframe.each(function() {
            $(this).bind("load", function() {
              if (_canAccessIFrame(this)) {
                $(this.contentDocument || this.contentWindow.document).bind(events[0], function(e) {
                  _onTouchstart(e);
                  _onTouchstart2(e)
                }).bind(events[1], function(e) {
                  _onTouchmove(e)
                }).bind(events[2], function(e) {
                  _onTouchend(e)
                })
              }
            })
          })
        }

        function _onTouchstart(e) {
          if (!_pointerTouch(e) || touchActive || _coordinates(e)[2]) {
            touchable = 0;
            return
          }
          touchable = 1;
          touchDrag = 0;
          docDrag = 0;
          draggable = 1;
          $this.removeClass("mCS_touch_action");
          var offset = mCSB_container.offset();
          dragY = _coordinates(e)[0] - offset.top;
          dragX = _coordinates(e)[1] - offset.left;
          touchIntent = [_coordinates(e)[0], _coordinates(e)[1]]
        }

        function _onTouchmove(e) {
          if (!_pointerTouch(e) || touchActive || _coordinates(e)[2]) {
            return
          }
          if (!o.documentTouchScroll) {
            e.preventDefault()
          }
          e.stopImmediatePropagation();
          if (docDrag && !touchDrag) {
            return
          }
          if (draggable) {
            runningTime = _getTime();
            var offset = mCustomScrollBox.offset(),
              y = _coordinates(e)[0] - offset.top,
              x = _coordinates(e)[1] - offset.left,
              easing = "mcsLinearOut";
            touchMoveY.push(y);
            touchMoveX.push(x);
            touchIntent[2] = Math.abs(_coordinates(e)[0] - touchIntent[0]);
            touchIntent[3] = Math.abs(_coordinates(e)[1] - touchIntent[1]);
            if (d.overflowed[0]) {
              var limit = mCSB_dragger[0].parent().height() - mCSB_dragger[0].height(),
                prevent = ((dragY - y) > 0 && (y - dragY) > -(limit * d.scrollRatio.y) && (touchIntent[3] * 2 < touchIntent[2] || o.axis === "yx"))
            }
            if (d.overflowed[1]) {
              var limitX = mCSB_dragger[1].parent().width() - mCSB_dragger[1].width(),
                preventX = ((dragX - x) > 0 && (x - dragX) > -(limitX * d.scrollRatio.x) && (touchIntent[2] * 2 < touchIntent[3] || o.axis === "yx"))
            }
            if (prevent || preventX) {
              if (!touchAction) {
                e.preventDefault()
              }
              touchDrag = 1
            } else {
              docDrag = 1;
              $this.addClass("mCS_touch_action")
            }
            if (touchAction) {
              e.preventDefault()
            }
            amount = o.axis === "yx" ? [(dragY - y), (dragX - x)] : o.axis === "x" ? [null, (dragX - x)] : [(dragY - y), null];
            mCSB_container[0].idleTimer = 250;
            if (d.overflowed[0]) {
              _drag(amount[0], durA, easing, "y", "all", !0)
            }
            if (d.overflowed[1]) {
              _drag(amount[1], durA, easing, "x", overwrite, !0)
            }
          }
        }

        function _onTouchstart2(e) {
          if (!_pointerTouch(e) || touchActive || _coordinates(e)[2]) {
            touchable = 0;
            return
          }
          touchable = 1;
          e.stopImmediatePropagation();
          _stop($this);
          startTime = _getTime();
          var offset = mCustomScrollBox.offset();
          touchStartY = _coordinates(e)[0] - offset.top;
          touchStartX = _coordinates(e)[1] - offset.left;
          touchMoveY = [];
          touchMoveX = []
        }

        function _onTouchend(e) {
          if (!_pointerTouch(e) || touchActive || _coordinates(e)[2]) {
            return
          }
          draggable = 0;
          e.stopImmediatePropagation();
          touchDrag = 0;
          docDrag = 0;
          endTime = _getTime();
          var offset = mCustomScrollBox.offset(),
            y = _coordinates(e)[0] - offset.top,
            x = _coordinates(e)[1] - offset.left;
          if ((endTime - runningTime) > 30) {
            return
          }
          speed = 1000 / (endTime - startTime);
          var easing = "mcsEaseOut",
            slow = speed < 2.5,
            diff = slow ? [touchMoveY[touchMoveY.length - 2], touchMoveX[touchMoveX.length - 2]] : [0, 0];
          distance = slow ? [(y - diff[0]), (x - diff[1])] : [y - touchStartY, x - touchStartX];
          var absDistance = [Math.abs(distance[0]), Math.abs(distance[1])];
          speed = slow ? [Math.abs(distance[0] / 4), Math.abs(distance[1] / 4)] : [speed, speed];
          var a = [Math.abs(mCSB_container[0].offsetTop) - (distance[0] * _m((absDistance[0] / speed[0]), speed[0])), Math.abs(mCSB_container[0].offsetLeft) - (distance[1] * _m((absDistance[1] / speed[1]), speed[1]))];
          amount = o.axis === "yx" ? [a[0], a[1]] : o.axis === "x" ? [null, a[1]] : [a[0], null];
          durB = [(absDistance[0] * 4) + o.scrollInertia, (absDistance[1] * 4) + o.scrollInertia];
          var md = parseInt(o.contentTouchScroll) || 0;
          amount[0] = absDistance[0] > md ? amount[0] : 0;
          amount[1] = absDistance[1] > md ? amount[1] : 0;
          if (d.overflowed[0]) {
            _drag(amount[0], durB[0], easing, "y", overwrite, !1)
          }
          if (d.overflowed[1]) {
            _drag(amount[1], durB[1], easing, "x", overwrite, !1)
          }
        }

        function _m(ds, s) {
          var r = [s * 1.5, s * 2, s / 1.5, s / 2];
          if (ds > 90) {
            return s > 4 ? r[0] : r[3]
          } else if (ds > 60) {
            return s > 3 ? r[3] : r[2]
          } else if (ds > 30) {
            return s > 8 ? r[1] : s > 6 ? r[0] : s > 4 ? s : r[2]
          } else {
            return s > 8 ? s : r[3]
          }
        }

        function _drag(amount, dur, easing, dir, overwrite, drag) {
          if (!amount) {
            return
          }
          _scrollTo($this, amount.toString(), {
            dur: dur,
            scrollEasing: easing,
            dir: dir,
            overwrite: overwrite,
            drag: drag
          })
        }
      },
      _selectable = function() {
        var $this = $(this),
          d = $this.data(pluginPfx),
          o = d.opt,
          seq = d.sequential,
          namespace = pluginPfx + "_" + d.idx,
          mCSB_container = $("#mCSB_" + d.idx + "_container"),
          wrapper = mCSB_container.parent(),
          action;
        mCSB_container.bind("mousedown." + namespace, function(e) {
          if (touchable) {
            return
          }
          if (!action) {
            action = 1;
            touchActive = !0
          }
        }).add(document).bind("mousemove." + namespace, function(e) {
          if (!touchable && action && _sel()) {
            var offset = mCSB_container.offset(),
              y = _coordinates(e)[0] - offset.top + mCSB_container[0].offsetTop,
              x = _coordinates(e)[1] - offset.left + mCSB_container[0].offsetLeft;
            if (y > 0 && y < wrapper.height() && x > 0 && x < wrapper.width()) {
              if (seq.step) {
                _seq("off", null, "stepped")
              }
            } else {
              if (o.axis !== "x" && d.overflowed[0]) {
                if (y < 0) {
                  _seq("on", 38)
                } else if (y > wrapper.height()) {
                  _seq("on", 40)
                }
              }
              if (o.axis !== "y" && d.overflowed[1]) {
                if (x < 0) {
                  _seq("on", 37)
                } else if (x > wrapper.width()) {
                  _seq("on", 39)
                }
              }
            }
          }
        }).bind("mouseup." + namespace + " dragend." + namespace, function(e) {
          if (touchable) {
            return
          }
          if (action) {
            action = 0;
            _seq("off", null)
          }
          touchActive = !1
        });

        function _sel() {
          return window.getSelection ? window.getSelection().toString() : document.selection && document.selection.type != "Control" ? document.selection.createRange().text : 0
        }

        function _seq(a, c, s) {
          seq.type = s && action ? "stepped" : "stepless";
          seq.scrollAmount = 10;
          _sequentialScroll($this, a, c, "mcsLinearOut", s ? 60 : null)
        }
      },
      _mousewheel = function() {
        if (!$(this).data(pluginPfx)) {
          return
        }
        var $this = $(this),
          d = $this.data(pluginPfx),
          o = d.opt,
          namespace = pluginPfx + "_" + d.idx,
          mCustomScrollBox = $("#mCSB_" + d.idx),
          mCSB_dragger = [$("#mCSB_" + d.idx + "_dragger_vertical"), $("#mCSB_" + d.idx + "_dragger_horizontal")],
          iframe = $("#mCSB_" + d.idx + "_container").find("iframe");
        if (iframe.length) {
          iframe.each(function() {
            $(this).bind("load", function() {
              if (_canAccessIFrame(this)) {
                $(this.contentDocument || this.contentWindow.document).bind("mousewheel." + namespace, function(e, delta) {
                  _onMousewheel(e, delta)
                })
              }
            })
          })
        }
        mCustomScrollBox.bind("mousewheel." + namespace, function(e, delta) {
          _onMousewheel(e, delta)
        });

        function _onMousewheel(e, delta) {
          _stop($this);
          if (_disableMousewheel($this, e.target)) {
            return
          }
          var deltaFactor = o.mouseWheel.deltaFactor !== "auto" ? parseInt(o.mouseWheel.deltaFactor) : (oldIE && e.deltaFactor < 100) ? 100 : e.deltaFactor || 100,
            dur = o.scrollInertia;
          if (o.axis === "x" || o.mouseWheel.axis === "x") {
            var dir = "x",
              px = [Math.round(deltaFactor * d.scrollRatio.x), parseInt(o.mouseWheel.scrollAmount)],
              amount = o.mouseWheel.scrollAmount !== "auto" ? px[1] : px[0] >= mCustomScrollBox.width() ? mCustomScrollBox.width() * 0.9 : px[0],
              contentPos = Math.abs($("#mCSB_" + d.idx + "_container")[0].offsetLeft),
              draggerPos = mCSB_dragger[1][0].offsetLeft,
              limit = mCSB_dragger[1].parent().width() - mCSB_dragger[1].width(),
              dlt = o.mouseWheel.axis === "y" ? (e.deltaY || delta) : e.deltaX
          } else {
            var dir = "y",
              px = [Math.round(deltaFactor * d.scrollRatio.y), parseInt(o.mouseWheel.scrollAmount)],
              amount = o.mouseWheel.scrollAmount !== "auto" ? px[1] : px[0] >= mCustomScrollBox.height() ? mCustomScrollBox.height() * 0.9 : px[0],
              contentPos = Math.abs($("#mCSB_" + d.idx + "_container")[0].offsetTop),
              draggerPos = mCSB_dragger[0][0].offsetTop,
              limit = mCSB_dragger[0].parent().height() - mCSB_dragger[0].height(),
              dlt = e.deltaY || delta
          }
          if ((dir === "y" && !d.overflowed[0]) || (dir === "x" && !d.overflowed[1])) {
            return
          }
          if (o.mouseWheel.invert || e.webkitDirectionInvertedFromDevice) {
            dlt = -dlt
          }
          if (o.mouseWheel.normalizeDelta) {
            dlt = dlt < 0 ? -1 : 1
          }
          if ((dlt > 0 && draggerPos !== 0) || (dlt < 0 && draggerPos !== limit) || o.mouseWheel.preventDefault) {
            e.stopImmediatePropagation();
            e.preventDefault()
          }
          if (e.deltaFactor < 5 && !o.mouseWheel.normalizeDelta) {
            amount = e.deltaFactor;
            dur = 17
          }
          _scrollTo($this, (contentPos - (dlt * amount)).toString(), {
            dir: dir,
            dur: dur
          })
        }
      },
      _canAccessIFrameCache = new Object(),
      _canAccessIFrame = function(iframe) {
        var result = !1,
          cacheKey = !1,
          html = null;
        if (iframe === undefined) {
          cacheKey = "#empty"
        } else if ($(iframe).attr("id") !== undefined) {
          cacheKey = $(iframe).attr("id")
        }
        if (cacheKey !== !1 && _canAccessIFrameCache[cacheKey] !== undefined) {
          return _canAccessIFrameCache[cacheKey]
        }
        if (!iframe) {
          try {
            var doc = top.document;
            html = doc.body.innerHTML
          } catch (err) {}
          result = (html !== null)
        } else {
          try {
            var doc = iframe.contentDocument || iframe.contentWindow.document;
            html = doc.body.innerHTML
          } catch (err) {}
          result = (html !== null)
        }
        if (cacheKey !== !1) {
          _canAccessIFrameCache[cacheKey] = result
        }
        return result
      },
      _iframe = function(evt) {
        var el = this.find("iframe");
        if (!el.length) {
          return
        }
        var val = !evt ? "none" : "auto";
        el.css("pointer-events", val)
      },
      _disableMousewheel = function(el, target) {
        var tag = target.nodeName.toLowerCase(),
          tags = el.data(pluginPfx).opt.mouseWheel.disableOver,
          focusTags = ["select", "textarea"];
        return $.inArray(tag, tags) > -1 && !($.inArray(tag, focusTags) > -1 && !$(target).is(":focus"))
      },
      _draggerRail = function() {
        var $this = $(this),
          d = $this.data(pluginPfx),
          namespace = pluginPfx + "_" + d.idx,
          mCSB_container = $("#mCSB_" + d.idx + "_container"),
          wrapper = mCSB_container.parent(),
          mCSB_draggerContainer = $(".mCSB_" + d.idx + "_scrollbar ." + classes[12]),
          clickable;
        mCSB_draggerContainer.bind("mousedown." + namespace + " touchstart." + namespace + " pointerdown." + namespace + " MSPointerDown." + namespace, function(e) {
          touchActive = !0;
          if (!$(e.target).hasClass("mCSB_dragger")) {
            clickable = 1
          }
        }).bind("touchend." + namespace + " pointerup." + namespace + " MSPointerUp." + namespace, function(e) {
          touchActive = !1
        }).bind("click." + namespace, function(e) {
          if (!clickable) {
            return
          }
          clickable = 0;
          if ($(e.target).hasClass(classes[12]) || $(e.target).hasClass("mCSB_draggerRail")) {
            _stop($this);
            var el = $(this),
              mCSB_dragger = el.find(".mCSB_dragger");
            if (el.parent(".mCSB_scrollTools_horizontal").length > 0) {
              if (!d.overflowed[1]) {
                return
              }
              var dir = "x",
                clickDir = e.pageX > mCSB_dragger.offset().left ? -1 : 1,
                to = Math.abs(mCSB_container[0].offsetLeft) - (clickDir * (wrapper.width() * 0.9))
            } else {
              if (!d.overflowed[0]) {
                return
              }
              var dir = "y",
                clickDir = e.pageY > mCSB_dragger.offset().top ? -1 : 1,
                to = Math.abs(mCSB_container[0].offsetTop) - (clickDir * (wrapper.height() * 0.9))
            }
            _scrollTo($this, to.toString(), {
              dir: dir,
              scrollEasing: "mcsEaseInOut"
            })
          }
        })
      },
      _focus = function() {
        var $this = $(this),
          d = $this.data(pluginPfx),
          o = d.opt,
          namespace = pluginPfx + "_" + d.idx,
          mCSB_container = $("#mCSB_" + d.idx + "_container"),
          wrapper = mCSB_container.parent();
        mCSB_container.bind("focusin." + namespace, function(e) {
          var el = $(document.activeElement),
            nested = mCSB_container.find(".mCustomScrollBox").length,
            dur = 0;
          if (!el.is(o.advanced.autoScrollOnFocus)) {
            return
          }
          _stop($this);
          clearTimeout($this[0]._focusTimeout);
          $this[0]._focusTimer = nested ? (dur + 17) * nested : 0;
          $this[0]._focusTimeout = setTimeout(function() {
            var to = [_childPos(el)[0], _childPos(el)[1]],
              contentPos = [mCSB_container[0].offsetTop, mCSB_container[0].offsetLeft],
              isVisible = [(contentPos[0] + to[0] >= 0 && contentPos[0] + to[0] < wrapper.height() - el.outerHeight(!1)), (contentPos[1] + to[1] >= 0 && contentPos[0] + to[1] < wrapper.width() - el.outerWidth(!1))],
              overwrite = (o.axis === "yx" && !isVisible[0] && !isVisible[1]) ? "none" : "all";
            if (o.axis !== "x" && !isVisible[0]) {
              _scrollTo($this, to[0].toString(), {
                dir: "y",
                scrollEasing: "mcsEaseInOut",
                overwrite: overwrite,
                dur: dur
              })
            }
            if (o.axis !== "y" && !isVisible[1]) {
              _scrollTo($this, to[1].toString(), {
                dir: "x",
                scrollEasing: "mcsEaseInOut",
                overwrite: overwrite,
                dur: dur
              })
            }
          }, $this[0]._focusTimer)
        })
      },
      _wrapperScroll = function() {
        var $this = $(this),
          d = $this.data(pluginPfx),
          namespace = pluginPfx + "_" + d.idx,
          wrapper = $("#mCSB_" + d.idx + "_container").parent();
        wrapper.bind("scroll." + namespace, function(e) {
          if (wrapper.scrollTop() !== 0 || wrapper.scrollLeft() !== 0) {
            $(".mCSB_" + d.idx + "_scrollbar").css("visibility", "hidden")
          }
        })
      },
      _buttons = function() {
        var $this = $(this),
          d = $this.data(pluginPfx),
          o = d.opt,
          seq = d.sequential,
          namespace = pluginPfx + "_" + d.idx,
          sel = ".mCSB_" + d.idx + "_scrollbar",
          btn = $(sel + ">a");
        btn.bind("contextmenu." + namespace, function(e) {
          e.preventDefault()
        }).bind("mousedown." + namespace + " touchstart." + namespace + " pointerdown." + namespace + " MSPointerDown." + namespace + " mouseup." + namespace + " touchend." + namespace + " pointerup." + namespace + " MSPointerUp." + namespace + " mouseout." + namespace + " pointerout." + namespace + " MSPointerOut." + namespace + " click." + namespace, function(e) {
          e.preventDefault();
          if (!_mouseBtnLeft(e)) {
            return
          }
          var btnClass = $(this).attr("class");
          seq.type = o.scrollButtons.scrollType;
          switch (e.type) {
            case "mousedown":
            case "touchstart":
            case "pointerdown":
            case "MSPointerDown":
              if (seq.type === "stepped") {
                return
              }
              touchActive = !0;
              d.tweenRunning = !1;
              _seq("on", btnClass);
              break;
            case "mouseup":
            case "touchend":
            case "pointerup":
            case "MSPointerUp":
            case "mouseout":
            case "pointerout":
            case "MSPointerOut":
              if (seq.type === "stepped") {
                return
              }
              touchActive = !1;
              if (seq.dir) {
                _seq("off", btnClass)
              }
              break;
            case "click":
              if (seq.type !== "stepped" || d.tweenRunning) {
                return
              }
              _seq("on", btnClass);
              break
          }

          function _seq(a, c) {
            seq.scrollAmount = o.scrollButtons.scrollAmount;
            _sequentialScroll($this, a, c)
          }
        })
      },
      _keyboard = function() {
        var $this = $(this),
          d = $this.data(pluginPfx),
          o = d.opt,
          seq = d.sequential,
          namespace = pluginPfx + "_" + d.idx,
          mCustomScrollBox = $("#mCSB_" + d.idx),
          mCSB_container = $("#mCSB_" + d.idx + "_container"),
          wrapper = mCSB_container.parent(),
          editables = "input,textarea,select,datalist,keygen,[contenteditable='true']",
          iframe = mCSB_container.find("iframe"),
          events = ["blur." + namespace + " keydown." + namespace + " keyup." + namespace];
        if (iframe.length) {
          iframe.each(function() {
            $(this).bind("load", function() {
              if (_canAccessIFrame(this)) {
                $(this.contentDocument || this.contentWindow.document).bind(events[0], function(e) {
                  _onKeyboard(e)
                })
              }
            })
          })
        }
        mCustomScrollBox.attr("tabindex", "0").bind(events[0], function(e) {
          _onKeyboard(e)
        });

        function _onKeyboard(e) {
          switch (e.type) {
            case "blur":
              if (d.tweenRunning && seq.dir) {
                _seq("off", null)
              }
              break;
            case "keydown":
            case "keyup":
              var code = e.keyCode ? e.keyCode : e.which,
                action = "on";
              if ((o.axis !== "x" && (code === 38 || code === 40)) || (o.axis !== "y" && (code === 37 || code === 39))) {
                if (((code === 38 || code === 40) && !d.overflowed[0]) || ((code === 37 || code === 39) && !d.overflowed[1])) {
                  return
                }
                if (e.type === "keyup") {
                  action = "off"
                }
                if (!$(document.activeElement).is(editables)) {
                  e.preventDefault();
                  e.stopImmediatePropagation();
                  _seq(action, code)
                }
              } else if (code === 33 || code === 34) {
                if (d.overflowed[0] || d.overflowed[1]) {
                  e.preventDefault();
                  e.stopImmediatePropagation()
                }
                if (e.type === "keyup") {
                  _stop($this);
                  var keyboardDir = code === 34 ? -1 : 1;
                  if (o.axis === "x" || (o.axis === "yx" && d.overflowed[1] && !d.overflowed[0])) {
                    var dir = "x",
                      to = Math.abs(mCSB_container[0].offsetLeft) - (keyboardDir * (wrapper.width() * 0.9))
                  } else {
                    var dir = "y",
                      to = Math.abs(mCSB_container[0].offsetTop) - (keyboardDir * (wrapper.height() * 0.9))
                  }
                  _scrollTo($this, to.toString(), {
                    dir: dir,
                    scrollEasing: "mcsEaseInOut"
                  })
                }
              } else if (code === 35 || code === 36) {
                if (!$(document.activeElement).is(editables)) {
                  if (d.overflowed[0] || d.overflowed[1]) {
                    e.preventDefault();
                    e.stopImmediatePropagation()
                  }
                  if (e.type === "keyup") {
                    if (o.axis === "x" || (o.axis === "yx" && d.overflowed[1] && !d.overflowed[0])) {
                      var dir = "x",
                        to = code === 35 ? Math.abs(wrapper.width() - mCSB_container.outerWidth(!1)) : 0
                    } else {
                      var dir = "y",
                        to = code === 35 ? Math.abs(wrapper.height() - mCSB_container.outerHeight(!1)) : 0
                    }
                    _scrollTo($this, to.toString(), {
                      dir: dir,
                      scrollEasing: "mcsEaseInOut"
                    })
                  }
                }
              }
              break
          }

          function _seq(a, c) {
            seq.type = o.keyboard.scrollType;
            seq.scrollAmount = o.keyboard.scrollAmount;
            if (seq.type === "stepped" && d.tweenRunning) {
              return
            }
            _sequentialScroll($this, a, c)
          }
        }
      },
      _sequentialScroll = function(el, action, trigger, e, s) {
        var d = el.data(pluginPfx),
          o = d.opt,
          seq = d.sequential,
          mCSB_container = $("#mCSB_" + d.idx + "_container"),
          once = seq.type === "stepped" ? !0 : !1,
          steplessSpeed = o.scrollInertia < 26 ? 26 : o.scrollInertia,
          steppedSpeed = o.scrollInertia < 1 ? 17 : o.scrollInertia;
        switch (action) {
          case "on":
            seq.dir = [(trigger === classes[16] || trigger === classes[15] || trigger === 39 || trigger === 37 ? "x" : "y"), (trigger === classes[13] || trigger === classes[15] || trigger === 38 || trigger === 37 ? -1 : 1)];
            _stop(el);
            if (_isNumeric(trigger) && seq.type === "stepped") {
              return
            }
            _on(once);
            break;
          case "off":
            _off();
            if (once || (d.tweenRunning && seq.dir)) {
              _on(!0)
            }
            break
        }

        function _on(once) {
          if (o.snapAmount) {
            seq.scrollAmount = !(o.snapAmount instanceof Array) ? o.snapAmount : seq.dir[0] === "x" ? o.snapAmount[1] : o.snapAmount[0]
          }
          var c = seq.type !== "stepped",
            t = s ? s : !once ? 1000 / 60 : c ? steplessSpeed / 1.5 : steppedSpeed,
            m = !once ? 2.5 : c ? 7.5 : 40,
            contentPos = [Math.abs(mCSB_container[0].offsetTop), Math.abs(mCSB_container[0].offsetLeft)],
            ratio = [d.scrollRatio.y > 10 ? 10 : d.scrollRatio.y, d.scrollRatio.x > 10 ? 10 : d.scrollRatio.x],
            amount = seq.dir[0] === "x" ? contentPos[1] + (seq.dir[1] * (ratio[1] * m)) : contentPos[0] + (seq.dir[1] * (ratio[0] * m)),
            px = seq.dir[0] === "x" ? contentPos[1] + (seq.dir[1] * parseInt(seq.scrollAmount)) : contentPos[0] + (seq.dir[1] * parseInt(seq.scrollAmount)),
            to = seq.scrollAmount !== "auto" ? px : amount,
            easing = e ? e : !once ? "mcsLinear" : c ? "mcsLinearOut" : "mcsEaseInOut",
            onComplete = !once ? !1 : !0;
          if (once && t < 17) {
            to = seq.dir[0] === "x" ? contentPos[1] : contentPos[0]
          }
          _scrollTo(el, to.toString(), {
            dir: seq.dir[0],
            scrollEasing: easing,
            dur: t,
            onComplete: onComplete
          });
          if (once) {
            seq.dir = !1;
            return
          }
          clearTimeout(seq.step);
          seq.step = setTimeout(function() {
            _on()
          }, t)
        }

        function _off() {
          clearTimeout(seq.step);
          _delete(seq, "step");
          _stop(el)
        }
      },
      _arr = function(val) {
        var o = $(this).data(pluginPfx).opt,
          vals = [];
        if (typeof val === "function") {
          val = val()
        }
        if (!(val instanceof Array)) {
          vals[0] = val.y ? val.y : val.x || o.axis === "x" ? null : val;
          vals[1] = val.x ? val.x : val.y || o.axis === "y" ? null : val
        } else {
          vals = val.length > 1 ? [val[0], val[1]] : o.axis === "x" ? [null, val[0]] : [val[0], null]
        }
        if (typeof vals[0] === "function") {
          vals[0] = vals[0]()
        }
        if (typeof vals[1] === "function") {
          vals[1] = vals[1]()
        }
        return vals
      },
      _to = function(val, dir) {
        if (val == null || typeof val == "undefined") {
          return
        }
        var $this = $(this),
          d = $this.data(pluginPfx),
          o = d.opt,
          mCSB_container = $("#mCSB_" + d.idx + "_container"),
          wrapper = mCSB_container.parent(),
          t = typeof val;
        if (!dir) {
          dir = o.axis === "x" ? "x" : "y"
        }
        var contentLength = dir === "x" ? mCSB_container.outerWidth(!1) - wrapper.width() : mCSB_container.outerHeight(!1) - wrapper.height(),
          contentPos = dir === "x" ? mCSB_container[0].offsetLeft : mCSB_container[0].offsetTop,
          cssProp = dir === "x" ? "left" : "top";
        switch (t) {
          case "function":
            return val();
            break;
          case "object":
            var obj = val.jquery ? val : $(val);
            if (!obj.length) {
              return
            }
            return dir === "x" ? _childPos(obj)[1] : _childPos(obj)[0];
            break;
          case "string":
          case "number":
            if (_isNumeric(val)) {
              return Math.abs(val)
            } else if (val.indexOf("%") !== -1) {
              return Math.abs(contentLength * parseInt(val) / 100)
            } else if (val.indexOf("-=") !== -1) {
              return Math.abs(contentPos - parseInt(val.split("-=")[1]))
            } else if (val.indexOf("+=") !== -1) {
              var p = (contentPos + parseInt(val.split("+=")[1]));
              return p >= 0 ? 0 : Math.abs(p)
            } else if (val.indexOf("px") !== -1 && _isNumeric(val.split("px")[0])) {
              return Math.abs(val.split("px")[0])
            } else {
              if (val === "top" || val === "left") {
                return 0
              } else if (val === "bottom") {
                return Math.abs(wrapper.height() - mCSB_container.outerHeight(!1))
              } else if (val === "right") {
                return Math.abs(wrapper.width() - mCSB_container.outerWidth(!1))
              } else if (val === "first" || val === "last") {
                var obj = mCSB_container.find(":" + val);
                return dir === "x" ? _childPos(obj)[1] : _childPos(obj)[0]
              } else {
                if ($(val).length) {
                  return dir === "x" ? _childPos($(val))[1] : _childPos($(val))[0]
                } else {
                  mCSB_container.css(cssProp, val);
                  methods.update.call(null, $this[0]);
                  return
                }
              }
            }
            break
        }
      },
      _autoUpdate = function(rem) {
        var $this = $(this),
          d = $this.data(pluginPfx),
          o = d.opt,
          mCSB_container = $("#mCSB_" + d.idx + "_container");
        if (rem) {
          clearTimeout(mCSB_container[0].autoUpdate);
          _delete(mCSB_container[0], "autoUpdate");
          return
        }
        upd();

        function upd() {
          clearTimeout(mCSB_container[0].autoUpdate);
          if ($this.parents("html").length === 0) {
            $this = null;
            return
          }
          mCSB_container[0].autoUpdate = setTimeout(function() {
            if (o.advanced.updateOnSelectorChange) {
              d.poll.change.n = sizesSum();
              if (d.poll.change.n !== d.poll.change.o) {
                d.poll.change.o = d.poll.change.n;
                doUpd(3);
                return
              }
            }
            if (o.advanced.updateOnContentResize) {
              d.poll.size.n = $this[0].scrollHeight + $this[0].scrollWidth + mCSB_container[0].offsetHeight + $this[0].offsetHeight + $this[0].offsetWidth;
              if (d.poll.size.n !== d.poll.size.o) {
                d.poll.size.o = d.poll.size.n;
                doUpd(1);
                return
              }
            }
            if (o.advanced.updateOnImageLoad) {
              if (!(o.advanced.updateOnImageLoad === "auto" && o.axis === "y")) {
                d.poll.img.n = mCSB_container.find("img").length;
                if (d.poll.img.n !== d.poll.img.o) {
                  d.poll.img.o = d.poll.img.n;
                  mCSB_container.find("img").each(function() {
                    imgLoader(this)
                  });
                  return
                }
              }
            }
            if (o.advanced.updateOnSelectorChange || o.advanced.updateOnContentResize || o.advanced.updateOnImageLoad) {
              upd()
            }
          }, o.advanced.autoUpdateTimeout)
        }

        function imgLoader(el) {
          if ($(el).hasClass(classes[2])) {
            doUpd();
            return
          }
          var img = new Image();

          function createDelegate(contextObject, delegateMethod) {
            return function() {
              return delegateMethod.apply(contextObject, arguments)
            }
          }

          function imgOnLoad() {
            this.onload = null;
            $(el).addClass(classes[2]);
            doUpd(2)
          }
          img.onload = createDelegate(img, imgOnLoad);
          img.src = el.src
        }

        function sizesSum() {
          if (o.advanced.updateOnSelectorChange === !0) {
            o.advanced.updateOnSelectorChange = "*"
          }
          var total = 0,
            sel = mCSB_container.find(o.advanced.updateOnSelectorChange);
          if (o.advanced.updateOnSelectorChange && sel.length > 0) {
            sel.each(function() {
              total += this.offsetHeight + this.offsetWidth
            })
          }
          return total
        }

        function doUpd(cb) {
          clearTimeout(mCSB_container[0].autoUpdate);
          methods.update.call(null, $this[0], cb)
        }
      },
      _snapAmount = function(to, amount, offset) {
        return (Math.round(to / amount) * amount - offset)
      },
      _stop = function(el) {
        var d = el.data(pluginPfx),
          sel = $("#mCSB_" + d.idx + "_container,#mCSB_" + d.idx + "_container_wrapper,#mCSB_" + d.idx + "_dragger_vertical,#mCSB_" + d.idx + "_dragger_horizontal");
        sel.each(function() {
          _stopTween.call(this)
        })
      },
      _scrollTo = function(el, to, options) {
        var d = el.data(pluginPfx),
          o = d.opt,
          defaults = {
            trigger: "internal",
            dir: "y",
            scrollEasing: "mcsEaseOut",
            drag: !1,
            dur: o.scrollInertia,
            overwrite: "all",
            callbacks: !0,
            onStart: !0,
            onUpdate: !0,
            onComplete: !0
          },
          options = $.extend(defaults, options),
          dur = [options.dur, (options.drag ? 0 : options.dur)],
          mCustomScrollBox = $("#mCSB_" + d.idx),
          mCSB_container = $("#mCSB_" + d.idx + "_container"),
          wrapper = mCSB_container.parent(),
          totalScrollOffsets = o.callbacks.onTotalScrollOffset ? _arr.call(el, o.callbacks.onTotalScrollOffset) : [0, 0],
          totalScrollBackOffsets = o.callbacks.onTotalScrollBackOffset ? _arr.call(el, o.callbacks.onTotalScrollBackOffset) : [0, 0];
        d.trigger = options.trigger;
        if (wrapper.scrollTop() !== 0 || wrapper.scrollLeft() !== 0) {
          $(".mCSB_" + d.idx + "_scrollbar").css("visibility", "visible");
          wrapper.scrollTop(0).scrollLeft(0)
        }
        if (to === "_resetY" && !d.contentReset.y) {
          if (_cb("onOverflowYNone")) {
            o.callbacks.onOverflowYNone.call(el[0])
          }
          d.contentReset.y = 1
        }
        if (to === "_resetX" && !d.contentReset.x) {
          if (_cb("onOverflowXNone")) {
            o.callbacks.onOverflowXNone.call(el[0])
          }
          d.contentReset.x = 1
        }
        if (to === "_resetY" || to === "_resetX") {
          return
        }
        if ((d.contentReset.y || !el[0].mcs) && d.overflowed[0]) {
          if (_cb("onOverflowY")) {
            o.callbacks.onOverflowY.call(el[0])
          }
          d.contentReset.x = null
        }
        if ((d.contentReset.x || !el[0].mcs) && d.overflowed[1]) {
          if (_cb("onOverflowX")) {
            o.callbacks.onOverflowX.call(el[0])
          }
          d.contentReset.x = null
        }
        if (o.snapAmount) {
          var snapAmount = !(o.snapAmount instanceof Array) ? o.snapAmount : options.dir === "x" ? o.snapAmount[1] : o.snapAmount[0];
          to = _snapAmount(to, snapAmount, o.snapOffset)
        }
        switch (options.dir) {
          case "x":
            var mCSB_dragger = $("#mCSB_" + d.idx + "_dragger_horizontal"),
              property = "left",
              contentPos = mCSB_container[0].offsetLeft,
              limit = [mCustomScrollBox.width() - mCSB_container.outerWidth(!1), mCSB_dragger.parent().width() - mCSB_dragger.width()],
              scrollTo = [to, to === 0 ? 0 : (to / d.scrollRatio.x)],
              tso = totalScrollOffsets[1],
              tsbo = totalScrollBackOffsets[1],
              totalScrollOffset = tso > 0 ? tso / d.scrollRatio.x : 0,
              totalScrollBackOffset = tsbo > 0 ? tsbo / d.scrollRatio.x : 0;
            break;
          case "y":
            var mCSB_dragger = $("#mCSB_" + d.idx + "_dragger_vertical"),
              property = "top",
              contentPos = mCSB_container[0].offsetTop,
              limit = [mCustomScrollBox.height() - mCSB_container.outerHeight(!1), mCSB_dragger.parent().height() - mCSB_dragger.height()],
              scrollTo = [to, to === 0 ? 0 : (to / d.scrollRatio.y)],
              tso = totalScrollOffsets[0],
              tsbo = totalScrollBackOffsets[0],
              totalScrollOffset = tso > 0 ? tso / d.scrollRatio.y : 0,
              totalScrollBackOffset = tsbo > 0 ? tsbo / d.scrollRatio.y : 0;
            break
        }
        if (scrollTo[1] < 0 || (scrollTo[0] === 0 && scrollTo[1] === 0)) {
          scrollTo = [0, 0]
        } else if (scrollTo[1] >= limit[1]) {
          scrollTo = [limit[0], limit[1]]
        } else {
          scrollTo[0] = -scrollTo[0]
        }
        if (!el[0].mcs) {
          _mcs();
          if (_cb("onInit")) {
            o.callbacks.onInit.call(el[0])
          }
        }
        clearTimeout(mCSB_container[0].onCompleteTimeout);
        _tweenTo(mCSB_dragger[0], property, Math.round(scrollTo[1]), dur[1], options.scrollEasing);
        if (!d.tweenRunning && ((contentPos === 0 && scrollTo[0] >= 0) || (contentPos === limit[0] && scrollTo[0] <= limit[0]))) {
          return
        }
        _tweenTo(mCSB_container[0], property, Math.round(scrollTo[0]), dur[0], options.scrollEasing, options.overwrite, {
          onStart: function() {
            if (options.callbacks && options.onStart && !d.tweenRunning) {
              if (_cb("onScrollStart")) {
                _mcs();
                o.callbacks.onScrollStart.call(el[0])
              }
              d.tweenRunning = !0;
              _onDragClasses(mCSB_dragger);
              d.cbOffsets = _cbOffsets()
            }
          },
          onUpdate: function() {
            if (options.callbacks && options.onUpdate) {
              if (_cb("whileScrolling")) {
                _mcs();
                o.callbacks.whileScrolling.call(el[0])
              }
            }
          },
          onComplete: function() {
            if (options.callbacks && options.onComplete) {
              if (o.axis === "yx") {
                clearTimeout(mCSB_container[0].onCompleteTimeout)
              }
              var t = mCSB_container[0].idleTimer || 0;
              mCSB_container[0].onCompleteTimeout = setTimeout(function() {
                if (_cb("onScroll")) {
                  _mcs();
                  o.callbacks.onScroll.call(el[0])
                }
                if (_cb("onTotalScroll") && scrollTo[1] >= limit[1] - totalScrollOffset && d.cbOffsets[0]) {
                  _mcs();
                  o.callbacks.onTotalScroll.call(el[0])
                }
                if (_cb("onTotalScrollBack") && scrollTo[1] <= totalScrollBackOffset && d.cbOffsets[1]) {
                  _mcs();
                  o.callbacks.onTotalScrollBack.call(el[0])
                }
                d.tweenRunning = !1;
                mCSB_container[0].idleTimer = 0;
                _onDragClasses(mCSB_dragger, "hide")
              }, t)
            }
          }
        });

        function _cb(cb) {
          return d && o.callbacks[cb] && typeof o.callbacks[cb] === "function"
        }

        function _cbOffsets() {
          return [o.callbacks.alwaysTriggerOffsets || contentPos >= limit[0] + tso, o.callbacks.alwaysTriggerOffsets || contentPos <= -tsbo]
        }

        function _mcs() {
          var cp = [mCSB_container[0].offsetTop, mCSB_container[0].offsetLeft],
            dp = [mCSB_dragger[0].offsetTop, mCSB_dragger[0].offsetLeft],
            cl = [mCSB_container.outerHeight(!1), mCSB_container.outerWidth(!1)],
            pl = [mCustomScrollBox.height(), mCustomScrollBox.width()];
          el[0].mcs = {
            content: mCSB_container,
            top: cp[0],
            left: cp[1],
            draggerTop: dp[0],
            draggerLeft: dp[1],
            topPct: Math.round((100 * Math.abs(cp[0])) / (Math.abs(cl[0]) - pl[0])),
            leftPct: Math.round((100 * Math.abs(cp[1])) / (Math.abs(cl[1]) - pl[1])),
            direction: options.dir
          }
        }
      },
      _tweenTo = function(el, prop, to, duration, easing, overwrite, callbacks) {
        if (!el._mTween) {
          el._mTween = {
            top: {},
            left: {}
          }
        }
        var callbacks = callbacks || {},
          onStart = callbacks.onStart || function() {},
          onUpdate = callbacks.onUpdate || function() {},
          onComplete = callbacks.onComplete || function() {},
          startTime = _getTime(),
          _delay, progress = 0,
          from = el.offsetTop,
          elStyle = el.style,
          _request, tobj = el._mTween[prop];
        if (prop === "left") {
          from = el.offsetLeft
        }
        var diff = to - from;
        tobj.stop = 0;
        if (overwrite !== "none") {
          _cancelTween()
        }
        _startTween();

        function _step() {
          if (tobj.stop) {
            return
          }
          if (!progress) {
            onStart.call()
          }
          progress = _getTime() - startTime;
          _tween();
          if (progress >= tobj.time) {
            tobj.time = (progress > tobj.time) ? progress + _delay - (progress - tobj.time) : progress + _delay - 1;
            if (tobj.time < progress + 1) {
              tobj.time = progress + 1
            }
          }
          if (tobj.time < duration) {
            tobj.id = _request(_step)
          } else {
            onComplete.call()
          }
        }

        function _tween() {
          if (duration > 0) {
            tobj.currVal = _ease(tobj.time, from, diff, duration, easing);
            elStyle[prop] = Math.round(tobj.currVal) + "px"
          } else {
            elStyle[prop] = to + "px"
          }
          onUpdate.call()
        }

        function _startTween() {
          _delay = 1000 / 60;
          tobj.time = progress + _delay;
          _request = (!window.requestAnimationFrame) ? function(f) {
            _tween();
            return setTimeout(f, 0.01)
          } : window.requestAnimationFrame;
          tobj.id = _request(_step)
        }

        function _cancelTween() {
          if (tobj.id == null) {
            return
          }
          if (!window.requestAnimationFrame) {
            clearTimeout(tobj.id)
          } else {
            window.cancelAnimationFrame(tobj.id)
          }
          tobj.id = null
        }

        function _ease(t, b, c, d, type) {
          switch (type) {
            case "linear":
            case "mcsLinear":
              return c * t / d + b;
              break;
            case "mcsLinearOut":
              t /= d;
              t--;
              return c * Math.sqrt(1 - t * t) + b;
              break;
            case "easeInOutSmooth":
              t /= d / 2;
              if (t < 1) return c / 2 * t * t + b;
              t--;
              return -c / 2 * (t * (t - 2) - 1) + b;
              break;
            case "easeInOutStrong":
              t /= d / 2;
              if (t < 1) return c / 2 * Math.pow(2, 10 * (t - 1)) + b;
              t--;
              return c / 2 * (-Math.pow(2, -10 * t) + 2) + b;
              break;
            case "easeInOut":
            case "mcsEaseInOut":
              t /= d / 2;
              if (t < 1) return c / 2 * t * t * t + b;
              t -= 2;
              return c / 2 * (t * t * t + 2) + b;
              break;
            case "easeOutSmooth":
              t /= d;
              t--;
              return -c * (t * t * t * t - 1) + b;
              break;
            case "easeOutStrong":
              return c * (-Math.pow(2, -10 * t / d) + 1) + b;
              break;
            case "easeOut":
            case "mcsEaseOut":
            default:
              var ts = (t /= d) * t,
                tc = ts * t;
              return b + c * (0.499999999999997 * tc * ts + -2.5 * ts * ts + 5.5 * tc + -6.5 * ts + 4 * t)
          }
        }
      },
      _getTime = function() {
        if (window.performance && window.performance.now) {
          return window.performance.now()
        } else {
          if (window.performance && window.performance.webkitNow) {
            return window.performance.webkitNow()
          } else {
            if (Date.now) {
              return Date.now()
            } else {
              return new Date().getTime()
            }
          }
        }
      },
      _stopTween = function() {
        var el = this;
        if (!el._mTween) {
          el._mTween = {
            top: {},
            left: {}
          }
        }
        var props = ["top", "left"];
        for (var i = 0; i < props.length; i++) {
          var prop = props[i];
          if (el._mTween[prop].id) {
            if (!window.requestAnimationFrame) {
              clearTimeout(el._mTween[prop].id)
            } else {
              window.cancelAnimationFrame(el._mTween[prop].id)
            }
            el._mTween[prop].id = null;
            el._mTween[prop].stop = 1
          }
        }
      },
      _delete = function(c, m) {
        try {
          delete c[m]
        } catch (e) {
          c[m] = null
        }
      },
      _mouseBtnLeft = function(e) {
        return !(e.which && e.which !== 1)
      },
      _pointerTouch = function(e) {
        var t = e.originalEvent.pointerType;
        return !(t && t !== "touch" && t !== 2)
      },
      _isNumeric = function(val) {
        return !isNaN(parseFloat(val)) && isFinite(val)
      },
      _childPos = function(el) {
        var p = el.parents(".mCSB_container");
        return [el.offset().top - p.offset().top, el.offset().left - p.offset().left]
      },
      _isTabHidden = function() {
        var prop = _getHiddenProp();
        if (!prop) return !1;
        return document[prop];

        function _getHiddenProp() {
          var pfx = ["webkit", "moz", "ms", "o"];
          if ("hidden" in document) return "hidden";
          for (var i = 0; i < pfx.length; i++) {
            if ((pfx[i] + "Hidden") in document)
              return pfx[i] + "Hidden"
          }
          return null
        }
      };
    $.fn[pluginNS] = function(method) {
      if (methods[method]) {
        return methods[method].apply(this, Array.prototype.slice.call(arguments, 1))
      } else if (typeof method === "object" || !method) {
        return methods.init.apply(this, arguments)
      } else {
        $.error("Method " + method + " does not exist")
      }
    };
    $[pluginNS] = function(method) {
      if (methods[method]) {
        return methods[method].apply(this, Array.prototype.slice.call(arguments, 1))
      } else if (typeof method === "object" || !method) {
        return methods.init.apply(this, arguments)
      } else {
        $.error("Method " + method + " does not exist")
      }
    };
    $[pluginNS].defaults = defaults;
    window[pluginNS] = !0;
    $(window).bind("load", function() {
      $(defaultSelector)[pluginNS]();
      $.extend($.expr[":"], {
        mcsInView: $.expr[":"].mcsInView || function(el) {
          var $el = $(el),
            content = $el.parents(".mCSB_container"),
            wrapper, cPos;
          if (!content.length) {
            return
          }
          wrapper = content.parent();
          cPos = [content[0].offsetTop, content[0].offsetLeft];
          return cPos[0] + _childPos($el)[0] >= 0 && cPos[0] + _childPos($el)[0] < wrapper.height() - $el.outerHeight(!1) && cPos[1] + _childPos($el)[1] >= 0 && cPos[1] + _childPos($el)[1] < wrapper.width() - $el.outerWidth(!1)
        },
        mcsInSight: $.expr[":"].mcsInSight || function(el, i, m) {
          var $el = $(el),
            elD, content = $el.parents(".mCSB_container"),
            wrapperView, pos, wrapperViewPct, pctVals = m[3] === "exact" ? [
              [1, 0],
              [1, 0]
            ] : [
              [0.9, 0.1],
              [0.6, 0.4]
            ];
          if (!content.length) {
            return
          }
          elD = [$el.outerHeight(!1), $el.outerWidth(!1)];
          pos = [content[0].offsetTop + _childPos($el)[0], content[0].offsetLeft + _childPos($el)[1]];
          wrapperView = [content.parent()[0].offsetHeight, content.parent()[0].offsetWidth];
          wrapperViewPct = [elD[0] < wrapperView[0] ? pctVals[0] : pctVals[1], elD[1] < wrapperView[1] ? pctVals[0] : pctVals[1]];
          return pos[0] - (wrapperView[0] * wrapperViewPct[0][0]) < 0 && pos[0] + elD[0] - (wrapperView[0] * wrapperViewPct[0][1]) >= 0 && pos[1] - (wrapperView[1] * wrapperViewPct[1][0]) < 0 && pos[1] + elD[1] - (wrapperView[1] * wrapperViewPct[1][1]) >= 0
        },
        mcsOverflow: $.expr[":"].mcsOverflow || function(el) {
          var d = $(el).data(pluginPfx);
          if (!d) {
            return
          }
          return d.overflowed[0] || d.overflowed[1]
        }
      })
    })
  }))
})), (function(factory) {
  if (typeof define === 'function' && define.amd) {
    define(['jquery'], factory)
  } else if (typeof exports === 'object') {
    module.exports = factory(require('jquery'))
  } else {
    factory(jQuery)
  }
}(function($) {
  'use strict';
  var readmore = 'readmore',
    defaults = {
      speed: 100,
      collapsedHeight: 200,
      heightMargin: 16,
      moreLink: '<a href="#">Read More</a>',
      lessLink: '<a href="#">Close</a>',
      embedCSS: !0,
      blockCSS: 'display: block; width: 100%;',
      startOpen: !1,
      blockProcessed: function() {},
      beforeToggle: function() {},
      afterToggle: function() {}
    },
    cssEmbedded = {},
    uniqueIdCounter = 0;

  function debounce(func, wait, immediate) {
    var timeout;
    return function() {
      var context = this,
        args = arguments;
      var later = function() {
        timeout = null;
        if (!immediate) {
          func.apply(context, args)
        }
      };
      var callNow = immediate && !timeout;
      clearTimeout(timeout);
      timeout = setTimeout(later, wait);
      if (callNow) {
        func.apply(context, args)
      }
    }
  }

  function uniqueId(prefix) {
    var id = ++uniqueIdCounter;
    return String(prefix == null ? 'rmjs-' : prefix) + id
  }

  function setBoxHeights(element) {
    var el = element.clone().css({
        height: 'auto',
        width: element.width(),
        maxHeight: 'none',
        overflow: 'hidden'
      }).insertAfter(element),
      expandedHeight = el.outerHeight(),
      cssMaxHeight = parseInt(el.css({
        maxHeight: ''
      }).css('max-height').replace(/[^-\d\.]/g, ''), 10),
      defaultHeight = element.data('defaultHeight');
    el.remove();
    var collapsedHeight = cssMaxHeight || element.data('collapsedHeight') || defaultHeight;
    element.data({
      expandedHeight: expandedHeight,
      maxHeight: cssMaxHeight,
      collapsedHeight: collapsedHeight
    }).css({
      maxHeight: 'none'
    })
  }
  var resizeBoxes = debounce(function() {
    $('[data-readmore]').each(function() {
      var current = $(this),
        isExpanded = (current.attr('aria-expanded') === 'true');
      setBoxHeights(current);
      current.css({
        height: current.data((isExpanded ? 'expandedHeight' : 'collapsedHeight'))
      })
    })
  }, 100);

  function embedCSS(options) {
    if (!cssEmbedded[options.selector]) {
      var styles = ' ';
      if (options.embedCSS && options.blockCSS !== '') {
        styles += options.selector + ' + [data-readmore-toggle], ' + options.selector + '[data-readmore]{' + options.blockCSS + '}'
      }
      styles += options.selector + '[data-readmore]{' + 'transition: height ' + options.speed + 'ms;' + 'overflow: hidden;' + '}';
      (function(d, u) {
        var css = d.createElement('style');
        css.type = 'text/css';
        if (css.styleSheet) {
          css.styleSheet.cssText = u
        } else {
          css.appendChild(d.createTextNode(u))
        }
        d.getElementsByTagName('head')[0].appendChild(css)
      }(document, styles));
      cssEmbedded[options.selector] = !0
    }
  }

  function Readmore(element, options) {
    this.element = element;
    this.options = $.extend({}, defaults, options);
    embedCSS(this.options);
    this._defaults = defaults;
    this._name = readmore;
    this.init();
    if (window.addEventListener) {
      window.addEventListener('load', resizeBoxes);
      window.addEventListener('resize', resizeBoxes)
    } else {
      window.attachEvent('load', resizeBoxes);
      window.attachEvent('resize', resizeBoxes)
    }
  }
  Readmore.prototype = {
    init: function() {
      var current = $(this.element);
      current.data({
        defaultHeight: this.options.collapsedHeight,
        heightMargin: this.options.heightMargin
      });
      setBoxHeights(current);
      var collapsedHeight = current.data('collapsedHeight'),
        heightMargin = current.data('heightMargin');
      if (current.outerHeight(!0) <= collapsedHeight + heightMargin) {
        if (this.options.blockProcessed && typeof this.options.blockProcessed === 'function') {
          this.options.blockProcessed(current, !1)
        }
        return !0
      } else {
        var id = current.attr('id') || uniqueId(),
          useLink = this.options.startOpen ? this.options.lessLink : this.options.moreLink;
        current.attr({
          'data-readmore': '',
          'aria-expanded': this.options.startOpen,
          'id': id
        });
        current.after($(useLink).on('click', (function(_this) {
          return function(event) {
            _this.toggle(this, current[0], event)
          }
        })(this)).attr({
          'data-readmore-toggle': id,
          'aria-controls': id
        }));
        if (!this.options.startOpen) {
          current.css({
            height: collapsedHeight
          })
        }
        if (this.options.blockProcessed && typeof this.options.blockProcessed === 'function') {
          this.options.blockProcessed(current, !0)
        }
      }
    },
    toggle: function(trigger, element, event) {
      if (event) {
        event.preventDefault()
      }
      if (!trigger) {
        trigger = $('[aria-controls="' + this.element.id + '"]')[0]
      }
      if (!element) {
        element = this.element
      }
      var $element = $(element),
        newHeight = '',
        newLink = '',
        expanded = !1,
        collapsedHeight = $element.data('collapsedHeight');
      if ($element.height() <= collapsedHeight) {
        newHeight = $element.data('expandedHeight') + 'px';
        newLink = 'lessLink';
        expanded = !0
      } else {
        newHeight = collapsedHeight;
        newLink = 'moreLink'
      }
      if (this.options.beforeToggle && typeof this.options.beforeToggle === 'function') {
        this.options.beforeToggle(trigger, $element, !expanded)
      }
      $element.css({
        'height': newHeight
      });
      $element.on('transitionend', (function(_this) {
        return function() {
          if (_this.options.afterToggle && typeof _this.options.afterToggle === 'function') {
            _this.options.afterToggle(trigger, $element, expanded)
          }
          $(this).attr({
            'aria-expanded': expanded
          }).off('transitionend')
        }
      })(this));
      $(trigger).replaceWith($(this.options[newLink]).on('click', (function(_this) {
        return function(event) {
          _this.toggle(this, element, event)
        }
      })(this)).attr({
        'data-readmore-toggle': $element.attr('id'),
        'aria-controls': $element.attr('id')
      }))
    },
    destroy: function() {
      $(this.element).each(function() {
        var current = $(this);
        current.attr({
          'data-readmore': null,
          'aria-expanded': null
        }).css({
          maxHeight: '',
          height: ''
        }).next('[data-readmore-toggle]').remove();
        current.removeData()
      })
    }
  };
  $.fn.readmore = function(options) {
    var args = arguments,
      selector = this.selector;
    options = options || {};
    if (typeof options === 'object') {
      return this.each(function() {
        if ($.data(this, 'plugin_' + readmore)) {
          var instance = $.data(this, 'plugin_' + readmore);
          instance.destroy.apply(instance)
        }
        options.selector = selector;
        $.data(this, 'plugin_' + readmore, new Readmore(this, options))
      })
    } else if (typeof options === 'string' && options[0] !== '_' && options !== 'init') {
      return this.each(function() {
        var instance = $.data(this, 'plugin_' + readmore);
        if (instance instanceof Readmore && typeof instance[options] === 'function') {
          instance[options].apply(instance, Array.prototype.slice.call(args, 1))
        }
      })
    }
  }
})),
function(u, s, g, m) {
  "use strict";

  function n(e, t) {
    var n, i, o = [],
      r = 0;
    e && e.isDefaultPrevented() || (e.preventDefault(), (i = (n = (t = e && e.data ? e.data.options : t || {}).$target || g(e.currentTarget)).attr("data-fancybox") || "") ? (r = (o = (o = t.selector ? g(t.selector) : e.data ? e.data.items : []).length ? o.filter('[data-fancybox="' + i + '"]') : g('[data-fancybox="' + i + '"]')).index(n)) < 0 && (r = 0) : o = [n], g.fancybox.open(o, t, r))
  }
  if (u.console = u.console || {
      info: function(e) {}
    }, g) {
    if (g.fn.fancybox) return console.info("fancyBox already initialized");
    var e = {
        loop: !1,
        gutter: 50,
        keyboard: !0,
        arrows: !0,
        infobar: !0,
        smallBtn: "auto",
        toolbar: "auto",
        buttons: ["zoom", "thumbs", "close"],
        idleTime: 3,
        protect: !1,
        modal: !1,
        image: {
          preload: !1
        },
        ajax: {
          settings: {
            data: {
              fancybox: !0
            }
          }
        },
        iframe: {
          tpl: '<iframe id="fancybox-frame{rnd}" name="fancybox-frame{rnd}" class="fancybox-iframe" frameborder="0" vspace="0" hspace="0" webkitAllowFullScreen mozallowfullscreen allowFullScreen allowtransparency="true" src=""></iframe>',
          preload: !0,
          css: {},
          attr: {
            scrolling: "auto"
          }
        },
        defaultType: "image",
        animationEffect: "zoom",
        animationDuration: 366,
        zoomOpacity: "auto",
        transitionEffect: "fade",
        transitionDuration: 366,
        slideClass: "",
        baseClass: "",
        baseTpl: '<div class="fancybox-container" role="dialog" tabindex="-1"><div class="fancybox-bg"></div><div class="fancybox-inner"><div class="fancybox-infobar"><span data-fancybox-index></span>&nbsp;/&nbsp;<span data-fancybox-count></span></div><div class="fancybox-toolbar">{{buttons}}</div><div class="fancybox-navigation">{{arrows}}</div><div class="fancybox-stage"></div><div class="fancybox-caption"></div></div></div>',
        spinnerTpl: '<div class="fancybox-loading"></div>',
        errorTpl: '<div class="fancybox-error"><p>{{ERROR}}</p></div>',
        btnTpl: {
          download: '<a download data-fancybox-download class="fancybox-button fancybox-button--download" title="{{DOWNLOAD}}" href="javascript:;"><svg viewBox="0 0 40 40"><path d="M13,16 L20,23 L27,16 M20,7 L20,23 M10,24 L10,28 L30,28 L30,24" /></svg></a>',
          zoom: '<button data-fancybox-zoom class="fancybox-button fancybox-button--zoom" title="{{ZOOM}}"><svg viewBox="0 0 40 40"><path d="M18,17 m-8,0 a8,8 0 1,0 16,0 a8,8 0 1,0 -16,0 M24,22 L31,29" /></svg></button>',
          close: '<button data-fancybox-close class="fancybox-button fancybox-button--close" title="{{CLOSE}}"><svg viewBox="0 0 40 40"><path d="M10,10 L30,30 M30,10 L10,30" /></svg></button>',
          smallBtn: '<button data-fancybox-close class="fancybox-close-small" title="{{CLOSE}}"><svg viewBox="0 0 32 32"><path d="M10,10 L22,22 M22,10 L10,22"></path></svg></button>',
          arrowLeft: '<a data-fancybox-prev class="fancybox-button fancybox-button--arrow_left" title="{{PREV}}" href="javascript:;"><svg viewBox="0 0 40 40"><path d="M18,12 L10,20 L18,28 M10,20 L30,20"></path></svg></a>',
          arrowRight: '<a data-fancybox-next class="fancybox-button fancybox-button--arrow_right" title="{{NEXT}}" href="javascript:;"><svg viewBox="0 0 40 40"><path d="M10,20 L30,20 M22,12 L30,20 L22,28"></path></svg></a>'
        },
        parentEl: "body",
        autoFocus: !1,
        backFocus: !0,
        trapFocus: !0,
        fullScreen: {
          autoStart: !1
        },
        touch: {
          vertical: !0,
          momentum: !0
        },
        hash: null,
        media: {},
        slideShow: {
          autoStart: !1,
          speed: 4e3
        },
        thumbs: {
          autoStart: !1,
          hideOnClose: !0,
          parentEl: ".fancybox-container",
          axis: "y"
        },
        wheel: "auto",
        onInit: g.noop,
        beforeLoad: g.noop,
        afterLoad: g.noop,
        beforeShow: g.noop,
        afterShow: g.noop,
        beforeClose: g.noop,
        afterClose: g.noop,
        onActivate: g.noop,
        onDeactivate: g.noop,
        clickContent: function(e, t) {
          return "image" === e.type && "zoom"
        },
        clickSlide: "close",
        clickOutside: "close",
        dblclickContent: !1,
        dblclickSlide: !1,
        dblclickOutside: !1,
        mobile: {
          idleTime: !1,
          clickContent: function(e, t) {
            return "image" === e.type && "toggleControls"
          },
          clickSlide: function(e, t) {
            return "image" === e.type ? "toggleControls" : "close"
          },
          dblclickContent: function(e, t) {
            return "image" === e.type && "zoom"
          },
          dblclickSlide: function(e, t) {
            return "image" === e.type && "zoom"
          }
        },
        lang: "en",
        i18n: {
          en: {
            CLOSE: "Close",
            NEXT: "Next",
            PREV: "Previous",
            ERROR: "The requested content cannot be loaded. <br/> Please try again later.",
            PLAY_START: "Start slideshow",
            PLAY_STOP: "Pause slideshow",
            FULL_SCREEN: "Full screen",
            THUMBS: "Thumbnails",
            DOWNLOAD: "Download",
            SHARE: "Share",
            ZOOM: "Zoom"
          },
          de: {
            CLOSE: "Schliessen",
            NEXT: "Weiter",
            PREV: "Zurück",
            ERROR: "Die angeforderten Daten konnten nicht geladen werden. <br/> Bitte versuchen Sie es später nochmal.",
            PLAY_START: "Diaschau starten",
            PLAY_STOP: "Diaschau beenden",
            FULL_SCREEN: "Vollbild",
            THUMBS: "Vorschaubilder",
            DOWNLOAD: "Herunterladen",
            SHARE: "Teilen",
            ZOOM: "Maßstab"
          }
        }
      },
      o = g(u),
      a = g(s),
      r = 0,
      f = u.requestAnimationFrame || u.webkitRequestAnimationFrame || u.mozRequestAnimationFrame || u.oRequestAnimationFrame || function(e) {
        return u.setTimeout(e, 1e3 / 60)
      },
      h = function() {
        var e, t = s.createElement("fakeelement"),
          n = {
            transition: "transitionend",
            OTransition: "oTransitionEnd",
            MozTransition: "transitionend",
            WebkitTransition: "webkitTransitionEnd"
          };
        for (e in n)
          if (t.style[e] !== m) return n[e];
        return "transitionend"
      }(),
      p = function(e) {
        return e && e.length && e[0].offsetHeight
      },
      l = function(e, t) {
        var n = g.extend(!0, {}, e, t);
        return g.each(t, function(e, t) {
          g.isArray(t) && (n[e] = t)
        }), n
      },
      i = function(e, t, n) {
        var i = this;
        i.opts = l({
          index: n
        }, g.fancybox.defaults), g.isPlainObject(t) && (i.opts = l(i.opts, t)), g.fancybox.isMobile && (i.opts = l(i.opts, i.opts.mobile)), i.id = i.opts.id || ++r, i.currIndex = parseInt(i.opts.index, 10) || 0, i.prevIndex = null, i.prevPos = null, i.currPos = 0, i.firstRun = !0, i.group = [], i.slides = {}, i.addContent(e), i.group.length && (i.$lastFocus = g(s.activeElement).trigger("blur"), i.init())
      };
    g.extend(i.prototype, {
      init: function() {
        var e, t, n, i = this,
          o = i.group[i.currIndex].opts,
          r = g.fancybox.scrollbarWidth;
        g.fancybox.getInstance() || !1 === o.hideScrollbar || (g("body").addClass("fancybox-active"), !g.fancybox.isMobile && s.body.scrollHeight > u.innerHeight && (r === m && (e = g('<div style="width:100px;height:100px;overflow:scroll;" />').appendTo("body"), r = g.fancybox.scrollbarWidth = e[0].offsetWidth - e[0].clientWidth, e.remove()), g("head").append('<style id="fancybox-style-noscroll" type="text/css">.compensate-for-scrollbar { margin-right: ' + r + "px; }</style>"), g("body").addClass("compensate-for-scrollbar"))), n = "", g.each(o.buttons, function(e, t) {
          n += o.btnTpl[t] || ""
        }), t = g(i.translate(i, o.baseTpl.replace("{{buttons}}", n).replace("{{arrows}}", o.btnTpl.arrowLeft + o.btnTpl.arrowRight))).attr("id", "fancybox-container-" + i.id).addClass("fancybox-is-hidden").addClass(o.baseClass).data("FancyBox", i).appendTo(o.parentEl), i.$refs = {
          container: t
        }, ["bg", "inner", "infobar", "toolbar", "stage", "caption", "navigation"].forEach(function(e) {
          i.$refs[e] = t.find(".fancybox-" + e)
        }), i.trigger("onInit"), i.activate(), i.jumpTo(i.currIndex)
      },
      translate: function(e, t) {
        var i = e.opts.i18n[e.opts.lang];
        return t.replace(/\{\{(\w+)\}\}/g, function(e, t) {
          var n = i[t];
          return n === m ? e : n
        })
      },
      addContent: function(e) {
        var t, c = this,
          n = g.makeArray(e);
        g.each(n, function(e, t) {
          var n, i, o, r, s, a = {},
            l = {};
          g.isPlainObject(t) ? l = (a = t).opts || t : "object" === g.type(t) && g(t).length ? (l = (n = g(t)).data() || {}, (l = g.extend(!0, {}, l, l.options)).$orig = n, a.src = c.opts.src || l.src || n.attr("href"), a.type || a.src || (a.type = "inline", a.src = t)) : a = {
            type: "html",
            src: t + ""
          }, a.opts = g.extend(!0, {}, c.opts, l), g.isArray(l.buttons) && (a.opts.buttons = l.buttons), i = a.type || a.opts.type, r = a.src || "", !i && r && ((o = r.match(/\.(mp4|mov|ogv)((\?|#).*)?$/i)) ? (i = "video", a.opts.videoFormat || (a.opts.videoFormat = "video/" + ("ogv" === o[1] ? "ogg" : o[1]))) : r.match(/(^data:image\/[a-z0-9+\/=]*,)|(\.(jp(e|g|eg)|gif|png|bmp|webp|svg|ico)((\?|#).*)?$)/i) ? i = "image" : r.match(/\.(pdf)((\?|#).*)?$/i) ? i = "iframe" : "#" === r.charAt(0) && (i = "inline")), i ? a.type = i : c.trigger("objectNeedsType", a), a.contentType || (a.contentType = -1 < g.inArray(a.type, ["html", "inline", "ajax"]) ? "html" : a.type), a.index = c.group.length, "auto" == a.opts.smallBtn && (a.opts.smallBtn = -1 < g.inArray(a.type, ["html", "inline", "ajax"])), "auto" === a.opts.toolbar && (a.opts.toolbar = !a.opts.smallBtn), a.opts.$trigger && a.index === c.opts.index && (a.opts.$thumb = a.opts.$trigger.find("img:first")), a.opts.$thumb && a.opts.$thumb.length || !a.opts.$orig || (a.opts.$thumb = a.opts.$orig.find("img:first")), "function" === g.type(a.opts.caption) && (a.opts.caption = a.opts.caption.apply(t, [c, a])), "function" === g.type(c.opts.caption) && (a.opts.caption = c.opts.caption.apply(t, [c, a])), a.opts.caption instanceof g || (a.opts.caption = a.opts.caption === m ? "" : a.opts.caption + ""), "ajax" === a.type && (1 < (s = r.split(/\s+/, 2)).length && (a.src = s.shift(), a.opts.filter = s.shift())), a.opts.modal && (a.opts = g.extend(!0, a.opts, {
            infobar: 0,
            toolbar: 0,
            smallBtn: 0,
            keyboard: 0,
            slideShow: 0,
            fullScreen: 0,
            thumbs: 0,
            touch: 0,
            clickContent: !1,
            clickSlide: !1,
            clickOutside: !1,
            dblclickContent: !1,
            dblclickSlide: !1,
            dblclickOutside: !1
          })), c.group.push(a)
        }), Object.keys(c.slides).length && (c.updateControls(), (t = c.Thumbs) && t.isActive && (t.create(), t.focus()))
      },
      addEvents: function() {
        var i = this;
        i.removeEvents(), i.$refs.container.on("click.fb-close", "[data-fancybox-close]", function(e) {
          e.stopPropagation(), e.preventDefault(), i.close(e)
        }).on("touchstart.fb-prev click.fb-prev", "[data-fancybox-prev]", function(e) {
          e.stopPropagation(), e.preventDefault(), i.previous()
        }).on("touchstart.fb-next click.fb-next", "[data-fancybox-next]", function(e) {
          e.stopPropagation(), e.preventDefault(), i.next()
        }).on("click.fb", "[data-fancybox-zoom]", function(e) {
          i[i.isScaledDown() ? "scaleToActual" : "scaleToFit"]()
        }), o.on("orientationchange.fb resize.fb", function(e) {
          e && e.originalEvent && "resize" === e.originalEvent.type ? f(function() {
            i.update()
          }) : (i.$refs.stage.hide(), setTimeout(function() {
            i.$refs.stage.show(), i.update()
          }, g.fancybox.isMobile ? 600 : 250))
        }), a.on("focusin.fb", function(e) {
          var t = g.fancybox ? g.fancybox.getInstance() : null;
          t.isClosing || !t.current || !t.current.opts.trapFocus || g(e.target).hasClass("fancybox-container") || g(e.target).is(s) || t && "fixed" !== g(e.target).css("position") && !t.$refs.container.has(e.target).length && (e.stopPropagation(), t.focus())
        }), a.on("keydown.fb", function(e) {
          var t = i.current,
            n = e.keyCode || e.which;
          if (t && t.opts.keyboard && !(e.ctrlKey || e.altKey || e.shiftKey || g(e.target).is("input") || g(e.target).is("textarea"))) return 8 === n || 27 === n ? (e.preventDefault(), void i.close(e)) : 37 === n || 38 === n ? (e.preventDefault(), void i.previous()) : 39 === n || 40 === n ? (e.preventDefault(), void i.next()) : void i.trigger("afterKeydown", e, n)
        }), i.group[i.currIndex].opts.idleTime && (i.idleSecondsCounter = 0, a.on("mousemove.fb-idle mouseleave.fb-idle mousedown.fb-idle touchstart.fb-idle touchmove.fb-idle scroll.fb-idle keydown.fb-idle", function(e) {
          i.idleSecondsCounter = 0, i.isIdle && i.showControls(), i.isIdle = !1
        }), i.idleInterval = u.setInterval(function() {
          i.idleSecondsCounter++, i.idleSecondsCounter >= i.group[i.currIndex].opts.idleTime && !i.isDragging && (i.isIdle = !0, i.idleSecondsCounter = 0, i.hideControls())
        }, 1e3))
      },
      removeEvents: function() {
        o.off("orientationchange.fb resize.fb"), a.off("focusin.fb keydown.fb .fb-idle"), this.$refs.container.off(".fb-close .fb-prev .fb-next"), this.idleInterval && (u.clearInterval(this.idleInterval), this.idleInterval = null)
      },
      previous: function(e) {
        return this.jumpTo(this.currPos - 1, e)
      },
      next: function(e) {
        return this.jumpTo(this.currPos + 1, e)
      },
      jumpTo: function(e, i) {
        var t, n, o, r, s, a, l, c = this,
          u = c.group.length;
        if (!(c.isDragging || c.isClosing || c.isAnimating && c.firstRun)) {
          if (e = parseInt(e, 10), !(n = c.current ? c.current.opts.loop : c.opts.loop) && (e < 0 || u <= e)) return !1;
          if (t = c.firstRun = !Object.keys(c.slides).length, !(u < 2 && !t && c.isDragging)) {
            if (r = c.current, c.prevIndex = c.currIndex, c.prevPos = c.currPos, o = c.createSlide(e), 1 < u && ((n || 0 < o.index) && c.createSlide(e - 1), (n || o.index < u - 1) && c.createSlide(e + 1)), c.current = o, c.currIndex = o.index, c.currPos = o.pos, c.trigger("beforeShow", t), c.updateControls(), a = g.fancybox.getTranslate(o.$slide), o.isMoved = (0 !== a.left || 0 !== a.top) && !o.$slide.hasClass("fancybox-animated"), o.forcedDuration = m, g.isNumeric(i) ? o.forcedDuration = i : i = o.opts[t ? "animationDuration" : "transitionDuration"], i = parseInt(i, 10), t) return o.opts.animationEffect && i && c.$refs.container.css("transition-duration", i + "ms"), c.$refs.container.removeClass("fancybox-is-hidden"), p(c.$refs.container), c.$refs.container.addClass("fancybox-is-open"), p(c.$refs.container), o.$slide.addClass("fancybox-slide--previous"), c.loadSlide(o), o.$slide.removeClass("fancybox-slide--previous").addClass("fancybox-slide--current"), void c.preload("image");
            g.each(c.slides, function(e, t) {
              g.fancybox.stop(t.$slide)
            }), o.$slide.removeClass("fancybox-slide--next fancybox-slide--previous").addClass("fancybox-slide--current"), o.isMoved ? (s = Math.round(o.$slide.width()), g.each(c.slides, function(e, t) {
              var n = t.pos - o.pos;
              g.fancybox.animate(t.$slide, {
                top: 0,
                left: n * s + n * t.opts.gutter
              }, i, function() {
                t.$slide.removeAttr("style").removeClass("fancybox-slide--next fancybox-slide--previous"), t.pos === c.currPos && (o.isMoved = !1, c.complete())
              })
            })) : c.$refs.stage.children().removeAttr("style"), o.isLoaded ? c.revealContent(o) : c.loadSlide(o), c.preload("image"), r.pos !== o.pos && (l = "fancybox-slide--" + (r.pos > o.pos ? "next" : "previous"), r.$slide.removeClass("fancybox-slide--complete fancybox-slide--current fancybox-slide--next fancybox-slide--previous"), r.isComplete = !1, i && (o.isMoved || o.opts.transitionEffect) && (o.isMoved ? r.$slide.addClass(l) : (l = "fancybox-animated " + l + " fancybox-fx-" + o.opts.transitionEffect, g.fancybox.animate(r.$slide, l, i, function() {
              r.$slide.removeClass(l).removeAttr("style")
            }))))
          }
        }
      },
      createSlide: function(e) {
        var t, n, i = this;
        return n = (n = e % i.group.length) < 0 ? i.group.length + n : n, !i.slides[e] && i.group[n] && (t = g('<div class="fancybox-slide"></div>').appendTo(i.$refs.stage), i.slides[e] = g.extend(!0, {}, i.group[n], {
          pos: e,
          $slide: t,
          isLoaded: !1
        }), i.updateSlide(i.slides[e])), i.slides[e]
      },
      scaleToActual: function(e, t, n) {
        var i, o, r, s, a, l = this,
          c = l.current,
          u = c.$content,
          d = g.fancybox.getTranslate(c.$slide).width,
          f = g.fancybox.getTranslate(c.$slide).height,
          h = c.width,
          p = c.height;
        !l.isAnimating && u && "image" == c.type && c.isLoaded && !c.hasError && (g.fancybox.stop(u), l.isAnimating = !0, e = e === m ? .5 * d : e, t = t === m ? .5 * f : t, (i = g.fancybox.getTranslate(u)).top -= g.fancybox.getTranslate(c.$slide).top, i.left -= g.fancybox.getTranslate(c.$slide).left, s = h / i.width, a = p / i.height, o = .5 * d - .5 * h, r = .5 * f - .5 * p, d < h && (0 < (o = i.left * s - (e * s - e)) && (o = 0), o < d - h && (o = d - h)), f < p && (0 < (r = i.top * a - (t * a - t)) && (r = 0), r < f - p && (r = f - p)), l.updateCursor(h, p), g.fancybox.animate(u, {
          top: r,
          left: o,
          scaleX: s,
          scaleY: a
        }, n || 330, function() {
          l.isAnimating = !1
        }), l.SlideShow && l.SlideShow.isActive && l.SlideShow.stop())
      },
      scaleToFit: function(e) {
        var t, n = this,
          i = n.current,
          o = i.$content;
        !n.isAnimating && o && "image" == i.type && i.isLoaded && !i.hasError && (g.fancybox.stop(o), n.isAnimating = !0, t = n.getFitPos(i), n.updateCursor(t.width, t.height), g.fancybox.animate(o, {
          top: t.top,
          left: t.left,
          scaleX: t.width / o.width(),
          scaleY: t.height / o.height()
        }, e || 330, function() {
          n.isAnimating = !1
        }))
      },
      getFitPos: function(e) {
        var t, n, i, o, r, s = e.$content,
          a = e.width || e.opts.width,
          l = e.height || e.opts.height,
          c = {};
        return !!(e.isLoaded && s && s.length) && (o = {
          top: parseInt(e.$slide.css("paddingTop"), 10),
          right: parseInt(e.$slide.css("paddingRight"), 10),
          bottom: parseInt(e.$slide.css("paddingBottom"), 10),
          left: parseInt(e.$slide.css("paddingLeft"), 10)
        }, t = parseInt(this.$refs.stage.width(), 10) - (o.left + o.right), n = parseInt(this.$refs.stage.height(), 10) - (o.top + o.bottom), a && l || (a = t, l = n), i = Math.min(1, t / a, n / l), a = Math.floor(i * a), l = Math.floor(i * l), "image" === e.type ? (c.top = Math.floor(.5 * (n - l)) + o.top, c.left = Math.floor(.5 * (t - a)) + o.left) : "video" === e.contentType && (a / (r = e.opts.width && e.opts.height ? a / l : e.opts.ratio || 16 / 9) < l ? l = a / r : l * r < a && (a = l * r)), c.width = a, c.height = l, c)
      },
      update: function() {
        var n = this;
        g.each(n.slides, function(e, t) {
          n.updateSlide(t)
        })
      },
      updateSlide: function(e, t) {
        var n = this,
          i = e && e.$content,
          o = e.width || e.opts.width,
          r = e.height || e.opts.height;
        i && (o || r || "video" === e.contentType) && !e.hasError && (g.fancybox.stop(i), g.fancybox.setTranslate(i, n.getFitPos(e)), e.pos === n.currPos && (n.isAnimating = !1, n.updateCursor())), e.$slide.trigger("refresh"), n.$refs.toolbar.toggleClass("compensate-for-scrollbar", e.$slide.get(0).scrollHeight > e.$slide.get(0).clientHeight), n.trigger("onUpdate", e)
      },
      centerSlide: function(e, t) {
        var n, i;
        this.current && (n = Math.round(e.$slide.width()), i = e.pos - this.current.pos, g.fancybox.animate(e.$slide, {
          top: 0,
          left: i * n + i * e.opts.gutter,
          opacity: 1
        }, t === m ? 0 : t, null, !1))
      },
      updateCursor: function(e, t) {
        var n, i = this,
          o = i.current,
          r = i.$refs.container.removeClass("fancybox-is-zoomable fancybox-can-zoomIn fancybox-can-drag fancybox-can-zoomOut");
        o && !i.isClosing && (n = i.isZoomable(), r.toggleClass("fancybox-is-zoomable", n), g("[data-fancybox-zoom]").prop("disabled", !n), n && ("zoom" === o.opts.clickContent || g.isFunction(o.opts.clickContent) && "zoom" === o.opts.clickContent(o)) ? i.isScaledDown(e, t) ? r.addClass("fancybox-can-zoomIn") : o.opts.touch ? r.addClass("fancybox-can-drag") : r.addClass("fancybox-can-zoomOut") : o.opts.touch && "video" !== o.contentType && r.addClass("fancybox-can-drag"))
      },
      isZoomable: function() {
        var e, t = this.current;
        if (t && !this.isClosing && "image" === t.type && !t.hasError) {
          if (!t.isLoaded) return !0;
          if (e = this.getFitPos(t), t.width > e.width || t.height > e.height) return !0
        }
        return !1
      },
      isScaledDown: function(e, t) {
        var n = !1,
          i = this.current,
          o = i.$content;
        return e !== m && t !== m ? n = e < i.width && t < i.height : o && (n = (n = g.fancybox.getTranslate(o)).width < i.width && n.height < i.height), n
      },
      canPan: function() {
        var e, t = !1,
          n = this.current;
        return "image" === n.type && (e = n.$content) && !n.hasError && (t = this.getFitPos(n), t = 1 < Math.abs(e.width() - t.width) || 1 < Math.abs(e.height() - t.height)), t
      },
      loadSlide: function(n) {
        var e, t, i, o = this;
        if (!n.isLoading && !n.isLoaded) {
          switch (n.isLoading = !0, o.trigger("beforeLoad", n), e = n.type, (t = n.$slide).off("refresh").trigger("onReset").addClass(n.opts.slideClass), e) {
            case "image":
              o.setImage(n);
              break;
            case "iframe":
              o.setIframe(n);
              break;
            case "html":
              o.setContent(n, n.src || n.content);
              break;
            case "video":
              o.setContent(n, '<video class="fancybox-video" controls controlsList="nodownload"><source src="' + n.src + '" type="' + n.opts.videoFormat + "\">Your browser doesn't support HTML5 video</video");
              break;
            case "inline":
              g(n.src).length ? o.setContent(n, g(n.src)) : o.setError(n);
              break;
            case "ajax":
              o.showLoading(n), i = g.ajax(g.extend({}, n.opts.ajax.settings, {
                url: n.src,
                success: function(e, t) {
                  "success" === t && o.setContent(n, e)
                },
                error: function(e, t) {
                  e && "abort" !== t && o.setError(n)
                }
              })), t.one("onReset", function() {
                i.abort()
              });
              break;
            default:
              o.setError(n)
          }
          return !0
        }
      },
      setImage: function(t) {
        var e, n, i, o, r, s = this,
          a = t.opts.srcset || t.opts.image.srcset;
        if (t.timouts = setTimeout(function() {
            var e = t.$image;
            !t.isLoading || e && e[0].complete || t.hasError || s.showLoading(t)
          }, 350), a) {
          o = u.devicePixelRatio || 1, r = u.innerWidth * o, (i = a.split(",").map(function(e) {
            var i = {};
            return e.trim().split(/\s+/).forEach(function(e, t) {
              var n = parseInt(e.substring(0, e.length - 1), 10);
              return 0 === t ? i.url = e : void(n && (i.value = n, i.postfix = e[e.length - 1]))
            }), i
          })).sort(function(e, t) {
            return e.value - t.value
          });
          for (var l = 0; l < i.length; l++) {
            var c = i[l];
            if ("w" === c.postfix && c.value >= r || "x" === c.postfix && c.value >= o) {
              n = c;
              break
            }
          }!n && i.length && (n = i[i.length - 1]), n && (t.src = n.url, t.width && t.height && "w" == n.postfix && (t.height = t.width / t.height * n.value, t.width = n.value), t.opts.srcset = a)
        }
        t.$content = g('<div class="fancybox-content"></div>').addClass("fancybox-is-hidden").appendTo(t.$slide.addClass("fancybox-slide--image")), e = t.opts.thumb || !(!t.opts.$thumb || !t.opts.$thumb.length) && t.opts.$thumb.attr("src"), !1 !== t.opts.preload && t.opts.width && t.opts.height && e && (t.width = t.opts.width, t.height = t.opts.height, t.$ghost = g("<img />").one("error", function() {
          g(this).remove(), t.$ghost = null
        }).one("load", function() {
          s.afterLoad(t)
        }).addClass("fancybox-image").appendTo(t.$content).attr("src", e)), s.setBigImage(t)
      },
      setBigImage: function(t) {
        var n = this,
          i = g("<img />");
        t.$image = i.one("error", function() {
          n.setError(t)
        }).one("load", function() {
          var e;
          t.$ghost || (n.resolveImageSlideSize(t, this.naturalWidth, this.naturalHeight), n.afterLoad(t)), t.timouts && (clearTimeout(t.timouts), t.timouts = null), n.isClosing || (t.opts.srcset && ((e = t.opts.sizes) && "auto" !== e || (e = (1 < t.width / t.height && 1 < o.width() / o.height() ? "100" : Math.round(t.width / t.height * 100)) + "vw"), i.attr("sizes", e).attr("srcset", t.opts.srcset)), t.$ghost && setTimeout(function() {
            t.$ghost && !n.isClosing && t.$ghost.hide()
          }, Math.min(300, Math.max(1e3, t.height / 1600))), n.hideLoading(t))
        }).addClass("fancybox-image").attr("src", t.src).appendTo(t.$content), (i[0].complete || "complete" == i[0].readyState) && i[0].naturalWidth && i[0].naturalHeight ? i.trigger("load") : i[0].error && i.trigger("error")
      },
      resolveImageSlideSize: function(e, t, n) {
        var i = parseInt(e.opts.width, 10),
          o = parseInt(e.opts.height, 10);
        e.width = t, e.height = n, 0 < i && (e.width = i, e.height = Math.floor(i * n / t)), 0 < o && (e.width = Math.floor(o * t / n), e.height = o)
      },
      setIframe: function(o) {
        var r, t = this,
          s = o.opts.iframe,
          e = o.$slide;
        o.$content = g('<div class="fancybox-content' + (s.preload ? " fancybox-is-hidden" : "") + '"></div>').css(s.css).appendTo(e), e.addClass("fancybox-slide--" + o.contentType), o.$iframe = r = g(s.tpl.replace(/\{rnd\}/g, (new Date).getTime())).attr(s.attr).appendTo(o.$content), s.preload ? (t.showLoading(o), r.on("load.fb error.fb", function(e) {
          this.isReady = 1, o.$slide.trigger("refresh"), t.afterLoad(o)
        }), e.on("refresh.fb", function() {
          var e, t = o.$content,
            n = s.css.width,
            i = s.css.height;
          if (1 === r[0].isReady) {
            try {
              e = r.contents().find("body")
            } catch (e) {}
            e && e.length && e.children().length && (t.css({
              width: "",
              height: ""
            }), n === m && (n = Math.ceil(Math.max(e[0].clientWidth, e.outerWidth(!0)))), n && t.width(n), i === m && (i = Math.ceil(Math.max(e[0].clientHeight, e.outerHeight(!0)))), i && t.height(i)), t.removeClass("fancybox-is-hidden")
          }
        })) : this.afterLoad(o), r.attr("src", o.src), e.one("onReset", function() {
          try {
            g(this).find("iframe").hide().unbind().attr("src", "//about:blank")
          } catch (e) {}
          g(this).off("refresh.fb").empty(), o.isLoaded = !1
        })
      },
      setContent: function(e, t) {
        var n;
        this.isClosing || (this.hideLoading(e), e.$content && g.fancybox.stop(e.$content), e.$slide.empty(), (n = t) && n.hasOwnProperty && n instanceof g && t.parent().length ? (t.parent().parent(".fancybox-slide--inline").trigger("onReset"), e.$placeholder = g("<div>").hide().insertAfter(t), t.css("display", "inline-block")) : e.hasError || ("string" === g.type(t) && (3 === (t = g("<div>").append(g.trim(t)).contents())[0].nodeType && (t = g("<div>").html(t))), e.opts.filter && (t = g("<div>").html(t).find(e.opts.filter))), e.$slide.one("onReset", function() {
          g(this).find("video,audio").trigger("pause"), e.$placeholder && (e.$placeholder.after(t.hide()).remove(), e.$placeholder = null), e.$smallBtn && (e.$smallBtn.remove(), e.$smallBtn = null), e.hasError || (g(this).empty(), e.isLoaded = !1)
        }), g(t).appendTo(e.$slide), g(t).is("video,audio") && (g(t).addClass("fancybox-video"), g(t).wrap("<div></div>"), e.contentType = "video", e.opts.width = e.opts.width || g(t).attr("width"), e.opts.height = e.opts.height || g(t).attr("height")), e.$content = e.$slide.children().filter("div,form,main,video,audio").first().addClass("fancybox-content"), e.$slide.addClass("fancybox-slide--" + e.contentType), this.afterLoad(e))
      },
      setError: function(e) {
        e.hasError = !0, e.$slide.trigger("onReset").removeClass("fancybox-slide--" + e.contentType).addClass("fancybox-slide--error"), e.contentType = "html", this.setContent(e, this.translate(e, e.opts.errorTpl)), e.pos === this.currPos && (this.isAnimating = !1)
      },
      showLoading: function(e) {
        (e = e || this.current) && !e.$spinner && (e.$spinner = g(this.translate(this, this.opts.spinnerTpl)).appendTo(e.$slide))
      },
      hideLoading: function(e) {
        (e = e || this.current) && e.$spinner && (e.$spinner.remove(), delete e.$spinner)
      },
      afterLoad: function(e) {
        var t = this;
        t.isClosing || (e.isLoading = !1, e.isLoaded = !0, t.trigger("afterLoad", e), t.hideLoading(e), e.pos === t.currPos && t.updateCursor(), !e.opts.smallBtn || e.$smallBtn && e.$smallBtn.length || (e.$smallBtn = g(t.translate(e, e.opts.btnTpl.smallBtn)).prependTo(e.$content)), e.opts.protect && e.$content && !e.hasError && (e.$content.on("contextmenu.fb", function(e) {
          return 2 == e.button && e.preventDefault(), !0
        }), "image" === e.type && g('<div class="fancybox-spaceball"></div>').appendTo(e.$content)), t.revealContent(e))
      },
      revealContent: function(t) {
        var e, n, i, o, r = this,
          s = t.$slide,
          a = !1,
          l = !1;
        return e = t.opts[r.firstRun ? "animationEffect" : "transitionEffect"], i = t.opts[r.firstRun ? "animationDuration" : "transitionDuration"], i = parseInt(t.forcedDuration === m ? i : t.forcedDuration, 10), t.pos === r.currPos && (t.isComplete ? e = !1 : r.isAnimating = !0), !t.isMoved && t.pos === r.currPos && i || (e = !1), "zoom" === e && (t.pos === r.currPos && i && "image" === t.type && !t.hasError && (l = r.getThumbPos(t)) ? a = r.getFitPos(t) : e = "fade"), "zoom" === e ? (a.scaleX = a.width / l.width, a.scaleY = a.height / l.height, "auto" == (o = t.opts.zoomOpacity) && (o = .1 < Math.abs(t.width / t.height - l.width / l.height)), o && (l.opacity = .1, a.opacity = 1), g.fancybox.setTranslate(t.$content.removeClass("fancybox-is-hidden"), l), p(t.$content), void g.fancybox.animate(t.$content, a, i, function() {
          r.isAnimating = !1, r.complete()
        })) : (r.updateSlide(t), e ? (g.fancybox.stop(s), n = "fancybox-animated fancybox-slide--" + (t.pos >= r.prevPos ? "next" : "previous") + " fancybox-fx-" + e, s.removeAttr("style").removeClass("fancybox-slide--current fancybox-slide--next fancybox-slide--previous").addClass(n), t.$content.removeClass("fancybox-is-hidden"), p(s), void g.fancybox.animate(s, "fancybox-slide--current", i, function(e) {
          s.removeClass(n).removeAttr("style"), t.pos === r.currPos && r.complete()
        }, !0)) : (p(s), t.$content.removeClass("fancybox-is-hidden"), void(t.pos === r.currPos && r.complete())))
      },
      getThumbPos: function(e) {
        var t, n = !1,
          i = e.opts.$thumb,
          o = i && i.length && i[0].ownerDocument === s ? i.offset() : 0;
        return o && function(e) {
          for (var t = e[0], i = t.getBoundingClientRect(), n = []; null !== t.parentElement;) "hidden" !== g(t.parentElement).css("overflow") && "auto" !== g(t.parentElement).css("overflow") || n.push(t.parentElement.getBoundingClientRect()), t = t.parentElement;
          return n.every(function(e) {
            var t = Math.min(i.right, e.right) - Math.max(i.left, e.left),
              n = Math.min(i.bottom, e.bottom) - Math.max(i.top, e.top);
            return 0 < t && 0 < n
          }) && 0 < i.bottom && 0 < i.right && i.left < g(u).width() && i.top < g(u).height()
        }(i) && (t = this.$refs.stage.offset(), n = {
          top: o.top - t.top + parseFloat(i.css("border-top-width") || 0),
          left: o.left - t.left + parseFloat(i.css("border-left-width") || 0),
          width: i.width(),
          height: i.height(),
          scaleX: 1,
          scaleY: 1
        }), n
      },
      complete: function() {
        var n = this,
          e = n.current,
          i = {};
        !e.isMoved && e.isLoaded && (e.isComplete || (e.isComplete = !0, e.$slide.siblings().trigger("onReset"), n.preload("inline"), p(e.$slide), e.$slide.addClass("fancybox-slide--complete"), g.each(n.slides, function(e, t) {
          t.pos >= n.currPos - 1 && t.pos <= n.currPos + 1 ? i[t.pos] = t : t && (g.fancybox.stop(t.$slide), t.$slide.off().remove())
        }), n.slides = i), n.isAnimating = !1, n.updateCursor(), n.trigger("afterShow"), e.$slide.find("video,audio").filter(":visible:first").trigger("play"), (g(s.activeElement).is("[disabled]") || e.opts.autoFocus && "image" != e.type && "iframe" !== e.type) && n.focus())
      },
      preload: function(e) {
        var t = this,
          n = t.slides[t.currPos + 1],
          i = t.slides[t.currPos - 1];
        n && n.type === e && t.loadSlide(n), i && i.type === e && t.loadSlide(i)
      },
      focus: function() {
        var e, t = this.current;
        this.isClosing || t && t.isComplete && t.$content && ((e = t.$content.find("input[autofocus]:enabled:visible:first")).length || (e = t.$content.find("button,:input,[tabindex],a").filter(":enabled:visible:first")), (e = e && e.length ? e : t.$content).trigger("focus"))
      },
      activate: function() {
        var t = this;
        g(".fancybox-container").each(function() {
          var e = g(this).data("FancyBox");
          e && e.id !== t.id && !e.isClosing && (e.trigger("onDeactivate"), e.removeEvents(), e.isVisible = !1)
        }), t.isVisible = !0, (t.current || t.isIdle) && (t.update(), t.updateControls()), t.trigger("onActivate"), t.addEvents()
      },
      close: function(e, t) {
        var n, i, o, r, s, a, l, c = this,
          u = c.current,
          d = function() {
            c.cleanUp(e)
          };
        return !c.isClosing && (!(c.isClosing = !0) === c.trigger("beforeClose", e) ? (c.isClosing = !1, f(function() {
          c.update()
        }), !1) : (c.removeEvents(), u.timouts && clearTimeout(u.timouts), o = u.$content, n = u.opts.animationEffect, i = g.isNumeric(t) ? t : n ? u.opts.animationDuration : 0, u.$slide.off(h).removeClass("fancybox-slide--complete fancybox-slide--next fancybox-slide--previous fancybox-animated"), u.$slide.siblings().trigger("onReset").remove(), i && c.$refs.container.removeClass("fancybox-is-open").addClass("fancybox-is-closing"), c.hideLoading(u), c.hideControls(), c.updateCursor(), "zoom" !== n || !0 !== e && o && i && "image" === u.type && !u.hasError && (l = c.getThumbPos(u)) || (n = "fade"), "zoom" === n ? (g.fancybox.stop(o), a = {
          top: (r = g.fancybox.getTranslate(o)).top,
          left: r.left,
          scaleX: r.width / l.width,
          scaleY: r.height / l.height,
          width: l.width,
          height: l.height
        }, "auto" == (s = u.opts.zoomOpacity) && (s = .1 < Math.abs(u.width / u.height - l.width / l.height)), s && (l.opacity = 0), g.fancybox.setTranslate(o, a), p(o), g.fancybox.animate(o, l, i, d)) : n && i ? !0 === e ? setTimeout(d, i) : g.fancybox.animate(u.$slide.removeClass("fancybox-slide--current"), "fancybox-animated fancybox-slide--previous fancybox-fx-" + n, i, d) : d(), !0))
      },
      cleanUp: function(e) {
        var t, n = this,
          i = g("body");
        n.current.$slide.trigger("onReset"), n.$refs.container.empty().remove(), n.trigger("afterClose", e), n.$lastFocus && n.current.opts.backFocus && n.$lastFocus.trigger("focus"), n.current = null, (t = g.fancybox.getInstance()) ? t.activate() : (i.removeClass("fancybox-active compensate-for-scrollbar"), g("#fancybox-style-noscroll").remove())
      },
      trigger: function(e, t) {
        var n, i = Array.prototype.slice.call(arguments, 1),
          o = this,
          r = t && t.opts ? t : o.current;
        return r ? i.unshift(r) : r = o, i.unshift(o), g.isFunction(r.opts[e]) && (n = r.opts[e].apply(r, i)), !1 === n ? n : void("afterClose" !== e && o.$refs ? o.$refs.container.trigger(e + ".fb", i) : a.trigger(e + ".fb", i))
      },
      updateControls: function(e) {
        var t = this,
          n = t.current,
          i = n.index,
          o = n.opts.caption,
          r = t.$refs.container,
          s = t.$refs.caption;
        n.$slide.trigger("refresh"), t.$caption = o && o.length ? s.html(o) : null, t.isHiddenControls || t.isIdle || t.showControls(), r.find("[data-fancybox-count]").html(t.group.length), r.find("[data-fancybox-index]").html(i + 1), r.find("[data-fancybox-prev]").toggleClass("disabled", !n.opts.loop && i <= 0), r.find("[data-fancybox-next]").toggleClass("disabled", !n.opts.loop && i >= t.group.length - 1), "image" === n.type ? r.find("[data-fancybox-zoom]").show().end().find("[data-fancybox-download]").attr("href", n.opts.image.src || n.src).show() : n.opts.toolbar && r.find("[data-fancybox-download],[data-fancybox-zoom]").hide()
      },
      hideControls: function() {
        this.isHiddenControls = !0, this.$refs.container.removeClass("fancybox-show-infobar fancybox-show-toolbar fancybox-show-caption fancybox-show-nav")
      },
      showControls: function() {
        var e = this,
          t = e.current ? e.current.opts : e.opts,
          n = e.$refs.container;
        e.isHiddenControls = !1, e.idleSecondsCounter = 0, n.toggleClass("fancybox-show-toolbar", !(!t.toolbar || !t.buttons)).toggleClass("fancybox-show-infobar", !!(t.infobar && 1 < e.group.length)).toggleClass("fancybox-show-nav", !!(t.arrows && 1 < e.group.length)).toggleClass("fancybox-is-modal", !!t.modal), e.$caption ? n.addClass("fancybox-show-caption ") : n.removeClass("fancybox-show-caption")
      },
      toggleControls: function() {
        this.isHiddenControls ? this.showControls() : this.hideControls()
      }
    }), g.fancybox = {
      version: "3.3.5",
      defaults: e,
      getInstance: function(e) {
        var t = g('.fancybox-container:not(".fancybox-is-closing"):last').data("FancyBox"),
          n = Array.prototype.slice.call(arguments, 1);
        return t instanceof i && ("string" === g.type(e) ? t[e].apply(t, n) : "function" === g.type(e) && e.apply(t, n), t)
      },
      open: function(e, t, n) {
        return new i(e, t, n)
      },
      close: function(e) {
        var t = this.getInstance();
        t && (t.close(), !0 === e && this.close())
      },
      destroy: function() {
        this.close(!0), a.add("body").off("click.fb-start", "**")
      },
      isMobile: s.createTouch !== m && /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent),
      use3d: (t = s.createElement("div"), u.getComputedStyle && u.getComputedStyle(t) && u.getComputedStyle(t).getPropertyValue("transform") && !(s.documentMode && s.documentMode < 11)),
      getTranslate: function(e) {
        var t;
        return !(!e || !e.length) && {
          top: (t = e[0].getBoundingClientRect()).top || 0,
          left: t.left || 0,
          width: t.width,
          height: t.height,
          opacity: parseFloat(e.css("opacity"))
        }
      },
      setTranslate: function(e, t) {
        var n = "",
          i = {};
        if (e && t) return t.left === m && t.top === m || (n = (t.left === m ? e.position().left : t.left) + "px, " + (t.top === m ? e.position().top : t.top) + "px", n = this.use3d ? "translate3d(" + n + ", 0px)" : "translate(" + n + ")"), t.scaleX !== m && t.scaleY !== m && (n = (n.length ? n + " " : "") + "scale(" + t.scaleX + ", " + t.scaleY + ")"), n.length && (i.transform = n), t.opacity !== m && (i.opacity = t.opacity), t.width !== m && (i.width = t.width), t.height !== m && (i.height = t.height), e.css(i)
      },
      animate: function(t, n, e, i, o) {
        var r = !1;
        g.isFunction(e) && (i = e, e = null), g.isPlainObject(n) || t.removeAttr("style"), g.fancybox.stop(t), t.on(h, function(e) {
          (!e || !e.originalEvent || t.is(e.originalEvent.target) && "z-index" != e.originalEvent.propertyName) && (g.fancybox.stop(t), r && g.fancybox.setTranslate(t, r), g.isPlainObject(n) ? !1 === o && t.removeAttr("style") : !0 !== o && t.removeClass(n), g.isFunction(i) && i(e))
        }), g.isNumeric(e) && t.css("transition-duration", e + "ms"), g.isPlainObject(n) ? (n.scaleX !== m && n.scaleY !== m && (r = g.extend({}, n, {
          width: t.width() * n.scaleX,
          height: t.height() * n.scaleY,
          scaleX: 1,
          scaleY: 1
        }), delete n.width, delete n.height, t.parent().hasClass("fancybox-slide--image") && t.parent().addClass("fancybox-is-scaling")), g.fancybox.setTranslate(t, n)) : t.addClass(n), t.data("timer", setTimeout(function() {
          t.trigger("transitionend")
        }, e + 16))
      },
      stop: function(e) {
        e && e.length && (clearTimeout(e.data("timer")), e.off("transitionend").css("transition-duration", ""), e.parent().removeClass("fancybox-is-scaling"))
      }
    }, g.fn.fancybox = function(e) {
      var t;
      return (t = (e = e || {}).selector || !1) ? g("body").off("click.fb-start", t).on("click.fb-start", t, {
        options: e
      }, n) : this.off("click.fb-start").on("click.fb-start", {
        items: this,
        options: e
      }, n), this
    }, a.on("click.fb-start", "[data-fancybox]", n), a.on("click.fb-start", "[data-trigger]", function(e) {
      n(e, {
        $target: g('[data-fancybox="' + g(e.currentTarget).attr("data-trigger") + '"]').eq(g(e.currentTarget).attr("data-index") || 0),
        $trigger: g(this)
      })
    })
  }
  var t
}(window, document, window.jQuery || jQuery),
function(h) {
  "use strict";
  var p = function(n, e, t) {
      if (n) return t = t || "", "object" === h.type(t) && (t = h.param(t, !0)), h.each(e, function(e, t) {
        n = n.replace("$" + e, t || "")
      }), t.length && (n += (0 < n.indexOf("?") ? "&" : "?") + t), n
    },
    i = {
      youtube: {
        matcher: /(youtube\.com|youtu\.be|youtube\-nocookie\.com)\/(watch\?(.*&)?v=|v\/|u\/|embed\/?)?(videoseries\?list=(.*)|[\w-]{11}|\?listType=(.*)&list=(.*))(.*)/i,
        params: {
          autoplay: 1,
          autohide: 1,
          fs: 1,
          rel: 0,
          hd: 1,
          wmode: "transparent",
          enablejsapi: 1,
          html5: 1
        },
        paramPlace: 8,
        type: "iframe",
        url: "//www.youtube.com/embed/$4",
        thumb: "//img.youtube.com/vi/$4/hqdefault.jpg"
      },
      vimeo: {
        matcher: /^.+vimeo.com\/(.*\/)?([\d]+)(.*)?/,
        params: {
          autoplay: 1,
          hd: 1,
          show_title: 1,
          show_byline: 1,
          show_portrait: 0,
          fullscreen: 1,
          api: 1
        },
        paramPlace: 3,
        type: "iframe",
        url: "//player.vimeo.com/video/$2"
      },
      instagram: {
        matcher: /(instagr\.am|instagram\.com)\/p\/([a-zA-Z0-9_\-]+)\/?/i,
        type: "image",
        url: "//$1/p/$2/media/?size=l"
      },
      gmap_place: {
        matcher: /(maps\.)?google\.([a-z]{2,3}(\.[a-z]{2})?)\/(((maps\/(place\/(.*)\/)?\@(.*),(\d+.?\d+?)z))|(\?ll=))(.*)?/i,
        type: "iframe",
        url: function(e) {
          return "//maps.google." + e[2] + "/?ll=" + (e[9] ? e[9] + "&z=" + Math.floor(e[10]) + (e[12] ? e[12].replace(/^\//, "&") : "") : e[12] + "").replace(/\?/, "&") + "&output=" + (e[12] && 0 < e[12].indexOf("layer=c") ? "svembed" : "embed")
        }
      },
      gmap_search: {
        matcher: /(maps\.)?google\.([a-z]{2,3}(\.[a-z]{2})?)\/(maps\/search\/)(.*)/i,
        type: "iframe",
        url: function(e) {
          return "//maps.google." + e[2] + "/maps?q=" + e[5].replace("query=", "q=").replace("api=1", "") + "&output=embed"
        }
      }
    };
  h(document).on("objectNeedsType.fb", function(e, t, o) {
    var n, r, s, a, l, c, u, d = o.src || "",
      f = !1;
    n = h.extend(!0, {}, i, o.opts.media), h.each(n, function(e, t) {
      if (s = d.match(t.matcher)) {
        if (f = t.type, u = e, c = {}, t.paramPlace && s[t.paramPlace]) {
          "?" == (l = s[t.paramPlace])[0] && (l = l.substring(1)), l = l.split("&");
          for (var n = 0; n < l.length; ++n) {
            var i = l[n].split("=", 2);
            2 == i.length && (c[i[0]] = decodeURIComponent(i[1].replace(/\+/g, " ")))
          }
        }
        return a = h.extend(!0, {}, t.params, o.opts[e], c), d = "function" === h.type(t.url) ? t.url.call(this, s, a, o) : p(t.url, s, a), r = "function" === h.type(t.thumb) ? t.thumb.call(this, s, a, o) : p(t.thumb, s), "youtube" === e ? d = d.replace(/&t=((\d+)m)?(\d+)s/, function(e, t, n, i) {
          return "&start=" + ((n ? 60 * parseInt(n, 10) : 0) + parseInt(i, 10))
        }) : "vimeo" === e && (d = d.replace("&%23", "#")), !1
      }
    }), f ? (o.opts.thumb || o.opts.$thumb && o.opts.$thumb.length || (o.opts.thumb = r), "iframe" === f && (o.opts = h.extend(!0, o.opts, {
      iframe: {
        preload: !1,
        attr: {
          scrolling: "no"
        }
      }
    })), h.extend(o, {
      type: f,
      src: d,
      origSrc: o.src,
      contentSource: u,
      contentType: "image" === f ? "image" : "gmap_place" == u || "gmap_search" == u ? "map" : "video"
    })) : d && (o.type = o.opts.defaultType)
  })
}(window.jQuery || jQuery),
function(g, a, m) {
  "use strict";
  var v = g.requestAnimationFrame || g.webkitRequestAnimationFrame || g.mozRequestAnimationFrame || g.oRequestAnimationFrame || function(e) {
      return g.setTimeout(e, 1e3 / 60)
    },
    y = g.cancelAnimationFrame || g.webkitCancelAnimationFrame || g.mozCancelAnimationFrame || g.oCancelAnimationFrame || function(e) {
      g.clearTimeout(e)
    },
    u = function(e) {
      var t = [];
      for (var n in e = (e = e.originalEvent || e || g.e).touches && e.touches.length ? e.touches : e.changedTouches && e.changedTouches.length ? e.changedTouches : [e]) e[n].pageX ? t.push({
        x: e[n].pageX,
        y: e[n].pageY
      }) : e[n].clientX && t.push({
        x: e[n].clientX,
        y: e[n].clientY
      });
      return t
    },
    b = function(e, t, n) {
      return t && e ? "x" === n ? e.x - t.x : "y" === n ? e.y - t.y : Math.sqrt(Math.pow(e.x - t.x, 2) + Math.pow(e.y - t.y, 2)) : 0
    },
    l = function(e) {
      if (e.is('a,area,button,[role="button"],input,label,select,summary,textarea,video,audio') || m.isFunction(e.get(0).onclick) || e.data("selectable")) return !0;
      for (var t = 0, n = e[0].attributes, i = n.length; t < i; t++)
        if ("data-fancybox-" === n[t].nodeName.substr(0, 14)) return !0;
      return !1
    },
    c = function(e) {
      for (var t = !1;
        (n = e.get(0), void 0, i = g.getComputedStyle(n)["overflow-y"], o = g.getComputedStyle(n)["overflow-x"], r = ("scroll" === i || "auto" === i) && n.scrollHeight > n.clientHeight, s = ("scroll" === o || "auto" === o) && n.scrollWidth > n.clientWidth, !(t = r || s)) && ((e = e.parent()).length && !e.hasClass("fancybox-stage") && !e.is("body")););
      var n, i, o, r, s;
      return t
    },
    n = function(e) {
      var t = this;
      t.instance = e, t.$bg = e.$refs.bg, t.$stage = e.$refs.stage, t.$container = e.$refs.container, t.destroy(), t.$container.on("touchstart.fb.touch mousedown.fb.touch", m.proxy(t, "ontouchstart"))
    };
  n.prototype.destroy = function() {
    this.$container.off(".fb.touch")
  }, n.prototype.ontouchstart = function(e) {
    var t = this,
      n = m(e.target),
      i = t.instance,
      o = i.current,
      r = o.$content,
      s = "touchstart" == e.type;
    if (s && t.$container.off("mousedown.fb.touch"), (!e.originalEvent || 2 != e.originalEvent.button) && n.length && !l(n) && !l(n.parent()) && (n.is("img") || !(e.originalEvent.clientX > n[0].clientWidth + n.offset().left))) {
      if (!o || i.isAnimating || i.isClosing) return e.stopPropagation(), void e.preventDefault();
      if (t.realPoints = t.startPoints = u(e), t.startPoints.length) {
        if (e.stopPropagation(), t.startEvent = e, t.canTap = !0, t.$target = n, t.$content = r, t.opts = o.opts.touch, t.isPanning = !1, t.isSwiping = !1, t.isZooming = !1, t.isScrolling = !1, t.startTime = (new Date).getTime(), t.distanceX = t.distanceY = t.distance = 0, t.canvasWidth = Math.round(o.$slide[0].clientWidth), t.canvasHeight = Math.round(o.$slide[0].clientHeight), t.contentLastPos = null, t.contentStartPos = m.fancybox.getTranslate(t.$content) || {
            top: 0,
            left: 0
          }, t.sliderStartPos = t.sliderLastPos || m.fancybox.getTranslate(o.$slide), t.stagePos = m.fancybox.getTranslate(i.$refs.stage), t.sliderStartPos.top -= t.stagePos.top, t.sliderStartPos.left -= t.stagePos.left, t.contentStartPos.top -= t.stagePos.top, t.contentStartPos.left -= t.stagePos.left, m(a).off(".fb.touch").on(s ? "touchend.fb.touch touchcancel.fb.touch" : "mouseup.fb.touch mouseleave.fb.touch", m.proxy(t, "ontouchend")).on(s ? "touchmove.fb.touch" : "mousemove.fb.touch", m.proxy(t, "ontouchmove")), m.fancybox.isMobile && a.addEventListener("scroll", t.onscroll, !0), !t.opts && !i.canPan() || !n.is(t.$stage) && !t.$stage.find(n).length) return void(n.is(".fancybox-image") && e.preventDefault());
        m.fancybox.isMobile && (c(n) || c(n.parent())) || e.preventDefault(), (1 === t.startPoints.length || o.hasError) && (t.instance.canPan() ? (m.fancybox.stop(t.$content), t.$content.css("transition-duration", ""), t.isPanning = !0) : t.isSwiping = !0, t.$container.addClass("fancybox-controls--isGrabbing")), 2 === t.startPoints.length && "image" === o.type && (o.isLoaded || o.$ghost) && (t.canTap = !1, t.isSwiping = !1, t.isPanning = !1, t.isZooming = !0, m.fancybox.stop(t.$content), t.$content.css("transition-duration", ""), t.centerPointStartX = .5 * (t.startPoints[0].x + t.startPoints[1].x) - m(g).scrollLeft(), t.centerPointStartY = .5 * (t.startPoints[0].y + t.startPoints[1].y) - m(g).scrollTop(), t.percentageOfImageAtPinchPointX = (t.centerPointStartX - t.contentStartPos.left) / t.contentStartPos.width, t.percentageOfImageAtPinchPointY = (t.centerPointStartY - t.contentStartPos.top) / t.contentStartPos.height, t.startDistanceBetweenFingers = b(t.startPoints[0], t.startPoints[1]))
      }
    }
  }, n.prototype.onscroll = function(e) {
    this.isScrolling = !0, a.removeEventListener("scroll", this.onscroll, !0)
  }, n.prototype.ontouchmove = function(e) {
    var t = this,
      n = m(e.target);
    return void 0 !== e.originalEvent.buttons && 0 === e.originalEvent.buttons ? void t.ontouchend(e) : t.isScrolling || !n.is(t.$stage) && !t.$stage.find(n).length ? void(t.canTap = !1) : (t.newPoints = u(e), void((t.opts || t.instance.canPan()) && t.newPoints.length && t.newPoints.length && (t.isSwiping && !0 === t.isSwiping || e.preventDefault(), t.distanceX = b(t.newPoints[0], t.startPoints[0], "x"), t.distanceY = b(t.newPoints[0], t.startPoints[0], "y"), t.distance = b(t.newPoints[0], t.startPoints[0]), 0 < t.distance && (t.isSwiping ? t.onSwipe(e) : t.isPanning ? t.onPan() : t.isZooming && t.onZoom()))))
  }, n.prototype.onSwipe = function(e) {
    var t, i = this,
      n = i.isSwiping,
      o = i.sliderStartPos.left || 0;
    if (!0 !== n) "x" == n && (0 < i.distanceX && (i.instance.group.length < 2 || 0 === i.instance.current.index && !i.instance.current.opts.loop) ? o += Math.pow(i.distanceX, .8) : i.distanceX < 0 && (i.instance.group.length < 2 || i.instance.current.index === i.instance.group.length - 1 && !i.instance.current.opts.loop) ? o -= Math.pow(-i.distanceX, .8) : o += i.distanceX), i.sliderLastPos = {
      top: "x" == n ? 0 : i.sliderStartPos.top + i.distanceY,
      left: o
    }, i.requestId && (y(i.requestId), i.requestId = null), i.requestId = v(function() {
      i.sliderLastPos && (m.each(i.instance.slides, function(e, t) {
        var n = t.pos - i.instance.currPos;
        m.fancybox.setTranslate(t.$slide, {
          top: i.sliderLastPos.top,
          left: i.sliderLastPos.left + n * i.canvasWidth + n * t.opts.gutter
        })
      }), i.$container.addClass("fancybox-is-sliding"))
    });
    else if (10 < Math.abs(i.distance)) {
      if (i.canTap = !1, i.instance.group.length < 2 && i.opts.vertical ? i.isSwiping = "y" : i.instance.isDragging || !1 === i.opts.vertical || "auto" === i.opts.vertical && 800 < m(g).width() ? i.isSwiping = "x" : (t = Math.abs(180 * Math.atan2(i.distanceY, i.distanceX) / Math.PI), i.isSwiping = 45 < t && t < 135 ? "y" : "x"), i.canTap = !1, "y" === i.isSwiping && m.fancybox.isMobile && (c(i.$target) || c(i.$target.parent()))) return void(i.isScrolling = !0);
      i.instance.isDragging = i.isSwiping, i.startPoints = i.newPoints, m.each(i.instance.slides, function(e, t) {
        m.fancybox.stop(t.$slide), t.$slide.css("transition-duration", ""), t.inTransition = !1, t.pos === i.instance.current.pos && (i.sliderStartPos.left = m.fancybox.getTranslate(t.$slide).left - m.fancybox.getTranslate(i.instance.$refs.stage).left)
      }), i.instance.SlideShow && i.instance.SlideShow.isActive && i.instance.SlideShow.stop()
    }
  }, n.prototype.onPan = function() {
    var e = this;
    return b(e.newPoints[0], e.realPoints[0]) < (m.fancybox.isMobile ? 10 : 5) ? void(e.startPoints = e.newPoints) : (e.canTap = !1, e.contentLastPos = e.limitMovement(), e.requestId && (y(e.requestId), e.requestId = null), void(e.requestId = v(function() {
      m.fancybox.setTranslate(e.$content, e.contentLastPos)
    })))
  }, n.prototype.limitMovement = function() {
    var e, t, n, i, o, r, s = this,
      a = s.canvasWidth,
      l = s.canvasHeight,
      c = s.distanceX,
      u = s.distanceY,
      d = s.contentStartPos,
      f = d.left,
      h = d.top,
      p = d.width,
      g = d.height;
    return o = a < p ? f + c : f, r = h + u, e = Math.max(0, .5 * a - .5 * p), t = Math.max(0, .5 * l - .5 * g), n = Math.min(a - p, .5 * a - .5 * p), i = Math.min(l - g, .5 * l - .5 * g), 0 < c && e < o && (o = e - 1 + Math.pow(-e + f + c, .8) || 0), c < 0 && o < n && (o = n + 1 - Math.pow(n - f - c, .8) || 0), 0 < u && t < r && (r = t - 1 + Math.pow(-t + h + u, .8) || 0), u < 0 && r < i && (r = i + 1 - Math.pow(i - h - u, .8) || 0), {
      top: r,
      left: o
    }
  }, n.prototype.limitPosition = function(e, t, n, i) {
    var o = this.canvasWidth,
      r = this.canvasHeight;
    return o < n ? e = (e = 0 < e ? 0 : e) < o - n ? o - n : e : e = Math.max(0, o / 2 - n / 2), r < i ? t = (t = 0 < t ? 0 : t) < r - i ? r - i : t : t = Math.max(0, r / 2 - i / 2), {
      top: t,
      left: e
    }
  }, n.prototype.onZoom = function() {
    var e = this,
      t = e.contentStartPos,
      n = t.width,
      i = t.height,
      o = t.left,
      r = t.top,
      s = b(e.newPoints[0], e.newPoints[1]) / e.startDistanceBetweenFingers,
      a = Math.floor(n * s),
      l = Math.floor(i * s),
      c = (n - a) * e.percentageOfImageAtPinchPointX,
      u = (i - l) * e.percentageOfImageAtPinchPointY,
      d = (e.newPoints[0].x + e.newPoints[1].x) / 2 - m(g).scrollLeft(),
      f = (e.newPoints[0].y + e.newPoints[1].y) / 2 - m(g).scrollTop(),
      h = d - e.centerPointStartX,
      p = {
        top: r + (u + (f - e.centerPointStartY)),
        left: o + (c + h),
        scaleX: s,
        scaleY: s
      };
    e.canTap = !1, e.newWidth = a, e.newHeight = l, e.contentLastPos = p, e.requestId && (y(e.requestId), e.requestId = null), e.requestId = v(function() {
      m.fancybox.setTranslate(e.$content, e.contentLastPos)
    })
  }, n.prototype.ontouchend = function(e) {
    var t = this,
      n = Math.max((new Date).getTime() - t.startTime, 1),
      i = t.isSwiping,
      o = t.isPanning,
      r = t.isZooming,
      s = t.isScrolling;
    return t.endPoints = u(e), t.$container.removeClass("fancybox-controls--isGrabbing"), m(a).off(".fb.touch"), a.removeEventListener("scroll", t.onscroll, !0), t.requestId && (y(t.requestId), t.requestId = null), t.isSwiping = !1, t.isPanning = !1, t.isZooming = !1, t.isScrolling = !1, t.instance.isDragging = !1, t.canTap ? t.onTap(e) : (t.speed = 366, t.velocityX = t.distanceX / n * .5, t.velocityY = t.distanceY / n * .5, t.speedX = Math.max(.5 * t.speed, Math.min(1.5 * t.speed, 1 / Math.abs(t.velocityX) * t.speed)), void(o ? t.endPanning() : r ? t.endZooming() : t.endSwiping(i, s)))
  }, n.prototype.endSwiping = function(e, t) {
    var n = this,
      i = !1,
      o = n.instance.group.length;
    n.sliderLastPos = null, "y" == e && !t && 50 < Math.abs(n.distanceY) ? (m.fancybox.animate(n.instance.current.$slide, {
      top: n.sliderStartPos.top + n.distanceY + 150 * n.velocityY,
      opacity: 0
    }, 200), i = n.instance.close(!0, 200)) : "x" == e && 50 < n.distanceX && 1 < o ? i = n.instance.previous(n.speedX) : "x" == e && n.distanceX < -50 && 1 < o && (i = n.instance.next(n.speedX)), !1 !== i || "x" != e && "y" != e || (t || o < 2 ? n.instance.centerSlide(n.instance.current, 150) : n.instance.jumpTo(n.instance.current.index)), n.$container.removeClass("fancybox-is-sliding")
  }, n.prototype.endPanning = function() {
    var e, t, n, i = this;
    i.contentLastPos && (!1 === i.opts.momentum ? (e = i.contentLastPos.left, t = i.contentLastPos.top) : (e = i.contentLastPos.left + i.velocityX * i.speed, t = i.contentLastPos.top + i.velocityY * i.speed), (n = i.limitPosition(e, t, i.contentStartPos.width, i.contentStartPos.height)).width = i.contentStartPos.width, n.height = i.contentStartPos.height, m.fancybox.animate(i.$content, n, 330))
  }, n.prototype.endZooming = function() {
    var e, t, n, i, o = this,
      r = o.instance.current,
      s = o.newWidth,
      a = o.newHeight;
    o.contentLastPos && (e = o.contentLastPos.left, i = {
      top: t = o.contentLastPos.top,
      left: e,
      width: s,
      height: a,
      scaleX: 1,
      scaleY: 1
    }, m.fancybox.setTranslate(o.$content, i), s < o.canvasWidth && a < o.canvasHeight ? o.instance.scaleToFit(150) : s > r.width || a > r.height ? o.instance.scaleToActual(o.centerPointStartX, o.centerPointStartY, 150) : (n = o.limitPosition(e, t, s, a), m.fancybox.setTranslate(o.$content, m.fancybox.getTranslate(o.$content)), m.fancybox.animate(o.$content, n, 150)))
  }, n.prototype.onTap = function(n) {
    var e, i = this,
      t = m(n.target),
      o = i.instance,
      r = o.current,
      s = n && u(n) || i.startPoints,
      a = s[0] ? s[0].x - m(g).scrollLeft() - i.stagePos.left : 0,
      l = s[0] ? s[0].y - m(g).scrollTop() - i.stagePos.top : 0,
      c = function(e) {
        var t = r.opts[e];
        if (m.isFunction(t) && (t = t.apply(o, [r, n])), t) switch (t) {
          case "close":
            o.close(i.startEvent);
            break;
          case "toggleControls":
            o.toggleControls(!0);
            break;
          case "next":
            o.next();
            break;
          case "nextOrClose":
            1 < o.group.length ? o.next() : o.close(i.startEvent);
            break;
          case "zoom":
            "image" == r.type && (r.isLoaded || r.$ghost) && (o.canPan() ? o.scaleToFit() : o.isScaledDown() ? o.scaleToActual(a, l) : o.group.length < 2 && o.close(i.startEvent))
        }
      };
    if ((!n.originalEvent || 2 != n.originalEvent.button) && (t.is("img") || !(a > t[0].clientWidth + t.offset().left))) {
      if (t.is(".fancybox-bg,.fancybox-inner,.fancybox-outer,.fancybox-container")) e = "Outside";
      else if (t.is(".fancybox-slide")) e = "Slide";
      else {
        if (!o.current.$content || !o.current.$content.find(t).addBack().filter(t).length) return;
        e = "Content"
      }
      if (i.tapped) {
        if (clearTimeout(i.tapped), i.tapped = null, 50 < Math.abs(a - i.tapX) || 50 < Math.abs(l - i.tapY)) return this;
        c("dblclick" + e)
      } else i.tapX = a, i.tapY = l, r.opts["dblclick" + e] && r.opts["dblclick" + e] !== r.opts["click" + e] ? i.tapped = setTimeout(function() {
        i.tapped = null, c("click" + e)
      }, 500) : c("click" + e);
      return this
    }
  }, m(a).on("onActivate.fb", function(e, t) {
    t && !t.Guestures && (t.Guestures = new n(t))
  })
}(window, document, window.jQuery || jQuery),
function(s, a) {
  "use strict";
  a.extend(!0, a.fancybox.defaults, {
    btnTpl: {
      slideShow: '<button data-fancybox-play class="fancybox-button fancybox-button--play" title="{{PLAY_START}}"><svg viewBox="0 0 40 40"><path d="M13,12 L27,20 L13,27 Z" /><path d="M15,10 v19 M23,10 v19" /></svg></button>'
    },
    slideShow: {
      autoStart: !1,
      speed: 3e3
    }
  });
  var n = function(e) {
    this.instance = e, this.init()
  };
  a.extend(n.prototype, {
    timer: null,
    isActive: !1,
    $button: null,
    init: function() {
      var e = this;
      e.$button = e.instance.$refs.toolbar.find("[data-fancybox-play]").on("click", function() {
        e.toggle()
      }), (e.instance.group.length < 2 || !e.instance.group[e.instance.currIndex].opts.slideShow) && e.$button.hide()
    },
    set: function(e) {
      var t = this;
      t.instance && t.instance.current && (!0 === e || t.instance.current.opts.loop || t.instance.currIndex < t.instance.group.length - 1) ? t.timer = setTimeout(function() {
        t.isActive && t.instance.jumpTo((t.instance.currIndex + 1) % t.instance.group.length)
      }, t.instance.current.opts.slideShow.speed) : (t.stop(), t.instance.idleSecondsCounter = 0, t.instance.showControls())
    },
    clear: function() {
      clearTimeout(this.timer), this.timer = null
    },
    start: function() {
      var e = this.instance.current;
      e && (this.isActive = !0, this.$button.attr("title", e.opts.i18n[e.opts.lang].PLAY_STOP).removeClass("fancybox-button--play").addClass("fancybox-button--pause"), this.set(!0))
    },
    stop: function() {
      var e = this.instance.current;
      this.clear(), this.$button.attr("title", e.opts.i18n[e.opts.lang].PLAY_START).removeClass("fancybox-button--pause").addClass("fancybox-button--play"), this.isActive = !1
    },
    toggle: function() {
      this.isActive ? this.stop() : this.start()
    }
  }), a(s).on({
    "onInit.fb": function(e, t) {
      t && !t.SlideShow && (t.SlideShow = new n(t))
    },
    "beforeShow.fb": function(e, t, n, i) {
      var o = t && t.SlideShow;
      i ? o && n.opts.slideShow.autoStart && o.start() : o && o.isActive && o.clear()
    },
    "afterShow.fb": function(e, t, n) {
      var i = t && t.SlideShow;
      i && i.isActive && i.set()
    },
    "afterKeydown.fb": function(e, t, n, i, o) {
      var r = t && t.SlideShow;
      !r || !n.opts.slideShow || 80 !== o && 32 !== o || a(s.activeElement).is("button,a,input") || (i.preventDefault(), r.toggle())
    },
    "beforeClose.fb onDeactivate.fb": function(e, t) {
      var n = t && t.SlideShow;
      n && n.stop()
    }
  }), a(s).on("visibilitychange", function() {
    var e = a.fancybox.getInstance(),
      t = e && e.SlideShow;
    t && t.isActive && (s.hidden ? t.clear() : t.set())
  })
}(document, window.jQuery || jQuery),
function(r, n) {
  "use strict";
  var t = function() {
    for (var e = [
        ["requestFullscreen", "exitFullscreen", "fullscreenElement", "fullscreenEnabled", "fullscreenchange", "fullscreenerror"],
        ["webkitRequestFullscreen", "webkitExitFullscreen", "webkitFullscreenElement", "webkitFullscreenEnabled", "webkitfullscreenchange", "webkitfullscreenerror"],
        ["webkitRequestFullScreen", "webkitCancelFullScreen", "webkitCurrentFullScreenElement", "webkitCancelFullScreen", "webkitfullscreenchange", "webkitfullscreenerror"],
        ["mozRequestFullScreen", "mozCancelFullScreen", "mozFullScreenElement", "mozFullScreenEnabled", "mozfullscreenchange", "mozfullscreenerror"],
        ["msRequestFullscreen", "msExitFullscreen", "msFullscreenElement", "msFullscreenEnabled", "MSFullscreenChange", "MSFullscreenError"]
      ], t = {}, n = 0; n < e.length; n++) {
      var i = e[n];
      if (i && i[1] in r) {
        for (var o = 0; o < i.length; o++) t[e[0][o]] = i[o];
        return t
      }
    }
    return !1
  }();
  if (t) {
    var i = {
      request: function(e) {
        (e = e || r.documentElement)[t.requestFullscreen](e.ALLOW_KEYBOARD_INPUT)
      },
      exit: function() {
        r[t.exitFullscreen]()
      },
      toggle: function(e) {
        e = e || r.documentElement, this.isFullscreen() ? this.exit() : this.request(e)
      },
      isFullscreen: function() {
        return Boolean(r[t.fullscreenElement])
      },
      enabled: function() {
        return Boolean(r[t.fullscreenEnabled])
      }
    };
    n.extend(!0, n.fancybox.defaults, {
      btnTpl: {
        fullScreen: '<button data-fancybox-fullscreen class="fancybox-button fancybox-button--fullscreen" title="{{FULL_SCREEN}}"><svg viewBox="0 0 40 40"><path d="M9,12 v16 h22 v-16 h-22 v8" /></svg></button>'
      },
      fullScreen: {
        autoStart: !1
      }
    }), n(r).on({
      "onInit.fb": function(e, t) {
        t && t.group[t.currIndex].opts.fullScreen ? (t.$refs.container.on("click.fb-fullscreen", "[data-fancybox-fullscreen]", function(e) {
          e.stopPropagation(), e.preventDefault(), i.toggle()
        }), t.opts.fullScreen && !0 === t.opts.fullScreen.autoStart && i.request(), t.FullScreen = i) : t && t.$refs.toolbar.find("[data-fancybox-fullscreen]").hide()
      },
      "afterKeydown.fb": function(e, t, n, i, o) {
        t && t.FullScreen && 70 === o && (i.preventDefault(), t.FullScreen.toggle())
      },
      "beforeClose.fb": function(e, t) {
        t && t.FullScreen && t.$refs.container.hasClass("fancybox-is-fullscreen") && i.exit()
      }
    }), n(r).on(t.fullscreenchange, function() {
      var e = i.isFullscreen(),
        t = n.fancybox.getInstance();
      t && (t.current && "image" === t.current.type && t.isAnimating && (t.current.$content.css("transition", "none"), t.isAnimating = !1, t.update(!0, !0, 0)), t.trigger("onFullscreenChange", e), t.$refs.container.toggleClass("fancybox-is-fullscreen", e))
    })
  } else n && n.fancybox && (n.fancybox.defaults.btnTpl.fullScreen = !1)
}(document, window.jQuery || jQuery),
function(e, r) {
  "use strict";
  var s = "fancybox-thumbs",
    a = s + "-active";
  r.fancybox.defaults = r.extend(!0, {
    btnTpl: {
      thumbs: '<button data-fancybox-thumbs class="fancybox-button fancybox-button--thumbs" title="{{THUMBS}}"><svg viewBox="0 0 120 120"><path d="M30,30 h14 v14 h-14 Z M50,30 h14 v14 h-14 Z M70,30 h14 v14 h-14 Z M30,50 h14 v14 h-14 Z M50,50 h14 v14 h-14 Z M70,50 h14 v14 h-14 Z M30,70 h14 v14 h-14 Z M50,70 h14 v14 h-14 Z M70,70 h14 v14 h-14 Z" /></svg></button>'
    },
    thumbs: {
      autoStart: !1,
      hideOnClose: !0,
      parentEl: ".fancybox-container",
      axis: "y"
    }
  }, r.fancybox.defaults);
  var i = function(e) {
    this.init(e)
  };
  r.extend(i.prototype, {
    $button: null,
    $grid: null,
    $list: null,
    isVisible: !1,
    isActive: !1,
    init: function(e) {
      var t, n, i = this;
      ((i.instance = e).Thumbs = i).opts = e.group[e.currIndex].opts.thumbs, t = (t = e.group[0]).opts.thumb || !(!t.opts.$thumb || !t.opts.$thumb.length) && t.opts.$thumb.attr("src"), 1 < e.group.length && (n = (n = e.group[1]).opts.thumb || !(!n.opts.$thumb || !n.opts.$thumb.length) && n.opts.$thumb.attr("src")), i.$button = e.$refs.toolbar.find("[data-fancybox-thumbs]"), i.opts && t && n && t && n ? (i.$button.show().on("click", function() {
        i.toggle()
      }), i.isActive = !0) : i.$button.hide()
    },
    create: function() {
      var n, e = this,
        t = e.instance,
        i = e.opts.parentEl,
        o = [];
      e.$grid || (e.$grid = r('<div class="' + s + " " + s + "-" + e.opts.axis + '"></div>').appendTo(t.$refs.container.find(i).addBack().filter(i)), e.$grid.on("click", "li", function() {
        t.jumpTo(r(this).attr("data-index"))
      })), e.$list || (e.$list = r("<ul>").appendTo(e.$grid)), r.each(t.group, function(e, t) {
        (n = t.opts.thumb || (t.opts.$thumb ? t.opts.$thumb.attr("src") : null)) || "image" !== t.type || (n = t.src), o.push('<li data-index="' + e + '" tabindex="0" class="fancybox-thumbs-loading"' + (n && n.length ? ' style="background-image:url(' + n + ')" />' : "") + "></li>")
      }), e.$list[0].innerHTML = o.join(""), "x" === e.opts.axis && e.$list.width(parseInt(e.$grid.css("padding-right"), 10) + t.group.length * e.$list.children().eq(0).outerWidth(!0))
    },
    focus: function(e) {
      var t, n, i = this,
        o = i.$list,
        r = i.$grid;
      i.instance.current && (n = (t = o.children().removeClass(a).filter('[data-index="' + i.instance.current.index + '"]').addClass(a)).position(), "y" === i.opts.axis && (n.top < 0 || n.top > o.height() - t.outerHeight()) ? o.stop().animate({
        scrollTop: o.scrollTop() + n.top
      }, e) : "x" === i.opts.axis && (n.left < r.scrollLeft() || n.left > r.scrollLeft() + (r.width() - t.outerWidth())) && o.parent().stop().animate({
        scrollLeft: n.left
      }, e))
    },
    update: function() {
      var e = this;
      e.instance.$refs.container.toggleClass("fancybox-show-thumbs", this.isVisible), e.isVisible ? (e.$grid || e.create(), e.instance.trigger("onThumbsShow"), e.focus(0)) : e.$grid && e.instance.trigger("onThumbsHide"), e.instance.update()
    },
    hide: function() {
      this.isVisible = !1, this.update()
    },
    show: function() {
      this.isVisible = !0, this.update()
    },
    toggle: function() {
      this.isVisible = !this.isVisible, this.update()
    }
  }), r(e).on({
    "onInit.fb": function(e, t) {
      var n;
      t && !t.Thumbs && ((n = new i(t)).isActive && !0 === n.opts.autoStart && n.show())
    },
    "beforeShow.fb": function(e, t, n, i) {
      var o = t && t.Thumbs;
      o && o.isVisible && o.focus(i ? 0 : 250)
    },
    "afterKeydown.fb": function(e, t, n, i, o) {
      var r = t && t.Thumbs;
      r && r.isActive && 71 === o && (i.preventDefault(), r.toggle())
    },
    "beforeClose.fb": function(e, t) {
      var n = t && t.Thumbs;
      n && n.isVisible && !1 !== n.opts.hideOnClose && n.$grid.hide()
    }
  })
}(document, window.jQuery || jQuery),
function(e, s) {
  "use strict";
  s.extend(!0, s.fancybox.defaults, {
    btnTpl: {
      share: '<button data-fancybox-share class="fancybox-button fancybox-button--share" title="{{SHARE}}"><svg viewBox="0 0 40 40"><path d="M6,30 C8,18 19,16 23,16 L23,16 L23,10 L33,20 L23,29 L23,24 C19,24 8,27 6,30 Z"></svg></button>'
    },
    share: {
      url: function(e, t) {
        return !e.currentHash && "inline" !== t.type && "html" !== t.type && (t.origSrc || t.src) || window.location
      },
      tpl: '<div class="fancybox-share"><h1>{{SHARE}}</h1><p><a class="fancybox-share__button fancybox-share__button--fb" href="https://www.facebook.com/sharer/sharer.php?u={{url}}"><svg viewBox="0 0 512 512" xmlns="http://www.w3.org/2000/svg"><path d="m287 456v-299c0-21 6-35 35-35h38v-63c-7-1-29-3-55-3-54 0-91 33-91 94v306m143-254h-205v72h196" /></svg><span>Facebook</span></a><a class="fancybox-share__button fancybox-share__button--tw" href="https://twitter.com/intent/tweet?url={{url}}&text={{descr}}"><svg viewBox="0 0 512 512" xmlns="http://www.w3.org/2000/svg"><path d="m456 133c-14 7-31 11-47 13 17-10 30-27 37-46-15 10-34 16-52 20-61-62-157-7-141 75-68-3-129-35-169-85-22 37-11 86 26 109-13 0-26-4-37-9 0 39 28 72 65 80-12 3-25 4-37 2 10 33 41 57 77 57-42 30-77 38-122 34 170 111 378-32 359-208 16-11 30-25 41-42z" /></svg><span>Twitter</span></a><a class="fancybox-share__button fancybox-share__button--pt" href="https://www.pinterest.com/pin/create/button/?url={{url}}&description={{descr}}&media={{media}}"><svg viewBox="0 0 512 512" xmlns="http://www.w3.org/2000/svg"><path d="m265 56c-109 0-164 78-164 144 0 39 15 74 47 87 5 2 10 0 12-5l4-19c2-6 1-8-3-13-9-11-15-25-15-45 0-58 43-110 113-110 62 0 96 38 96 88 0 67-30 122-73 122-24 0-42-19-36-44 6-29 20-60 20-81 0-19-10-35-31-35-25 0-44 26-44 60 0 21 7 36 7 36l-30 125c-8 37-1 83 0 87 0 3 4 4 5 2 2-3 32-39 42-75l16-64c8 16 31 29 56 29 74 0 124-67 124-157 0-69-58-132-146-132z" fill="#fff"/></svg><span>Pinterest</span></a></p><p><input class="fancybox-share__input" type="text" value="{{url_raw}}" /></p></div>'
    }
  }), s(e).on("click", "[data-fancybox-share]", function() {
    var e, t, n, i, o = s.fancybox.getInstance(),
      r = o.current || null;
    r && ("function" === s.type(r.opts.share.url) && (e = r.opts.share.url.apply(r, [o, r])), t = r.opts.share.tpl.replace(/\{\{media\}\}/g, "image" === r.type ? encodeURIComponent(r.src) : "").replace(/\{\{url\}\}/g, encodeURIComponent(e)).replace(/\{\{url_raw\}\}/g, (n = e, i = {
      "&": "&amp;",
      "<": "&lt;",
      ">": "&gt;",
      '"': "&quot;",
      "'": "&#39;",
      "/": "&#x2F;",
      "`": "&#x60;",
      "=": "&#x3D;"
    }, String(n).replace(/[&<>"'`=\/]/g, function(e) {
      return i[e]
    }))).replace(/\{\{descr\}\}/g, o.$caption ? encodeURIComponent(o.$caption.text()) : ""), s.fancybox.open({
      src: o.translate(o, t),
      type: "html",
      opts: {
        animationEffect: !1,
        afterLoad: function(e, t) {
          o.$refs.container.one("beforeClose.fb", function() {
            e.close(null, 0)
          }), t.$content.find(".fancybox-share__links a").click(function() {
            return window.open(this.href, "Share", "width=550, height=450"), !1
          })
        }
      }
    }))
  })
}(document, window.jQuery || jQuery),
function(r, s, o) {
  "use strict";

  function a() {
    var e = s.location.hash.substr(1),
      t = e.split("-"),
      n = 1 < t.length && /^\+?\d+$/.test(t[t.length - 1]) && parseInt(t.pop(-1), 10) || 1;
    return {
      hash: e,
      index: n < 1 ? 1 : n,
      gallery: t.join("-")
    }
  }

  function t(e) {
    "" !== e.gallery && o("[data-fancybox='" + o.escapeSelector(e.gallery) + "']").eq(e.index - 1).trigger("click.fb-start")
  }

  function l(e) {
    var t, n;
    return !!e && ("" !== (n = (t = e.current ? e.current.opts : e.opts).hash || (t.$orig ? t.$orig.data("fancybox") : "")) && n)
  }
  o.escapeSelector || (o.escapeSelector = function(e) {
    return (e + "").replace(/([\0-\x1f\x7f]|^-?\d)|^-$|[^\x80-\uFFFF\w-]/g, function(e, t) {
      return t ? "\0" === e ? "�" : e.slice(0, -1) + "\\" + e.charCodeAt(e.length - 1).toString(16) + " " : "\\" + e
    })
  }), o(function() {
    !1 !== o.fancybox.defaults.hash && (o(r).on({
      "onInit.fb": function(e, t) {
        var n, i;
        !1 !== t.group[t.currIndex].opts.hash && (n = a(), (i = l(t)) && n.gallery && i == n.gallery && (t.currIndex = n.index - 1))
      },
      "beforeShow.fb": function(e, t, n, i) {
        var o;
        n && !1 !== n.opts.hash && ((o = l(t)) && (t.currentHash = o + (1 < t.group.length ? "-" + (n.index + 1) : ""), s.location.hash !== "#" + t.currentHash && (t.origHash || (t.origHash = s.location.hash), t.hashTimer && clearTimeout(t.hashTimer), t.hashTimer = setTimeout(function() {
          "replaceState" in s.history ? (s.history[i ? "pushState" : "replaceState"]({}, r.title, s.location.pathname + s.location.search + "#" + t.currentHash), i && (t.hasCreatedHistory = !0)) : s.location.hash = t.currentHash, t.hashTimer = null
        }, 300))))
      },
      "beforeClose.fb": function(e, t, n) {
        !1 !== n.opts.hash && (l(t), t.currentHash && t.hasCreatedHistory ? s.history.back() : t.currentHash && ("replaceState" in s.history ? s.history.replaceState({}, r.title, s.location.pathname + s.location.search + (t.origHash || "")) : s.location.hash = t.origHash), t.currentHash = null, clearTimeout(t.hashTimer))
      }
    }), o(s).on("hashchange.fb", function() {
      var i, e = a();
      o.each(o(".fancybox-container").get().reverse(), function(e, t) {
        var n = o(t).data("FancyBox");
        if (n.currentHash) return i = n, !1
      }), i ? !i.currentHash || i.currentHash === e.gallery + "-" + e.index || 1 === e.index && i.currentHash == e.gallery || (i.currentHash = null, i.close()) : "" !== e.gallery && t(e)
    }), setTimeout(function() {
      o.fancybox.getInstance() || t(a())
    }, 50))
  })
}(document, window, window.jQuery || jQuery),
function(e, t) {
  "use strict";
  var o = (new Date).getTime();
  t(e).on({
    "onInit.fb": function(e, i, t) {
      i.$refs.stage.on("mousewheel DOMMouseScroll wheel MozMousePixelScroll", function(e) {
        var t = i.current,
          n = (new Date).getTime();
        i.group.length < 2 || !1 === t.opts.wheel || "auto" === t.opts.wheel && "image" !== t.type || (e.preventDefault(), e.stopPropagation(), t.$slide.hasClass("fancybox-animated") || (e = e.originalEvent || e, n - o < 250 || (o = n, i[(-e.deltaY || -e.deltaX || e.wheelDelta || -e.detail) < 0 ? "next" : "previous"]())))
      })
    }
  })
}(document, window.jQuery || jQuery),
function(le) {
  function ce(e, t) {
    return this instanceof ce ? (le.isPlainObject(e) ? t = e : (t = t || {}).alias = e, this.el = void 0, this.opts = le.extend(!0, {}, this.defaults, t), this.noMasksCache = t && void 0 !== t.definitions, this.userOptions = t || {}, this.events = {}, this.dataAttribute = "data-inputmask", void this.resolveAlias(this.opts.alias, t, this.opts)) : new ce(e, t)
  }

  function o(n, r) {
    function e(e, t, n) {
      if (null !== e && "" !== e) {
        if (1 === e.length && !1 === n.greedy && 0 !== n.repeat && (n.placeholder = ""), 0 < n.repeat || "*" === n.repeat || "+" === n.repeat) {
          var i = "*" === n.repeat ? 0 : "+" === n.repeat ? 1 : n.repeat;
          e = n.groupmarker.start + e + n.groupmarker.end + n.quantifiermarker.start + i + "," + n.repeat + n.quantifiermarker.end
        }
        var o;
        return void 0 === ce.prototype.masksCache[e] || !0 === r ? (o = {
          mask: e,
          maskToken: ce.prototype.analyseMask(e, n),
          validPositions: {},
          _buffer: void 0,
          buffer: void 0,
          tests: {},
          metadata: t,
          maskLength: void 0
        }, !0 !== r && (ce.prototype.masksCache[n.numericInput ? e.split("").reverse().join("") : e] = o, o = le.extend(!0, {}, ce.prototype.masksCache[n.numericInput ? e.split("").reverse().join("") : e]))) : o = le.extend(!0, {}, ce.prototype.masksCache[n.numericInput ? e.split("").reverse().join("") : e]), o
      }
    }
    var t;
    if (le.isFunction(n.mask) && (n.mask = n.mask(n)), le.isArray(n.mask)) {
      if (1 < n.mask.length) {
        n.keepStatic = null === n.keepStatic || n.keepStatic;
        var i = n.groupmarker.start;
        return le.each(n.numericInput ? n.mask.reverse() : n.mask, function(e, t) {
          1 < i.length && (i += n.groupmarker.end + n.alternatormarker + n.groupmarker.start), i += void 0 === t.mask || le.isFunction(t.mask) ? t : t.mask
        }), e(i += n.groupmarker.end, n.mask, n)
      }
      n.mask = n.mask.pop()
    }
    return n.mask && (t = void 0 === n.mask.mask || le.isFunction(n.mask.mask) ? e(n.mask, n.mask, n) : e(n.mask.mask, n.mask, n)), t
  }

  function ue(e, t, R) {
    function n(e, t, n) {
      t = t || 0;
      var i, o, r, s = [],
        a = 0,
        l = C();
      for (-1 === (X = void 0 !== U ? U.maxLength : void 0) && (X = void 0); !0 === e && N().validPositions[a] ? (o = (r = N().validPositions[a]).match, i = r.locator.slice(), s.push(!0 === n ? r.input : !1 === n ? o.nativeDef : E(a, o))) : (o = (r = p(a, i, a - 1)).match, i = r.locator.slice(), (!1 === R.jitMasking || a < l || Number.isFinite(R.jitMasking) && R.jitMasking > a) && s.push(!1 === n ? o.nativeDef : E(a, o))), a++, (void 0 === X || a < X) && (null !== o.fn || "" !== o.def) || a < t;);
      return "" === s[s.length - 1] && s.pop(), N().maskLength = a + 1, s
    }

    function N() {
      return t
    }

    function k(e) {
      var t = N();
      !(t.buffer = void 0) !== e && (t._buffer = void 0, t.validPositions = {}, t.p = 0)
    }

    function C(e, t, n) {
      var i = -1,
        o = -1,
        r = n || N().validPositions;
      for (var s in void 0 === e && (e = -1), r) {
        var a = parseInt(s);
        r[a] && (t || null !== r[a].match.fn) && (a <= e && (i = a), e <= a && (o = a))
      }
      return -1 !== i && 1 < e - i || o < e ? i : o
    }

    function b(e, t, n, i) {
      function o(e) {
        var t = N().validPositions[e];
        if (void 0 !== t && null === t.match.fn) {
          var n = N().validPositions[e - 1],
            i = N().validPositions[e + 1];
          return void 0 !== n && void 0 !== i
        }
        return !1
      }
      var r, s = e,
        a = le.extend(!0, {}, N().validPositions),
        l = !1;
      for (N().p = e, r = t - 1; s <= r; r--) void 0 !== N().validPositions[r] && (!0 !== n && (!N().validPositions[r].match.optionality && o(r) || !1 === R.canClearPosition(N(), r, C(), i, R)) || delete N().validPositions[r]);
      for (k(!0), r = s + 1; r <= C();) {
        for (; void 0 !== N().validPositions[s];) s++;
        var c = N().validPositions[s];
        if (r < s && (r = s + 1), void 0 === N().validPositions[r] && j(r) || void 0 !== c) r++;
        else {
          var u = p(r);
          !1 === l && a[s] && a[s].match.def === u.match.def ? (N().validPositions[s] = le.extend(!0, {}, a[s]), N().validPositions[s].input = u.input, delete N().validPositions[r], r++) : x(s, u.match.def) ? !1 !== A(s, u.input || E(r), !0) && (delete N().validPositions[r], r++, l = !0) : j(r) || (r++, s--), s++
        }
      }
      k(!0)
    }

    function h(e, t) {
      for (var n, i = e, o = C(), r = N().validPositions[o] || _(0)[0], s = void 0 !== r.alternation ? r.locator[r.alternation].toString().split(",") : [], a = 0; a < i.length && (!((n = i[a]).match && (R.greedy && !0 !== n.match.optionalQuantifier || (!1 === n.match.optionality || !1 === n.match.newBlockMarker) && !0 !== n.match.optionalQuantifier) && (void 0 === r.alternation || r.alternation !== n.alternation || void 0 !== n.locator[r.alternation] && $(n.locator[r.alternation].toString().split(","), s))) || !0 === t && (null !== n.match.fn || /[0-9a-bA-Z]/.test(n.match.def))); a++);
      return n
    }

    function p(e, t, n) {
      return N().validPositions[e] || h(_(e, t ? t.slice() : t, n))
    }

    function u(e) {
      return N().validPositions[e] ? N().validPositions[e] : _(e)[0]
    }

    function x(e, t) {
      for (var n = !1, i = _(e), o = 0; o < i.length; o++)
        if (i[o].match && i[o].match.def === t) {
          n = !0;
          break
        }
      return n
    }

    function _(E, e, t) {
      function D(A, j, e, t) {
        function P(e, n, t) {
          function r(n, i) {
            var o = 0 === le.inArray(n, i.matches);
            return o || le.each(i.matches, function(e, t) {
              if (!0 === t.isQuantifier && (o = r(n, i.matches[e - 1]))) return !1
            }), o
          }

          function i(e, o, r) {
            var s, a;
            return (N().tests[e] || N().validPositions[e]) && le.each(N().tests[e] || [N().validPositions[e]], function(e, t) {
              var n = void 0 !== r ? r : t.alternation,
                i = t.locator[n] ? t.locator[n].toString().indexOf(o) : -1;
              (void 0 === a || i < a) && -1 !== i && (s = t, a = i)
            }), s ? s.locator.slice(s.alternation + 1) : void 0 !== r ? i(e, o) : void 0
          }
          if (1e4 < M) throw "Inputmask: There is probably an error in your mask definition or in the code. Create an issue on github with an example of the mask you are using. " + N().mask;
          if (M === E && void 0 === e.matches) return I.push({
            match: e,
            locator: n.reverse(),
            cd: L
          }), !0;
          if (void 0 !== e.matches) {
            if (e.isGroup && t !== e) {
              if (e = P(A.matches[le.inArray(e, A.matches) + 1], n)) return !0
            } else if (e.isOptional) {
              var o = e;
              if (e = D(e, j, n, t)) {
                if (!r(O = I[I.length - 1].match, o)) return !0;
                H = !0, M = E
              }
            } else if (e.isAlternator) {
              var s, a = e,
                l = [],
                c = I.slice(),
                u = n.length,
                d = 0 < j.length ? j.shift() : -1;
              if (-1 === d || "string" == typeof d) {
                var f, h = M,
                  p = j.slice(),
                  g = [];
                if ("string" == typeof d) g = d.split(",");
                else
                  for (f = 0; f < a.matches.length; f++) g.push(f);
                for (var m = 0; m < g.length; m++) {
                  if (f = parseInt(g[m]), I = [], j = i(M, f, u) || p.slice(), !0 !== (e = P(a.matches[f] || A.matches[f], [f].concat(n), t) || e) && void 0 !== e && g[g.length - 1] < a.matches.length) {
                    var v = le.inArray(e, A.matches) + 1;
                    A.matches.length > v && ((e = P(A.matches[v], [v].concat(n.slice(1, n.length)), t)) && (g.push(v.toString()), le.each(I, function(e, t) {
                      t.alternation = n.length - 1
                    })))
                  }
                  s = I.slice(), M = h, I = [];
                  for (var y = 0; y < s.length; y++) {
                    var b = s[y],
                      x = !1;
                    b.alternation = b.alternation || u;
                    for (var w = 0; w < l.length; w++) {
                      var S = l[w];
                      if (("string" != typeof d || -1 !== le.inArray(b.locator[b.alternation].toString(), g)) && (b.match.def === S.match.def || ($ = S, null === (T = b).match.fn && null !== $.match.fn && $.match.fn.test(T.match.def, N(), E, !1, R, !1)))) {
                        x = b.match.nativeDef === S.match.nativeDef, b.alternation == S.alternation && -1 === S.locator[S.alternation].toString().indexOf(b.locator[b.alternation]) && (S.locator[S.alternation] = S.locator[S.alternation] + "," + b.locator[b.alternation], S.alternation = b.alternation, null == b.match.fn && (S.na = S.na || b.locator[b.alternation].toString(), -1 === S.na.indexOf(b.locator[b.alternation]) && (S.na = S.na + "," + b.locator[b.alternation])));
                        break
                      }
                    }
                    x || l.push(b)
                  }
                }
                "string" == typeof d && (l = le.map(l, function(e, t) {
                  if (isFinite(t)) {
                    var n = e.alternation,
                      i = e.locator[n].toString().split(",");
                    e.locator[n] = void 0, e.alternation = void 0;
                    for (var o = 0; o < i.length; o++) - 1 !== le.inArray(i[o], g) && (void 0 !== e.locator[n] ? (e.locator[n] += ",", e.locator[n] += i[o]) : e.locator[n] = parseInt(i[o]), e.alternation = n);
                    if (void 0 !== e.locator[n]) return e
                  }
                })), I = c.concat(l), M = E, H = 0 < I.length, j = p.slice()
              } else e = P(a.matches[d] || A.matches[d], [d].concat(n), t);
              if (e) return !0
            } else if (e.isQuantifier && t !== A.matches[le.inArray(e, A.matches) - 1])
              for (var k = e, C = 0 < j.length ? j.shift() : 0; C < (isNaN(k.quantifier.max) ? C + 1 : k.quantifier.max) && M <= E; C++) {
                var _ = A.matches[le.inArray(k, A.matches) - 1];
                if (e = P(_, [C].concat(n), _)) {
                  if ((O = I[I.length - 1].match).optionalQuantifier = C > k.quantifier.min - 1, r(O, _)) {
                    if (C > k.quantifier.min - 1) {
                      H = !0, M = E;
                      break
                    }
                    return !0
                  }
                  return !0
                }
              } else if (e = D(e, j, n, t)) return !0
          } else M++;
          var T, $
        }
        for (var n = 0 < j.length ? j.shift() : 0; n < A.matches.length; n++)
          if (!0 !== A.matches[n].isQuantifier) {
            var i = P(A.matches[n], [n].concat(e), t);
            if (i && M === E) return i;
            if (E < M) break
          }
      }

      function n(e) {
        return R.keepStatic && 0 < E && e.length > 1 + ("" === e[e.length - 1].match.def ? 1 : 0) && !0 !== e[0].match.optionality && !0 !== e[0].match.optionalQuantifier && null === e[0].match.fn && !/[0-9a-bA-Z]/.test(e[0].match.def) ? [h(e)] : e
      }
      var O, i, o, r = N().maskToken,
        M = e ? t : 0,
        s = e ? e.slice() : [0],
        I = [],
        H = !1,
        L = e ? e.join("") : "";
      if (-1 < E) {
        if (void 0 === e) {
          for (var a, l = E - 1; void 0 === (a = N().validPositions[l] || N().tests[l]) && -1 < l;) l--;
          void 0 !== a && -1 < l && (i = a, o = [], le.isArray(i) || (i = [i]), 0 < i.length && (void 0 === i[0].alternation ? 0 === (o = h(i.slice()).locator.slice()).length && (o = i[0].locator.slice()) : le.each(i, function(e, t) {
            if ("" !== t.def)
              if (0 === o.length) o = t.locator.slice();
              else
                for (var n = 0; n < o.length; n++) t.locator[n] && -1 === o[n].toString().indexOf(t.locator[n]) && (o[n] += "," + t.locator[n])
          })), L = (s = o).join(""), M = l)
        }
        if (N().tests[E] && N().tests[E][0].cd === L) return n(N().tests[E]);
        for (var c = s.shift(); c < r.length; c++) {
          if (D(r[c], s, [c]) && M === E || E < M) break
        }
      }
      return (0 === I.length || H) && I.push({
        match: {
          fn: null,
          cardinality: 0,
          optionality: !0,
          casing: null,
          def: "",
          placeholder: ""
        },
        locator: [],
        cd: L
      }), void 0 !== e && N().tests[E] ? n(le.extend(!0, [], I)) : (N().tests[E] = le.extend(!0, [], I), n(N().tests[E]))
    }

    function g() {
      return void 0 === N()._buffer && (N()._buffer = n(!1, 1), void 0 === N().buffer && N()._buffer.slice()), N()._buffer
    }

    function w(e) {
      return void 0 !== N().buffer && !0 !== e || (N().buffer = n(!0, C(), !0)), N().buffer
    }

    function T(e, t, n) {
      var i;
      if (!0 === e) k(), e = 0, t = n.length;
      else
        for (i = e; i < t; i++) delete N().validPositions[i];
      for (i = e; i < t; i++) k(!0), n[i] !== R.skipOptionalPartCharacter && A(i, n[i], !0, !0)
    }

    function $(e, t) {
      for (var n = R.greedy ? t : t.slice(0, 1), i = !1, o = 0; o < e.length; o++)
        if (-1 !== le.inArray(e[o], n)) {
          i = !0;
          break
        }
      return i
    }

    function A(g, e, t, S, n) {
      function m(e) {
        var t = K ? 1 < e.begin - e.end || e.begin - e.end == 1 && R.insertMode : 1 < e.end - e.begin || e.end - e.begin == 1 && R.insertMode;
        return t && 0 === e.begin && e.end === N().maskLength ? "full" : t
      }

      function o(d, f, h) {
        var p = !1;
        return le.each(_(d), function(e, t) {
          for (var n = t.match, i = f ? 1 : 0, o = "", r = n.cardinality; i < r; r--) o += (s = d - (r - 1), void 0 === N().validPositions[s] ? E(s) : N().validPositions[s].input);
          var s;
          if (f && (o += f), w(!0), !1 !== (p = null != n.fn ? n.fn.test(o, N(), d, h, R, m(g)) : (f === n.def || f === R.skipOptionalPartCharacter) && "" !== n.def && {
              c: n.placeholder || n.def,
              pos: d
            })) {
            var a = void 0 !== p.c ? p.c : f;
            a = a === R.skipOptionalPartCharacter && null === n.fn ? n.placeholder || n.def : a;
            var l = d,
              c = w();
            if (void 0 !== p.remove && (le.isArray(p.remove) || (p.remove = [p.remove]), le.each(p.remove.sort(function(e, t) {
                return t - e
              }), function(e, t) {
                b(t, t + 1, !0)
              })), void 0 !== p.insert && (le.isArray(p.insert) || (p.insert = [p.insert]), le.each(p.insert.sort(function(e, t) {
                return e - t
              }), function(e, t) {
                A(t.pos, t.c, !0, S)
              })), p.refreshFromBuffer) {
              var u = p.refreshFromBuffer;
              if (T((h = !0) === u ? u : u.start, u.end, c), void 0 === p.pos && void 0 === p.c) return p.pos = C(), !1;
              if ((l = void 0 !== p.pos ? p.pos : d) !== d) return p = le.extend(p, A(l, a, !0, S)), !1
            } else if (!0 !== p && void 0 !== p.pos && p.pos !== d && (l = p.pos, T(d, l, w().slice()), l !== d)) return p = le.extend(p, A(l, a, !0)), !1;
            return (!0 === p || void 0 !== p.pos || void 0 !== p.c) && (0 < e && k(!0), v(l, le.extend({}, t, {
              input: function(e, t, n) {
                switch (R.casing || t.casing) {
                  case "upper":
                    e = e.toUpperCase();
                    break;
                  case "lower":
                    e = e.toLowerCase();
                    break;
                  case "title":
                    var i = N().validPositions[n - 1];
                    e = 0 === n || i && i.input === String.fromCharCode(ce.keyCode.SPACE) ? e.toUpperCase() : e.toLowerCase()
                }
                return e
              }(a, n, l)
            }), S, m(g)) || (p = !1), !1)
          }
        }), p
      }

      function i(e, t) {
        var n = N().validPositions[t];
        if (n)
          for (var i = n.locator, o = i.length, r = e; r < t; r++)
            if (void 0 === N().validPositions[r] && !j(r, !0)) {
              var s = _(r),
                a = s[0],
                l = -1;
              le.each(s, function(e, t) {
                for (var n = 0; n < o && void 0 !== t.locator[n] && $(t.locator[n].toString().split(","), i[n].toString().split(",")); n++) l < n && (l = n, a = t)
              }), v(r, le.extend({}, a, {
                input: a.match.placeholder || a.match.def
              }), !0)
            }
      }

      function v(e, t, n, i) {
        if (i || R.insertMode && void 0 !== N().validPositions[e] && void 0 === n) {
          var o, r = le.extend(!0, {}, N().validPositions),
            s = C(void 0, !0);
          for (o = e; o <= s; o++) delete N().validPositions[o];
          N().validPositions[e] = le.extend(!0, {}, t);
          var a, l = !0,
            c = N().validPositions,
            u = !1,
            d = N().maskLength;
          for (o = a = e; o <= s; o++) {
            var f = r[o];
            if (void 0 !== f)
              for (var h = a; h < N().maskLength && (null == f.match.fn && c[o] && (!0 === c[o].match.optionalQuantifier || !0 === c[o].match.optionality) || null != f.match.fn);) {
                if (h++, !1 === u && r[h] && r[h].match.def === f.match.def) N().validPositions[h] = le.extend(!0, {}, r[h]), N().validPositions[h].input = f.input, y(h), a = h, l = !0;
                else if (x(h, f.match.def)) {
                  var p = A(h, f.input, !0, !0);
                  l = !1 !== p, a = p.caret || p.insert ? C() : h, u = !0
                } else l = !0 === f.generatedInput;
                if (N().maskLength < d && (N().maskLength = d), l) break
              }
            if (!l) break
          }
          if (!l) return N().validPositions = le.extend(!0, {}, r), k(!0), !1
        } else N().validPositions[e] = le.extend(!0, {}, t);
        return k(!0), !0
      }

      function y(e) {
        for (var t = e - 1; - 1 < t && !N().validPositions[t]; t--);
        var n, i;
        for (t++; t < e; t++) void 0 === N().validPositions[t] && (!1 === R.jitMasking || R.jitMasking > t) && ("" === (i = _(t, p(t - 1).locator, t - 1).slice())[i.length - 1].match.def && i.pop(), (n = h(i)) && (n.match.def === R.radixPointDefinitionSymbol || !j(t, !0) || le.inArray(R.radixPoint, w()) < t && n.match.fn && n.match.fn.test(E(t), N(), t, !1, R)) && (!1 !== (s = o(t, n.match.placeholder || (null == n.match.fn ? n.match.def : "" !== E(t) ? E(t) : w()[t]), !0)) && (N().validPositions[s.pos || t].generatedInput = !0)))
      }
      t = !0 === t;
      var r = g;
      void 0 !== g.begin && (r = K && !m(g) ? g.end : g.begin);
      var s = !1,
        a = le.extend(!0, {}, N().validPositions);
      if (y(r), m(g) && (D(void 0, ce.keyCode.DELETE, g), r = N().p), r < N().maskLength && (s = o(r, e, t), (!t || !0 === S) && !1 === s)) {
        var l = N().validPositions[r];
        if (!l || null !== l.match.fn || l.match.def !== e && e !== R.skipOptionalPartCharacter) {
          if ((R.insertMode || void 0 === N().validPositions[P(r)]) && !j(r, !0)) {
            var c = _(r).slice();
            "" === c[c.length - 1].match.def && c.pop();
            var u = h(c, !0);
            u && null === u.match.fn && (o(r, u = u.match.placeholder || u.match.def, t), N().validPositions[r].generatedInput = !0);
            for (var d = r + 1, f = P(r); d <= f; d++)
              if (!1 !== (s = o(d, e, t))) {
                i(r, void 0 !== s.pos ? s.pos : d), r = d;
                break
              }
          }
        } else s = {
          caret: P(r)
        }
      }
      return !1 === s && R.keepStatic && !t && !0 !== n && (s = function(u, d, f) {
        var e, h, t, n, p, g, m, v, y = le.extend(!0, {}, N().validPositions),
          b = !1,
          i = C();
        for (n = N().validPositions[i]; 0 <= i; i--)
          if ((t = N().validPositions[i]) && void 0 !== t.alternation) {
            if (e = i, h = N().validPositions[e].alternation, n.locator[t.alternation] !== t.locator[t.alternation]) break;
            n = t
          }
        if (void 0 !== h) {
          v = parseInt(e);
          var x = void 0 !== n.locator[n.alternation || h] ? n.locator[n.alternation || h] : m[0];
          0 < x.length && (x = x.split(",")[0]);
          var w = N().validPositions[v],
            o = N().validPositions[v - 1];
          le.each(_(v, o ? o.locator : void 0, v - 1), function(e, t) {
            m = t.locator[h] ? t.locator[h].toString().split(",") : [];
            for (var n = 0; n < m.length; n++) {
              var i = [],
                o = 0,
                r = 0,
                s = !1;
              if (x < m[n] && (void 0 === t.na || -1 === le.inArray(m[n], t.na.split(",")))) {
                N().validPositions[v] = le.extend(!0, {}, t);
                var a = N().validPositions[v].locator;
                for (N().validPositions[v].locator[h] = parseInt(m[n]), null == t.match.fn ? (w.input !== t.match.def && (s = !0) !== w.generatedInput && i.push(w.input), r++, N().validPositions[v].generatedInput = !/[0-9a-bA-Z]/.test(t.match.def), N().validPositions[v].input = t.match.def) : N().validPositions[v].input = w.input, p = v + 1; p < C(void 0, !0) + 1; p++)(g = N().validPositions[p]) && !0 !== g.generatedInput && /[0-9a-bA-Z]/.test(g.input) ? i.push(g.input) : p < u && o++, delete N().validPositions[p];
                for (s && i[0] === t.match.def && i.shift(), k(!0), b = !0; 0 < i.length;) {
                  var l = i.shift();
                  if (l !== R.skipOptionalPartCharacter && !(b = A(C(void 0, !0) + 1, l, !1, S, !0))) break
                }
                if (b) {
                  N().validPositions[v].locator = a;
                  var c = C(u) + 1;
                  for (p = v + 1; p < C() + 1; p++)(void 0 === (g = N().validPositions[p]) || null == g.match.fn) && p < u + (r - o) && r++;
                  b = A(c < (u += r - o) ? c : u, d, f, S, !0)
                }
                if (b) return !1;
                k(), N().validPositions = le.extend(!0, {}, y)
              }
            }
          })
        }
        return b
      }(r, e, t)), !0 === s && (s = {
        pos: r
      }), le.isFunction(R.postValidation) && !1 !== s && !t && !0 !== S && (s = !!R.postValidation(w(!0), s, R) && s), void 0 === s.pos && (s.pos = r), !1 === s && (k(!0), N().validPositions = le.extend(!0, {}, a)), s
    }

    function j(e, t) {
      var n;
      if (t ? "" === (n = p(e).match).def && (n = u(e).match) : n = u(e).match, null != n.fn) return n.fn;
      if (!0 !== t && -1 < e) {
        var i = _(e);
        return i.length > 1 + ("" === i[i.length - 1].match.def ? 1 : 0)
      }
      return !1
    }

    function P(e, t) {
      var n = N().maskLength;
      if (n <= e) return n;
      for (var i = e; ++i < n && (!0 === t && (!0 !== u(i).match.newBlockMarker || !j(i)) || !0 !== t && !j(i)););
      return i
    }

    function m(e, t) {
      var n, i = e;
      if (i <= 0) return 0;
      for (; 0 < --i && (!0 === t && !0 !== u(i).match.newBlockMarker || !0 !== t && !j(i) && ((n = _(i)).length < 2 || 2 === n.length && "" === n[1].match.def)););
      return i
    }

    function v(e, t, n, i, o) {
      if (i && le.isFunction(R.onBeforeWrite)) {
        var r = R.onBeforeWrite(i, t, n, R);
        if (r) {
          if (r.refreshFromBuffer) {
            var s = r.refreshFromBuffer;
            T(!0 === s ? s : s.start, s.end, r.buffer || t), t = w(!0)
          }
          void 0 !== n && (n = void 0 !== r.caret ? r.caret : n)
        }
      }
      e.inputmask._valueSet(t.join("")), void 0 === n || void 0 !== i && "blur" === i.type ? Y(e, t, n) : y(e, n), !0 === o && (ee = !0, le(e).trigger("input"))
    }

    function E(e, t) {
      if (void 0 !== (t = t || u(e).match).placeholder) return t.placeholder;
      if (null === t.fn) {
        if (-1 < e && void 0 === N().validPositions[e]) {
          var n, i = _(e),
            o = [];
          if (i.length > 1 + ("" === i[i.length - 1].match.def ? 1 : 0))
            for (var r = 0; r < i.length; r++)
              if (!0 !== i[r].match.optionality && !0 !== i[r].match.optionalQuantifier && (null === i[r].match.fn || void 0 === n || !1 !== i[r].match.fn.test(n.match.def, N(), e, !0, R)) && (o.push(i[r]), null === i[r].match.fn && (n = i[r]), 1 < o.length && /[0-9a-bA-Z]/.test(o[0].match.def))) return R.placeholder.charAt(e % R.placeholder.length)
        }
        return t.def
      }
      return R.placeholder.charAt(e % R.placeholder.length)
    }

    function d(l, e, c, t, n, i) {
      var o = t.slice(),
        u = "",
        d = 0,
        f = void 0;
      if (k(), N().p = P(-1), !c)
        if (!0 !== R.autoUnmask) {
          var r = g().slice(0, P(-1)).join(""),
            s = o.join("").match(new RegExp("^" + ce.escapeRegex(r), "g"));
          s && 0 < s.length && (o.splice(0, s.length * r.length), d = P(d))
        } else d = P(d);
      if (le.each(o, function(e, t) {
          if (void 0 !== t) {
            var n = new le.Event("keypress");
            n.which = t.charCodeAt(0), u += t;
            var i = C(void 0, !0),
              o = N().validPositions[i],
              r = p(i + 1, o ? o.locator.slice() : void 0, i);
            if (! function() {
                var e = !1,
                  t = g().slice(d, P(d)).join("").indexOf(u);
                if (-1 !== t && !j(d)) {
                  e = !0;
                  for (var n = g().slice(d, d + t), i = 0; i < n.length; i++)
                    if (" " !== n[i]) {
                      e = !1;
                      break
                    }
                }
                return e
              }() || c || R.autoUnmask) {
              var s = c ? e : null == r.match.fn && r.match.optionality && i + 1 < N().p ? i + 1 : N().p;
              f = O.call(l, n, !0, !1, c, s), d = s + 1, u = ""
            } else f = O.call(l, n, !0, !1, !0, i + 1);
            if (!c && le.isFunction(R.onBeforeWrite) && ((f = R.onBeforeWrite(n, w(), f.forwardPosition, R)) && f.refreshFromBuffer)) {
              var a = f.refreshFromBuffer;
              T(!0 === a ? a : a.start, a.end, f.buffer), k(!0), f.caret && (N().p = f.caret)
            }
          }
        }), e) {
        var a = void 0,
          h = C();
        document.activeElement === l && (n || f) && (a = y(l).begin, n && !1 === f && (a = P(C(a))), f && !0 !== i && (a < h + 1 || -1 === h) && (a = R.numericInput && void 0 === f.caret ? m(f.forwardPosition) : f.forwardPosition)), v(l, w(), a, n || new le.Event("checkval"))
      }
    }

    function i(e) {
      if (e && void 0 === e.inputmask) return e.value;
      var t = [],
        n = N().validPositions;
      for (var i in n) n[i].match && null != n[i].match.fn && t.push(n[i].input);
      var o = 0 === t.length ? "" : (K ? t.reverse() : t).join("");
      if (le.isFunction(R.onUnMask)) {
        var r = (K ? w().slice().reverse() : w()).join("");
        o = R.onUnMask(r, o, R) || o
      }
      return o
    }

    function y(e, t, n, i) {
      function o(e) {
        !0 === i || !K || "number" != typeof e || R.greedy && "" === R.placeholder || (e = w().join("").length - e);
        return e
      }
      var r;
      if ("number" != typeof t) return e.setSelectionRange ? (t = e.selectionStart, n = e.selectionEnd) : window.getSelection ? (r = window.getSelection().getRangeAt(0)).commonAncestorContainer.parentNode !== e && r.commonAncestorContainer !== e || (t = r.startOffset, n = r.endOffset) : document.selection && document.selection.createRange && (n = (t = 0 - (r = document.selection.createRange()).duplicate().moveStart("character", -e.inputmask._valueGet().length)) + r.text.length), {
        begin: o(t),
        end: o(n)
      };
      t = o(t), n = "number" == typeof(n = o(n)) ? n : t;
      var s = parseInt(((e.ownerDocument.defaultView || window).getComputedStyle ? (e.ownerDocument.defaultView || window).getComputedStyle(e, null) : e.currentStyle).fontSize) * n;
      if (e.scrollLeft = s > e.scrollWidth ? s : 0, de || !1 !== R.insertMode || t !== n || n++, e.setSelectionRange) e.selectionStart = t, e.selectionEnd = n;
      else if (window.getSelection) {
        if (r = document.createRange(), void 0 === e.firstChild || null === e.firstChild) {
          var a = document.createTextNode("");
          e.appendChild(a)
        }
        r.setStart(e.firstChild, t < e.inputmask._valueGet().length ? t : e.inputmask._valueGet().length), r.setEnd(e.firstChild, n < e.inputmask._valueGet().length ? n : e.inputmask._valueGet().length), r.collapse(!0);
        var l = window.getSelection();
        l.removeAllRanges(), l.addRange(r)
      } else e.createTextRange && ((r = e.createTextRange()).collapse(!0), r.moveEnd("character", n), r.moveStart("character", t), r.select());
      Y(e, void 0, {
        begin: t,
        end: n
      })
    }

    function s(e) {
      var t, n, i = w(),
        o = i.length,
        r = C(),
        s = {},
        a = N().validPositions[r],
        l = void 0 !== a ? a.locator.slice() : void 0;
      for (t = r + 1; t < i.length; t++) l = (n = p(t, l, t - 1)).locator.slice(), s[t] = le.extend(!0, {}, n);
      var c = a && void 0 !== a.alternation ? a.locator[a.alternation] : void 0;
      for (t = o - 1; r < t && (((n = s[t]).match.optionality || n.match.optionalQuantifier || c && (c !== s[t].locator[a.alternation] && null != n.match.fn || null === n.match.fn && n.locator[a.alternation] && $(n.locator[a.alternation].toString().split(","), c.toString().split(",")) && "" !== _(t)[0].def)) && i[t] === E(t, n.match)); t--) o--;
      return e ? {
        l: o,
        def: s[o] ? s[o].match : void 0
      } : o
    }

    function a(e) {
      for (var t = s(), n = e.length - 1; t < n && !j(n); n--);
      return e.splice(t, n + 1 - t), e
    }

    function S(e) {
      if (le.isFunction(R.isComplete)) return R.isComplete(e, R);
      if ("*" !== R.repeat) {
        var t = !1,
          n = s(!0),
          i = m(n.l);
        if (void 0 === n.def || n.def.newBlockMarker || n.def.optionality || n.def.optionalQuantifier) {
          t = !0;
          for (var o = 0; o <= i; o++) {
            var r = p(o).match;
            if (null !== r.fn && void 0 === N().validPositions[o] && !0 !== r.optionality && !0 !== r.optionalQuantifier || null === r.fn && e[o] !== E(o, r)) {
              t = !1;
              break
            }
          }
        }
        return t
      }
    }

    function D(s, e, t, n) {
      if ((R.numericInput || K) && (e === ce.keyCode.BACKSPACE ? e = ce.keyCode.DELETE : e === ce.keyCode.DELETE && (e = ce.keyCode.BACKSPACE), K)) {
        var i = t.end;
        t.end = t.begin, t.begin = i
      }
      e === ce.keyCode.BACKSPACE && (t.end - t.begin < 1 || !1 === R.insertMode) ? (t.begin = m(t.begin), void 0 === N().validPositions[t.begin] || N().validPositions[t.begin].input !== R.groupSeparator && N().validPositions[t.begin].input !== R.radixPoint || t.begin--) : e === ce.keyCode.DELETE && t.begin === t.end && (t.end = j(t.end, !0) ? t.end + 1 : P(t.end) + 1, void 0 === N().validPositions[t.begin] || N().validPositions[t.begin].input !== R.groupSeparator && N().validPositions[t.begin].input !== R.radixPoint || t.end++), b(t.begin, t.end, !1, n), !0 !== n && function() {
        if (R.keepStatic) {
          for (var e = [], t = C(-1, !0), n = le.extend(!0, {}, N().validPositions), i = N().validPositions[t]; 0 <= t; t--) {
            var o = N().validPositions[t];
            if (o) {
              if (!0 !== o.generatedInput && /[0-9a-bA-Z]/.test(o.input) && e.push(o.input), delete N().validPositions[t], void 0 !== o.alternation && o.locator[o.alternation] !== i.locator[o.alternation]) break;
              i = o
            }
          }
          if (-1 < t)
            for (N().p = P(C(-1, !0)); 0 < e.length;) {
              var r = new le.Event("keypress");
              r.which = e.pop().charCodeAt(0), O.call(s, r, !0, !1, !1, N().p)
            } else N().validPositions = le.extend(!0, {}, n)
        }
      }();
      var o = C(t.begin, !0);
      o < t.begin ? N().p = P(o) : !0 !== n && (N().p = t.begin)
    }

    function l(e) {
      var t, n, i, o, r = this,
        s = le(r),
        a = e.keyCode,
        l = y(r);
      if (a === ce.keyCode.BACKSPACE || a === ce.keyCode.DELETE || he && a === ce.keyCode.BACKSPACE_SAFARI || e.ctrlKey && a === ce.keyCode.X && (t = "cut", n = document.createElement("input"), (o = (i = "on" + t) in n) || (n.setAttribute(i, "return;"), o = "function" == typeof n[i]), n = null, !o)) e.preventDefault(), D(r, a, l), v(r, w(!0), N().p, e, r.inputmask._valueGet() !== w().join("")), r.inputmask._valueGet() === g().join("") ? s.trigger("cleared") : !0 === S(w()) && s.trigger("complete"), R.showTooltip && (r.title = R.tooltip || N().mask);
      else if (a === ce.keyCode.END || a === ce.keyCode.PAGE_DOWN) {
        e.preventDefault();
        var c = P(C());
        R.insertMode || c !== N().maskLength || e.shiftKey || c--, y(r, e.shiftKey ? l.begin : c, c, !0)
      } else a === ce.keyCode.HOME && !e.shiftKey || a === ce.keyCode.PAGE_UP ? (e.preventDefault(), y(r, 0, e.shiftKey ? l.begin : 0, !0)) : (R.undoOnEscape && a === ce.keyCode.ESCAPE || 90 === a && e.ctrlKey) && !0 !== e.altKey ? (d(r, !0, !1, V.split("")), s.trigger("click")) : a !== ce.keyCode.INSERT || e.shiftKey || e.ctrlKey ? !0 === R.tabThrough && a === ce.keyCode.TAB ? (!0 === e.shiftKey ? (null === u(l.begin).match.fn && (l.begin = P(l.begin)), l.end = m(l.begin, !0), l.begin = m(l.end, !0)) : (l.begin = P(l.begin, !0), l.end = P(l.begin, !0), l.end < N().maskLength && l.end--), l.begin < N().maskLength && (e.preventDefault(), y(r, l.begin, l.end))) : e.shiftKey || (!1 === R.insertMode ? a === ce.keyCode.RIGHT ? setTimeout(function() {
        var e = y(r);
        y(r, e.begin)
      }, 0) : a === ce.keyCode.LEFT && setTimeout(function() {
        var e = y(r);
        y(r, K ? e.begin + 1 : e.begin - 1)
      }, 0) : setTimeout(function() {
        Y(r)
      }, 0)) : (R.insertMode = !R.insertMode, y(r, R.insertMode || l.begin !== N().maskLength ? l.begin : l.begin - 1));
      R.onKeyDown.call(this, e, w(), y(r).begin, R), te = -1 !== le.inArray(a, R.ignorables)
    }

    function O(e, t, n, i, o) {
      var r = le(this),
        s = e.which || e.charCode || e.keyCode;
      if (!(!0 === t || e.ctrlKey && e.altKey) && (e.ctrlKey || e.metaKey || te)) return s === ce.keyCode.ENTER && V !== w().join("") && (V = w().join(""), setTimeout(function() {
        r.trigger("change")
      }, 0)), !0;
      if (s) {
        46 === s && !1 === e.shiftKey && "," === R.radixPoint && (s = 44);
        var a, l = t ? {
            begin: o,
            end: o
          } : y(this),
          c = String.fromCharCode(s);
        N().writeOutBuffer = !0;
        var u = A(l, c, i);
        if (!1 !== u && (k(!0), a = void 0 !== u.caret ? u.caret : t ? u.pos + 1 : P(u.pos), N().p = a), !1 !== n) {
          var d = this;
          if (setTimeout(function() {
              R.onKeyValidation.call(d, s, u, R)
            }, 0), N().writeOutBuffer && !1 !== u) {
            var f = w();
            v(this, f, R.numericInput && void 0 === u.caret ? m(a) : a, e, !0 !== t), !0 !== t && setTimeout(function() {
              !0 === S(f) && r.trigger("complete")
            }, 0)
          }
        }
        if (R.showTooltip && (this.title = R.tooltip || N().mask), e.preventDefault(), t) return u.forwardPosition = a, u
      }
    }

    function o(e) {
      var t, n = this,
        i = e.originalEvent || e,
        o = le(n),
        r = n.inputmask._valueGet(!0),
        s = y(n);
      K && (t = s.end, s.end = s.begin, s.begin = t);
      var a = r.substr(0, s.begin),
        l = r.substr(s.end, r.length);
      if (a === (K ? g().reverse() : g()).slice(0, s.begin).join("") && (a = ""), l === (K ? g().reverse() : g()).slice(s.end).join("") && (l = ""), K && (t = a, a = l, l = t), window.clipboardData && window.clipboardData.getData) r = a + window.clipboardData.getData("Text") + l;
      else {
        if (!i.clipboardData || !i.clipboardData.getData) return !0;
        r = a + i.clipboardData.getData("text/plain") + l
      }
      var c = r;
      if (le.isFunction(R.onBeforePaste)) {
        if (!1 === (c = R.onBeforePaste(r, R))) return e.preventDefault();
        c || (c = r)
      }
      return d(n, !1, !1, K ? c.split("").reverse() : c.toString().split("")), v(n, w(), P(C()), e, V !== w().join("")), !0 === S(w()) && o.trigger("complete"), e.preventDefault()
    }

    function r(e) {
      var t = this,
        n = t.inputmask._valueGet();
      if (w().join("") !== n) {
        var i = y(t);
        if (n = n.replace(new RegExp("(" + ce.escapeRegex(g().join("")) + ")*"), ""), fe) {
          var o = n.replace(w().join(""), "");
          if (1 === o.length) {
            var r = new le.Event("keypress");
            return r.which = o.charCodeAt(0), O.call(t, r, !0, !0, !1, N().validPositions[i.begin - 1] ? i.begin : i.begin - 1), !1
          }
        }
        if (i.begin > n.length && (y(t, n.length), i = y(t)), w().length - n.length != 1 || n.charAt(i.begin) === w()[i.begin] || n.charAt(i.begin + 1) === w()[i.begin] || j(i.begin)) {
          for (var s = C() + 1, a = g().join(""); null === n.match(ce.escapeRegex(a) + "$");) a = a.slice(1);
          d(t, !0, !1, n = (n = n.replace(a, "")).split(""), e, i.begin < s), !0 === S(w()) && le(t).trigger("complete")
        } else e.keyCode = ce.keyCode.BACKSPACE, l.call(t, e);
        e.preventDefault()
      }
    }

    function c(e) {
      var t = this.inputmask._valueGet();
      d(this, !0, !1, (le.isFunction(R.onBeforeMask) && R.onBeforeMask(t, R) || t).split("")), V = w().join(""), (R.clearMaskOnLostFocus || R.clearIncomplete) && this.inputmask._valueGet() === g().join("") && this.inputmask._valueSet("")
    }

    function f(e) {
      var t = this.inputmask._valueGet();
      R.showMaskOnFocus && (!R.showMaskOnHover || R.showMaskOnHover && "" === t) && (this.inputmask._valueGet() !== w().join("") ? v(this, w(), P(C())) : !1 === ne && y(this, P(C()))), !0 === R.positionCaretOnTab && setTimeout(function() {
        I.apply(this, [e])
      }, 0), V = w().join("")
    }

    function M(e) {
      if (ne = !1, R.clearMaskOnLostFocus && document.activeElement !== this) {
        var t = w().slice(),
          n = this.inputmask._valueGet();
        n !== this.getAttribute("placeholder") && "" !== n && (-1 === C() && n === g().join("") ? t = [] : a(t), v(this, t))
      }
    }

    function I(e) {
      var r = this;
      setTimeout(function() {
        if (document.activeElement === r) {
          var e = y(r);
          if (e.begin === e.end) switch (R.positionCaretOnClick) {
            case "none":
              break;
            case "radixFocus":
              if (function(e) {
                  if ("" !== R.radixPoint) {
                    var t = N().validPositions;
                    if (void 0 === t[e] || t[e].input === E(e)) {
                      if (e < P(-1)) return !0;
                      var n = le.inArray(R.radixPoint, w());
                      if (-1 !== n) {
                        for (var i in t)
                          if (n < i && t[i].input !== E(i)) return !1;
                        return !0
                      }
                    }
                  }
                  return !1
                }(e.begin)) {
                var t = le.inArray(R.radixPoint, w().join(""));
                y(r, R.numericInput ? P(t) : t);
                break
              }
              default:
                var n = e.begin,
                  i = P(C(n, !0));
                if (n < i) y(r, j(n) || j(n - 1) ? n : P(n));
                else {
                  var o = E(i);
                  ("" !== o && w()[i] !== o && !0 !== u(i).match.optionalQuantifier || !j(i) && u(i).match.def === o) && (i = P(i)), y(r, i)
                }
          }
        }
      }, 0)
    }

    function H(e) {
      var t = this;
      setTimeout(function() {
        y(t, 0, P(C()))
      }, 0)
    }

    function L(e) {
      var t = this,
        n = le(t),
        i = y(t),
        o = e.originalEvent || e,
        r = window.clipboardData || o.clipboardData,
        s = K ? w().slice(i.end, i.begin) : w().slice(i.begin, i.end);
      r.setData("text", K ? s.reverse().join("") : s.join("")), document.execCommand && document.execCommand("copy"), D(t, ce.keyCode.DELETE, i), v(t, w(), N().p, e, V !== w().join("")), t.inputmask._valueGet() === g().join("") && n.trigger("cleared"), R.showTooltip && (t.title = R.tooltip || N().mask)
    }

    function F(e) {
      var t = le(this);
      if (this.inputmask) {
        var n = this.inputmask._valueGet(),
          i = w().slice();
        V !== i.join("") && setTimeout(function() {
          t.trigger("change"), V = i.join("")
        }, 0), "" !== n && (R.clearMaskOnLostFocus && (-1 === C() && n === g().join("") ? i = [] : a(i)), !1 === S(i) && (setTimeout(function() {
          t.trigger("incomplete")
        }, 0), R.clearIncomplete && (k(), i = R.clearMaskOnLostFocus ? [] : g().slice())), v(this, i, void 0, e))
      }
    }

    function B(e) {
      ne = !0, document.activeElement !== this && R.showMaskOnHover && this.inputmask._valueGet() !== w().join("") && v(this, w())
    }

    function z(e) {
      V !== w().join("") && G.trigger("change"), R.clearMaskOnLostFocus && -1 === C() && U.inputmask._valueGet && U.inputmask._valueGet() === g().join("") && U.inputmask._valueSet(""), R.removeMaskOnSubmit && (U.inputmask._valueSet(U.inputmask.unmaskedvalue(), !0), setTimeout(function() {
        v(U, w())
      }, 0))
    }

    function q(e) {
      setTimeout(function() {
        G.trigger("setvalue")
      }, 0)
    }

    function W(c) {
      function t() {
        Q.style.position = "absolute", Q.style.top = n.top + "px", Q.style.left = n.left + "px", Q.style.width = parseInt(c.offsetWidth) - parseInt(u.paddingLeft) - parseInt(u.paddingRight) - parseInt(u.borderLeftWidth) - parseInt(u.borderRightWidth) + "px", Q.style.height = parseInt(c.offsetHeight) - parseInt(u.paddingTop) - parseInt(u.paddingBottom) - parseInt(u.borderTopWidth) - parseInt(u.borderBottomWidth) + "px", Q.style.lineHeight = Q.style.height, Q.style.zIndex = isNaN(u.zIndex) ? -1 : u.zIndex - 1, Q.style.webkitAppearance = "textfield", Q.style.mozAppearance = "textfield", Q.style.Appearance = "textfield"
      }
      var n = le(c).position(),
        u = (c.ownerDocument.defaultView || window).getComputedStyle(c, null);
      for (var e in c.parentNode, Q = document.createElement("div"), document.body.appendChild(Q), u) isNaN(e) && "cssText" !== e && -1 == e.indexOf("webkit") && (Q.style[e] = u[e]);
      c.style.backgroundColor = "transparent", c.style.color = "transparent", c.style.webkitAppearance = "caret", c.style.mozAppearance = "caret", c.style.Appearance = "caret", t(), le(window).on("resize", function(e) {
        n = le(c).position(), u = (c.ownerDocument.defaultView || window).getComputedStyle(c, null), t()
      }), le(c).on("click", function(e) {
        return y(c, function(e) {
          var t, n = document.createElement("span");
          for (var i in u) isNaN(i) && -1 !== i.indexOf("font") && (n.style[i] = u[i]);
          n.style.textTransform = u.textTransform, n.style.letterSpacing = u.letterSpacing, n.style.position = "absolute", n.style.height = "auto", n.style.width = "auto", n.style.visibility = "hidden", n.style.whiteSpace = "nowrap", document.body.appendChild(n);
          var o, r = c.inputmask._valueGet(),
            s = 0;
          for (t = 0, o = r.length; t <= o; t++) {
            if (n.innerHTML += r.charAt(t) || "_", n.offsetWidth >= e) {
              var a = e - s,
                l = n.offsetWidth - e;
              n.innerHTML = r.charAt(t), t = (a -= n.offsetWidth / 3) < l ? t - 1 : t;
              break
            }
            s = n.offsetWidth
          }
          return document.body.removeChild(n), t
        }(e.clientX)), I.call(this, [e])
      })
    }

    function Y(e, t, n) {
      function i() {
        r || null !== a.fn && void 0 !== l.input ? r && null !== a.fn && void 0 !== l.input && (r = !1, o += "</span>") : (r = !0, o += "<span class='im-static''>")
      }
      if (void 0 !== Q) {
        t = t || w(), void 0 === n ? n = y(e) : void 0 === n.begin && (n = {
          begin: n,
          end: n
        });
        var o = "",
          r = !1;
        if ("" != t)
          for (var s, a, l, c = 0, u = C(); c === n.begin && document.activeElement === e && (o += "<span class='im-caret' style='border-right-width: 1px;border-right-style: solid;'></span>"), N().validPositions[c] ? (l = N().validPositions[c], a = l.match, s = l.locator.slice(), i(), o += l.input) : (l = p(c, s, c - 1), a = l.match, s = l.locator.slice(), (!1 === R.jitMasking || c < u || Number.isFinite(R.jitMasking) && R.jitMasking > c) && (i(), o += E(c, a))), c++, (void 0 === X || c < X) && (null !== a.fn || "" !== a.def) || c < u;);
        Q.innerHTML = o
      }
    }
    var V, U, G, X, Q, Z, K = !1,
      J = !1,
      ee = !1,
      te = !1,
      ne = !1,
      ie = {
        on: function(e, t, r) {
          var n = function(e) {
            if (void 0 === this.inputmask && "FORM" !== this.nodeName) {
              var t = le.data(this, "_inputmask_opts");
              t ? new ce(t).mask(this) : ie.off(this)
            } else {
              if ("setvalue" === e.type || !(this.disabled || this.readOnly && !("keydown" === e.type && e.ctrlKey && 67 === e.keyCode || !1 === R.tabThrough && e.keyCode === ce.keyCode.TAB))) {
                switch (e.type) {
                  case "input":
                    if (!0 === ee) return ee = !1, e.preventDefault();
                    break;
                  case "keydown":
                    ee = J = !1;
                    break;
                  case "keypress":
                    if (!0 === J) return e.preventDefault();
                    J = !0;
                    break;
                  case "click":
                    if (fe || he) {
                      var n = this,
                        i = arguments;
                      return setTimeout(function() {
                        r.apply(n, i)
                      }, 0), !1
                    }
                }
                var o = r.apply(this, arguments);
                return !1 === o && (e.preventDefault(), e.stopPropagation()), o
              }
              e.preventDefault()
            }
          };
          e.inputmask.events[t] = e.inputmask.events[t] || [], e.inputmask.events[t].push(n), -1 !== le.inArray(t, ["submit", "reset"]) ? null != e.form && le(e.form).on(t, n) : le(e).on(t, n)
        },
        off: function(i, e) {
          var t;
          i.inputmask && i.inputmask.events && (e ? (t = [])[e] = i.inputmask.events[e] : t = i.inputmask.events, le.each(t, function(e, t) {
            for (; 0 < t.length;) {
              var n = t.pop(); - 1 !== le.inArray(e, ["submit", "reset"]) ? null != i.form && le(i.form).off(e, n) : le(i).off(e, n)
            }
            delete i.inputmask.events[e]
          }))
        }
      };
    if (void 0 !== e) switch (e.action) {
      case "isComplete":
        return U = e.el, S(w());
      case "unmaskedvalue":
        return void 0 !== (U = e.el) && void 0 !== U.inputmask ? (t = U.inputmask.maskset, R = U.inputmask.opts, K = U.inputmask.isRTL) : (Z = e.value, R.numericInput && (K = !0), Z = (le.isFunction(R.onBeforeMask) && R.onBeforeMask(Z, R) || Z).split(""), d(void 0, !1, !1, K ? Z.reverse() : Z), le.isFunction(R.onBeforeWrite) && R.onBeforeWrite(void 0, w(), 0, R)), i(U);
      case "mask":
        U = e.el, t = U.inputmask.maskset, R = U.inputmask.opts, K = U.inputmask.isRTL,
          function(e) {
            if (function(e, t) {
                var n = e.getAttribute("type"),
                  i = "INPUT" === e.tagName && -1 !== le.inArray(n, t.supportsInputType) || e.isContentEditable || "TEXTAREA" === e.tagName;
                if (!i && "INPUT" === e.tagName) {
                  var o = document.createElement("input");
                  o.setAttribute("type", n), i = "text" === o.type, o = null
                }
                return i
              }(e, R) && (G = le(U = e), R.showTooltip && (U.title = R.tooltip || N().mask), ("rtl" === U.dir || R.rightAlign) && (U.style.textAlign = "right"), ("rtl" === U.dir || R.numericInput) && (U.dir = "ltr", U.removeAttribute("dir"), U.inputmask.isRTL = !0, K = !0), !0 === R.colorMask && W(U), pe && (U.hasOwnProperty("inputmode") && (U.inputmode = R.inputmode, U.setAttribute("inputmode", R.inputmode)), "rtfm" === R.androidHack && (!0 !== R.colorMask && W(U), U.type = "password")), ie.off(U), function(e) {
                function t() {
                  return this.inputmask ? this.inputmask.opts.autoUnmask ? this.inputmask.unmaskedvalue() : -1 !== C() || !0 !== R.nullable ? document.activeElement === this && R.clearMaskOnLostFocus ? (K ? a(w().slice()).reverse() : a(w().slice())).join("") : i.call(this) : "" : i.call(this)
                }

                function n(e) {
                  o.call(this, e), this.inputmask && le(this).trigger("setvalue")
                }
                var i, o, r;
                if (!e.inputmask.__valueGet) {
                  if (!0 !== R.noValuePatching) {
                    if (Object.getOwnPropertyDescriptor) {
                      "function" != typeof Object.getPrototypeOf && (Object.getPrototypeOf = "object" == typeof "test".__proto__ ? function(e) {
                        return e.__proto__
                      } : function(e) {
                        return e.constructor.prototype
                      });
                      var s = Object.getPrototypeOf ? Object.getOwnPropertyDescriptor(Object.getPrototypeOf(e), "value") : void 0;
                      s && s.get && s.set ? (i = s.get, o = s.set, Object.defineProperty(e, "value", {
                        get: t,
                        set: n,
                        configurable: !0
                      })) : "INPUT" !== e.tagName && (i = function() {
                        return this.textContent
                      }, o = function(e) {
                        this.textContent = e
                      }, Object.defineProperty(e, "value", {
                        get: t,
                        set: n,
                        configurable: !0
                      }))
                    } else document.__lookupGetter__ && e.__lookupGetter__("value") && (i = e.__lookupGetter__("value"), o = e.__lookupSetter__("value"), e.__defineGetter__("value", t), e.__defineSetter__("value", n));
                    e.inputmask.__valueGet = i, e.inputmask.__valueSet = o
                  }
                  e.inputmask._valueGet = function(e) {
                    return K && !0 !== e ? i.call(this.el).split("").reverse().join("") : i.call(this.el)
                  }, e.inputmask._valueSet = function(e, t) {
                    o.call(this.el, null == e ? "" : !0 !== t && K ? e.split("").reverse().join("") : e)
                  }, void 0 === i && (i = function() {
                    return this.value
                  }, o = function(e) {
                    this.value = e
                  }, function(e) {
                    if (le.valHooks && (void 0 === le.valHooks[e] || !0 !== le.valHooks[e].inputmaskpatch)) {
                      var n = le.valHooks[e] && le.valHooks[e].get ? le.valHooks[e].get : function(e) {
                          return e.value
                        },
                        o = le.valHooks[e] && le.valHooks[e].set ? le.valHooks[e].set : function(e, t) {
                          return e.value = t, e
                        };
                      le.valHooks[e] = {
                        get: function(e) {
                          if (e.inputmask) {
                            if (e.inputmask.opts.autoUnmask) return e.inputmask.unmaskedvalue();
                            var t = n(e);
                            return -1 !== C(void 0, void 0, e.inputmask.maskset.validPositions) || !0 !== R.nullable ? t : ""
                          }
                          return n(e)
                        },
                        set: function(e, t) {
                          var n, i = le(e);
                          return n = o(e, t), e.inputmask && i.trigger("setvalue"), n
                        },
                        inputmaskpatch: !0
                      }
                    }
                  }(e.type), r = e, ie.on(r, "mouseenter", function(e) {
                    var t = le(this);
                    this.inputmask._valueGet() !== w().join("") && t.trigger("setvalue")
                  }))
                }
              }(U), ie.on(U, "submit", z), ie.on(U, "reset", q), ie.on(U, "mouseenter", B), ie.on(U, "blur", F), ie.on(U, "focus", f), ie.on(U, "mouseleave", M), !0 !== R.colorMask && ie.on(U, "click", I), ie.on(U, "dblclick", H), ie.on(U, "paste", o), ie.on(U, "dragdrop", o), ie.on(U, "drop", o), ie.on(U, "cut", L), ie.on(U, "complete", R.oncomplete), ie.on(U, "incomplete", R.onincomplete), ie.on(U, "cleared", R.oncleared), !0 !== R.inputEventOnly && (ie.on(U, "keydown", l), ie.on(U, "keypress", O)), ie.on(U, "compositionstart", le.noop), ie.on(U, "compositionupdate", le.noop), ie.on(U, "compositionend", le.noop), ie.on(U, "keyup", le.noop), ie.on(U, "input", r), ie.on(U, "setvalue", c), g(), "" !== U.inputmask._valueGet() || !1 === R.clearMaskOnLostFocus || document.activeElement === U)) {
              var t = le.isFunction(R.onBeforeMask) && R.onBeforeMask(U.inputmask._valueGet(), R) || U.inputmask._valueGet();
              d(U, !0, !1, t.split(""));
              var n = w().slice();
              V = n.join(""), !1 === S(n) && R.clearIncomplete && k(), R.clearMaskOnLostFocus && document.activeElement !== U && (-1 === C() ? n = [] : a(n)), v(U, n), document.activeElement === U && y(U, P(C()))
            }
          }(U);
        break;
      case "format":
        return R.numericInput && (K = !0), Z = (le.isFunction(R.onBeforeMask) && R.onBeforeMask(e.value, R) || e.value).split(""), d(void 0, !1, !1, K ? Z.reverse() : Z), le.isFunction(R.onBeforeWrite) && R.onBeforeWrite(void 0, w(), 0, R), e.metadata ? {
          value: K ? w().slice().reverse().join("") : w().join(""),
          metadata: ue({
            action: "getmetadata"
          }, t, R)
        } : K ? w().slice().reverse().join("") : w().join("");
      case "isValid":
        R.numericInput && (K = !0), e.value ? (Z = e.value.split(""), d(void 0, !1, !0, K ? Z.reverse() : Z)) : e.value = w().join("");
        for (var oe = w(), re = s(), se = oe.length - 1; re < se && !j(se); se--);
        return oe.splice(re, se + 1 - re), S(oe) && e.value === w().join("");
      case "getemptymask":
        return g().join("");
      case "remove":
        U = e.el, G = le(U), t = U.inputmask.maskset, R = U.inputmask.opts, U.inputmask._valueSet(i(U)), ie.off(U), Object.getOwnPropertyDescriptor && Object.getPrototypeOf ? Object.getOwnPropertyDescriptor(Object.getPrototypeOf(U), "value") && U.inputmask.__valueGet && Object.defineProperty(U, "value", {
          get: U.inputmask.__valueGet,
          set: U.inputmask.__valueSet,
          configurable: !0
        }) : document.__lookupGetter__ && U.__lookupGetter__("value") && U.inputmask.__valueGet && (U.__defineGetter__("value", U.inputmask.__valueGet), U.__defineSetter__("value", U.inputmask.__valueSet)), U.inputmask = void 0;
        break;
      case "getmetadata":
        if (le.isArray(t.metadata)) {
          var ae = n(!0, 0, !1).join("");
          return le.each(t.metadata, function(e, t) {
            if (t.mask === ae) return ae = t, !1
          }), ae
        }
        return t.metadata
    }
  }
  var e = navigator.userAgent,
    de = /mobile/i.test(e),
    fe = /iemobile/i.test(e),
    he = /iphone/i.test(e) && !fe,
    pe = /android/i.test(e) && !fe;
  ce.prototype = {
    defaults: {
      placeholder: "_",
      optionalmarker: {
        start: "[",
        end: "]"
      },
      quantifiermarker: {
        start: "{",
        end: "}"
      },
      groupmarker: {
        start: "(",
        end: ")"
      },
      alternatormarker: "|",
      escapeChar: "\\",
      mask: null,
      oncomplete: le.noop,
      onincomplete: le.noop,
      oncleared: le.noop,
      repeat: 0,
      greedy: !0,
      autoUnmask: !1,
      removeMaskOnSubmit: !1,
      clearMaskOnLostFocus: !0,
      insertMode: !0,
      clearIncomplete: !1,
      aliases: {},
      alias: null,
      onKeyDown: le.noop,
      onBeforeMask: null,
      onBeforePaste: function(e, t) {
        return le.isFunction(t.onBeforeMask) ? t.onBeforeMask(e, t) : e
      },
      onBeforeWrite: null,
      onUnMask: null,
      showMaskOnFocus: !0,
      showMaskOnHover: !0,
      onKeyValidation: le.noop,
      skipOptionalPartCharacter: " ",
      showTooltip: !1,
      tooltip: void 0,
      numericInput: !1,
      rightAlign: !1,
      undoOnEscape: !0,
      radixPoint: "",
      radixPointDefinitionSymbol: void 0,
      groupSeparator: "",
      keepStatic: null,
      positionCaretOnTab: !0,
      tabThrough: !1,
      supportsInputType: ["text", "tel", "password"],
      definitions: {
        9: {
          validator: "[0-9]",
          cardinality: 1,
          definitionSymbol: "*"
        },
        a: {
          validator: "[A-Za-zА-яЁёÀ-ÿµ]",
          cardinality: 1,
          definitionSymbol: "*"
        },
        "*": {
          validator: "[0-9A-Za-zА-яЁёÀ-ÿµ]",
          cardinality: 1
        }
      },
      ignorables: [8, 9, 13, 19, 27, 33, 34, 35, 36, 37, 38, 39, 40, 45, 46, 93, 112, 113, 114, 115, 116, 117, 118, 119, 120, 121, 122, 123],
      isComplete: null,
      canClearPosition: le.noop,
      postValidation: null,
      staticDefinitionSymbol: void 0,
      jitMasking: !1,
      nullable: !0,
      inputEventOnly: !1,
      noValuePatching: !1,
      positionCaretOnClick: "lvp",
      casing: null,
      inputmode: "verbatim",
      colorMask: !1,
      androidHack: !1
    },
    masksCache: {},
    mask: function(e) {
      var u = this;
      return "string" == typeof e && (e = document.getElementById(e) || document.querySelectorAll(e)), e = e.nodeName ? [e] : e, le.each(e, function(e, t) {
        var n = le.extend(!0, {}, u.opts);
        ! function(n, e, i, o) {
          function t(e, t) {
            null !== (t = void 0 !== t ? t : n.getAttribute(o + "-" + e)) && ("string" == typeof t && (0 === e.indexOf("on") ? t = window[t] : "false" === t ? t = !1 : "true" === t && (t = !0)), i[e] = t)
          }
          var r, s, a, l, c = n.getAttribute(o);
          if (c && "" !== c && (c = c.replace(new RegExp("'", "g"), '"'), s = JSON.parse("{" + c + "}")), s)
            for (l in a = void 0, s)
              if ("alias" === l.toLowerCase()) {
                a = s[l];
                break
              }
          for (r in t("alias", a), i.alias && u.resolveAlias(i.alias, i, e), e) {
            if (s)
              for (l in a = void 0, s)
                if (l.toLowerCase() === r.toLowerCase()) {
                  a = s[l];
                  break
                }
            t(r, a)
          }
          le.extend(!0, e, i)
        }(t, n, le.extend(!0, {}, u.userOptions), u.dataAttribute);
        var i = o(n, u.noMasksCache);
        void 0 !== i && (void 0 !== t.inputmask && t.inputmask.remove(), t.inputmask = new ce, t.inputmask.opts = n, t.inputmask.noMasksCache = u.noMasksCache, t.inputmask.userOptions = le.extend(!0, {}, u.userOptions), (t.inputmask.el = t).inputmask.maskset = i, t.inputmask.isRTL = !1, le.data(t, "_inputmask_opts", n), ue({
          action: "mask",
          el: t
        }))
      }), e && e[0] && e[0].inputmask || this
    },
    option: function(e, t) {
      return "string" == typeof e ? this.opts[e] : "object" == typeof e ? (le.extend(this.userOptions, e), this.el && !0 !== t && this.mask(this.el), this) : void 0
    },
    unmaskedvalue: function(e) {
      return ue({
        action: "unmaskedvalue",
        el: this.el,
        value: e
      }, this.el && this.el.inputmask ? this.el.inputmask.maskset : o(this.opts, this.noMasksCache), this.opts)
    },
    remove: function() {
      if (this.el) return ue({
        action: "remove",
        el: this.el
      }), this.el.inputmask = void 0, this.el
    },
    getemptymask: function() {
      return ue({
        action: "getemptymask"
      }, this.maskset || o(this.opts, this.noMasksCache), this.opts)
    },
    hasMaskedValue: function() {
      return !this.opts.autoUnmask
    },
    isComplete: function() {
      return ue({
        action: "isComplete",
        el: this.el
      }, this.maskset || o(this.opts, this.noMasksCache), this.opts)
    },
    getmetadata: function() {
      return ue({
        action: "getmetadata"
      }, this.maskset || o(this.opts, this.noMasksCache), this.opts)
    },
    isValid: function(e) {
      return ue({
        action: "isValid",
        value: e
      }, this.maskset || o(this.opts, this.noMasksCache), this.opts)
    },
    format: function(e, t) {
      return ue({
        action: "format",
        value: e,
        metadata: t
      }, this.maskset || o(this.opts, this.noMasksCache), this.opts)
    },
    analyseMask: function(e, d) {
      function t(e, t, n, i) {
        this.matches = [], this.isGroup = e || !1, this.isOptional = t || !1, this.isQuantifier = n || !1, this.isAlternator = i || !1, this.quantifier = {
          min: 1,
          max: 1
        }
      }

      function o(e, t, n) {
        var i = d.definitions[t];
        n = void 0 !== n ? n : e.matches.length;
        var o = e.matches[n - 1];
        if (i && !g) {
          i.placeholder = le.isFunction(i.placeholder) ? i.placeholder(d) : i.placeholder;
          for (var r = i.prevalidator, s = r ? r.length : 0, a = 1; a < i.cardinality; a++) {
            var l = a <= s ? r[a - 1] : [],
              c = l.validator,
              u = l.cardinality;
            e.matches.splice(n++, 0, {
              fn: c ? "string" == typeof c ? new RegExp(c) : new function() {
                this.test = c
              } : new RegExp("."),
              cardinality: u || 1,
              optionality: e.isOptional,
              newBlockMarker: void 0 === o || o.def !== (i.definitionSymbol || t),
              casing: i.casing,
              def: i.definitionSymbol || t,
              placeholder: i.placeholder,
              nativeDef: t
            }), o = e.matches[n - 1]
          }
          e.matches.splice(n++, 0, {
            fn: i.validator ? "string" == typeof i.validator ? new RegExp(i.validator) : new function() {
              this.test = i.validator
            } : new RegExp("."),
            cardinality: i.cardinality,
            optionality: e.isOptional,
            newBlockMarker: void 0 === o || o.def !== (i.definitionSymbol || t),
            casing: i.casing,
            def: i.definitionSymbol || t,
            placeholder: i.placeholder,
            nativeDef: t
          })
        } else e.matches.splice(n++, 0, {
          fn: null,
          cardinality: 0,
          optionality: e.isOptional,
          newBlockMarker: void 0 === o || o.def !== t,
          casing: null,
          def: d.staticDefinitionSymbol || t,
          placeholder: void 0 !== d.staticDefinitionSymbol ? t : void 0,
          nativeDef: t
        }), g = !1
      }

      function r(e, t) {
        e && e.isGroup && (e.isGroup = !1, o(e, d.groupmarker.start, 0), !0 !== t && o(e, d.groupmarker.end))
      }

      function n(e, t, n, i) {
        0 < t.matches.length && (void 0 === i || i) && r(t.matches[t.matches.length - 1]), o(t, e)
      }

      function i() {
        if (0 < v.length) {
          if (c = v[v.length - 1], n(a, c, 0, !c.isAlternator), c.isAlternator) {
            u = v.pop();
            for (var e = 0; e < u.matches.length; e++) u.matches[e].isGroup = !1;
            0 < v.length ? (c = v[v.length - 1]).matches.push(u) : m.matches.push(u)
          }
        } else n(a, m)
      }
      for (var s, a, l, c, u, f, h, p = /(?:[?*+]|\{[0-9\+\*]+(?:,[0-9\+\*]*)?\})|[^.?*+^${[]()|\\]+|./g, g = !1, m = new t, v = [], y = []; s = p.exec(e);)
        if (a = s[0], g) i();
        else switch (a.charAt(0)) {
          case d.escapeChar:
            g = !0;
            break;
          case d.optionalmarker.end:
          case d.groupmarker.end:
            if (void 0 !== (l = v.pop()))
              if (0 < v.length) {
                if ((c = v[v.length - 1]).matches.push(l), c.isAlternator) {
                  u = v.pop();
                  for (var b = 0; b < u.matches.length; b++) u.matches[b].isGroup = !1;
                  0 < v.length ? (c = v[v.length - 1]).matches.push(u) : m.matches.push(u)
                }
              } else m.matches.push(l);
            else i();
            break;
          case d.optionalmarker.start:
            r(m.matches[m.matches.length - 1]), v.push(new t(!1, !0));
            break;
          case d.groupmarker.start:
            r(m.matches[m.matches.length - 1]), v.push(new t(!0));
            break;
          case d.quantifiermarker.start:
            var x = new t(!1, !1, !0),
              w = (a = a.replace(/[{}]/g, "")).split(","),
              S = isNaN(w[0]) ? w[0] : parseInt(w[0]),
              k = 1 === w.length ? S : isNaN(w[1]) ? w[1] : parseInt(w[1]);
            if ("*" !== k && "+" !== k || (S = "*" === k ? 0 : 1), x.quantifier = {
                min: S,
                max: k
              }, 0 < v.length) {
              var C = v[v.length - 1].matches;
              (s = C.pop()).isGroup || ((h = new t(!0)).matches.push(s), s = h), C.push(s), C.push(x)
            } else(s = m.matches.pop()).isGroup || ((h = new t(!0)).matches.push(s), s = h), m.matches.push(s), m.matches.push(x);
            break;
          case d.alternatormarker:
            0 < v.length ? (c = v[v.length - 1], f = c.matches.pop()) : f = m.matches.pop(), f.isAlternator ? v.push(f) : ((u = new t(!1, !1, !1, !0)).matches.push(f), v.push(u));
            break;
          default:
            i()
        }
      for (; 0 < v.length;) r(l = v.pop(), !0), m.matches.push(l);
      return 0 < m.matches.length && (r(f = m.matches[m.matches.length - 1]), y.push(m)), d.numericInput && function e(t) {
        for (var n in t.matches = t.matches.reverse(), t.matches) {
          var i = parseInt(n);
          if (t.matches[n].isQuantifier && t.matches[i + 1] && t.matches[i + 1].isGroup) {
            var o = t.matches[n];
            t.matches.splice(n, 1), t.matches.splice(i + 1, 0, o)
          }
          void 0 !== t.matches[n].matches ? t.matches[n] = e(t.matches[n]) : t.matches[n] = ((r = t.matches[n]) === d.optionalmarker.start ? r = d.optionalmarker.end : r === d.optionalmarker.end ? r = d.optionalmarker.start : r === d.groupmarker.start ? r = d.groupmarker.end : r === d.groupmarker.end && (r = d.groupmarker.start), r)
        }
        var r;
        return t
      }(y[0]), y
    },
    resolveAlias: function(e, t, n) {
      var i = n.aliases[e];
      return i ? (i.alias && this.resolveAlias(i.alias, void 0, n), le.extend(!0, n, i), le.extend(!0, n, t), !0) : (null === n.mask && (n.mask = e), !1)
    }
  }, ce.extendDefaults = function(e) {
    le.extend(!0, ce.prototype.defaults, e)
  }, ce.extendDefinitions = function(e) {
    le.extend(!0, ce.prototype.defaults.definitions, e)
  }, ce.extendAliases = function(e) {
    le.extend(!0, ce.prototype.defaults.aliases, e)
  }, ce.format = function(e, t, n) {
    return ce(t).format(e, n)
  }, ce.unmask = function(e, t) {
    return ce(t).unmaskedvalue(e)
  }, ce.isValid = function(e, t) {
    return ce(t).isValid(e)
  }, ce.remove = function(e) {
    le.each(e, function(e, t) {
      t.inputmask && t.inputmask.remove()
    })
  }, ce.escapeRegex = function(e) {
    return e.replace(new RegExp("(\\" + ["/", ".", "*", "+", "?", "|", "(", ")", "[", "]", "{", "}", "\\", "$", "^"].join("|\\") + ")", "gim"), "\\$1")
  }, ce.keyCode = {
    ALT: 18,
    BACKSPACE: 8,
    BACKSPACE_SAFARI: 127,
    CAPS_LOCK: 20,
    COMMA: 188,
    COMMAND: 91,
    COMMAND_LEFT: 91,
    COMMAND_RIGHT: 93,
    CONTROL: 17,
    DELETE: 46,
    DOWN: 40,
    END: 35,
    ENTER: 13,
    ESCAPE: 27,
    HOME: 36,
    INSERT: 45,
    LEFT: 37,
    MENU: 93,
    NUMPAD_ADD: 107,
    NUMPAD_DECIMAL: 110,
    NUMPAD_DIVIDE: 111,
    NUMPAD_ENTER: 108,
    NUMPAD_MULTIPLY: 106,
    NUMPAD_SUBTRACT: 109,
    PAGE_DOWN: 34,
    PAGE_UP: 33,
    PERIOD: 190,
    RIGHT: 39,
    SHIFT: 16,
    SPACE: 32,
    TAB: 9,
    UP: 38,
    WINDOWS: 91,
    X: 88
  }, window.Inputmask = ce
}(jQuery),
function(o, r) {
  void 0 === o.fn.inputmask && (o.fn.inputmask = function(e, t) {
    var n, i = this[0];
    if (void 0 === t && (t = {}), "string" == typeof e) switch (e) {
      case "unmaskedvalue":
        return i && i.inputmask ? i.inputmask.unmaskedvalue() : o(i).val();
      case "remove":
        return this.each(function() {
          this.inputmask && this.inputmask.remove()
        });
      case "getemptymask":
        return i && i.inputmask ? i.inputmask.getemptymask() : "";
      case "hasMaskedValue":
        return !(!i || !i.inputmask) && i.inputmask.hasMaskedValue();
      case "isComplete":
        return !i || !i.inputmask || i.inputmask.isComplete();
      case "getmetadata":
        return i && i.inputmask ? i.inputmask.getmetadata() : void 0;
      case "setvalue":
        o(i).val(t), i && void 0 === i.inputmask && o(i).triggerHandler("setvalue");
        break;
      case "option":
        if ("string" != typeof t) return this.each(function() {
          if (void 0 !== this.inputmask) return this.inputmask.option(t)
        });
        if (i && void 0 !== i.inputmask) return i.inputmask.option(t);
        break;
      default:
        return t.alias = e, n = new r(t), this.each(function() {
          n.mask(this)
        })
    } else {
      if ("object" == typeof e) return n = new r(e), void 0 === e.mask && void 0 === e.alias ? this.each(function() {
        return void 0 !== this.inputmask ? this.inputmask.option(e) : void n.mask(this)
      }) : this.each(function() {
        n.mask(this)
      });
      if (void 0 === e) return this.each(function() {
        (n = new r(t)).mask(this)
      })
    }
  }), o.fn.inputmask
}(jQuery, Inputmask), jQuery, Inputmask,
function(s, a) {
  a.extendAliases({
    "dd/mm/yyyy": {
      mask: "1/2/y",
      placeholder: "dd/mm/yyyy",
      regex: {
        val1pre: new RegExp("[0-3]"),
        val1: new RegExp("0[1-9]|[12][0-9]|3[01]"),
        val2pre: function(e) {
          var t = a.escapeRegex.call(this, e);
          return new RegExp("((0[1-9]|[12][0-9]|3[01])" + t + "[01])")
        },
        val2: function(e) {
          var t = a.escapeRegex.call(this, e);
          return new RegExp("((0[1-9]|[12][0-9])" + t + "(0[1-9]|1[012]))|(30" + t + "(0[13-9]|1[012]))|(31" + t + "(0[13578]|1[02]))")
        }
      },
      leapday: "29/02/",
      separator: "/",
      yearrange: {
        minyear: 1900,
        maxyear: 2099
      },
      isInYearRange: function(e, t, n) {
        if (isNaN(e)) return !1;
        var i = parseInt(e.concat(t.toString().slice(e.length))),
          o = parseInt(e.concat(n.toString().slice(e.length)));
        return !isNaN(i) && t <= i && i <= n || !isNaN(o) && t <= o && o <= n
      },
      determinebaseyear: function(e, t, n) {
        var i = (new Date).getFullYear();
        if (i < e) return e;
        if (t < i) {
          for (var o = t.toString().slice(0, 2), r = t.toString().slice(2, 4); t < o + n;) o--;
          var s = o + r;
          return s < e ? e : s
        }
        if (e <= i && i <= t) {
          for (var a = i.toString().slice(0, 2); t < a + n;) a--;
          var l = a + n;
          return l < e ? e : l
        }
        return i
      },
      onKeyDown: function(e, t, n, i) {
        var o = s(this);
        if (e.ctrlKey && e.keyCode === a.keyCode.RIGHT) {
          var r = new Date;
          o.val(r.getDate().toString() + (r.getMonth() + 1).toString() + r.getFullYear().toString()), o.trigger("setvalue")
        }
      },
      getFrontValue: function(e, t, n) {
        for (var i = 0, o = 0, r = 0; r < e.length && "2" !== e.charAt(r); r++) {
          var s = n.definitions[e.charAt(r)];
          s ? (i += o, o = s.cardinality) : o++
        }
        return t.join("").substr(i, o)
      },
      postValidation: function(e, t, n) {
        var i, o, r, s = e.join("");
        return 0 === n.mask.indexOf("y") ? (o = s.substr(0, 4), i = s.substr(4, 11)) : (o = s.substr(6, 11), i = s.substr(0, 6)), t && (i !== n.leapday || (r = o, isNaN(r) || 29 === new Date(r, 2, 0).getDate()))
      },
      definitions: {
        1: {
          validator: function(e, t, n, i, o) {
            var r = o.regex.val1.test(e);
            return i || r || e.charAt(1) !== o.separator && -1 === "-./".indexOf(e.charAt(1)) || !(r = o.regex.val1.test("0" + e.charAt(0))) ? r : (t.buffer[n - 1] = "0", {
              refreshFromBuffer: {
                start: n - 1,
                end: n
              },
              pos: n,
              c: e.charAt(0)
            })
          },
          cardinality: 2,
          prevalidator: [{
            validator: function(e, t, n, i, o) {
              var r = e;
              isNaN(t.buffer[n + 1]) || (r += t.buffer[n + 1]);
              var s = 1 === r.length ? o.regex.val1pre.test(r) : o.regex.val1.test(r);
              if (!i && !s) {
                if (s = o.regex.val1.test(e + "0")) return t.buffer[n] = e, t.buffer[++n] = "0", {
                  pos: n,
                  c: "0"
                };
                if (s = o.regex.val1.test("0" + e)) return t.buffer[n] = "0", {
                  pos: ++n
                }
              }
              return s
            },
            cardinality: 1
          }]
        },
        2: {
          validator: function(e, t, n, i, o) {
            var r = o.getFrontValue(t.mask, t.buffer, o); - 1 !== r.indexOf(o.placeholder[0]) && (r = "01" + o.separator);
            var s = o.regex.val2(o.separator).test(r + e);
            return i || s || e.charAt(1) !== o.separator && -1 === "-./".indexOf(e.charAt(1)) || !(s = o.regex.val2(o.separator).test(r + "0" + e.charAt(0))) ? s : (t.buffer[n - 1] = "0", {
              refreshFromBuffer: {
                start: n - 1,
                end: n
              },
              pos: n,
              c: e.charAt(0)
            })
          },
          cardinality: 2,
          prevalidator: [{
            validator: function(e, t, n, i, o) {
              isNaN(t.buffer[n + 1]) || (e += t.buffer[n + 1]);
              var r = o.getFrontValue(t.mask, t.buffer, o); - 1 !== r.indexOf(o.placeholder[0]) && (r = "01" + o.separator);
              var s = 1 === e.length ? o.regex.val2pre(o.separator).test(r + e) : o.regex.val2(o.separator).test(r + e);
              return i || s || !(s = o.regex.val2(o.separator).test(r + "0" + e)) ? s : (t.buffer[n] = "0", {
                pos: ++n
              })
            },
            cardinality: 1
          }]
        },
        y: {
          validator: function(e, t, n, i, o) {
            return o.isInYearRange(e, o.yearrange.minyear, o.yearrange.maxyear)
          },
          cardinality: 4,
          prevalidator: [{
            validator: function(e, t, n, i, o) {
              var r = o.isInYearRange(e, o.yearrange.minyear, o.yearrange.maxyear);
              if (!i && !r) {
                var s = o.determinebaseyear(o.yearrange.minyear, o.yearrange.maxyear, e + "0").toString().slice(0, 1);
                if (r = o.isInYearRange(s + e, o.yearrange.minyear, o.yearrange.maxyear)) return t.buffer[n++] = s.charAt(0), {
                  pos: n
                };
                if (s = o.determinebaseyear(o.yearrange.minyear, o.yearrange.maxyear, e + "0").toString().slice(0, 2), r = o.isInYearRange(s + e, o.yearrange.minyear, o.yearrange.maxyear)) return t.buffer[n++] = s.charAt(0), t.buffer[n++] = s.charAt(1), {
                  pos: n
                }
              }
              return r
            },
            cardinality: 1
          }, {
            validator: function(e, t, n, i, o) {
              var r = o.isInYearRange(e, o.yearrange.minyear, o.yearrange.maxyear);
              if (!i && !r) {
                var s = o.determinebaseyear(o.yearrange.minyear, o.yearrange.maxyear, e).toString().slice(0, 2);
                if (r = o.isInYearRange(e[0] + s[1] + e[1], o.yearrange.minyear, o.yearrange.maxyear)) return t.buffer[n++] = s.charAt(1), {
                  pos: n
                };
                if (s = o.determinebaseyear(o.yearrange.minyear, o.yearrange.maxyear, e).toString().slice(0, 2), r = o.isInYearRange(s + e, o.yearrange.minyear, o.yearrange.maxyear)) return t.buffer[n - 1] = s.charAt(0), t.buffer[n++] = s.charAt(1), t.buffer[n++] = e.charAt(0), {
                  refreshFromBuffer: {
                    start: n - 3,
                    end: n
                  },
                  pos: n
                }
              }
              return r
            },
            cardinality: 2
          }, {
            validator: function(e, t, n, i, o) {
              return o.isInYearRange(e, o.yearrange.minyear, o.yearrange.maxyear)
            },
            cardinality: 3
          }]
        }
      },
      insertMode: !1,
      autoUnmask: !1
    },
    "mm/dd/yyyy": {
      placeholder: "mm/dd/yyyy",
      alias: "dd/mm/yyyy",
      regex: {
        val2pre: function(e) {
          var t = a.escapeRegex.call(this, e);
          return new RegExp("((0[13-9]|1[012])" + t + "[0-3])|(02" + t + "[0-2])")
        },
        val2: function(e) {
          var t = a.escapeRegex.call(this, e);
          return new RegExp("((0[1-9]|1[012])" + t + "(0[1-9]|[12][0-9]))|((0[13-9]|1[012])" + t + "30)|((0[13578]|1[02])" + t + "31)")
        },
        val1pre: new RegExp("[01]"),
        val1: new RegExp("0[1-9]|1[012]")
      },
      leapday: "02/29/",
      onKeyDown: function(e, t, n, i) {
        var o = s(this);
        if (e.ctrlKey && e.keyCode === a.keyCode.RIGHT) {
          var r = new Date;
          o.val((r.getMonth() + 1).toString() + r.getDate().toString() + r.getFullYear().toString()), o.trigger("setvalue")
        }
      }
    },
    "yyyy/mm/dd": {
      mask: "y/1/2",
      placeholder: "yyyy/mm/dd",
      alias: "mm/dd/yyyy",
      leapday: "/02/29",
      onKeyDown: function(e, t, n, i) {
        var o = s(this);
        if (e.ctrlKey && e.keyCode === a.keyCode.RIGHT) {
          var r = new Date;
          o.val(r.getFullYear().toString() + (r.getMonth() + 1).toString() + r.getDate().toString()), o.trigger("setvalue")
        }
      }
    },
    "dd.mm.yyyy": {
      mask: "1.2.y",
      placeholder: "dd.mm.yyyy",
      leapday: "29.02.",
      separator: ".",
      alias: "dd/mm/yyyy"
    },
    "dd-mm-yyyy": {
      mask: "1-2-y",
      placeholder: "dd-mm-yyyy",
      leapday: "29-02-",
      separator: "-",
      alias: "dd/mm/yyyy"
    },
    "mm.dd.yyyy": {
      mask: "1.2.y",
      placeholder: "mm.dd.yyyy",
      leapday: "02.29.",
      separator: ".",
      alias: "mm/dd/yyyy"
    },
    "mm-dd-yyyy": {
      mask: "1-2-y",
      placeholder: "mm-dd-yyyy",
      leapday: "02-29-",
      separator: "-",
      alias: "mm/dd/yyyy"
    },
    "yyyy.mm.dd": {
      mask: "y.1.2",
      placeholder: "yyyy.mm.dd",
      leapday: ".02.29",
      separator: ".",
      alias: "yyyy/mm/dd"
    },
    "yyyy-mm-dd": {
      mask: "y-1-2",
      placeholder: "yyyy-mm-dd",
      leapday: "-02-29",
      separator: "-",
      alias: "yyyy/mm/dd"
    },
    datetime: {
      mask: "1/2/y h:s",
      placeholder: "dd/mm/yyyy hh:mm",
      alias: "dd/mm/yyyy",
      regex: {
        hrspre: new RegExp("[012]"),
        hrs24: new RegExp("2[0-4]|1[3-9]"),
        hrs: new RegExp("[01][0-9]|2[0-4]"),
        ampm: new RegExp("^[a|p|A|P][m|M]"),
        mspre: new RegExp("[0-5]"),
        ms: new RegExp("[0-5][0-9]")
      },
      timeseparator: ":",
      hourFormat: "24",
      definitions: {
        h: {
          validator: function(e, t, n, i, o) {
            if ("24" === o.hourFormat && 24 === parseInt(e, 10)) return t.buffer[n - 1] = "0", {
              refreshFromBuffer: {
                start: n - 1,
                end: n
              },
              c: t.buffer[n] = "0"
            };
            var r = o.regex.hrs.test(e);
            if (!i && !r && (e.charAt(1) === o.timeseparator || -1 !== "-.:".indexOf(e.charAt(1))) && (r = o.regex.hrs.test("0" + e.charAt(0)))) return t.buffer[n - 1] = "0", t.buffer[n] = e.charAt(0), {
              refreshFromBuffer: {
                start: ++n - 2,
                end: n
              },
              pos: n,
              c: o.timeseparator
            };
            if (r && "24" !== o.hourFormat && o.regex.hrs24.test(e)) {
              var s = parseInt(e, 10);
              return t.buffer[n + 5] = 24 === s ? "a" : "p", t.buffer[n + 6] = "m", (s -= 12) < 10 ? (t.buffer[n] = s.toString(), t.buffer[n - 1] = "0") : (t.buffer[n] = s.toString().charAt(1), t.buffer[n - 1] = s.toString().charAt(0)), {
                refreshFromBuffer: {
                  start: n - 1,
                  end: n + 6
                },
                c: t.buffer[n]
              }
            }
            return r
          },
          cardinality: 2,
          prevalidator: [{
            validator: function(e, t, n, i, o) {
              var r = o.regex.hrspre.test(e);
              return i || r || !(r = o.regex.hrs.test("0" + e)) ? r : (t.buffer[n] = "0", {
                pos: ++n
              })
            },
            cardinality: 1
          }]
        },
        s: {
          validator: "[0-5][0-9]",
          cardinality: 2,
          prevalidator: [{
            validator: function(e, t, n, i, o) {
              var r = o.regex.mspre.test(e);
              return i || r || !(r = o.regex.ms.test("0" + e)) ? r : (t.buffer[n] = "0", {
                pos: ++n
              })
            },
            cardinality: 1
          }]
        },
        t: {
          validator: function(e, t, n, i, o) {
            return o.regex.ampm.test(e + "m")
          },
          casing: "lower",
          cardinality: 1
        }
      },
      insertMode: !1,
      autoUnmask: !1
    },
    datetime12: {
      mask: "1/2/y h:s t\\m",
      placeholder: "dd/mm/yyyy hh:mm xm",
      alias: "datetime",
      hourFormat: "12"
    },
    "mm/dd/yyyy hh:mm xm": {
      mask: "1/2/y h:s t\\m",
      placeholder: "mm/dd/yyyy hh:mm xm",
      alias: "datetime12",
      regex: {
        val2pre: function(e) {
          var t = a.escapeRegex.call(this, e);
          return new RegExp("((0[13-9]|1[012])" + t + "[0-3])|(02" + t + "[0-2])")
        },
        val2: function(e) {
          var t = a.escapeRegex.call(this, e);
          return new RegExp("((0[1-9]|1[012])" + t + "(0[1-9]|[12][0-9]))|((0[13-9]|1[012])" + t + "30)|((0[13578]|1[02])" + t + "31)")
        },
        val1pre: new RegExp("[01]"),
        val1: new RegExp("0[1-9]|1[012]")
      },
      leapday: "02/29/",
      onKeyDown: function(e, t, n, i) {
        var o = s(this);
        if (e.ctrlKey && e.keyCode === a.keyCode.RIGHT) {
          var r = new Date;
          o.val((r.getMonth() + 1).toString() + r.getDate().toString() + r.getFullYear().toString()), o.trigger("setvalue")
        }
      }
    },
    "hh:mm t": {
      mask: "h:s t\\m",
      placeholder: "hh:mm xm",
      alias: "datetime",
      hourFormat: "12"
    },
    "h:s t": {
      mask: "h:s t\\m",
      placeholder: "hh:mm xm",
      alias: "datetime",
      hourFormat: "12"
    },
    "hh:mm:ss": {
      mask: "h:s:s",
      placeholder: "hh:mm:ss",
      alias: "datetime",
      autoUnmask: !1
    },
    "hh:mm": {
      mask: "h:s",
      placeholder: "hh:mm",
      alias: "datetime",
      autoUnmask: !1
    },
    date: {
      alias: "dd/mm/yyyy"
    },
    "mm/yyyy": {
      mask: "1/y",
      placeholder: "mm/yyyy",
      leapday: "donotuse",
      separator: "/",
      alias: "mm/dd/yyyy"
    },
    shamsi: {
      regex: {
        val2pre: function(e) {
          var t = a.escapeRegex.call(this, e);
          return new RegExp("((0[1-9]|1[012])" + t + "[0-3])")
        },
        val2: function(e) {
          var t = a.escapeRegex.call(this, e);
          return new RegExp("((0[1-9]|1[012])" + t + "(0[1-9]|[12][0-9]))|((0[1-9]|1[012])" + t + "30)|((0[1-6])" + t + "31)")
        },
        val1pre: new RegExp("[01]"),
        val1: new RegExp("0[1-9]|1[012]")
      },
      yearrange: {
        minyear: 1300,
        maxyear: 1499
      },
      mask: "y/1/2",
      leapday: "/12/30",
      placeholder: "yyyy/mm/dd",
      alias: "mm/dd/yyyy",
      clearIncomplete: !0
    }
  })
}(jQuery, Inputmask),
function(e, t) {
  t.extendDefinitions({
    A: {
      validator: "[A-Za-zА-яЁёÀ-ÿµ]",
      cardinality: 1,
      casing: "upper"
    },
    "&": {
      validator: "[0-9A-Za-zА-яЁёÀ-ÿµ]",
      cardinality: 1,
      casing: "upper"
    },
    "#": {
      validator: "[0-9A-Fa-f]",
      cardinality: 1,
      casing: "upper"
    }
  }), t.extendAliases({
    url: {
      definitions: {
        i: {
          validator: ".",
          cardinality: 1
        }
      },
      mask: "(\\http://)|(\\http\\s://)|(ftp://)|(ftp\\s://)i{+}",
      insertMode: !1,
      autoUnmask: !1,
      inputmode: "url"
    },
    ip: {
      mask: "i[i[i]].i[i[i]].i[i[i]].i[i[i]]",
      definitions: {
        i: {
          validator: function(e, t, n, i, o) {
            return -1 < n - 1 && "." !== t.buffer[n - 1] ? (e = t.buffer[n - 1] + e, e = -1 < n - 2 && "." !== t.buffer[n - 2] ? t.buffer[n - 2] + e : "0" + e) : e = "00" + e, new RegExp("25[0-5]|2[0-4][0-9]|[01][0-9][0-9]").test(e)
          },
          cardinality: 1
        }
      },
      onUnMask: function(e, t, n) {
        return e
      },
      inputmode: "numeric"
    },
    email: {
      mask: "*{1,64}[.*{1,64}][.*{1,64}][.*{1,63}]@-{1,63}.-{1,63}[.-{1,63}][.-{1,63}]",
      greedy: !1,
      onBeforePaste: function(e, t) {
        return (e = e.toLowerCase()).replace("mailto:", "")
      },
      definitions: {
        "*": {
          validator: "[0-9A-Za-z!#$%&'*+/=?^_`{|}~-]",
          cardinality: 1,
          casing: "lower"
        },
        "-": {
          validator: "[0-9A-Za-z-]",
          cardinality: 1,
          casing: "lower"
        }
      },
      onUnMask: function(e, t, n) {
        return e
      },
      inputmode: "email"
    },
    mac: {
      mask: "##:##:##:##:##:##"
    },
    vin: {
      mask: "V{13}9{4}",
      definitions: {
        V: {
          validator: "[A-HJ-NPR-Za-hj-npr-z\\d]",
          cardinality: 1,
          casing: "upper"
        }
      },
      clearIncomplete: !0,
      autoUnmask: !0
    }
  })
}(jQuery, Inputmask),
function(g, m) {
  m.extendAliases({
    numeric: {
      mask: function(i) {
        function e(e) {
          for (var t = "", n = 0; n < e.length; n++) t += i.definitions[e.charAt(n)] || i.optionalmarker.start === e.charAt(n) || i.optionalmarker.end === e.charAt(n) || i.quantifiermarker.start === e.charAt(n) || i.quantifiermarker.end === e.charAt(n) || i.groupmarker.start === e.charAt(n) || i.groupmarker.end === e.charAt(n) || i.alternatormarker === e.charAt(n) ? "\\" + e.charAt(n) : e.charAt(n);
          return t
        }
        if (0 !== i.repeat && isNaN(i.integerDigits) && (i.integerDigits = i.repeat), i.repeat = 0, i.groupSeparator === i.radixPoint && ("." === i.radixPoint ? i.groupSeparator = "," : "," === i.radixPoint ? i.groupSeparator = "." : i.groupSeparator = ""), " " === i.groupSeparator && (i.skipOptionalPartCharacter = void 0), i.autoGroup = i.autoGroup && "" !== i.groupSeparator, i.autoGroup && ("string" == typeof i.groupSize && isFinite(i.groupSize) && (i.groupSize = parseInt(i.groupSize)), isFinite(i.integerDigits))) {
          var t = Math.floor(i.integerDigits / i.groupSize),
            n = i.integerDigits % i.groupSize;
          i.integerDigits = parseInt(i.integerDigits) + (0 === n ? t - 1 : t), i.integerDigits < 1 && (i.integerDigits = "*")
        }
        1 < i.placeholder.length && (i.placeholder = i.placeholder.charAt(0)), "radixFocus" === i.positionCaretOnClick && "" === i.placeholder && !1 === i.integerOptional && (i.positionCaretOnClick = "lvp"), i.definitions[";"] = i.definitions["~"], i.definitions[";"].definitionSymbol = "~", !0 === i.numericInput && (i.positionCaretOnClick = "radixFocus" === i.positionCaretOnClick ? "lvp" : i.positionCaretOnClick, i.digitsOptional = !1, isNaN(i.digits) && (i.digits = 2), i.decimalProtect = !1);
        var o = "[+]";
        if (o += e(i.prefix), o += !0 === i.integerOptional ? "~{1," + i.integerDigits + "}" : "~{" + i.integerDigits + "}", void 0 !== i.digits) {
          i.decimalProtect && (i.radixPointDefinitionSymbol = ":");
          var r = i.digits.toString().split(",");
          isFinite(r[0] && r[1] && isFinite(r[1])) ? o += (i.decimalProtect ? ":" : i.radixPoint) + ";{" + i.digits + "}" : (isNaN(i.digits) || 0 < parseInt(i.digits)) && (o += i.digitsOptional ? "[" + (i.decimalProtect ? ":" : i.radixPoint) + ";{1," + i.digits + "}]" : (i.decimalProtect ? ":" : i.radixPoint) + ";{" + i.digits + "}")
        }
        return o += e(i.suffix), o += "[-]", i.greedy = !1, null !== i.min && (i.min = i.min.toString().replace(new RegExp(m.escapeRegex(i.groupSeparator), "g"), ""), "," === i.radixPoint && (i.min = i.min.replace(i.radixPoint, "."))), null !== i.max && (i.max = i.max.toString().replace(new RegExp(m.escapeRegex(i.groupSeparator), "g"), ""), "," === i.radixPoint && (i.max = i.max.replace(i.radixPoint, "."))), o
      },
      placeholder: "",
      greedy: !1,
      digits: "*",
      digitsOptional: !0,
      radixPoint: ".",
      positionCaretOnClick: "radixFocus",
      groupSize: 3,
      groupSeparator: "",
      autoGroup: !1,
      allowPlus: !0,
      allowMinus: !0,
      negationSymbol: {
        front: "-",
        back: ""
      },
      integerDigits: "+",
      integerOptional: !0,
      prefix: "",
      suffix: "",
      rightAlign: !0,
      decimalProtect: !0,
      min: null,
      max: null,
      step: 1,
      insertMode: !0,
      autoUnmask: !1,
      unmaskAsNumber: !1,
      inputmode: "numeric",
      postFormat: function(e, t, n) {
        var i, o;
        !0 === n.numericInput && (e = e.reverse(), isFinite(t) && (t = e.join("").length - t - 1));
        var r = e[t = t >= e.length ? e.length - 1 : t < 0 ? 0 : t],
          s = e.slice();
        r === n.groupSeparator && (s.splice(t--, 1), r = s[t]);
        var a = s.join("").match(new RegExp("^" + m.escapeRegex(n.negationSymbol.front)));
        t > ((a = null !== a && 1 === a.length) ? n.negationSymbol.front.length : 0) + n.prefix.length && t < s.length - n.suffix.length && (s[t] = "!");
        var l = s.join(""),
          c = s.join();
        if (a && (l = (l = l.replace(new RegExp("^" + m.escapeRegex(n.negationSymbol.front)), "")).replace(new RegExp(m.escapeRegex(n.negationSymbol.back) + "$"), "")), 0 < (l = (l = l.replace(new RegExp(m.escapeRegex(n.suffix) + "$"), "")).replace(new RegExp("^" + m.escapeRegex(n.prefix)), "")).length && n.autoGroup || -1 !== l.indexOf(n.groupSeparator)) {
          var u = m.escapeRegex(n.groupSeparator),
            d = (l = l.replace(new RegExp(u, "g"), "")).split(r === n.radixPoint ? "!" : n.radixPoint);
          if (l = "" === n.radixPoint ? l : d[0], r !== n.negationSymbol.front && (l = l.replace("!", "?")), l.length > n.groupSize)
            for (var f = new RegExp("([-+]?[\\d?]+)([\\d?]{" + n.groupSize + "})"); f.test(l) && "" !== n.groupSeparator;) l = (l = l.replace(f, "$1" + n.groupSeparator + "$2")).replace(n.groupSeparator + n.groupSeparator, n.groupSeparator);
          l = l.replace("?", "!"), "" !== n.radixPoint && 1 < d.length && (l += (r === n.radixPoint ? "!" : n.radixPoint) + d[1])
        }
        l = n.prefix + l + n.suffix, a && (l = n.negationSymbol.front + l + n.negationSymbol.back);
        var h = c !== l.split("").join(),
          p = g.inArray("!", l);
        if (-1 === p && (p = t), h) {
          for (e.length = l.length, i = 0, o = l.length; i < o; i++) e[i] = l.charAt(i);
          e[p] = r
        }
        return p = n.numericInput && isFinite(t) ? e.join("").length - p - 1 : p, n.numericInput && (e = e.reverse(), g.inArray(n.radixPoint, e) < p && e.join("").length - n.suffix.length !== p && (p -= 1)), {
          pos: p,
          refreshFromBuffer: h,
          buffer: e,
          isNegative: a
        }
      },
      onBeforeWrite: function(e, t, n, i) {
        var o;
        if (e && ("blur" === e.type || "checkval" === e.type || "keydown" === e.type)) {
          var r = i.numericInput ? t.slice().reverse().join("") : t.join(""),
            s = r.replace(i.prefix, "");
          s = (s = s.replace(i.suffix, "")).replace(new RegExp(m.escapeRegex(i.groupSeparator), "g"), ""), "," === i.radixPoint && (s = s.replace(i.radixPoint, "."));
          var a = s.match(new RegExp("[-" + m.escapeRegex(i.negationSymbol.front) + "]", "g"));
          if (a = null !== a && 1 === a.length, s = (s = s.replace(new RegExp("[-" + m.escapeRegex(i.negationSymbol.front) + "]", "g"), "")).replace(new RegExp(m.escapeRegex(i.negationSymbol.back) + "$"), ""), isNaN(i.placeholder) && (s = s.replace(new RegExp(m.escapeRegex(i.placeholder), "g"), "")), "" !== (s = s === i.negationSymbol.front ? s + "0" : s) && isFinite(s)) {
            var l = parseFloat(s),
              c = a ? -1 * l : l;
            if (null !== i.min && isFinite(i.min) && c < parseFloat(i.min) ? (l = Math.abs(i.min), a = i.min < 0, r = void 0) : null !== i.max && isFinite(i.max) && c > parseFloat(i.max) && (l = Math.abs(i.max), a = i.max < 0, r = void 0), s = l.toString().replace(".", i.radixPoint).split(""), isFinite(i.digits)) {
              var u = g.inArray(i.radixPoint, s),
                d = g.inArray(i.radixPoint, r); - 1 === u && (s.push(i.radixPoint), u = s.length - 1);
              for (var f = 1; f <= i.digits; f++) i.digitsOptional || void 0 !== s[u + f] && s[u + f] !== i.placeholder.charAt(0) ? -1 !== d && void 0 !== r[d + f] && (s[u + f] = s[u + f] || r[d + f]) : s[u + f] = "0";
              s[s.length - 1] === i.radixPoint && delete s[s.length - 1]
            }
            if (l.toString() !== s && l.toString() + "." !== s || a) return s = (i.prefix + s.join("")).split(""), !a || 0 === l && "blur" === e.type || (s.unshift(i.negationSymbol.front), s.push(i.negationSymbol.back)), i.numericInput && (s = s.reverse()), (o = i.postFormat(s, i.numericInput ? n : n - 1, i)).buffer && (o.refreshFromBuffer = o.buffer.join("") !== t.join("")), o
          }
        }
        if (i.autoGroup) return (o = i.postFormat(t, i.numericInput ? n : n - 1, i)).caret = n < (o.isNegative ? i.negationSymbol.front.length : 0) + i.prefix.length || n > o.buffer.length - (o.isNegative ? i.negationSymbol.back.length : 0) ? o.pos : o.pos + 1, o
      },
      regex: {
        integerPart: function(e) {
          return new RegExp("[" + m.escapeRegex(e.negationSymbol.front) + "+]?\\d+")
        },
        integerNPart: function(e) {
          return new RegExp("[\\d" + m.escapeRegex(e.groupSeparator) + m.escapeRegex(e.placeholder.charAt(0)) + "]+")
        }
      },
      signHandler: function(e, t, n, i, o) {
        if (!i && o.allowMinus && "-" === e || o.allowPlus && "+" === e) {
          var r = t.buffer.join("").match(o.regex.integerPart(o));
          if (r && 0 < r[0].length) return t.buffer[r.index] === ("-" === e ? "+" : o.negationSymbol.front) ? "-" === e ? "" !== o.negationSymbol.back ? {
            pos: 0,
            c: o.negationSymbol.front,
            remove: 0,
            caret: n,
            insert: {
              pos: t.buffer.length - 1,
              c: o.negationSymbol.back
            }
          } : {
            pos: 0,
            c: o.negationSymbol.front,
            remove: 0,
            caret: n
          } : "" !== o.negationSymbol.back ? {
            pos: 0,
            c: "+",
            remove: [0, t.buffer.length - 1],
            caret: n
          } : {
            pos: 0,
            c: "+",
            remove: 0,
            caret: n
          } : t.buffer[0] === ("-" === e ? o.negationSymbol.front : "+") ? "-" === e && "" !== o.negationSymbol.back ? {
            remove: [0, t.buffer.length - 1],
            caret: n - 1
          } : {
            remove: 0,
            caret: n - 1
          } : "-" === e ? "" !== o.negationSymbol.back ? {
            pos: 0,
            c: o.negationSymbol.front,
            caret: n + 1,
            insert: {
              pos: t.buffer.length,
              c: o.negationSymbol.back
            }
          } : {
            pos: 0,
            c: o.negationSymbol.front,
            caret: n + 1
          } : {
            pos: 0,
            c: e,
            caret: n + 1
          }
        }
        return !1
      },
      radixHandler: function(e, t, n, i, o) {
        if (!i && !0 !== o.numericInput && e === o.radixPoint && void 0 !== o.digits && (isNaN(o.digits) || 0 < parseInt(o.digits))) {
          var r = g.inArray(o.radixPoint, t.buffer),
            s = t.buffer.join("").match(o.regex.integerPart(o));
          if (-1 !== r && t.validPositions[r]) return t.validPositions[r - 1] ? {
            caret: r + 1
          } : {
            pos: s.index,
            c: s[0],
            caret: r + 1
          };
          if (!s || "0" === s[0] && s.index + 1 !== n) return t.buffer[s ? s.index : n] = "0", {
            pos: (s ? s.index : n) + 1,
            c: o.radixPoint
          }
        }
        return !1
      },
      leadingZeroHandler: function(e, t, n, i, o, r) {
        if (!i)
          if ((s = t.buffer.slice("")).splice(0, o.prefix.length), s.splice(s.length - o.suffix.length, o.suffix.length), !0 === o.numericInput) {
            var s;
            if ("0" === (s = s.reverse())[0] && void 0 === t.validPositions[n - 1]) return {
              pos: n,
              remove: s.length - 1
            }
          } else {
            n -= o.prefix.length;
            var a = g.inArray(o.radixPoint, s),
              l = s.slice(0, -1 !== a ? a : void 0).join("").match(o.regex.integerNPart(o));
            if (l && (-1 === a || n <= a)) {
              var c = -1 === a ? 0 : parseInt(s.slice(a + 1).join(""));
              if (0 === l[0].indexOf("" !== o.placeholder ? o.placeholder.charAt(0) : "0") && (l.index + 1 === n || !0 !== r && 0 === c)) return t.buffer.splice(l.index + o.prefix.length, 1), {
                pos: l.index + o.prefix.length,
                remove: l.index + o.prefix.length
              };
              if ("0" === e && n <= l.index && l[0] !== o.groupSeparator) return !1
            }
          }
        return !0
      },
      definitions: {
        "~": {
          validator: function(e, t, n, i, o, r) {
            var s = o.signHandler(e, t, n, i, o);
            if (!s && (!(s = o.radixHandler(e, t, n, i, o)) && (!0 === (s = i ? new RegExp("[0-9" + m.escapeRegex(o.groupSeparator) + "]").test(e) : new RegExp("[0-9]").test(e)) && !0 === (s = o.leadingZeroHandler(e, t, n, i, o, r))))) {
              var a = g.inArray(o.radixPoint, t.buffer);
              s = -1 !== a && (!1 === o.digitsOptional || t.validPositions[n]) && !0 !== o.numericInput && a < n && !i ? {
                pos: n,
                remove: n
              } : {
                pos: n
              }
            }
            return s
          },
          cardinality: 1
        },
        "+": {
          validator: function(e, t, n, i, o) {
            var r = o.signHandler(e, t, n, i, o);
            return !r && (i && o.allowMinus && e === o.negationSymbol.front || o.allowMinus && "-" === e || o.allowPlus && "+" === e) && (r = !(!i && "-" === e) || ("" !== o.negationSymbol.back ? {
              pos: n,
              c: "-" === e ? o.negationSymbol.front : "+",
              caret: n + 1,
              insert: {
                pos: t.buffer.length,
                c: o.negationSymbol.back
              }
            } : {
              pos: n,
              c: "-" === e ? o.negationSymbol.front : "+",
              caret: n + 1
            })), r
          },
          cardinality: 1,
          placeholder: ""
        },
        "-": {
          validator: function(e, t, n, i, o) {
            var r = o.signHandler(e, t, n, i, o);
            return !r && i && o.allowMinus && e === o.negationSymbol.back && (r = !0), r
          },
          cardinality: 1,
          placeholder: ""
        },
        ":": {
          validator: function(e, t, n, i, o) {
            var r = o.signHandler(e, t, n, i, o);
            if (!r) {
              var s = "[" + m.escapeRegex(o.radixPoint) + "]";
              (r = new RegExp(s).test(e)) && t.validPositions[n] && t.validPositions[n].match.placeholder === o.radixPoint && (r = {
                caret: n + 1
              })
            }
            return r
          },
          cardinality: 1,
          placeholder: function(e) {
            return e.radixPoint
          }
        }
      },
      onUnMask: function(e, t, n) {
        if ("" === t && !0 === n.nullable) return t;
        var i = e.replace(n.prefix, "");
        return i = (i = i.replace(n.suffix, "")).replace(new RegExp(m.escapeRegex(n.groupSeparator), "g"), ""), n.unmaskAsNumber ? ("" !== n.radixPoint && -1 !== i.indexOf(n.radixPoint) && (i = i.replace(m.escapeRegex.call(this, n.radixPoint), ".")), Number(i)) : i
      },
      isComplete: function(e, t) {
        var n = e.join(""),
          i = e.slice();
        if (t.postFormat(i, 0, t), i.join("") !== n) return !1;
        var o = n.replace(t.prefix, "");
        return o = (o = o.replace(t.suffix, "")).replace(new RegExp(m.escapeRegex(t.groupSeparator), "g"), ""), "," === t.radixPoint && (o = o.replace(m.escapeRegex(t.radixPoint), ".")), isFinite(o)
      },
      onBeforeMask: function(e, t) {
        if (!0 === t.numericInput && (e = e.split("").reverse().join("")), "" !== t.radixPoint && isFinite(e)) {
          var n = e.split("."),
            i = "" !== t.groupSeparator ? parseInt(t.groupSize) : 0;
          2 === n.length && (n[0].length > i || n[1].length > i) && (e = e.toString().replace(".", t.radixPoint))
        }
        var o = e.match(/,/g),
          r = e.match(/\./g);
        if (r && o ? r.length > o.length ? e = (e = e.replace(/\./g, "")).replace(",", t.radixPoint) : o.length > r.length ? e = (e = e.replace(/,/g, "")).replace(".", t.radixPoint) : e = e.indexOf(".") < e.indexOf(",") ? e.replace(/\./g, "") : e = e.replace(/,/g, "") : e = e.replace(new RegExp(m.escapeRegex(t.groupSeparator), "g"), ""), 0 === t.digits && (-1 !== e.indexOf(".") ? e = e.substring(0, e.indexOf(".")) : -1 !== e.indexOf(",") && (e = e.substring(0, e.indexOf(",")))), "" !== t.radixPoint && isFinite(t.digits) && -1 !== e.indexOf(t.radixPoint)) {
          var s = e.split(t.radixPoint)[1].match(new RegExp("\\d*"))[0];
          if (parseInt(t.digits) < s.toString().length) {
            var a = Math.pow(10, parseInt(t.digits));
            e = e.replace(m.escapeRegex(t.radixPoint), "."), e = (e = Math.round(parseFloat(e) * a) / a).toString().replace(".", t.radixPoint)
          }
        }
        return !0 === t.numericInput && (e = e.split("").reverse().join("")), e.toString()
      },
      canClearPosition: function(e, t, n, i, o) {
        var r = e.validPositions[t].input;
        return r !== o.radixPoint || null !== e.validPositions[t].match.fn && !1 === o.decimalProtect || isFinite(r) || t === n || r === o.groupSeparator || r === o.negationSymbol.front || r === o.negationSymbol.back
      },
      onKeyDown: function(e, t, n, i) {
        var o = g(this);
        if (e.ctrlKey) switch (e.keyCode) {
          case m.keyCode.UP:
            o.val(parseFloat(this.inputmask.unmaskedvalue()) + parseInt(i.step)), o.trigger("setvalue");
            break;
          case m.keyCode.DOWN:
            o.val(parseFloat(this.inputmask.unmaskedvalue()) - parseInt(i.step)), o.trigger("setvalue")
        }
      }
    },
    currency: {
      prefix: "$ ",
      groupSeparator: ",",
      alias: "numeric",
      placeholder: "0",
      autoGroup: !0,
      digits: 2,
      digitsOptional: !1,
      clearMaskOnLostFocus: !1
    },
    decimal: {
      alias: "numeric"
    },
    integer: {
      alias: "numeric",
      digits: 0,
      radixPoint: ""
    },
    percentage: {
      alias: "numeric",
      digits: 2,
      radixPoint: ".",
      placeholder: "0",
      autoGroup: !1,
      min: 0,
      max: 100,
      suffix: " %",
      allowPlus: !1,
      allowMinus: !1
    }
  })
}(jQuery, Inputmask),
function(s, e) {
  function t(e, t) {
    var n = (e.mask || e).replace(/#/g, "9").replace(/\)/, "9").replace(/[+()#-]/g, ""),
      i = (t.mask || t).replace(/#/g, "9").replace(/\)/, "9").replace(/[+()#-]/g, ""),
      o = (e.mask || e).split("#")[0],
      r = (t.mask || t).split("#")[0];
    return 0 === r.indexOf(o) ? -1 : 0 === o.indexOf(r) ? 1 : n.localeCompare(i)
  }
  var n = e.prototype.analyseMask;
  e.prototype.analyseMask = function(l, r) {
    var c = {};
    return r.phoneCodes && 1e3 < r.phoneCodes.length && (function e(t, n, i) {
      i = i || c, "" !== (n = n || "") && (i[n] = {});
      for (var o = "", r = i[n] || i, s = t.length - 1; 0 <= s; s--) r[o = (l = t[s].mask || t[s]).substr(0, 1)] = r[o] || [], r[o].unshift(l.substr(1)), t.splice(s, 1);
      for (var a in r) 500 < r[a].length && e(r[a].slice(), a, r)
    }((l = l.substr(1, l.length - 2)).split(r.groupmarker.end + r.alternatormarker + r.groupmarker.start)), l = function e(t) {
      var n = "",
        i = [];
      for (var o in t) s.isArray(t[o]) ? 1 === t[o].length ? i.push(o + t[o]) : i.push(o + r.groupmarker.start + t[o].join(r.groupmarker.end + r.alternatormarker + r.groupmarker.start) + r.groupmarker.end) : i.push(o + e(t[o]));
      return n + (1 === i.length ? i[0] : r.groupmarker.start + i.join(r.groupmarker.end + r.alternatormarker + r.groupmarker.start) + r.groupmarker.end)
    }(c)), n.call(this, l, r)
  }, e.extendAliases({
    abstractphone: {
      groupmarker: {
        start: "<",
        end: ">"
      },
      countrycode: "",
      phoneCodes: [],
      mask: function(e) {
        return e.definitions = {
          "#": e.definitions[9]
        }, e.phoneCodes.sort(t)
      },
      keepStatic: !0,
      onBeforeMask: function(e, t) {
        var n = e.replace(/^0{1,2}/, "").replace(/[\s]/g, "");
        return (1 < n.indexOf(t.countrycode) || -1 === n.indexOf(t.countrycode)) && (n = "+" + t.countrycode + n), n
      },
      onUnMask: function(e, t, n) {
        return t
      },
      inputmode: "tel"
    }
  })
}(jQuery, Inputmask),
function(b, e) {
  e.extendAliases({
    Regex: {
      mask: "r",
      greedy: !1,
      repeat: "*",
      regex: null,
      regexTokens: null,
      tokenizer: /\[\^?]?(?:[^\\\]]+|\\[\S\s]?)*]?|\\(?:0(?:[0-3][0-7]{0,2}|[4-7][0-7]?)?|[1-9][0-9]*|x[0-9A-Fa-f]{2}|u[0-9A-Fa-f]{4}|c[A-Za-z]|[\S\s]?)|\((?:\?[:=!]?)?|(?:[?*+]|\{[0-9]+(?:,[0-9]*)?\})\??|[^.?*+^${[()|\\]+|./g,
      quantifierFilter: /[0-9]+[^,]/,
      isComplete: function(e, t) {
        return new RegExp(t.regex).test(e.join(""))
      },
      definitions: {
        r: {
          validator: function(e, t, n, i, c) {
            function u(e, t) {
              this.matches = [], this.isGroup = e || !1, this.isQuantifier = t || !1, this.quantifier = {
                min: 1,
                max: 1
              }, this.repeaterPart = void 0
            }

            function g(e, t) {
              var n = !1;
              t && (v += "(", y++);
              for (var i = 0; i < e.matches.length; i++) {
                var o = e.matches[i];
                if (!0 === o.isGroup) n = g(o, !0);
                else if (!0 === o.isQuantifier) {
                  var r = b.inArray(o, e.matches),
                    s = e.matches[r - 1],
                    a = v;
                  if (isNaN(o.quantifier.max)) {
                    for (; o.repeaterPart && o.repeaterPart !== v && o.repeaterPart.length > v.length && !(n = g(s, !0)););
                    (n = n || g(s, !0)) && (o.repeaterPart = v), v = a + o.quantifier.max
                  } else {
                    for (var l = 0, c = o.quantifier.max - 1; l < c && !(n = g(s, !0)); l++);
                    v = a + "{" + o.quantifier.min + "," + o.quantifier.max + "}"
                  }
                } else if (void 0 !== o.matches)
                  for (var u = 0; u < o.length && !(n = g(o[u], t)); u++);
                else {
                  var d;
                  if ("[" == o.charAt(0)) {
                    d = v, d += o;
                    for (var f = 0; f < y; f++) d += ")";
                    n = new RegExp("^(" + d + ")$").test(m)
                  } else
                    for (var h = 0, p = o.length; h < p; h++)
                      if ("\\" !== o.charAt(h)) {
                        d = v, d = (d += o.substr(0, h + 1)).replace(/\|$/, "");
                        for (f = 0; f < y; f++) d += ")";
                        if (n = new RegExp("^(" + d + ")$").test(m)) break
                      }
                  v += o
                }
                if (n) break
              }
              return t && (v += ")", y--), n
            }
            var m, d, o = t.buffer.slice(),
              v = "",
              r = !1,
              y = 0;
            null === c.regexTokens && function() {
              var e, t, n = new u,
                i = [];
              for (c.regexTokens = []; e = c.tokenizer.exec(c.regex);) switch (t = e[0], t.charAt(0)) {
                case "(":
                  i.push(new u(!0));
                  break;
                case ")":
                  d = i.pop(), 0 < i.length ? i[i.length - 1].matches.push(d) : n.matches.push(d);
                  break;
                case "{":
                case "+":
                case "*":
                  var o = new u(!1, !0),
                    r = (t = t.replace(/[{}]/g, "")).split(","),
                    s = isNaN(r[0]) ? r[0] : parseInt(r[0]),
                    a = 1 === r.length ? s : isNaN(r[1]) ? r[1] : parseInt(r[1]);
                  if (o.quantifier = {
                      min: s,
                      max: a
                    }, 0 < i.length) {
                    var l = i[i.length - 1].matches;
                    (e = l.pop()).isGroup || ((d = new u(!0)).matches.push(e), e = d), l.push(e), l.push(o)
                  } else(e = n.matches.pop()).isGroup || ((d = new u(!0)).matches.push(e), e = d), n.matches.push(e), n.matches.push(o);
                  break;
                default:
                  0 < i.length ? i[i.length - 1].matches.push(t) : n.matches.push(t)
              }
              0 < n.matches.length && c.regexTokens.push(n)
            }(), o.splice(n, 0, e), m = o.join("");
            for (var s = 0; s < c.regexTokens.length; s++) {
              var a = c.regexTokens[s];
              if (r = g(a, a.isGroup)) break
            }
            return r
          },
          cardinality: 1
        }
      }
    }
  })
}(jQuery, Inputmask),
function(l) {
  "use strict";
  var c, t, i, o, r = "dotdotdot";
  l[r] && "3.2.2" < l[r].version || (l[r] = function(e, t) {
    this.$dot = e, this.api = ["getInstance", "truncate", "restore", "destroy", "watch", "unwatch"], this.opts = t;
    var n = this.$dot.data(r);
    return n && n.destroy(), this.init(), this.truncate(), this.opts.watch && this.watch(), this
  }, l[r].version = "3.2.2", l[r].uniqueId = 0, l[r].defaults = {
    ellipsis: "… ",
    callback: function(e) {},
    truncate: "word",
    tolerance: 0,
    keep: null,
    watch: "window",
    height: null
  }, l[r].prototype = {
    init: function() {
      this.watchTimeout = null, this.watchInterval = null, this.uniqueId = l[r].uniqueId++, this.originalStyle = this.$dot.attr("style") || "", this.originalContent = this._getOriginalContent(), "break-word" !== this.$dot.css("word-wrap") && this.$dot.css("word-wrap", "break-word"), "nowrap" === this.$dot.css("white-space") && this.$dot.css("white-space", "normal"), null === this.opts.height && (this.opts.height = this._getMaxHeight()), "string" == typeof this.opts.ellipsis && (this.opts.ellipsis = document.createTextNode(this.opts.ellipsis))
    },
    getInstance: function() {
      return this
    },
    truncate: function() {
      this.$inner = this.$dot.wrapInner("<div />").children().css({
        display: "block",
        height: "auto",
        width: "auto",
        border: "none",
        padding: 0,
        margin: 0
      }), this.$inner.empty().append(this.originalContent.clone(!0)), this.maxHeight = this._getMaxHeight();
      var e = !1;
      return this._fits() || (e = !0, this._truncateToNode(this.$inner[0])), this.$dot[e ? "addClass" : "removeClass"](c.truncated), this.$inner.replaceWith(this.$inner.contents()), this.$inner = null, this.opts.callback.call(this.$dot[0], e), e
    },
    restore: function() {
      this.unwatch(), this.$dot.empty().append(this.originalContent).attr("style", this.originalStyle).removeClass(c.truncated)
    },
    destroy: function() {
      this.restore(), this.$dot.data(r, null)
    },
    watch: function() {
      var t = this;
      this.unwatch();
      var n = {};
      "window" == this.opts.watch ? o.on(i.resize + t.uniqueId, function(e) {
        t.watchTimeout && clearTimeout(t.watchTimeout), t.watchTimeout = setTimeout(function() {
          n = t._watchSizes(n, o, "width", "height")
        }, 100)
      }) : this.watchInterval = setInterval(function() {
        n = t._watchSizes(n, t.$dot, "innerWidth", "innerHeight")
      }, 500)
    },
    unwatch: function() {
      o.off(i.resize + this.uniqueId), this.watchInterval && clearInterval(this.watchInterval), this.watchTimeout && clearTimeout(this.watchTimeout)
    },
    _api: function() {
      var n = this,
        i = {};
      return l.each(this.api, function(e) {
        var t = this;
        i[t] = function() {
          var e = n[t].apply(n, arguments);
          return void 0 === e ? i : e
        }
      }), i
    },
    _truncateToNode: function(e) {
      var n = [],
        i = [];
      if (l(e).contents().each(function() {
          var e = l(this);
          if (!e.hasClass(c.keep)) {
            var t = document.createComment("");
            e.replaceWith(t), i.push(this), n.push(t)
          }
        }), i.length) {
        for (var t = 0; t < i.length; t++) {
          l(n[t]).replaceWith(i[t]), l(i[t]).append(this.opts.ellipsis);
          var o = this._fits();
          if (l(this.opts.ellipsis, i[t]).remove(), !o) {
            if ("node" == this.opts.truncate && 1 < t) return void l(i[t - 2]).remove();
            break
          }
        }
        for (var r = t; r < n.length; r++) l(n[r]).remove();
        var s = i[Math.max(0, Math.min(t, i.length - 1))];
        if (1 == s.nodeType) {
          var a = l("<" + s.nodeName + " />");
          a.append(this.opts.ellipsis), l(s).replaceWith(a), this._fits() ? a.replaceWith(s) : (a.remove(), s = i[Math.max(0, t - 1)])
        }
        1 == s.nodeType ? this._truncateToNode(s) : this._truncateToWord(s)
      }
    },
    _truncateToWord: function(e) {
      for (var t = e, n = this.__getTextContent(t), i = -1 !== n.indexOf(" ") ? " " : "　", o = n.split(i), r = "", s = o.length; 0 <= s; s--)
        if (r = o.slice(0, s).join(i), this.__setTextContent(t, this._addEllipsis(r)), this._fits()) {
          "letter" == this.opts.truncate && (this.__setTextContent(t, o.slice(0, s + 1).join(i)), this._truncateToLetter(t));
          break
        }
    },
    _truncateToLetter: function(e) {
      for (var t = this.__getTextContent(e).split(""), n = "", i = t.length; 0 <= i && (!(n = t.slice(0, i).join("")).length || (this.__setTextContent(e, this._addEllipsis(n)), !this._fits())); i--);
    },
    _fits: function() {
      return this.$inner.innerHeight() <= this.maxHeight + this.opts.tolerance
    },
    _addEllipsis: function(e) {
      for (var t = [" ", "　", ",", ";", ".", "!", "?"]; - 1 < l.inArray(e.slice(-1), t);) e = e.slice(0, -1);
      return e + this.__getTextContent(this.opts.ellipsis)
    },
    _getOriginalContent: function() {
      var t = this;
      return this.$dot.find("script, style").addClass(c.keep), this.opts.keep && this.$dot.find(this.opts.keep).addClass(c.keep), this.$dot.find("*").not("." + c.keep).add(this.$dot).contents().each(function() {
        var e = l(this);
        if (3 == this.nodeType) {
          if ("" == l.trim(t.__getTextContent(this))) {
            if (e.parent().is("table, thead, tbody, tfoot, tr, dl, ul, ol, video")) return void e.remove();
            if (e.prev().is("div, p, table, td, td, dt, dd, li")) return void e.remove();
            if (e.next().is("div, p, table, td, td, dt, dd, li")) return void e.remove();
            if (!e.prev().length) return void e.remove();
            if (!e.next().length) return void e.remove()
          }
        } else 8 == this.nodeType && e.remove()
      }), this.$dot.contents()
    },
    _getMaxHeight: function() {
      if ("number" == typeof this.opts.height) return this.opts.height;
      for (var e = ["maxHeight", "height"], t = 0, n = 0; n < e.length; n++)
        if ("px" == (t = window.getComputedStyle(this.$dot[0])[e[n]]).slice(-2)) {
          t = parseFloat(t);
          break
        }
      e = [];
      switch (this.$dot.css("boxSizing")) {
        case "border-box":
          e.push("borderTopWidth"), e.push("borderBottomWidth");
        case "padding-box":
          e.push("paddingTop"), e.push("paddingBottom")
      }
      for (n = 0; n < e.length; n++) {
        var i = window.getComputedStyle(this.$dot[0])[e[n]];
        "px" == i.slice(-2) && (t -= parseFloat(i))
      }
      return Math.max(t, 0)
    },
    _watchSizes: function(e, t, n, i) {
      if (this.$dot.is(":visible")) {
        var o = {
          width: t[n](),
          height: t[i]()
        };
        return e.width == o.width && e.height == o.height || this.truncate(), o
      }
      return e
    },
    __getTextContent: function(e) {
      for (var t = ["nodeValue", "textContent", "innerText"], n = 0; n < t.length; n++)
        if ("string" == typeof e[t[n]]) return e[t[n]];
      return ""
    },
    __setTextContent: function(e, t) {
      for (var n = ["nodeValue", "textContent", "innerText"], i = 0; i < n.length; i++) e[n[i]] = t
    }
  }, l.fn[r] = function(e) {
    return o = l(window), c = {}, t = {}, i = {}, l.each([c, t, i], function(e, i) {
      i.add = function(e) {
        for (var t = 0, n = (e = e.split(" ")).length; t < n; t++) i[e[t]] = i.ddd(e[t])
      }
    }), c.ddd = function(e) {
      return "ddd-" + e
    }, c.add("truncated keep"), t.ddd = function(e) {
      return "ddd-" + e
    }, i.ddd = function(e) {
      return e + ".ddd"
    }, i.add("resize"), e = l.extend(!0, {}, l[r].defaults, e), this.each(function() {
      l(this).data(r, new l[r](l(this), e)._api())
    })
  })
}(jQuery),
function(e) {
  "function" == typeof define && define.amd ? define(["jquery"], e) : "object" == typeof exports ? module.exports = e($ || require("jquery")) : e(jQuery)
}(function(H) {
  "use strict";

  function o(e, t) {
    this.element = e, this.options = H.extend({}, i, t);
    var n = this.options.locale;
    void 0 !== this.options.locales[n] && H.extend(this.options, this.options.locales[n]), this.init()
  }

  function L(e) {
    if (!H(e.target).parents().hasClass("jq-selectbox") && "OPTION" != e.target.nodeName && H("div.jq-selectbox.opened").length) {
      var t = H("div.jq-selectbox.opened"),
        n = H("div.jq-selectbox__search input", t),
        i = H("div.jq-selectbox__dropdown", t);
      t.find("select").data("_" + r).options.onSelectClosed.call(t), n.length && n.val("").keyup(), i.hide().find("li.sel").addClass("selected"), t.removeClass("focused opened dropup dropdown")
    }
  }
  var r = "styler",
    i = {
      idSuffix: "-styler",
      filePlaceholder: "Файл не выбран",
      fileBrowse: "Обзор...",
      fileNumber: "Выбрано файлов: %s",
      selectPlaceholder: "Выберите...",
      selectSearch: !1,
      selectSearchLimit: 10,
      selectSearchNotFound: "Совпадений не найдено",
      selectSearchPlaceholder: "Поиск...",
      selectVisibleOptions: 0,
      selectSmartPositioning: !0,
      locale: "ru",
      locales: {
        en: {
          filePlaceholder: "No file selected",
          fileBrowse: "Browse...",
          fileNumber: "Selected files: %s",
          selectPlaceholder: "Select...",
          selectSearchNotFound: "No matches found",
          selectSearchPlaceholder: "Search..."
        }
      },
      onSelectOpened: function() {},
      onSelectClosed: function() {},
      onFormStyled: function() {}
    };
  o.prototype = {
    init: function() {
      function D() {
        void 0 !== O.attr("id") && "" !== O.attr("id") && (this.id = O.attr("id") + M.idSuffix), this.title = O.attr("title"), this.classes = O.attr("class"), this.data = O.data()
      }
      var O = H(this.element),
        M = this.options,
        I = !(!navigator.userAgent.match(/(iPad|iPhone|iPod)/i) || navigator.userAgent.match(/(Windows\sPhone)/i)),
        e = !(!navigator.userAgent.match(/Android/i) || navigator.userAgent.match(/(Windows\sPhone)/i));
      if (O.is(":checkbox")) {
        var t = function() {
          var e = new D,
            t = H('<div class="jq-checkbox"><div class="jq-checkbox__div"></div></div>').attr({
              id: e.id,
              title: e.title
            }).addClass(e.classes).data(e.data);
          O.after(t).prependTo(t), O.is(":checked") && t.addClass("checked"), O.is(":disabled") && t.addClass("disabled"), t.click(function(e) {
            e.preventDefault(), O.triggerHandler("click"), t.is(".disabled") || (O.is(":checked") ? (O.prop("checked", !1), t.removeClass("checked")) : (O.prop("checked", !0), t.addClass("checked")), O.focus().change())
          }), O.closest("label").add('label[for="' + O.attr("id") + '"]').on("click.styler", function(e) {
            H(e.target).is("a") || H(e.target).closest(t).length || (t.triggerHandler("click"), e.preventDefault())
          }), O.on("change.styler", function() {
            O.is(":checked") ? t.addClass("checked") : t.removeClass("checked")
          }).on("keydown.styler", function(e) {
            32 == e.which && t.click()
          }).on("focus.styler", function() {
            t.is(".disabled") || t.addClass("focused")
          }).on("blur.styler", function() {
            t.removeClass("focused")
          })
        };
        t(), O.on("refresh", function() {
          O.closest("label").add('label[for="' + O.attr("id") + '"]').off(".styler"), O.off(".styler").parent().before(O).remove(), t()
        })
      } else if (O.is(":radio")) {
        var n = function() {
          var e = new D,
            n = H('<div class="jq-radio"><div class="jq-radio__div"></div></div>').attr({
              id: e.id,
              title: e.title
            }).addClass(e.classes).data(e.data);
          O.after(n).prependTo(n), O.is(":checked") && n.addClass("checked"), O.is(":disabled") && n.addClass("disabled"), H.fn.commonParents = function() {
            var e = this;
            return e.first().parents().filter(function() {
              return H(this).find(e).length === e.length
            })
          }, H.fn.commonParent = function() {
            return H(this).commonParents().first()
          }, n.click(function(e) {
            if (e.preventDefault(), O.triggerHandler("click"), !n.is(".disabled")) {
              var t = H('input[name="' + O.attr("name") + '"]');
              t.commonParent().find(t).prop("checked", !1).parent().removeClass("checked"), O.prop("checked", !0).parent().addClass("checked"), O.focus().change()
            }
          }), O.closest("label").add('label[for="' + O.attr("id") + '"]').on("click.styler", function(e) {
            H(e.target).is("a") || H(e.target).closest(n).length || (n.triggerHandler("click"), e.preventDefault())
          }), O.on("change.styler", function() {
            O.parent().addClass("checked")
          }).on("focus.styler", function() {
            n.is(".disabled") || n.addClass("focused")
          }).on("blur.styler", function() {
            n.removeClass("focused")
          })
        };
        n(), O.on("refresh", function() {
          O.closest("label").add('label[for="' + O.attr("id") + '"]').off(".styler"), O.off(".styler").parent().before(O).remove(), n()
        })
      } else if (O.is(":file")) {
        var i = function() {
          var e = new D,
            i = O.data("placeholder");
          void 0 === i && (i = M.filePlaceholder);
          var t = O.data("browse");
          void 0 !== t && "" !== t || (t = M.fileBrowse);
          var o = H('<div class="jq-file"><div class="jq-file__name">' + i + '</div><div class="jq-file__browse">' + t + "</div></div>").attr({
            id: e.id,
            title: e.title
          }).addClass(e.classes).data(e.data);
          O.after(o).appendTo(o), O.is(":disabled") && o.addClass("disabled");
          var n = O.val(),
            r = H("div.jq-file__name", o);
          n && r.text(n.replace(/.+[\\\/]/, "")), O.on("change.styler", function() {
            var e = O.val();
            if (O.is("[multiple]")) {
              e = "";
              var t = O[0].files.length;
              if (0 < t) {
                var n = O.data("number");
                void 0 === n && (n = M.fileNumber), e = n = n.replace("%s", t)
              }
            }
            r.text(e.replace(/.+[\\\/]/, "")), "" === e ? (r.text(i), o.removeClass("changed")) : o.addClass("changed")
          }).on("focus.styler", function() {
            o.addClass("focused")
          }).on("blur.styler", function() {
            o.removeClass("focused")
          }).on("click.styler", function() {
            o.removeClass("focused")
          })
        };
        i(), O.on("refresh", function() {
          O.off(".styler").parent().before(O).remove(), i()
        })
      } else if (O.is('input[type="number"]')) {
        var o = function() {
          var e = new D,
            t = H('<div class="jq-number"><div class="jq-number__spin minus"></div><div class="jq-number__spin plus"></div></div>').attr({
              id: e.id,
              title: e.title
            }).addClass(e.classes).data(e.data);
          O.after(t).prependTo(t).wrap('<div class="jq-number__field"></div>'), O.is(":disabled") && t.addClass("disabled");
          var r, s, a, n = null,
            i = null;
          void 0 !== O.attr("min") && (r = O.attr("min")), void 0 !== O.attr("max") && (s = O.attr("max")), a = void 0 !== O.attr("step") && H.isNumeric(O.attr("step")) ? Number(O.attr("step")) : Number(1);
          var o = function(e) {
            var t, n = O.val();
            H.isNumeric(n) || (n = 0, O.val("0")), e.is(".minus") ? t = Number(n) - a : e.is(".plus") && (t = Number(n) + a);
            var i = (a.toString().split(".")[1] || []).length;
            if (0 < i) {
              for (var o = "1"; o.length <= i;) o += "0";
              t = Math.round(t * o) / o
            }
            H.isNumeric(r) && H.isNumeric(s) ? r <= t && t <= s && O.val(t) : H.isNumeric(r) && !H.isNumeric(s) ? r <= t && O.val(t) : !H.isNumeric(r) && H.isNumeric(s) ? t <= s && O.val(t) : O.val(t)
          };
          t.is(".disabled") || (t.on("mousedown", "div.jq-number__spin", function() {
            var e = H(this);
            o(e), n = setTimeout(function() {
              i = setInterval(function() {
                o(e)
              }, 40)
            }, 350)
          }).on("mouseup mouseout", "div.jq-number__spin", function() {
            clearTimeout(n), clearInterval(i)
          }).on("mouseup", "div.jq-number__spin", function() {
            O.change().trigger("input")
          }), O.on("focus.styler", function() {
            t.addClass("focused")
          }).on("blur.styler", function() {
            t.removeClass("focused")
          }))
        };
        o(), O.on("refresh", function() {
          O.off(".styler").closest(".jq-number").before(O).remove(), o()
        })
      } else if (O.is("select")) {
        var r = function() {
          function A(t) {
            var n = t.prop("scrollHeight") - t.outerHeight(),
              i = null,
              o = null;
            t.off("mousewheel DOMMouseScroll").on("mousewheel DOMMouseScroll", function(e) {
              i = e.originalEvent.detail < 0 || 0 < e.originalEvent.wheelDelta ? 1 : -1, ((o = t.scrollTop()) >= n && i < 0 || o <= 0 && 0 < i) && (e.stopPropagation(), e.preventDefault())
            })
          }

          function j() {
            for (var e = 0; e < P.length; e++) {
              var t = P.eq(e),
                n = "",
                i = "",
                o = "",
                r = "",
                s = "",
                a = "",
                l = "",
                c = "",
                u = "";
              t.prop("selected") && (i = "selected sel"), t.is(":disabled") && (i = "disabled"), t.is(":selected:disabled") && (i = "selected sel disabled"), void 0 !== t.attr("id") && "" !== t.attr("id") && (r = ' id="' + t.attr("id") + M.idSuffix + '"'), void 0 !== t.attr("title") && "" !== P.attr("title") && (s = ' title="' + t.attr("title") + '"'), void 0 !== t.attr("class") && (l = " " + t.attr("class"), u = ' data-jqfs-class="' + t.attr("class") + '"');
              var d = t.data();
              for (var f in d) "" !== d[f] && (a += " data-" + f + '="' + d[f] + '"');
              i + l !== "" && (o = ' class="' + i + l + '"'), n = "<li" + u + a + o + s + r + ">" + t.html() + "</li>", t.parent().is("optgroup") && (void 0 !== t.parent().attr("class") && (c = " " + t.parent().attr("class")), n = "<li" + u + a + ' class="' + i + l + " option" + c + '"' + s + r + ">" + t.html() + "</li>", t.is(":first-child") && (n = '<li class="optgroup' + c + '">' + t.parent().attr("label") + "</li>" + n)), E += n
            }
          }
          var P = H("option", O),
            E = "";
          if (O.is("[multiple]")) {
            if (e || I) return;
            ! function() {
              var e = new D,
                t = H('<div class="jq-select-multiple jqselect"></div>').attr({
                  id: e.id,
                  title: e.title
                }).addClass(e.classes).data(e.data);
              O.after(t), j(), t.append("<ul>" + E + "</ul>");
              var n = H("ul", t),
                o = H("li", t),
                i = O.attr("size"),
                r = n.outerHeight(),
                s = o.outerHeight();
              void 0 !== i && 0 < i ? n.css({
                height: s * i
              }) : n.css({
                height: 4 * s
              }), r > t.height() && (n.css("overflowY", "scroll"), A(n), o.filter(".selected").length && n.scrollTop(n.scrollTop() + o.filter(".selected").position().top)), O.prependTo(t), O.is(":disabled") ? (t.addClass("disabled"), P.each(function() {
                H(this).is(":selected") && o.eq(H(this).index()).addClass("selected")
              })) : (o.filter(":not(.disabled):not(.optgroup)").click(function(e) {
                O.focus();
                var t = H(this);
                if (e.ctrlKey || e.metaKey || t.addClass("selected"), e.shiftKey || t.addClass("first"), e.ctrlKey || e.metaKey || e.shiftKey || t.siblings().removeClass("selected first"), (e.ctrlKey || e.metaKey) && (t.is(".selected") ? t.removeClass("selected first") : t.addClass("selected first"), t.siblings().removeClass("first")), e.shiftKey) {
                  var n = !1,
                    i = !1;
                  t.siblings().removeClass("selected").siblings(".first").addClass("selected"), t.prevAll().each(function() {
                    H(this).is(".first") && (n = !0)
                  }), t.nextAll().each(function() {
                    H(this).is(".first") && (i = !0)
                  }), n && t.prevAll().each(function() {
                    if (H(this).is(".selected")) return !1;
                    H(this).not(".disabled, .optgroup").addClass("selected")
                  }), i && t.nextAll().each(function() {
                    if (H(this).is(".selected")) return !1;
                    H(this).not(".disabled, .optgroup").addClass("selected")
                  }), 1 == o.filter(".selected").length && t.addClass("first")
                }
                P.prop("selected", !1), o.filter(".selected").each(function() {
                  var e = H(this),
                    t = e.index();
                  e.is(".option") && (t -= e.prevAll(".optgroup").length), P.eq(t).prop("selected", !0)
                }), O.change()
              }), P.each(function(e) {
                H(this).data("optionIndex", e)
              }), O.on("change.styler", function() {
                o.removeClass("selected");
                var t = [];
                P.filter(":selected").each(function() {
                  t.push(H(this).data("optionIndex"))
                }), o.not(".optgroup").filter(function(e) {
                  return -1 < H.inArray(e, t)
                }).addClass("selected")
              }).on("focus.styler", function() {
                t.addClass("focused")
              }).on("blur.styler", function() {
                t.removeClass("focused")
              }), r > t.height() && O.on("keydown.styler", function(e) {
                38 != e.which && 37 != e.which && 33 != e.which || n.scrollTop(n.scrollTop() + o.filter(".selected").position().top - s), 40 != e.which && 39 != e.which && 34 != e.which || n.scrollTop(n.scrollTop() + o.filter(".selected:last").position().top - n.innerHeight() + 2 * s)
              }))
            }()
          } else ! function() {
            var e = new D,
              t = "",
              n = O.data("placeholder"),
              i = O.data("search"),
              o = O.data("search-limit"),
              r = O.data("search-not-found"),
              s = O.data("search-placeholder"),
              l = O.data("smart-positioning");
            void 0 === n && (n = M.selectPlaceholder), void 0 !== i && "" !== i || (i = M.selectSearch), void 0 !== o && "" !== o || (o = M.selectSearchLimit), void 0 !== r && "" !== r || (r = M.selectSearchNotFound), void 0 === s && (s = M.selectSearchPlaceholder), void 0 !== l && "" !== l || (l = M.selectSmartPositioning);
            var c = H('<div class="jq-selectbox jqselect"><div class="jq-selectbox__select"><div class="jq-selectbox__select-text"></div><div class="jq-selectbox__trigger"><div class="jq-selectbox__trigger-arrow"></div></div></div></div>').attr({
              id: e.id,
              title: e.title
            }).addClass(e.classes).data(e.data);
            O.after(c).prependTo(c);
            var u = c.css("z-index");
            u = 0 < u ? u : 1;
            var a = H("div.jq-selectbox__select", c),
              d = H("div.jq-selectbox__select-text", c),
              f = P.filter(":selected");
            j(), i && (t = '<div class="jq-selectbox__search"><input type="search" autocomplete="off" placeholder="' + s + '"></div><div class="jq-selectbox__not-found">' + r + "</div>");
            var h = H('<div class="jq-selectbox__dropdown">' + t + "<ul>" + E + "</ul></div>");
            c.append(h);
            var p = H("ul", h),
              g = H("li", h),
              m = H("input", h),
              v = H("div.jq-selectbox__not-found", h).hide();
            g.length < o && m.parent().hide(), "" === P.first().text() && P.first().is(":selected") && !1 !== n ? d.text(n).addClass("placeholder") : d.text(f.text());
            var y = 0,
              b = 0;
            if (g.css({
                display: "inline-block"
              }), g.each(function() {
                var e = H(this);
                e.innerWidth() > y && (y = e.innerWidth(), b = e.width())
              }), g.css({
                display: ""
              }), d.is(".placeholder") && d.width() > y) d.width(d.width());
            else {
              var x = c.clone().appendTo("body").width("auto"),
                w = x.outerWidth();
              x.remove(), w == c.outerWidth() && d.width(b)
            }
            y > c.width() && h.width(y), "" === P.first().text() && "" !== O.data("placeholder") && g.first().hide();
            var S = c.outerHeight(!0),
              k = m.parent().outerHeight(!0) || 0,
              C = p.css("max-height"),
              _ = g.filter(".selected");
            if (_.length < 1 && g.first().addClass("selected sel"), void 0 === g.data("li-height")) {
              var T = g.outerHeight();
              !1 !== n && (T = g.eq(1).outerHeight()), g.data("li-height", T)
            }
            var $ = h.css("top");
            if ("auto" == h.css("left") && h.css({
                left: 0
              }), "auto" == h.css("top") && (h.css({
                top: S
              }), $ = S), h.hide(), _.length && (P.first().text() != f.text() && c.addClass("changed"), c.data("jqfs-class", _.data("jqfs-class")), c.addClass(_.data("jqfs-class"))), O.is(":disabled")) return c.addClass("disabled");
            a.click(function() {
              if (H("div.jq-selectbox").filter(".opened").length && M.onSelectClosed.call(H("div.jq-selectbox").filter(".opened")), O.focus(), !I) {
                var t = H(window),
                  n = g.data("li-height"),
                  i = c.offset().top,
                  o = t.height() - S - (i - t.scrollTop()),
                  e = O.data("visible-options");
                void 0 !== e && "" !== e || (e = M.selectVisibleOptions);
                var r = 5 * n,
                  s = n * e;
                0 < e && e < 6 && (r = s), 0 === e && (s = "auto");
                var a = function() {
                  h.height("auto").css({
                    bottom: "auto",
                    top: $
                  });
                  var e = function() {
                    p.css("max-height", Math.floor((o - 20 - k) / n) * n)
                  };
                  e(), p.css("max-height", s), "none" != C && p.css("max-height", C), o < h.outerHeight() + 20 && e()
                };
                !0 === l || 1 === l ? r + k + 20 < o ? (a(), c.removeClass("dropup").addClass("dropdown")) : (function() {
                  h.height("auto").css({
                    top: "auto",
                    bottom: $
                  });
                  var e = function() {
                    p.css("max-height", Math.floor((i - t.scrollTop() - 20 - k) / n) * n)
                  };
                  e(), p.css("max-height", s), "none" != C && p.css("max-height", C), i - t.scrollTop() - 20 < h.outerHeight() + 20 && e()
                }(), c.removeClass("dropdown").addClass("dropup")) : !1 === l || 0 === l ? r + k + 20 < o && (a(), c.removeClass("dropup").addClass("dropdown")) : (h.height("auto").css({
                  bottom: "auto",
                  top: $
                }), p.css("max-height", s), "none" != C && p.css("max-height", C)), c.offset().left + h.outerWidth() > t.width() && h.css({
                  left: "auto",
                  right: 0
                }), H("div.jqselect").css({
                  zIndex: u - 1
                }).removeClass("opened"), c.css({
                  zIndex: u
                }), h.is(":hidden") ? (H("div.jq-selectbox__dropdown:visible").hide(), h.show(), c.addClass("opened focused"), M.onSelectOpened.call(c)) : (h.hide(), c.removeClass("opened dropup dropdown"), H("div.jq-selectbox").filter(".opened").length && M.onSelectClosed.call(c)), m.length && (m.val("").keyup(), v.hide(), m.keyup(function() {
                  var e = H(this).val();
                  g.each(function() {
                    H(this).html().match(new RegExp(".*?" + e + ".*?", "i")) ? H(this).show() : H(this).hide()
                  }), "" === P.first().text() && "" !== O.data("placeholder") && g.first().hide(), g.filter(":visible").length < 1 ? v.show() : v.hide()
                })), g.filter(".selected").length && ("" === O.val() ? p.scrollTop(0) : (p.innerHeight() / n % 2 != 0 && (n /= 2), p.scrollTop(p.scrollTop() + g.filter(".selected").position().top - p.innerHeight() / 2 + n))), A(p)
              }
            }), g.hover(function() {
              H(this).siblings().removeClass("selected")
            });
            g.filter(".selected").text();
            g.filter(":not(.disabled):not(.optgroup)").click(function() {
              O.focus();
              var e = H(this),
                t = e.text();
              if (!e.is(".selected")) {
                var n = e.index();
                n -= e.prevAll(".optgroup").length, e.addClass("selected sel").siblings().removeClass("selected sel"), P.prop("selected", !1).eq(n).prop("selected", !0), t, d.text(t), c.data("jqfs-class") && c.removeClass(c.data("jqfs-class")), c.data("jqfs-class", e.data("jqfs-class")), c.addClass(e.data("jqfs-class")), O.change()
              }
              h.hide(), c.removeClass("opened dropup dropdown"), M.onSelectClosed.call(c)
            }), h.mouseout(function() {
              H("li.sel", h).addClass("selected")
            }), O.on("change.styler", function() {
              d.text(P.filter(":selected").text()).removeClass("placeholder"), g.removeClass("selected sel").not(".optgroup").eq(O[0].selectedIndex).addClass("selected sel"), P.first().text() != g.filter(".selected").text() ? c.addClass("changed") : c.removeClass("changed")
            }).on("focus.styler", function() {
              c.addClass("focused"), H("div.jqselect").not(".focused").removeClass("opened dropup dropdown").find("div.jq-selectbox__dropdown").hide()
            }).on("blur.styler", function() {
              c.removeClass("focused")
            }).on("keydown.styler keyup.styler", function(e) {
              var t = g.data("li-height");
              "" === O.val() ? d.text(n).addClass("placeholder") : d.text(P.filter(":selected").text()), g.removeClass("selected sel").not(".optgroup").eq(O[0].selectedIndex).addClass("selected sel"), 38 != e.which && 37 != e.which && 33 != e.which && 36 != e.which || ("" === O.val() ? p.scrollTop(0) : p.scrollTop(p.scrollTop() + g.filter(".selected").position().top)), 40 != e.which && 39 != e.which && 34 != e.which && 35 != e.which || p.scrollTop(p.scrollTop() + g.filter(".selected").position().top - p.innerHeight() + t), 13 == e.which && (e.preventDefault(), h.hide(), c.removeClass("opened dropup dropdown"), M.onSelectClosed.call(c))
            }).on("keydown.styler", function(e) {
              32 == e.which && (e.preventDefault(), a.click())
            }), L.registered || (H(document).on("click", L), L.registered = !0)
          }()
        };
        r(), O.on("refresh", function() {
          O.off(".styler").parent().before(O).remove(), r()
        })
      } else O.is(":reset") && O.on("click", function() {
        setTimeout(function() {
          O.closest("form").find("input, select").trigger("refresh")
        }, 1)
      })
    },
    destroy: function() {
      var e = H(this.element);
      e.is(":checkbox") || e.is(":radio") ? (e.removeData("_" + r).off(".styler refresh").removeAttr("style").parent().before(e).remove(), e.closest("label").add('label[for="' + e.attr("id") + '"]').off(".styler")) : e.is('input[type="number"]') ? e.removeData("_" + r).off(".styler refresh").closest(".jq-number").before(e).remove() : (e.is(":file") || e.is("select")) && e.removeData("_" + r).off(".styler refresh").removeAttr("style").parent().before(e).remove()
    }
  }, H.fn[r] = function(t) {
    var n, i = arguments;
    return void 0 === t || "object" == typeof t ? (this.each(function() {
      H.data(this, "_" + r) || H.data(this, "_" + r, new o(this, t))
    }).promise().done(function() {
      var e = H(this[0]).data("_" + r);
      e && e.options.onFormStyled.call()
    }), this) : "string" == typeof t && "_" !== t[0] && "init" !== t ? (this.each(function() {
      var e = H.data(this, "_" + r);
      e instanceof o && "function" == typeof e[t] && (n = e[t].apply(e, Array.prototype.slice.call(i, 1)))
    }), void 0 !== n ? n : this) : void 0
  }, L.registered = !1
}),
function(e, t) {
  "object" == typeof exports && "undefined" != typeof module ? module.exports = t() : "function" == typeof define && define.amd ? define(t) : e.StickySidebar = t()
}(this, function() {
  "use strict";

  function e(e) {
    return e && e.__esModule && Object.prototype.hasOwnProperty.call(e, "default") ? e.default : e
  }

  function t(e, t) {
    return e(t = {
      exports: {}
    }, t.exports), t.exports
  }
  "undefined" != typeof window ? window : "undefined" != typeof global ? global : "undefined" != typeof self && self;
  var i = t(function(e, t) {
    ! function(e) {
      Object.defineProperty(e, "__esModule", {
        value: !0
      });
      var u, i, t = function() {
          function i(e, t) {
            for (var n = 0; n < t.length; n++) {
              var i = t[n];
              i.enumerable = i.enumerable || !1, i.configurable = !0, "value" in i && (i.writable = !0), Object.defineProperty(e, i.key, i)
            }
          }
          return function(e, t, n) {
            return t && i(e.prototype, t), n && i(e, n), e
          }
        }(),
        n = (i = {
          topSpacing: 0,
          bottomSpacing: 0,
          containerSelector: !(u = ".stickySidebar"),
          innerWrapperSelector: ".inner-wrapper-sticky",
          stickyClass: "is-affixed",
          resizeSensor: !0,
          minWidth: !1
        }, function() {
          function c(e) {
            var t = this,
              n = 1 < arguments.length && void 0 !== arguments[1] ? arguments[1] : {};
            if (function(e, t) {
                if (!(e instanceof c)) throw new TypeError("Cannot call a class as a function")
              }(this), this.options = c.extend(i, n), this.sidebar = "string" == typeof e ? document.querySelector(e) : e, void 0 === this.sidebar) throw new Error("There is no specific sidebar element.");
            this.sidebarInner = !1, this.container = this.sidebar.parentElement, this.affixedType = "STATIC", this.direction = "down", this.support = {
              transform: !1,
              transform3d: !1
            }, this._initialized = !1, this._reStyle = !1, this._breakpoint = !1, this.dimensions = {
              translateY: 0,
              maxTranslateY: 0,
              topSpacing: 0,
              lastTopSpacing: 0,
              bottomSpacing: 0,
              lastBottomSpacing: 0,
              sidebarHeight: 0,
              sidebarWidth: 0,
              containerTop: 0,
              containerHeight: 0,
              viewportHeight: 0,
              viewportTop: 0,
              lastViewportTop: 0
            }, ["handleEvent"].forEach(function(e) {
              t[e] = t[e].bind(t)
            }), this.initialize()
          }
          return t(c, [{
            key: "initialize",
            value: function() {
              var n = this;
              if (this._setSupportFeatures(), this.options.innerWrapperSelector && (this.sidebarInner = this.sidebar.querySelector(this.options.innerWrapperSelector), null === this.sidebarInner && (this.sidebarInner = !1)), !this.sidebarInner) {
                var e = document.createElement("div");
                for (e.setAttribute("class", "inner-wrapper-sticky"), this.sidebar.appendChild(e); this.sidebar.firstChild != e;) e.appendChild(this.sidebar.firstChild);
                this.sidebarInner = this.sidebar.querySelector(".inner-wrapper-sticky")
              }
              if (this.options.containerSelector) {
                var t = document.querySelectorAll(this.options.containerSelector);
                if ((t = Array.prototype.slice.call(t)).forEach(function(e, t) {
                    e.contains(n.sidebar) && (n.container = e)
                  }), !t.length) throw new Error("The container does not contains on the sidebar.")
              }
              "function" != typeof this.options.topSpacing && (this.options.topSpacing = parseInt(this.options.topSpacing) || 0), "function" != typeof this.options.bottomSpacing && (this.options.bottomSpacing = parseInt(this.options.bottomSpacing) || 0), this._widthBreakpoint(), this.calcDimensions(), this.stickyPosition(), this.bindEvents(), this._initialized = !0
            }
          }, {
            key: "bindEvents",
            value: function() {
              window.addEventListener("resize", this, {
                passive: !0,
                capture: !1
              }), window.addEventListener("scroll", this, {
                passive: !0,
                capture: !1
              }), this.sidebar.addEventListener("update" + u, this), this.options.resizeSensor && "undefined" != typeof ResizeSensor && (new ResizeSensor(this.sidebarInner, this.handleEvent), new ResizeSensor(this.container, this.handleEvent))
            }
          }, {
            key: "handleEvent",
            value: function(e) {
              this.updateSticky(e)
            }
          }, {
            key: "calcDimensions",
            value: function() {
              if (!this._breakpoint) {
                var e = this.dimensions;
                e.containerTop = c.offsetRelative(this.container).top, e.containerHeight = this.container.clientHeight, e.containerBottom = e.containerTop + e.containerHeight, e.sidebarHeight = this.sidebarInner.offsetHeight, e.sidebarWidth = this.sidebarInner.offsetWidth, e.viewportHeight = window.innerHeight, e.maxTranslateY = e.containerHeight - e.sidebarHeight, this._calcDimensionsWithScroll()
              }
            }
          }, {
            key: "_calcDimensionsWithScroll",
            value: function() {
              var e = this.dimensions;
              e.sidebarLeft = c.offsetRelative(this.sidebar).left, e.viewportTop = document.documentElement.scrollTop || document.body.scrollTop, e.viewportBottom = e.viewportTop + e.viewportHeight, e.viewportLeft = document.documentElement.scrollLeft || document.body.scrollLeft, e.topSpacing = this.options.topSpacing, e.bottomSpacing = this.options.bottomSpacing, "function" == typeof e.topSpacing && (e.topSpacing = parseInt(e.topSpacing(this.sidebar)) || 0), "function" == typeof e.bottomSpacing && (e.bottomSpacing = parseInt(e.bottomSpacing(this.sidebar)) || 0), "VIEWPORT-TOP" === this.affixedType ? e.topSpacing < e.lastTopSpacing && (e.translateY += e.lastTopSpacing - e.topSpacing, this._reStyle = !0) : "VIEWPORT-BOTTOM" === this.affixedType && e.bottomSpacing < e.lastBottomSpacing && (e.translateY += e.lastBottomSpacing - e.bottomSpacing, this._reStyle = !0), e.lastTopSpacing = e.topSpacing, e.lastBottomSpacing = e.bottomSpacing
            }
          }, {
            key: "isSidebarFitsViewport",
            value: function() {
              var e = this.dimensions,
                t = "down" === this.scrollDirection ? e.lastBottomSpacing : e.lastTopSpacing;
              return this.dimensions.sidebarHeight + t < this.dimensions.viewportHeight
            }
          }, {
            key: "observeScrollDir",
            value: function() {
              var e = this.dimensions;
              if (e.lastViewportTop !== e.viewportTop) {
                var t = "down" === this.direction ? Math.min : Math.max;
                e.viewportTop === t(e.viewportTop, e.lastViewportTop) && (this.direction = "down" === this.direction ? "up" : "down")
              }
            }
          }, {
            key: "getAffixType",
            value: function() {
              this._calcDimensionsWithScroll();
              var e = this.dimensions,
                t = e.viewportTop + e.topSpacing,
                n = this.affixedType;
              return t <= e.containerTop || e.containerHeight <= e.sidebarHeight ? (e.translateY = 0, n = "STATIC") : n = "up" === this.direction ? this._getAffixTypeScrollingUp() : this._getAffixTypeScrollingDown(), e.translateY = Math.max(0, e.translateY), e.translateY = Math.min(e.containerHeight, e.translateY), e.translateY = Math.round(e.translateY), e.lastViewportTop = e.viewportTop, n
            }
          }, {
            key: "_getAffixTypeScrollingDown",
            value: function() {
              var e = this.dimensions,
                t = e.sidebarHeight + e.containerTop,
                n = e.viewportTop + e.topSpacing,
                i = e.viewportBottom - e.bottomSpacing,
                o = this.affixedType;
              return this.isSidebarFitsViewport() ? e.sidebarHeight + n >= e.containerBottom ? (e.translateY = e.containerBottom - t, o = "CONTAINER-BOTTOM") : n >= e.containerTop && (e.translateY = n - e.containerTop, o = "VIEWPORT-TOP") : e.containerBottom <= i ? (e.translateY = e.containerBottom - t, o = "CONTAINER-BOTTOM") : t + e.translateY <= i ? (e.translateY = i - t, o = "VIEWPORT-BOTTOM") : e.containerTop + e.translateY <= n && 0 !== e.translateY && e.maxTranslateY !== e.translateY && (o = "VIEWPORT-UNBOTTOM"), o
            }
          }, {
            key: "_getAffixTypeScrollingUp",
            value: function() {
              var e = this.dimensions,
                t = e.sidebarHeight + e.containerTop,
                n = e.viewportTop + e.topSpacing,
                i = e.viewportBottom - e.bottomSpacing,
                o = this.affixedType;
              return n <= e.translateY + e.containerTop ? (e.translateY = n - e.containerTop, o = "VIEWPORT-TOP") : e.containerBottom <= i ? (e.translateY = e.containerBottom - t, o = "CONTAINER-BOTTOM") : this.isSidebarFitsViewport() || e.containerTop <= n && 0 !== e.translateY && e.maxTranslateY !== e.translateY && (o = "VIEWPORT-UNBOTTOM"), o
            }
          }, {
            key: "_getStyle",
            value: function(e) {
              if (void 0 !== e) {
                var t = {
                    inner: {},
                    outer: {}
                  },
                  n = this.dimensions;
                switch (e) {
                  case "VIEWPORT-TOP":
                    t.inner = {
                      position: "fixed",
                      top: n.topSpacing,
                      left: n.sidebarLeft - n.viewportLeft,
                      width: n.sidebarWidth
                    };
                    break;
                  case "VIEWPORT-BOTTOM":
                    t.inner = {
                      position: "fixed",
                      top: "auto",
                      left: n.sidebarLeft,
                      bottom: n.bottomSpacing,
                      width: n.sidebarWidth
                    };
                    break;
                  case "CONTAINER-BOTTOM":
                  case "VIEWPORT-UNBOTTOM":
                    var i = this._getTranslate(0, n.translateY + "px");
                    t.inner = i ? {
                      transform: i
                    } : {
                      position: "absolute",
                      top: n.translateY,
                      width: n.sidebarWidth
                    }
                }
                switch (e) {
                  case "VIEWPORT-TOP":
                  case "VIEWPORT-BOTTOM":
                  case "VIEWPORT-UNBOTTOM":
                  case "CONTAINER-BOTTOM":
                    t.outer = {
                      height: n.sidebarHeight,
                      position: "relative"
                    }
                }
                return t.outer = c.extend({
                  height: "",
                  position: ""
                }, t.outer), t.inner = c.extend({
                  position: "relative",
                  top: "",
                  left: "",
                  bottom: "",
                  width: "",
                  transform: ""
                }, t.inner), t
              }
            }
          }, {
            key: "stickyPosition",
            value: function(e) {
              if (!this._breakpoint) {
                e = this._reStyle || e || !1, this.options.topSpacing, this.options.bottomSpacing;
                var t = this.getAffixType(),
                  n = this._getStyle(t);
                if ((this.affixedType != t || e) && t) {
                  var i = "affix." + t.toLowerCase().replace("viewport-", "") + u;
                  for (var o in c.eventTrigger(this.sidebar, i), "STATIC" === t ? c.removeClass(this.sidebar, this.options.stickyClass) : c.addClass(this.sidebar, this.options.stickyClass), n.outer) {
                    var r = "number" == typeof n.outer[o] ? "px" : "";
                    this.sidebar.style[o] = n.outer[o] + r
                  }
                  for (var s in n.inner) {
                    var a = "number" == typeof n.inner[s] ? "px" : "";
                    this.sidebarInner.style[s] = n.inner[s] + a
                  }
                  var l = "affixed." + t.toLowerCase().replace("viewport-", "") + u;
                  c.eventTrigger(this.sidebar, l)
                } else this._initialized && (this.sidebarInner.style.left = n.inner.left);
                this.affixedType = t
              }
            }
          }, {
            key: "_widthBreakpoint",
            value: function() {
              window.innerWidth <= this.options.minWidth ? (this._breakpoint = !0, this.affixedType = "STATIC", this.sidebar.removeAttribute("style"), c.removeClass(this.sidebar, this.options.stickyClass), this.sidebarInner.removeAttribute("style")) : this._breakpoint = !1
            }
          }, {
            key: "updateSticky",
            value: function() {
              var e, t = this,
                n = 0 < arguments.length && void 0 !== arguments[0] ? arguments[0] : {};
              this._running || (this._running = !0, e = n.type, requestAnimationFrame(function() {
                switch (e) {
                  case "scroll":
                    t._calcDimensionsWithScroll(), t.observeScrollDir(), t.stickyPosition();
                    break;
                  case "resize":
                  default:
                    t._widthBreakpoint(), t.calcDimensions(), t.stickyPosition(!0)
                }
                t._running = !1
              }))
            }
          }, {
            key: "_setSupportFeatures",
            value: function() {
              var e = this.support;
              e.transform = c.supportTransform(), e.transform3d = c.supportTransform(!0)
            }
          }, {
            key: "_getTranslate",
            value: function() {
              var e = 0 < arguments.length && void 0 !== arguments[0] ? arguments[0] : 0,
                t = 1 < arguments.length && void 0 !== arguments[1] ? arguments[1] : 0,
                n = 2 < arguments.length && void 0 !== arguments[2] ? arguments[2] : 0;
              return this.support.transform3d ? "translate3d(" + e + ", " + t + ", " + n + ")" : !!this.support.translate && "translate(" + e + ", " + t + ")"
            }
          }, {
            key: "destroy",
            value: function() {
              window.removeEventListener("resize", this, {
                capture: !1
              }), window.removeEventListener("scroll", this, {
                capture: !1
              }), this.sidebar.classList.remove(this.options.stickyClass), this.sidebar.style.minHeight = "", this.sidebar.removeEventListener("update" + u, this);
              var e = {
                inner: {},
                outer: {}
              };
              for (var t in e.inner = {
                  position: "",
                  top: "",
                  left: "",
                  bottom: "",
                  width: "",
                  transform: ""
                }, e.outer = {
                  height: "",
                  position: ""
                }, e.outer) this.sidebar.style[t] = e.outer[t];
              for (var n in e.inner) this.sidebarInner.style[n] = e.inner[n];
              this.options.resizeSensor && "undefined" != typeof ResizeSensor && (ResizeSensor.detach(this.sidebarInner, this.handleEvent), ResizeSensor.detach(this.container, this.handleEvent))
            }
          }], [{
            key: "supportTransform",
            value: function(e) {
              var n = !1,
                t = e ? "perspective" : "transform",
                i = t.charAt(0).toUpperCase() + t.slice(1),
                o = document.createElement("support").style;
              return (t + " " + ["Webkit", "Moz", "O", "ms"].join(i + " ") + i).split(" ").forEach(function(e, t) {
                if (void 0 !== o[e]) return n = e, !1
              }), n
            }
          }, {
            key: "eventTrigger",
            value: function(e, t, n) {
              try {
                var i = new CustomEvent(t, {
                  detail: n
                })
              } catch (e) {
                (i = document.createEvent("CustomEvent")).initCustomEvent(t, !0, !0, n)
              }
              e.dispatchEvent(i)
            }
          }, {
            key: "extend",
            value: function(e, t) {
              var n = {};
              for (var i in e) void 0 !== t[i] ? n[i] = t[i] : n[i] = e[i];
              return n
            }
          }, {
            key: "offsetRelative",
            value: function(e) {
              var t = {
                left: 0,
                top: 0
              };
              do {
                var n = e.offsetTop,
                  i = e.offsetLeft;
                isNaN(n) || (t.top += n), isNaN(i) || (t.left += i), e = "BODY" === e.tagName ? e.parentElement : e.offsetParent
              } while (e);
              return t
            }
          }, {
            key: "addClass",
            value: function(e, t) {
              c.hasClass(e, t) || (e.classList ? e.classList.add(t) : e.className += " " + t)
            }
          }, {
            key: "removeClass",
            value: function(e, t) {
              c.hasClass(e, t) && (e.classList ? e.classList.remove(t) : e.className = e.className.replace(new RegExp("(^|\\b)" + t.split(" ").join("|") + "(\\b|$)", "gi"), " "))
            }
          }, {
            key: "hasClass",
            value: function(e, t) {
              return e.classList ? e.classList.contains(t) : new RegExp("(^| )" + t + "( |$)", "gi").test(e.className)
            }
          }, {
            key: "defaults",
            get: function() {
              return i
            }
          }]), c
        }());
      e.default = n, window.StickySidebar = n
    }(t)
  });
  return e(i), e(t(function(e, t) {
    var n, r;
    r = (n = i) && n.__esModule ? n : {
        default: n
      },
      function() {
        if ("undefined" != typeof window) {
          var i = window.$ || window.jQuery || window.Zepto,
            o = "stickySidebar";
          if (i) {
            i.fn.stickySidebar = function(n) {
              return this.each(function() {
                var e = i(this),
                  t = i(this).data(o);
                if (t || (t = new r.default(this, "object" == typeof n && n), e.data(o, t)), "string" == typeof n) {
                  if (void 0 === t[n] && -1 === ["destroy", "updateSticky"].indexOf(n)) throw new Error('No method named "' + n + '"');
                  t[n]()
                }
              })
            }, i.fn.stickySidebar.Constructor = r.default;
            var e = i.fn.stickySidebar;
            i.fn.stickySidebar.noConflict = function() {
              return i.fn.stickySidebar = e, this
            }
          }
        }
      }()
  }))
}),
function(e, t) {
  "function" == typeof define && define.amd ? define(t) : "object" == typeof exports ? module.exports = t() : e.ResizeSensor = t()
}("undefined" != typeof window ? window : this, function() {
  if ("undefined" == typeof window) return null;
  var w = window.requestAnimationFrame || window.mozRequestAnimationFrame || window.webkitRequestAnimationFrame || function(e) {
    return window.setTimeout(e, 20)
  };

  function o(e, t) {
    var n = Object.prototype.toString.call(e),
      i = "[object Array]" === n || "[object NodeList]" === n || "[object HTMLCollection]" === n || "[object Object]" === n || "undefined" != typeof jQuery && e instanceof jQuery || "undefined" != typeof Elements && e instanceof Elements,
      o = 0,
      r = e.length;
    if (i)
      for (; o < r; o++) t(e[o]);
    else t(e)
  }

  function S(e) {
    if (!e.getBoundingClientRect) return {
      width: e.offsetWidth,
      height: e.offsetHeight
    };
    var t = e.getBoundingClientRect();
    return {
      width: Math.round(t.width),
      height: Math.round(t.height)
    }
  }
  var r = function(t, n) {
    var i;

    function x() {
      var n, i, o = [];
      this.add = function(e) {
        o.push(e)
      }, this.call = function(e) {
        for (n = 0, i = o.length; n < i; n++) o[n].call(this, e)
      }, this.remove = function(e) {
        var t = [];
        for (n = 0, i = o.length; n < i; n++) o[n] !== e && t.push(o[n]);
        o = t
      }, this.length = function() {
        return o.length
      }
    }
    "undefined" != typeof ResizeObserver ? (i = new ResizeObserver(function(e) {
      o(e, function(e) {
        n.call(this, {
          width: e.contentRect.width,
          height: e.contentRect.height
        })
      })
    }), void 0 !== t && o(t, function(e) {
      i.observe(e)
    })) : o(t, function(e) {
      ! function(e, t) {
        if (e)
          if (e.resizedAttached) e.resizedAttached.add(t);
          else {
            e.resizedAttached = new x, e.resizedAttached.add(t), e.resizeSensor = document.createElement("div"), e.resizeSensor.dir = "ltr", e.resizeSensor.className = "resize-sensor";
            var n = "position: absolute; left: -10px; top: -10px; right: 0; bottom: 0; overflow: hidden; z-index: -1; visibility: hidden;",
              i = "position: absolute; left: 0; top: 0; transition: 0s;";
            e.resizeSensor.style.cssText = n, e.resizeSensor.innerHTML = '<div class="resize-sensor-expand" style="' + n + '"><div style="' + i + '"></div></div><div class="resize-sensor-shrink" style="' + n + '"><div style="' + i + ' width: 200%; height: 200%"></div></div>', e.appendChild(e.resizeSensor);
            var o = window.getComputedStyle(e).getPropertyValue("position");
            "absolute" !== o && "relative" !== o && "fixed" !== o && (e.style.position = "relative");
            var r, s, a, l = e.resizeSensor.childNodes[0],
              c = l.childNodes[0],
              u = e.resizeSensor.childNodes[1],
              d = S(e),
              f = d.width,
              h = d.height,
              p = !0,
              g = function() {
                c.style.width = "100000px", c.style.height = "100000px", l.scrollLeft = 1e5, l.scrollTop = 1e5, u.scrollLeft = 1e5, u.scrollTop = 1e5
              },
              m = function() {
                if (p) {
                  if (!l.scrollTop && !l.scrollLeft) return g(), void(a || (a = w(function() {
                    a = 0, m()
                  })));
                  p = !1
                }
                g()
              };
            e.resizeSensor.resetSensor = m;
            var v = function() {
                s = 0, r && (f = d.width, h = d.height, e.resizedAttached && e.resizedAttached.call(d))
              },
              y = function() {
                d = S(e), (r = d.width !== f || d.height !== h) && !s && (s = w(v)), m()
              },
              b = function(e, t, n) {
                e.attachEvent ? e.attachEvent("on" + t, n) : e.addEventListener(t, n)
              };
            b(l, "scroll", y), b(u, "scroll", y), w(m)
          }
      }(e, n)
    }), this.detach = function(e) {
      "undefined" != typeof ResizeObserver ? o(t, function(e) {
        i.unobserve(e)
      }) : r.detach(t, e)
    }, this.reset = function() {
      t.resizeSensor.resetSensor()
    }
  };
  return r.reset = function(e, t) {
    o(e, function(e) {
      e.resizeSensor.resetSensor()
    })
  }, r.detach = function(e, t) {
    o(e, function(e) {
      e && (e.resizedAttached && "function" == typeof t && (e.resizedAttached.remove(t), e.resizedAttached.length()) || e.resizeSensor && (e.contains(e.resizeSensor) && e.removeChild(e.resizeSensor), delete e.resizeSensor, delete e.resizedAttached))
    })
  }, r
}), $(document).ready(function() {
    new ScrollFlow
  }), $.fn.ScrollFlow = function(e) {
    new ScrollFlow(e)
  }, ScrollFlow = function(e) {
    this.init(e)
  }, $.extend(ScrollFlow.prototype, {
    init: function(e) {
      this.options = $.extend({
        useMobileTimeouts: !0,
        mobileTimeout: 100,
        durationOnLoad: 0,
        durationOnResize: 250,
        durationOnScroll: 500
      }, e), this.lastScrollTop = 0, this.bindScroll(), this.bindResize(), this.update(this.options.durationOnLoad)
    },
    bindScroll: function() {
      var e = this;
      $(window).scroll(function() {
        e.update()
      }), $(window).bind("gesturechange", function() {
        e.update()
      })
    },
    bindResize: function() {
      var e = this;
      $(window).resize(function() {
        e.update(e.options.durationOnResize)
      })
    },
    update: function(n) {
      var i = this;
      winHeight = $(window).height(), scrollTop = $(window).scrollTop(), $(".scrollflow").each(function(e, t) {
        objOffset = $(t).offset(), objOffsetTop = parseInt(objOffset.top), effectDuration = i.options.durationOnScroll, effectDuration = void 0 !== n ? n : effectDuration, effectiveFromPercentage = isNaN(parseInt($(t).attr("data-scrollflow-start") / 100)) ? -.25 : parseInt($(t).attr("data-scrollflow-start")) / 100, scrollDistancePercentage = isNaN(parseInt($(t).attr("data-scrollflow-distance") / 100)) ? .5 : parseInt($(t).attr("data-scrollflow-distance")) / 100, effectiveFrom = objOffsetTop - winHeight * (1 - effectiveFromPercentage), effectiveTo = objOffsetTop - winHeight * (1 - scrollDistancePercentage), parallaxScale = .8, parallaxOpacity = 0, parallaxOffset = -100, factor = 0, scrollTop > effectiveFrom && (factor = (scrollTop - effectiveFrom) / (effectiveTo - effectiveFrom), factor = 1 < factor ? 1 : factor), options = {
          opacity: 1,
          scale: 1,
          translateX: 0,
          translateY: 0
        }, $(t).hasClass("-opacity") && (options.opacity = 0 + factor), $(t).hasClass("-pop") && (options.scale = .8 + .2 * factor), $(t).hasClass("-slide-left") && (options.translateX = -1 * (100 * factor - 100)), $(t).hasClass("-slide-right") && (options.translateX = 100 * factor - 100), $(t).hasClass("-slide-top") && (options.translateY = -1 * (100 * factor - 100)), $(t).hasClass("-slide-bottom") && (options.translateY = 100 * factor - 100), $(t).css({
          webkitFilter: "opacity(" + options.opacity + ")",
          mozFilter: "opacity(" + options.opacity + ")",
          oFilter: "opacity(" + options.opacity + ")",
          msFilter: "opacity(" + options.opacity + ")",
          filter: "opacity(" + options.opacity + ")",
          webkitTransform: "translate3d( " + parseInt(options.translateX) + "px, " + parseInt(options.translateY) + "px, 0px ) scale(" + options.scale + ")",
          mozTransform: "translate3d( " + parseInt(options.translateX) + "px, " + parseInt(options.translateY) + "px, 0px ) scale(" + options.scale + ")",
          oTransform: "translate3d( " + parseInt(options.translateX) + "px, " + parseInt(options.translateY) + "px, 0px ) scale(" + options.scale + ")",
          msTransform: "translate3d( " + parseInt(options.translateX) + "px, " + parseInt(options.translateY) + "px, 0px ) scale(" + options.scale + ")",
          transform: "translate3d( " + parseInt(options.translateX) + "px, " + parseInt(options.translateY) + "px, 0px ) scale(" + options.scale + ")",
          transition: "all " + effectDuration + "ms ease-out"
        })
      })
    }
  }),
  function(e, t) {
    "object" == typeof exports && "object" == typeof module ? module.exports = t() : "function" == typeof define && define.amd ? define([], t) : "object" == typeof exports ? exports.axios = t() : e.axios = t()
  }(this, function() {
    return function(n) {
      var i = {};

      function o(e) {
        if (i[e]) return i[e].exports;
        var t = i[e] = {
          exports: {},
          id: e,
          loaded: !1
        };
        return n[e].call(t.exports, t, t.exports, o), t.loaded = !0, t.exports
      }
      return o.m = n, o.c = i, o.p = "", o(0)
    }([function(e, t, n) {
      e.exports = n(1)
    }, function(e, t, n) {
      "use strict";
      var i = n(2),
        o = n(3),
        r = n(4);

      function s(e) {
        var t = new r(e),
          n = o(r.prototype.request, t);
        return i.extend(n, r.prototype, t), i.extend(n, t), n
      }
      var a = s();
      a.Axios = r, a.create = function(e) {
        return s(e)
      }, a.Cancel = n(22), a.CancelToken = n(23), a.isCancel = n(19), a.all = function(e) {
        return Promise.all(e)
      }, a.spread = n(24), e.exports = a, e.exports.default = a
    }, function(e, t, n) {
      "use strict";
      var o = n(3),
        i = Object.prototype.toString;

      function r(e) {
        return "[object Array]" === i.call(e)
      }

      function s(e) {
        return null !== e && "object" == typeof e
      }

      function a(e) {
        return "[object Function]" === i.call(e)
      }

      function l(e, t) {
        if (null != e)
          if ("object" == typeof e || r(e) || (e = [e]), r(e))
            for (var n = 0, i = e.length; n < i; n++) t.call(null, e[n], n, e);
          else
            for (var o in e) Object.prototype.hasOwnProperty.call(e, o) && t.call(null, e[o], o, e)
      }
      e.exports = {
        isArray: r,
        isArrayBuffer: function(e) {
          return "[object ArrayBuffer]" === i.call(e)
        },
        isFormData: function(e) {
          return "undefined" != typeof FormData && e instanceof FormData
        },
        isArrayBufferView: function(e) {
          return "undefined" != typeof ArrayBuffer && ArrayBuffer.isView ? ArrayBuffer.isView(e) : e && e.buffer && e.buffer instanceof ArrayBuffer
        },
        isString: function(e) {
          return "string" == typeof e
        },
        isNumber: function(e) {
          return "number" == typeof e
        },
        isObject: s,
        isUndefined: function(e) {
          return void 0 === e
        },
        isDate: function(e) {
          return "[object Date]" === i.call(e)
        },
        isFile: function(e) {
          return "[object File]" === i.call(e)
        },
        isBlob: function(e) {
          return "[object Blob]" === i.call(e)
        },
        isFunction: a,
        isStream: function(e) {
          return s(e) && a(e.pipe)
        },
        isURLSearchParams: function(e) {
          return void 0 !== URLSearchParams && e instanceof URLSearchParams
        },
        isStandardBrowserEnv: function() {
          return "undefined" != typeof window && "undefined" != typeof document && "function" == typeof document.createElement
        },
        forEach: l,
        merge: function n() {
          var i = {};

          function e(e, t) {
            "object" == typeof i[t] && "object" == typeof e ? i[t] = n(i[t], e) : i[t] = e
          }
          for (var t = 0, o = arguments.length; t < o; t++) l(arguments[t], e);
          return i
        },
        extend: function(n, e, i) {
          return l(e, function(e, t) {
            n[t] = i && "function" == typeof e ? o(e, i) : e
          }), n
        },
        trim: function(e) {
          return e.replace(/^\s*/, "").replace(/\s*$/, "")
        }
      }
    }, function(e, t) {
      "use strict";
      e.exports = function(n, i) {
        return function() {
          for (var e = new Array(arguments.length), t = 0; t < e.length; t++) e[t] = arguments[t];
          return n.apply(i, e)
        }
      }
    }, function(e, t, n) {
      "use strict";
      var i = n(5),
        o = n(2),
        r = n(16),
        s = n(17),
        a = n(20),
        l = n(21);

      function c(e) {
        this.defaults = o.merge(i, e), this.interceptors = {
          request: new r,
          response: new r
        }
      }
      c.prototype.request = function(e) {
        "string" == typeof e && (e = o.merge({
          url: arguments[0]
        }, arguments[1])), (e = o.merge(i, this.defaults, {
          method: "get"
        }, e)).baseURL && !a(e.url) && (e.url = l(e.baseURL, e.url));
        var t = [s, void 0],
          n = Promise.resolve(e);
        for (this.interceptors.request.forEach(function(e) {
            t.unshift(e.fulfilled, e.rejected)
          }), this.interceptors.response.forEach(function(e) {
            t.push(e.fulfilled, e.rejected)
          }); t.length;) n = n.then(t.shift(), t.shift());
        return n
      }, o.forEach(["delete", "get", "head"], function(n) {
        c.prototype[n] = function(e, t) {
          return this.request(o.merge(t || {}, {
            method: n,
            url: e
          }))
        }
      }), o.forEach(["post", "put", "patch"], function(i) {
        c.prototype[i] = function(e, t, n) {
          return this.request(o.merge(n || {}, {
            method: i,
            url: e,
            data: t
          }))
        }
      }), e.exports = c
    }, function(e, t, n) {
      "use strict";
      var i, o = n(2),
        r = n(6),
        s = /^\)\]\}',?\n/,
        a = {
          "Content-Type": "application/x-www-form-urlencoded"
        };

      function l(e, t) {
        !o.isUndefined(e) && o.isUndefined(e["Content-Type"]) && (e["Content-Type"] = t)
      }
      e.exports = {
        adapter: ("undefined" != typeof XMLHttpRequest ? i = n(7) : "undefined" != typeof process && (i = n(7)), i),
        transformRequest: [function(e, t) {
          return r(t, "Content-Type"), o.isFormData(e) || o.isArrayBuffer(e) || o.isStream(e) || o.isFile(e) || o.isBlob(e) ? e : o.isArrayBufferView(e) ? e.buffer : o.isURLSearchParams(e) ? (l(t, "application/x-www-form-urlencoded;charset=utf-8"), e.toString()) : o.isObject(e) ? (l(t, "application/json;charset=utf-8"), JSON.stringify(e)) : e
        }],
        transformResponse: [function(e) {
          if ("string" == typeof e) {
            e = e.replace(s, "");
            try {
              e = JSON.parse(e)
            } catch (e) {}
          }
          return e
        }],
        headers: {
          common: {
            Accept: "application/json, text/plain, */*"
          },
          patch: o.merge(a),
          post: o.merge(a),
          put: o.merge(a)
        },
        timeout: 0,
        xsrfCookieName: "XSRF-TOKEN",
        xsrfHeaderName: "X-XSRF-TOKEN",
        maxContentLength: -1,
        validateStatus: function(e) {
          return 200 <= e && e < 300
        }
      }
    }, function(e, t, n) {
      "use strict";
      var o = n(2);
      e.exports = function(n, i) {
        o.forEach(n, function(e, t) {
          t !== i && t.toUpperCase() === i.toUpperCase() && (n[i] = e, delete n[t])
        })
      }
    }, function(e, t, f) {
      "use strict";
      var h = f(2),
        p = f(8),
        g = f(11),
        m = f(12),
        v = f(13),
        y = f(9),
        b = "undefined" != typeof window && window.btoa || f(14);
      e.exports = function(d) {
        return new Promise(function(n, i) {
          var o = d.data,
            r = d.headers;
          h.isFormData(o) && delete r["Content-Type"];
          var s = new XMLHttpRequest,
            e = "onreadystatechange",
            a = !1;
          if ("undefined" == typeof window || !window.XDomainRequest || "withCredentials" in s || v(d.url) || (s = new window.XDomainRequest, e = "onload", a = !0, s.onprogress = function() {}, s.ontimeout = function() {}), d.auth) {
            var t = d.auth.username || "",
              l = d.auth.password || "";
            r.Authorization = "Basic " + b(t + ":" + l)
          }
          if (s.open(d.method.toUpperCase(), g(d.url, d.params, d.paramsSerializer), !0), s.timeout = d.timeout, s[e] = function() {
              if (s && (4 === s.readyState || a) && (0 !== s.status || s.responseURL && 0 === s.responseURL.indexOf("file:"))) {
                var e = "getAllResponseHeaders" in s ? m(s.getAllResponseHeaders()) : null,
                  t = {
                    data: d.responseType && "text" !== d.responseType ? s.response : s.responseText,
                    status: 1223 === s.status ? 204 : s.status,
                    statusText: 1223 === s.status ? "No Content" : s.statusText,
                    headers: e,
                    config: d,
                    request: s
                  };
                p(n, i, t), s = null
              }
            }, s.onerror = function() {
              i(y("Network Error", d)), s = null
            }, s.ontimeout = function() {
              i(y("timeout of " + d.timeout + "ms exceeded", d, "ECONNABORTED")), s = null
            }, h.isStandardBrowserEnv()) {
            var c = f(15),
              u = (d.withCredentials || v(d.url)) && d.xsrfCookieName ? c.read(d.xsrfCookieName) : void 0;
            u && (r[d.xsrfHeaderName] = u)
          }
          if ("setRequestHeader" in s && h.forEach(r, function(e, t) {
              void 0 === o && "content-type" === t.toLowerCase() ? delete r[t] : s.setRequestHeader(t, e)
            }), d.withCredentials && (s.withCredentials = !0), d.responseType) try {
            s.responseType = d.responseType
          } catch (e) {
            if ("json" !== s.responseType) throw e
          }
          "function" == typeof d.onDownloadProgress && s.addEventListener("progress", d.onDownloadProgress), "function" == typeof d.onUploadProgress && s.upload && s.upload.addEventListener("progress", d.onUploadProgress), d.cancelToken && d.cancelToken.promise.then(function(e) {
            s && (s.abort(), i(e), s = null)
          }), void 0 === o && (o = null), s.send(o)
        })
      }
    }, function(e, t, n) {
      "use strict";
      var o = n(9);
      e.exports = function(e, t, n) {
        var i = n.config.validateStatus;
        n.status && i && !i(n.status) ? t(o("Request failed with status code " + n.status, n.config, null, n)) : e(n)
      }
    }, function(e, t, n) {
      "use strict";
      var r = n(10);
      e.exports = function(e, t, n, i) {
        var o = new Error(e);
        return r(o, t, n, i)
      }
    }, function(e, t) {
      "use strict";
      e.exports = function(e, t, n, i) {
        return e.config = t, n && (e.code = n), e.response = i, e
      }
    }, function(e, t, n) {
      "use strict";
      var r = n(2);

      function s(e) {
        return encodeURIComponent(e).replace(/%40/gi, "@").replace(/%3A/gi, ":").replace(/%24/g, "$").replace(/%2C/gi, ",").replace(/%20/g, "+").replace(/%5B/gi, "[").replace(/%5D/gi, "]")
      }
      e.exports = function(e, t, n) {
        if (!t) return e;
        var i;
        if (n) i = n(t);
        else if (r.isURLSearchParams(t)) i = t.toString();
        else {
          var o = [];
          r.forEach(t, function(e, t) {
            null != e && (r.isArray(e) && (t += "[]"), r.isArray(e) || (e = [e]), r.forEach(e, function(e) {
              r.isDate(e) ? e = e.toISOString() : r.isObject(e) && (e = JSON.stringify(e)), o.push(s(t) + "=" + s(e))
            }))
          }), i = o.join("&")
        }
        return i && (e += (-1 === e.indexOf("?") ? "?" : "&") + i), e
      }
    }, function(e, t, n) {
      "use strict";
      var r = n(2);
      e.exports = function(e) {
        var t, n, i, o = {};
        return e && r.forEach(e.split("\n"), function(e) {
          i = e.indexOf(":"), t = r.trim(e.substr(0, i)).toLowerCase(), n = r.trim(e.substr(i + 1)), t && (o[t] = o[t] ? o[t] + ", " + n : n)
        }), o
      }
    }, function(e, t, n) {
      "use strict";
      var s = n(2);
      e.exports = s.isStandardBrowserEnv() ? function() {
        var n, i = /(msie|trident)/i.test(navigator.userAgent),
          o = document.createElement("a");

        function r(e) {
          var t = e;
          return i && (o.setAttribute("href", t), t = o.href), o.setAttribute("href", t), {
            href: o.href,
            protocol: o.protocol ? o.protocol.replace(/:$/, "") : "",
            host: o.host,
            search: o.search ? o.search.replace(/^\?/, "") : "",
            hash: o.hash ? o.hash.replace(/^#/, "") : "",
            hostname: o.hostname,
            port: o.port,
            pathname: "/" === o.pathname.charAt(0) ? o.pathname : "/" + o.pathname
          }
        }
        return n = r(window.location.href),
          function(e) {
            var t = s.isString(e) ? r(e) : e;
            return t.protocol === n.protocol && t.host === n.host
          }
      }() : function() {
        return !0
      }
    }, function(e, t) {
      "use strict";

      function a() {
        this.message = "String contains an invalid character"
      }(a.prototype = new Error).code = 5, a.prototype.name = "InvalidCharacterError", e.exports = function(e) {
        for (var t, n, i = String(e), o = "", r = 0, s = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/="; i.charAt(0 | r) || (s = "=", r % 1); o += s.charAt(63 & t >> 8 - r % 1 * 8)) {
          if (255 < (n = i.charCodeAt(r += .75))) throw new a;
          t = t << 8 | n
        }
        return o
      }
    }, function(e, t, n) {
      "use strict";
      var a = n(2);
      e.exports = a.isStandardBrowserEnv() ? {
        write: function(e, t, n, i, o, r) {
          var s = [];
          s.push(e + "=" + encodeURIComponent(t)), a.isNumber(n) && s.push("expires=" + new Date(n).toGMTString()), a.isString(i) && s.push("path=" + i), a.isString(o) && s.push("domain=" + o), !0 === r && s.push("secure"), document.cookie = s.join("; ")
        },
        read: function(e) {
          var t = document.cookie.match(new RegExp("(^|;\\s*)(" + e + ")=([^;]*)"));
          return t ? decodeURIComponent(t[3]) : null
        },
        remove: function(e) {
          this.write(e, "", Date.now() - 864e5)
        }
      } : {
        write: function() {},
        read: function() {
          return null
        },
        remove: function() {}
      }
    }, function(e, t, n) {
      "use strict";
      var i = n(2);

      function o() {
        this.handlers = []
      }
      o.prototype.use = function(e, t) {
        return this.handlers.push({
          fulfilled: e,
          rejected: t
        }), this.handlers.length - 1
      }, o.prototype.eject = function(e) {
        this.handlers[e] && (this.handlers[e] = null)
      }, o.prototype.forEach = function(t) {
        i.forEach(this.handlers, function(e) {
          null !== e && t(e)
        })
      }, e.exports = o
    }, function(e, t, n) {
      "use strict";
      var i = n(2),
        o = n(18),
        r = n(19),
        s = n(5);

      function a(e) {
        e.cancelToken && e.cancelToken.throwIfRequested()
      }
      e.exports = function(t) {
        return a(t), t.headers = t.headers || {}, t.data = o(t.data, t.headers, t.transformRequest), t.headers = i.merge(t.headers.common || {}, t.headers[t.method] || {}, t.headers || {}), i.forEach(["delete", "get", "head", "post", "put", "patch", "common"], function(e) {
          delete t.headers[e]
        }), (t.adapter || s.adapter)(t).then(function(e) {
          return a(t), e.data = o(e.data, e.headers, t.transformResponse), e
        }, function(e) {
          return r(e) || (a(t), e && e.response && (e.response.data = o(e.response.data, e.response.headers, t.transformResponse))), Promise.reject(e)
        })
      }
    }, function(e, t, n) {
      "use strict";
      var i = n(2);
      e.exports = function(t, n, e) {
        return i.forEach(e, function(e) {
          t = e(t, n)
        }), t
      }
    }, function(e, t) {
      "use strict";
      e.exports = function(e) {
        return !(!e || !e.__CANCEL__)
      }
    }, function(e, t) {
      "use strict";
      e.exports = function(e) {
        return /^([a-z][a-z\d\+\-\.]*:)?\/\//i.test(e)
      }
    }, function(e, t) {
      "use strict";
      e.exports = function(e, t) {
        return e.replace(/\/+$/, "") + "/" + t.replace(/^\/+/, "")
      }
    }, function(e, t) {
      "use strict";

      function n(e) {
        this.message = e
      }
      n.prototype.toString = function() {
        return "Cancel" + (this.message ? ": " + this.message : "")
      }, n.prototype.__CANCEL__ = !0, e.exports = n
    }, function(e, t, n) {
      "use strict";
      var i = n(22);

      function o(e) {
        if ("function" != typeof e) throw new TypeError("executor must be a function.");
        var t;
        this.promise = new Promise(function(e) {
          t = e
        });
        var n = this;
        e(function(e) {
          n.reason || (n.reason = new i(e), t(n.reason))
        })
      }
      o.prototype.throwIfRequested = function() {
        if (this.reason) throw this.reason
      }, o.source = function() {
        var t;
        return {
          token: new o(function(e) {
            t = e
          }),
          cancel: t
        }
      }, e.exports = o
    }, function(e, t) {
      "use strict";
      e.exports = function(t) {
        return function(e) {
          return t.apply(null, e)
        }
      }
    }])
  });
var objectFitImages = function() {
  "use strict";

  function r(e, t, n) {
    i.width = t || 1, i.height = n || 1, e[l].width === i.width && e[l].height === i.height || (e[l].width = i.width, e[l].height = i.height, g.call(e, "src", i.toDataURL()))
  }

  function s(e, t) {
    e.naturalWidth ? t(e) : setTimeout(s, 100, e, t)
  }

  function a(t) {
    var n, i, e = function(e) {
        for (var t, n = getComputedStyle(e).fontFamily, i = {}; null !== (t = c.exec(n));) i[t[1]] = t[2];
        return i
      }(t),
      o = t[l];
    if (e["object-fit"] = e["object-fit"] || "fill", !o.img) {
      if ("fill" === e["object-fit"]) return;
      if (!o.skipTest && u && !e["object-position"]) return
    }
    if (!o.img) {
      o.img = new Image(t.width, t.height), o.img.srcset = p.call(t, "data-ofi-srcset") || t.srcset, o.img.src = p.call(t, "data-ofi-src") || t.src, g.call(t, "data-ofi-src", t.src), g.call(t, "data-ofi-srcset", t.srcset), r(t, t.naturalWidth || t.width, t.naturalHeight || t.height), t.srcset && (t.srcset = "");
      try {
        n = t, i = {
          get: function(e) {
            return n[l].img[e || "src"]
          },
          set: function(e, t) {
            return n[l].img[t || "src"] = e, g.call(n, "data-ofi-" + t, e), a(n), e
          }
        }, Object.defineProperty(n, "src", i), Object.defineProperty(n, "currentSrc", {
          get: function() {
            return i.get("currentSrc")
          }
        }), Object.defineProperty(n, "srcset", {
          get: function() {
            return i.get("srcset")
          },
          set: function(e) {
            return i.set(e, "srcset")
          }
        })
      } catch (e) {
        window.console && console.log("http://bit.ly/ofi-old-browser")
      }
    }(function(e) {
      if (e.srcset && !h && window.picturefill) {
        var t = window.picturefill._;
        e[t.ns] && e[t.ns].evaled || t.fillImg(e, {
          reselect: !0
        }), e[t.ns].curSrc || (e[t.ns].supported = !1, t.fillImg(e, {
          reselect: !0
        })), e.currentSrc = e[t.ns].curSrc || e.src
      }
    })(o.img), t.style.backgroundImage = "url(" + (o.img.currentSrc || o.img.src).replace("(", "%28").replace(")", "%29") + ")", t.style.backgroundPosition = e["object-position"] || "center", t.style.backgroundRepeat = "no-repeat", /scale-down/.test(e["object-fit"]) ? s(o.img, function() {
      o.img.naturalWidth > t.width || o.img.naturalHeight > t.height ? t.style.backgroundSize = "contain" : t.style.backgroundSize = "auto"
    }) : t.style.backgroundSize = e["object-fit"].replace("none", "auto").replace("fill", "100% 100%"), s(o.img, function(e) {
      r(t, e.naturalWidth, e.naturalHeight)
    })
  }

  function o(e, t) {
    var n = !m && !e;
    if (t = t || {}, e = e || "img", d && !t.skipTest || !f) return !1;
    "string" == typeof e ? e = document.querySelectorAll(e) : "length" in e || (e = [e]);
    for (var i = 0; i < e.length; i++) e[i][l] = e[i][l] || {
      skipTest: t.skipTest
    }, a(e[i]);
    n && (document.body.addEventListener("load", function(e) {
      "IMG" === e.target.tagName && o(e.target, {
        skipTest: t.skipTest
      })
    }, !0), m = !0, e = "img"), t.watchMQ && window.addEventListener("resize", o.bind(null, e, {
      skipTest: t.skipTest
    }))
  }
  var l = "bfred-it:object-fit-images",
    c = /(object-fit|object-position)\s*:\s*([-\w\s%]+)/g,
    e = new Image,
    i = document.createElement("canvas"),
    u = "object-fit" in e.style,
    d = "object-position" in e.style,
    f = "background-size" in e.style && window.HTMLCanvasElement,
    h = "string" == typeof e.currentSrc,
    p = e.getAttribute,
    g = e.setAttribute,
    m = !1;
  return o.supportsObjectFit = u, o.supportsObjectPosition = d,
    function() {
      function n(e, t) {
        return e[l] && e[l].img && ("src" === t || "srcset" === t) ? e[l].img : e
      }
      d || (HTMLImageElement.prototype.getAttribute = function(e) {
        return p.call(n(this, e), e)
      }, HTMLImageElement.prototype.setAttribute = function(e, t) {
        return g.call(n(this, e), e, String(t))
      })
    }(), o
}();
! function(e) {
  if ("object" == typeof exports && "undefined" != typeof module) module.exports = e();
  else if ("function" == typeof define && define.amd) define([], e);
  else {
    ("undefined" != typeof window ? window : "undefined" != typeof global ? global : "undefined" != typeof self ? self : this).flexibility = e()
  }
}(function() {
  return function r(s, a, l) {
    function c(n, e) {
      if (!a[n]) {
        if (!s[n]) {
          var t = "function" == typeof require && require;
          if (!e && t) return t(n, !0);
          if (u) return u(n, !0);
          var i = new Error("Cannot find module '" + n + "'");
          throw i.code = "MODULE_NOT_FOUND", i
        }
        var o = a[n] = {
          exports: {}
        };
        s[n][0].call(o.exports, function(e) {
          var t = s[n][1][e];
          return c(t || e)
        }, o, o.exports, r, s, a, l)
      }
      return a[n].exports
    }
    for (var u = "function" == typeof require && require, e = 0; e < l.length; e++) c(l[e]);
    return c
  }({
    1: [function(e, t, n) {
      t.exports = function(e) {
        var t, n, i, o = -1;
        if (1 < e.lines.length && "flex-start" === e.style.alignContent)
          for (t = 0; i = e.lines[++o];) i.crossStart = t, t += i.cross;
        else if (1 < e.lines.length && "flex-end" === e.style.alignContent)
          for (t = e.flexStyle.crossSpace; i = e.lines[++o];) i.crossStart = t, t += i.cross;
        else if (1 < e.lines.length && "center" === e.style.alignContent)
          for (t = e.flexStyle.crossSpace / 2; i = e.lines[++o];) i.crossStart = t, t += i.cross;
        else if (1 < e.lines.length && "space-between" === e.style.alignContent)
          for (n = e.flexStyle.crossSpace / (e.lines.length - 1), t = 0; i = e.lines[++o];) i.crossStart = t, t += i.cross + n;
        else if (1 < e.lines.length && "space-around" === e.style.alignContent)
          for (t = (n = 2 * e.flexStyle.crossSpace / (2 * e.lines.length)) / 2; i = e.lines[++o];) i.crossStart = t, t += i.cross + n;
        else
          for (n = e.flexStyle.crossSpace / e.lines.length, t = e.flexStyle.crossInnerBefore; i = e.lines[++o];) i.crossStart = t, i.cross += n, t += i.cross
      }
    }, {}],
    2: [function(e, t, n) {
      t.exports = function(e) {
        for (var t, n = -1; line = e.lines[++n];)
          for (t = -1; child = line.children[++t];) {
            var i = child.style.alignSelf;
            "auto" === i && (i = e.style.alignItems), "flex-start" === i ? child.flexStyle.crossStart = line.crossStart : "flex-end" === i ? child.flexStyle.crossStart = line.crossStart + line.cross - child.flexStyle.crossOuter : "center" === i ? child.flexStyle.crossStart = line.crossStart + (line.cross - child.flexStyle.crossOuter) / 2 : (child.flexStyle.crossStart = line.crossStart, child.flexStyle.crossOuter = line.cross, child.flexStyle.cross = child.flexStyle.crossOuter - child.flexStyle.crossBefore - child.flexStyle.crossAfter)
          }
      }
    }, {}],
    3: [function(e, t, n) {
      t.exports = function(e, t) {
        var n = "row" === t || "row-reverse" === t,
          i = e.mainAxis;
        i ? n && "inline" === i || !n && "block" === i || (e.flexStyle = {
          main: e.flexStyle.cross,
          cross: e.flexStyle.main,
          mainOffset: e.flexStyle.crossOffset,
          crossOffset: e.flexStyle.mainOffset,
          mainBefore: e.flexStyle.crossBefore,
          mainAfter: e.flexStyle.crossAfter,
          crossBefore: e.flexStyle.mainBefore,
          crossAfter: e.flexStyle.mainAfter,
          mainInnerBefore: e.flexStyle.crossInnerBefore,
          mainInnerAfter: e.flexStyle.crossInnerAfter,
          crossInnerBefore: e.flexStyle.mainInnerBefore,
          crossInnerAfter: e.flexStyle.mainInnerAfter,
          mainBorderBefore: e.flexStyle.crossBorderBefore,
          mainBorderAfter: e.flexStyle.crossBorderAfter,
          crossBorderBefore: e.flexStyle.mainBorderBefore,
          crossBorderAfter: e.flexStyle.mainBorderAfter
        }) : (e.flexStyle = n ? {
          main: e.style.width,
          cross: e.style.height,
          mainOffset: e.style.offsetWidth,
          crossOffset: e.style.offsetHeight,
          mainBefore: e.style.marginLeft,
          mainAfter: e.style.marginRight,
          crossBefore: e.style.marginTop,
          crossAfter: e.style.marginBottom,
          mainInnerBefore: e.style.paddingLeft,
          mainInnerAfter: e.style.paddingRight,
          crossInnerBefore: e.style.paddingTop,
          crossInnerAfter: e.style.paddingBottom,
          mainBorderBefore: e.style.borderLeftWidth,
          mainBorderAfter: e.style.borderRightWidth,
          crossBorderBefore: e.style.borderTopWidth,
          crossBorderAfter: e.style.borderBottomWidth
        } : {
          main: e.style.height,
          cross: e.style.width,
          mainOffset: e.style.offsetHeight,
          crossOffset: e.style.offsetWidth,
          mainBefore: e.style.marginTop,
          mainAfter: e.style.marginBottom,
          crossBefore: e.style.marginLeft,
          crossAfter: e.style.marginRight,
          mainInnerBefore: e.style.paddingTop,
          mainInnerAfter: e.style.paddingBottom,
          crossInnerBefore: e.style.paddingLeft,
          crossInnerAfter: e.style.paddingRight,
          mainBorderBefore: e.style.borderTopWidth,
          mainBorderAfter: e.style.borderBottomWidth,
          crossBorderBefore: e.style.borderLeftWidth,
          crossBorderAfter: e.style.borderRightWidth
        }, "content-box" === e.style.boxSizing && ("number" == typeof e.flexStyle.main && (e.flexStyle.main += e.flexStyle.mainInnerBefore + e.flexStyle.mainInnerAfter + e.flexStyle.mainBorderBefore + e.flexStyle.mainBorderAfter), "number" == typeof e.flexStyle.cross && (e.flexStyle.cross += e.flexStyle.crossInnerBefore + e.flexStyle.crossInnerAfter + e.flexStyle.crossBorderBefore + e.flexStyle.crossBorderAfter)));
        e.mainAxis = n ? "inline" : "block", e.crossAxis = n ? "block" : "inline", "number" == typeof e.style.flexBasis && (e.flexStyle.main = e.style.flexBasis + e.flexStyle.mainInnerBefore + e.flexStyle.mainInnerAfter + e.flexStyle.mainBorderBefore + e.flexStyle.mainBorderAfter), e.flexStyle.mainOuter = e.flexStyle.main, e.flexStyle.crossOuter = e.flexStyle.cross, "auto" === e.flexStyle.mainOuter && (e.flexStyle.mainOuter = e.flexStyle.mainOffset), "auto" === e.flexStyle.crossOuter && (e.flexStyle.crossOuter = e.flexStyle.crossOffset), "number" == typeof e.flexStyle.mainBefore && (e.flexStyle.mainOuter += e.flexStyle.mainBefore), "number" == typeof e.flexStyle.mainAfter && (e.flexStyle.mainOuter += e.flexStyle.mainAfter), "number" == typeof e.flexStyle.crossBefore && (e.flexStyle.crossOuter += e.flexStyle.crossBefore), "number" == typeof e.flexStyle.crossAfter && (e.flexStyle.crossOuter += e.flexStyle.crossAfter)
      }
    }, {}],
    4: [function(e, t, n) {
      var o = e("../reduce");
      t.exports = function(n) {
        if (0 < n.mainSpace) {
          var i = o(n.children, function(e, t) {
            return e + parseFloat(t.style.flexGrow)
          }, 0);
          0 < i && (n.main = o(n.children, function(e, t) {
            return "auto" === t.flexStyle.main ? t.flexStyle.main = t.flexStyle.mainOffset + parseFloat(t.style.flexGrow) / i * n.mainSpace : t.flexStyle.main += parseFloat(t.style.flexGrow) / i * n.mainSpace, t.flexStyle.mainOuter = t.flexStyle.main + t.flexStyle.mainBefore + t.flexStyle.mainAfter, e + t.flexStyle.mainOuter
          }, 0), n.mainSpace = 0)
        }
      }
    }, {
      "../reduce": 12
    }],
    5: [function(e, t, n) {
      var o = e("../reduce");
      t.exports = function(n) {
        if (n.mainSpace < 0) {
          var i = o(n.children, function(e, t) {
            return e + parseFloat(t.style.flexShrink)
          }, 0);
          0 < i && (n.main = o(n.children, function(e, t) {
            return t.flexStyle.main += parseFloat(t.style.flexShrink) / i * n.mainSpace, t.flexStyle.mainOuter = t.flexStyle.main + t.flexStyle.mainBefore + t.flexStyle.mainAfter, e + t.flexStyle.mainOuter
          }, 0), n.mainSpace = 0)
        }
      }
    }, {
      "../reduce": 12
    }],
    6: [function(e, t, n) {
      var o = e("../reduce");
      t.exports = function(e) {
        var t;
        e.lines = [t = {
          main: 0,
          cross: 0,
          children: []
        }];
        for (var n, i = -1; n = e.children[++i];) "nowrap" === e.style.flexWrap || 0 === t.children.length || "auto" === e.flexStyle.main || e.flexStyle.main - e.flexStyle.mainInnerBefore - e.flexStyle.mainInnerAfter - e.flexStyle.mainBorderBefore - e.flexStyle.mainBorderAfter >= t.main + n.flexStyle.mainOuter ? (t.main += n.flexStyle.mainOuter, t.cross = Math.max(t.cross, n.flexStyle.crossOuter)) : e.lines.push(t = {
          main: n.flexStyle.mainOuter,
          cross: n.flexStyle.crossOuter,
          children: []
        }), t.children.push(n);
        e.flexStyle.mainLines = o(e.lines, function(e, t) {
          return Math.max(e, t.main)
        }, 0), e.flexStyle.crossLines = o(e.lines, function(e, t) {
          return e + t.cross
        }, 0), "auto" === e.flexStyle.main && (e.flexStyle.main = Math.max(e.flexStyle.mainOffset, e.flexStyle.mainLines + e.flexStyle.mainInnerBefore + e.flexStyle.mainInnerAfter + e.flexStyle.mainBorderBefore + e.flexStyle.mainBorderAfter)), "auto" === e.flexStyle.cross && (e.flexStyle.cross = Math.max(e.flexStyle.crossOffset, e.flexStyle.crossLines + e.flexStyle.crossInnerBefore + e.flexStyle.crossInnerAfter + e.flexStyle.crossBorderBefore + e.flexStyle.crossBorderAfter)), e.flexStyle.crossSpace = e.flexStyle.cross - e.flexStyle.crossInnerBefore - e.flexStyle.crossInnerAfter - e.flexStyle.crossBorderBefore - e.flexStyle.crossBorderAfter - e.flexStyle.crossLines, e.flexStyle.mainOuter = e.flexStyle.main + e.flexStyle.mainBefore + e.flexStyle.mainAfter, e.flexStyle.crossOuter = e.flexStyle.cross + e.flexStyle.crossBefore + e.flexStyle.crossAfter
      }
    }, {
      "../reduce": 12
    }],
    7: [function(o, e, t) {
      e.exports = function(e) {
        for (var t, n = -1; t = e.children[++n];) o("./flex-direction")(t, e.style.flexDirection);
        o("./flex-direction")(e, e.style.flexDirection), o("./order")(e), o("./flexbox-lines")(e), o("./align-content")(e), n = -1;
        for (var i; i = e.lines[++n];) i.mainSpace = e.flexStyle.main - e.flexStyle.mainInnerBefore - e.flexStyle.mainInnerAfter - e.flexStyle.mainBorderBefore - e.flexStyle.mainBorderAfter - i.main, o("./flex-grow")(i), o("./flex-shrink")(i), o("./margin-main")(i), o("./margin-cross")(i), o("./justify-content")(i, e.style.justifyContent, e);
        o("./align-items")(e)
      }
    }, {
      "./align-content": 1,
      "./align-items": 2,
      "./flex-direction": 3,
      "./flex-grow": 4,
      "./flex-shrink": 5,
      "./flexbox-lines": 6,
      "./justify-content": 8,
      "./margin-cross": 9,
      "./margin-main": 10,
      "./order": 11
    }],
    8: [function(e, t, n) {
      t.exports = function(e, t, n) {
        var i, o, r, s = n.flexStyle.mainInnerBefore,
          a = -1;
        if ("flex-end" === t)
          for (i = e.mainSpace, i += s; r = e.children[++a];) r.flexStyle.mainStart = i, i += r.flexStyle.mainOuter;
        else if ("center" === t)
          for (i = e.mainSpace / 2, i += s; r = e.children[++a];) r.flexStyle.mainStart = i, i += r.flexStyle.mainOuter;
        else if ("space-between" === t)
          for (o = e.mainSpace / (e.children.length - 1), i = 0, i += s; r = e.children[++a];) r.flexStyle.mainStart = i, i += r.flexStyle.mainOuter + o;
        else if ("space-around" === t)
          for (i = (o = 2 * e.mainSpace / (2 * e.children.length)) / 2, i += s; r = e.children[++a];) r.flexStyle.mainStart = i, i += r.flexStyle.mainOuter + o;
        else
          for (i = 0, i += s; r = e.children[++a];) r.flexStyle.mainStart = i, i += r.flexStyle.mainOuter
      }
    }, {}],
    9: [function(e, t, n) {
      t.exports = function(e) {
        for (var t, n = -1; t = e.children[++n];) {
          var i = 0;
          "auto" === t.flexStyle.crossBefore && ++i, "auto" === t.flexStyle.crossAfter && ++i;
          var o = e.cross - t.flexStyle.crossOuter;
          "auto" === t.flexStyle.crossBefore && (t.flexStyle.crossBefore = o / i), "auto" === t.flexStyle.crossAfter && (t.flexStyle.crossAfter = o / i), "auto" === t.flexStyle.cross ? t.flexStyle.crossOuter = t.flexStyle.crossOffset + t.flexStyle.crossBefore + t.flexStyle.crossAfter : t.flexStyle.crossOuter = t.flexStyle.cross + t.flexStyle.crossBefore + t.flexStyle.crossAfter
        }
      }
    }, {}],
    10: [function(e, t, n) {
      t.exports = function(e) {
        for (var t, n = 0, i = -1; t = e.children[++i];) "auto" === t.flexStyle.mainBefore && ++n, "auto" === t.flexStyle.mainAfter && ++n;
        if (0 < n) {
          for (i = -1; t = e.children[++i];) "auto" === t.flexStyle.mainBefore && (t.flexStyle.mainBefore = e.mainSpace / n), "auto" === t.flexStyle.mainAfter && (t.flexStyle.mainAfter = e.mainSpace / n), "auto" === t.flexStyle.main ? t.flexStyle.mainOuter = t.flexStyle.mainOffset + t.flexStyle.mainBefore + t.flexStyle.mainAfter : t.flexStyle.mainOuter = t.flexStyle.main + t.flexStyle.mainBefore + t.flexStyle.mainAfter;
          e.mainSpace = 0
        }
      }
    }, {}],
    11: [function(e, t, n) {
      var i = /^(column|row)-reverse$/;
      t.exports = function(e) {
        e.children.sort(function(e, t) {
          return e.style.order - t.style.order || e.index - t.index
        }), i.test(e.style.flexDirection) && e.children.reverse()
      }
    }, {}],
    12: [function(e, t, n) {
      t.exports = function(e, t, n) {
        for (var i = e.length, o = -1; ++o < i;) o in e && (n = t(n, e[o], o));
        return n
      }
    }, {}],
    13: [function(e, t, n) {
      var i = e("./read"),
        o = e("./write"),
        r = e("./readAll"),
        s = e("./writeAll");
      t.exports = function(e) {
        s(r(e))
      }, t.exports.read = i, t.exports.write = o, t.exports.readAll = r, t.exports.writeAll = s
    }, {
      "./read": 15,
      "./readAll": 16,
      "./write": 17,
      "./writeAll": 18
    }],
    14: [function(e, t, n) {
      t.exports = function(e, t, n) {
        var i = e[t],
          o = String(i).match(l);
        if (!o) {
          var r = t.match(d);
          return r ? "none" === e["border" + r[1] + "Style"] ? 0 : u[i] || 0 : i
        }
        var s = o[1],
          a = o[2];
        return "px" === a ? 1 * s : "cm" === a ? .3937 * s * 96 : "in" === a ? 96 * s : "mm" === a ? .3937 * s * 96 / 10 : "pc" === a ? 12 * s * 96 / 72 : "pt" === a ? 96 * s / 72 : "rem" === a ? 16 * s : function(e, t) {
          c.style.cssText = "border:none!important;clip:rect(0 0 0 0)!important;display:block!important;font-size:1em!important;height:0!important;margin:0!important;padding:0!important;position:relative!important;width:" + e + "!important", t.parentNode.insertBefore(c, t.nextSibling);
          var n = c.offsetWidth;
          return t.parentNode.removeChild(c), n
        }(i, n)
      };
      var l = /^([-+]?\d*\.?\d+)(%|[a-z]+)$/,
        c = document.createElement("div"),
        u = {
          medium: 4,
          none: 0,
          thick: 6,
          thin: 2
        },
        d = /^border(Bottom|Left|Right|Top)Width$/
    }, {}],
    15: [function(e, t, n) {
      t.exports = function(e) {
        var t = {
          alignContent: "stretch",
          alignItems: "stretch",
          alignSelf: "auto",
          borderBottomStyle: "none",
          borderBottomWidth: 0,
          borderLeftStyle: "none",
          borderLeftWidth: 0,
          borderRightStyle: "none",
          borderRightWidth: 0,
          borderTopStyle: "none",
          borderTopWidth: 0,
          boxSizing: "content-box",
          display: "inline",
          flexBasis: "auto",
          flexDirection: "row",
          flexGrow: 0,
          flexShrink: 1,
          flexWrap: "nowrap",
          justifyContent: "flex-start",
          height: "auto",
          marginTop: 0,
          marginRight: 0,
          marginLeft: 0,
          marginBottom: 0,
          paddingTop: 0,
          paddingRight: 0,
          paddingLeft: 0,
          paddingBottom: 0,
          maxHeight: "none",
          maxWidth: "none",
          minHeight: 0,
          minWidth: 0,
          order: 0,
          position: "static",
          width: "auto"
        };
        if (e instanceof Element) {
          var n = e.hasAttribute("data-style"),
            i = n ? e.getAttribute("data-style") : e.getAttribute("style") || "";
          n || e.setAttribute("data-style", i),
            function(e, t) {
              for (var n in e) {
                var i = n in t;
                i && !l.test(n) && (e[n] = t[n])
              }
            }(t, window.getComputedStyle && getComputedStyle(e) || {});
          var o = e.currentStyle || {};
          for (var r in function(e, t) {
                for (var n in e) {
                  var i = n in t;
                  if (i) e[n] = t[n];
                  else {
                    var o = n.replace(/[A-Z]/g, "-$&").toLowerCase(),
                      r = o in t;
                    r && (e[n] = t[o])
                  }
                }
                "-js-display" in t && (e.display = t["-js-display"])
              }(t, o),
              function(e, t) {
                for (var n; n = a.exec(t);) {
                  var i = n[1].toLowerCase().replace(/-[a-z]/g, function(e) {
                    return e.slice(1).toUpperCase()
                  });
                  e[i] = n[2]
                }
              }(t, i), t) t[r] = c(t, r, e);
          var s = e.getBoundingClientRect();
          t.offsetHeight = s.height || e.offsetHeight, t.offsetWidth = s.width || e.offsetWidth
        }
        return {
          element: e,
          style: t
        }
      };
      var a = /([^\s:;]+)\s*:\s*([^;]+?)\s*(;|$)/g,
        l = /^(alignSelf|height|width)$/,
        c = e("./getComputedLength")
    }, {
      "./getComputedLength": 14
    }],
    16: [function(e, t, n) {
      t.exports = function(e) {
        var t = [];
        return function e(t, n) {
          for (var i, o = (g = p = h = void 0, h = (f = t) instanceof Element, p = h && f.getAttribute("data-style"), g = h && f.currentStyle && f.currentStyle["-js-display"], y.test(p) || b.test(g)), r = [], s = -1; i = t.childNodes[++s];) {
            var a = 3 === i.nodeType && !/^\s*$/.test(i.nodeValue);
            if (o && a) {
              var l = i;
              (i = t.insertBefore(document.createElement("flex-item"), l)).appendChild(l)
            }
            var c = i instanceof Element;
            if (c) {
              var u = e(i, n);
              if (o) {
                var d = i.style;
                d.display = "inline-block", d.position = "absolute", u.style = v(i).style, r.push(u)
              }
            }
          }
          var f, h, p, g, m = {
            element: t,
            children: r
          };
          return o && (m.style = v(t).style, n.push(m)), m
        }(e, t), t
      };
      var v = e("../read"),
        y = /(^|;)\s*display\s*:\s*(inline-)?flex\s*(;|$)/i,
        b = /^(inline-)?flex$/i
    }, {
      "../read": 15
    }],
    17: [function(e, t, n) {
      function a(e) {
        return "string" == typeof e ? e : Math.max(e, 0) + "px"
      }
      t.exports = function(e) {
        l(e);
        var t = e.element.style,
          n = "inline" === e.mainAxis ? ["main", "cross"] : ["cross", "main"];
        t.boxSizing = "content-box", t.display = "block", t.position = "relative", t.width = a(e.flexStyle[n[0]] - e.flexStyle[n[0] + "InnerBefore"] - e.flexStyle[n[0] + "InnerAfter"] - e.flexStyle[n[0] + "BorderBefore"] - e.flexStyle[n[0] + "BorderAfter"]), t.height = a(e.flexStyle[n[1]] - e.flexStyle[n[1] + "InnerBefore"] - e.flexStyle[n[1] + "InnerAfter"] - e.flexStyle[n[1] + "BorderBefore"] - e.flexStyle[n[1] + "BorderAfter"]);
        for (var i, o = -1; i = e.children[++o];) {
          var r = i.element.style,
            s = "inline" === i.mainAxis ? ["main", "cross"] : ["cross", "main"];
          r.boxSizing = "content-box", r.display = "block", r.position = "absolute", "auto" !== i.flexStyle[s[0]] && (r.width = a(i.flexStyle[s[0]] - i.flexStyle[s[0] + "InnerBefore"] - i.flexStyle[s[0] + "InnerAfter"] - i.flexStyle[s[0] + "BorderBefore"] - i.flexStyle[s[0] + "BorderAfter"])), "auto" !== i.flexStyle[s[1]] && (r.height = a(i.flexStyle[s[1]] - i.flexStyle[s[1] + "InnerBefore"] - i.flexStyle[s[1] + "InnerAfter"] - i.flexStyle[s[1] + "BorderBefore"] - i.flexStyle[s[1] + "BorderAfter"])), r.top = a(i.flexStyle[s[1] + "Start"]), r.left = a(i.flexStyle[s[0] + "Start"]), r.marginTop = a(i.flexStyle[s[1] + "Before"]), r.marginRight = a(i.flexStyle[s[0] + "After"]), r.marginBottom = a(i.flexStyle[s[1] + "After"]), r.marginLeft = a(i.flexStyle[s[0] + "Before"])
        }
      };
      var l = e("../flexbox")
    }, {
      "../flexbox": 7
    }],
    18: [function(e, t, n) {
      t.exports = function(e) {
        for (var t, n = -1; t = e[++n];) i(t)
      };
      var i = e("../write")
    }, {
      "../write": 17
    }]
  }, {}, [13])(13)
}),
function(e, t) {
  "object" == typeof exports && "undefined" != typeof module ? module.exports = t() : "function" == typeof define && define.amd ? define(t) : e.ES6Promise = t()
}(this, function() {
  "use strict";

  function c(e) {
    return "function" == typeof e
  }

  function t() {
    var e = setTimeout;
    return function() {
      return e(n, 1)
    }
  }

  function n() {
    for (var e = 0; e < C; e += 2) {
      (0, O[e])(O[e + 1]), O[e] = void 0, O[e + 1] = void 0
    }
    C = 0
  }

  function a(e, t) {
    var n = arguments,
      i = this,
      o = new this.constructor(u);
    void 0 === o[I] && y(o);
    var r, s = i._state;
    return s ? (r = n[s - 1], $(function() {
      return v(s, o, r, i._result)
    })) : g(i, o, e, t), o
  }

  function l(e) {
    if (e && "object" == typeof e && e.constructor === this) return e;
    var t = new this(u);
    return f(t, e), t
  }

  function u() {}

  function s(e) {
    try {
      return e.then
    } catch (e) {
      return N.error = e, N
    }
  }

  function d(e, t, n) {
    var i, o, r, s;
    t.constructor === e.constructor && n === a && t.constructor.resolve === l ? (r = e, (s = t)._state === L ? h(r, s._result) : s._state === R ? p(r, s._result) : g(s, void 0, function(e) {
      return f(r, e)
    }, function(e) {
      return p(r, e)
    })) : n === N ? p(e, N.error) : void 0 === n ? h(e, t) : c(n) ? (i = t, o = n, $(function(t) {
      var n = !1,
        e = function(e, t, n, i) {
          try {
            e.call(t, n, i)
          } catch (e) {
            return e
          }
        }(o, i, function(e) {
          n || (n = !0, i !== e ? f(t, e) : h(t, e))
        }, function(e) {
          n || (n = !0, p(t, e))
        }, t._label);
      !n && e && (n = !0, p(t, e))
    }, e)) : h(e, t)
  }

  function f(e, t) {
    var n;
    e === t ? p(e, new TypeError("You cannot resolve a promise with itself")) : "function" == typeof(n = t) || "object" == typeof n && null !== n ? d(e, t, s(t)) : h(e, t)
  }

  function i(e) {
    e._onerror && e._onerror(e._result), m(e)
  }

  function h(e, t) {
    e._state === H && (e._result = t, e._state = L, 0 !== e._subscribers.length && $(m, e))
  }

  function p(e, t) {
    e._state === H && (e._state = R, e._result = t, $(i, e))
  }

  function g(e, t, n, i) {
    var o = e._subscribers,
      r = o.length;
    e._onerror = null, o[r] = t, o[r + L] = n, o[r + R] = i, 0 === r && e._state && $(m, e)
  }

  function m(e) {
    var t = e._subscribers,
      n = e._state;
    if (0 !== t.length) {
      for (var i = void 0, o = void 0, r = e._result, s = 0; s < t.length; s += 3) i = t[s], o = t[s + n], i ? v(n, i, o, r) : o(r);
      e._subscribers.length = 0
    }
  }

  function e() {
    this.error = null
  }

  function v(e, t, n, i) {
    var o = c(n),
      r = void 0,
      s = void 0,
      a = void 0,
      l = void 0;
    if (o) {
      if ((r = function(e, t) {
          try {
            return e(t)
          } catch (e) {
            return F.error = e, F
          }
        }(n, i)) === F ? (l = !0, s = r.error, r = null) : a = !0, t === r) return void p(t, new TypeError("A promises callback cannot return that same promise."))
    } else r = i, a = !0;
    t._state !== H || (o && a ? f(t, r) : l ? p(t, s) : e === L ? h(t, r) : e === R && p(t, r))
  }

  function y(e) {
    e[I] = B++, e._state = void 0, e._result = void 0, e._subscribers = []
  }

  function o(e, t) {
    this._instanceConstructor = e, this.promise = new e(u), this.promise[I] || y(this.promise), k(t) ? (this._input = t, this.length = t.length, this._remaining = t.length, this._result = new Array(this.length), 0 === this.length ? h(this.promise, this._result) : (this.length = this.length || 0, this._enumerate(), 0 === this._remaining && h(this.promise, this._result))) : p(this.promise, new Error("Array Methods must be provided an Array"))
  }

  function b(e) {
    this[I] = B++, this._result = this._state = void 0, this._subscribers = [], u !== e && ("function" != typeof e && function() {
      throw new TypeError("You must pass a resolver function as the first argument to the promise constructor")
    }(), this instanceof b ? function(t, e) {
      try {
        e(function(e) {
          f(t, e)
        }, function(e) {
          p(t, e)
        })
      } catch (e) {
        p(t, e)
      }
    }(this, e) : function() {
      throw new TypeError("Failed to construct 'Promise': Please use the 'new' operator, this object constructor cannot be called as a function.")
    }())
  }
  var r, x, w, S, k = Array.isArray ? Array.isArray : function(e) {
      return "[object Array]" === Object.prototype.toString.call(e)
    },
    C = 0,
    _ = void 0,
    T = void 0,
    $ = function(e, t) {
      O[C] = e, O[C + 1] = t, 2 === (C += 2) && (T ? T(n) : M())
    },
    A = "undefined" != typeof window ? window : void 0,
    j = A || {},
    P = j.MutationObserver || j.WebKitMutationObserver,
    E = "undefined" == typeof self && "undefined" != typeof process && "[object process]" === {}.toString.call(process),
    D = "undefined" != typeof Uint8ClampedArray && "undefined" != typeof importScripts && "undefined" != typeof MessageChannel,
    O = new Array(1e3),
    M = void 0;
  M = E ? function() {
    return process.nextTick(n)
  } : P ? (x = 0, w = new P(n), S = document.createTextNode(""), w.observe(S, {
    characterData: !0
  }), function() {
    S.data = x = ++x % 2
  }) : D ? ((r = new MessageChannel).port1.onmessage = n, function() {
    return r.port2.postMessage(0)
  }) : void 0 === A && "function" == typeof require ? function() {
    try {
      var e = require("vertx");
      return void 0 !== (_ = e.runOnLoop || e.runOnContext) ? function() {
        _(n)
      } : t()
    } catch (e) {
      return t()
    }
  }() : t();
  var I = Math.random().toString(36).substring(16),
    H = void 0,
    L = 1,
    R = 2,
    N = new e,
    F = new e,
    B = 0;
  return o.prototype._enumerate = function() {
    for (var e = this.length, t = this._input, n = 0; this._state === H && n < e; n++) this._eachEntry(t[n], n)
  }, o.prototype._eachEntry = function(t, e) {
    var n = this._instanceConstructor,
      i = n.resolve;
    if (i === l) {
      var o = s(t);
      if (o === a && t._state !== H) this._settledAt(t._state, e, t._result);
      else if ("function" != typeof o) this._remaining--, this._result[e] = t;
      else if (n === b) {
        var r = new n(u);
        d(r, t, o), this._willSettleAt(r, e)
      } else this._willSettleAt(new n(function(e) {
        return e(t)
      }), e)
    } else this._willSettleAt(i(t), e)
  }, o.prototype._settledAt = function(e, t, n) {
    var i = this.promise;
    i._state === H && (this._remaining--, e === R ? p(i, n) : this._result[t] = n), 0 === this._remaining && h(i, this._result)
  }, o.prototype._willSettleAt = function(e, t) {
    var n = this;
    g(e, void 0, function(e) {
      return n._settledAt(L, t, e)
    }, function(e) {
      return n._settledAt(R, t, e)
    })
  }, b.all = function(e) {
    return new o(this, e).promise
  }, b.race = function(o) {
    var r = this;
    return new r(k(o) ? function(e, t) {
      for (var n = o.length, i = 0; i < n; i++) r.resolve(o[i]).then(e, t)
    } : function(e, t) {
      return t(new TypeError("You must pass an array to race."))
    })
  }, b.resolve = l, b.reject = function(e) {
    var t = new this(u);
    return p(t, e), t
  }, b._setScheduler = function(e) {
    T = e
  }, b._setAsap = function(e) {
    $ = e
  }, b._asap = $, b.prototype = {
    constructor: b,
    then: a,
    catch: function(e) {
      return this.then(null, e)
    }
  }, b.polyfill = function() {
    var e = void 0;
    if ("undefined" != typeof global) e = global;
    else if ("undefined" != typeof self) e = self;
    else try {
      e = Function("return this")()
    } catch (e) {
      throw new Error("polyfill failed because global object is unavailable in this environment")
    }
    var t = e.Promise;
    if (t) {
      var n = null;
      try {
        n = Object.prototype.toString.call(t.resolve())
      } catch (e) {}
      if ("[object Promise]" === n && !t.cast) return
    }
    e.Promise = b
  }, b.Promise = b
}), ES6Promise.polyfill();
var URLSearchParams = URLSearchParams || function() {
  "use strict";

  function s(e) {
    return encodeURIComponent(e).replace(t, o)
  }

  function a(e) {
    return decodeURIComponent(e.replace(n, " "))
  }

  function l(e) {
    if (this[u] = Object.create(null), e) {
      "?" === e.charAt(0) && (e = e.slice(1));
      for (var t, n, i = (e || "").split("&"), o = 0, r = i.length; o < r; o++) - 1 < (t = (n = i[o]).indexOf("=")) ? this.append(a(n.slice(0, t)), a(n.slice(t + 1))) : n.length && this.append(a(n), "")
    }
  }
  var c = l.prototype,
    t = /[!'\(\)~]|%20|%00/g,
    n = /\+/g,
    i = {
      "!": "%21",
      "'": "%27",
      "(": "%28",
      ")": "%29",
      "~": "%7E",
      "%20": "+",
      "%00": "\0"
    },
    o = function(e) {
      return i[e]
    },
    r = function() {
      try {
        return !!Symbol.iterator
      } catch (e) {
        return !1
      }
    }(),
    u = "__URLSearchParams__:" + Math.random();
  c.append = function(e, t) {
    var n = this[u];
    e in n ? n[e].push("" + t) : n[e] = ["" + t]
  }, c.delete = function(e) {
    delete this[u][e]
  }, c.get = function(e) {
    var t = this[u];
    return e in t ? t[e][0] : null
  }, c.getAll = function(e) {
    var t = this[u];
    return e in t ? t[e].slice(0) : []
  }, c.has = function(e) {
    return e in this[u]
  }, c.set = function(e, t) {
    this[u][e] = ["" + t]
  }, c.forEach = function(n, i) {
    var e = this[u];
    Object.getOwnPropertyNames(e).forEach(function(t) {
      e[t].forEach(function(e) {
        n.call(i, e, t, this)
      }, this)
    }, this)
  }, c.keys = function() {
    var n = [];
    this.forEach(function(e, t) {
      n.push(t)
    });
    var e = {
      next: function() {
        var e = n.shift();
        return {
          done: void 0 === e,
          value: e
        }
      }
    };
    return r && (e[Symbol.iterator] = function() {
      return e
    }), e
  }, c.values = function() {
    var t = [];
    this.forEach(function(e) {
      t.push(e)
    });
    var e = {
      next: function() {
        var e = t.shift();
        return {
          done: void 0 === e,
          value: e
        }
      }
    };
    return r && (e[Symbol.iterator] = function() {
      return e
    }), e
  }, c.entries = function() {
    var n = [];
    this.forEach(function(e, t) {
      n.push([t, e])
    });
    var e = {
      next: function() {
        var e = n.shift();
        return {
          done: void 0 === e,
          value: e
        }
      }
    };
    return r && (e[Symbol.iterator] = function() {
      return e
    }), e
  }, r && (c[Symbol.iterator] = c.entries), c.toJSON = function() {
    return {}
  }, c.toString = function() {
    var e, t, n, i, o = this[u],
      r = [];
    for (t in o)
      for (n = s(t), e = 0, i = o[t]; e < i.length; e++) r.push(n + "=" + s(i[e]));
    return r.join("&")
  };
  var d = Object.defineProperty,
    f = Object.getOwnPropertyDescriptor,
    h = function(e) {
      var t = e.append;
      e.append = c.append, l.call(e, e._usp.search.slice(1)), e.append = t
    },
    p = function(e, t) {
      if (!(e instanceof t)) throw new TypeError("'searchParams' accessed on an object that does not implement interface " + t.name)
    },
    e = function(t) {
      var n, i, e = t.prototype,
        o = f(e, "searchParams"),
        r = f(e, "href"),
        s = f(e, "search");
      !o && s && s.set && (i = function(n) {
        function i(e, t) {
          c.append.call(this, e, t), e = this.toString(), n.set.call(this._usp, e ? "?" + e : "")
        }

        function o(e) {
          c.delete.call(this, e), e = this.toString(), n.set.call(this._usp, e ? "?" + e : "")
        }

        function r(e, t) {
          c.set.call(this, e, t), e = this.toString(), n.set.call(this._usp, e ? "?" + e : "")
        }
        return function(e, t) {
          return e.append = i, e.delete = o, e.set = r, d(e, "_usp", {
            configurable: !0,
            writable: !0,
            value: t
          })
        }
      }(s), n = function(e, t) {
        return d(e, "_searchParams", {
          configurable: !0,
          writable: !0,
          value: i(t, e)
        }), t
      }, Object.defineProperties(e, {
        href: {
          get: function() {
            return r.get.call(this)
          },
          set: function(e) {
            var t = this._searchParams;
            r.set.call(this, e), t && h(t)
          }
        },
        search: {
          get: function() {
            return s.get.call(this)
          },
          set: function(e) {
            var t = this._searchParams;
            s.set.call(this, e), t && h(t)
          }
        },
        searchParams: {
          get: function() {
            return p(this, t), this._searchParams || n(this, new l(this.search.slice(1)))
          },
          set: function(e) {
            p(this, t), n(this, e)
          }
        }
      }))
    };
  return e(HTMLAnchorElement), /^function|object$/.test(typeof URL) && URL.prototype && e(URL), l
}();
! function(r, a, e, g) {
  "use strict";
  var s, l = ["", "webkit", "Moz", "MS", "ms", "o"],
    t = a.createElement("div"),
    n = "function",
    c = Math.round,
    m = Math.abs,
    v = Date.now;

  function u(e, t, n) {
    return setTimeout(y(e, n), t)
  }

  function i(e, t, n) {
    return !!Array.isArray(e) && (d(e, n[t], n), !0)
  }

  function d(e, t, n) {
    var i;
    if (e)
      if (e.forEach) e.forEach(t, n);
      else if (e.length !== g)
      for (i = 0; i < e.length;) t.call(n, e[i], i, e), i++;
    else
      for (i in e) e.hasOwnProperty(i) && t.call(n, e[i], i, e)
  }

  function o(i, e, t) {
    var o = "DEPRECATED METHOD: " + e + "\n" + t + " AT \n";
    return function() {
      var e = new Error("get-stack-trace"),
        t = e && e.stack ? e.stack.replace(/^[^\(]+?[\n$]/gm, "").replace(/^\s+at\s+/gm, "").replace(/^Object.<anonymous>\s*\(/gm, "{anonymous}()@") : "Unknown Stack Trace",
        n = r.console && (r.console.warn || r.console.log);
      return n && n.call(r.console, o, t), i.apply(this, arguments)
    }
  }
  s = "function" != typeof Object.assign ? function(e) {
    if (e === g || null === e) throw new TypeError("Cannot convert undefined or null to object");
    for (var t = Object(e), n = 1; n < arguments.length; n++) {
      var i = arguments[n];
      if (i !== g && null !== i)
        for (var o in i) i.hasOwnProperty(o) && (t[o] = i[o])
    }
    return t
  } : Object.assign;
  var f = o(function(e, t, n) {
      for (var i = Object.keys(t), o = 0; o < i.length;)(!n || n && e[i[o]] === g) && (e[i[o]] = t[i[o]]), o++;
      return e
    }, "extend", "Use `assign`."),
    h = o(function(e, t) {
      return f(e, t, !0)
    }, "merge", "Use `assign`.");

  function p(e, t, n) {
    var i, o = t.prototype;
    (i = e.prototype = Object.create(o)).constructor = e, i._super = o, n && s(i, n)
  }

  function y(e, t) {
    return function() {
      return e.apply(t, arguments)
    }
  }

  function b(e, t) {
    return typeof e == n ? e.apply(t && t[0] || g, t) : e
  }

  function x(e, t) {
    return e === g ? t : e
  }

  function w(t, e, n) {
    d(_(e), function(e) {
      t.addEventListener(e, n, !1)
    })
  }

  function S(t, e, n) {
    d(_(e), function(e) {
      t.removeEventListener(e, n, !1)
    })
  }

  function k(e, t) {
    for (; e;) {
      if (e == t) return !0;
      e = e.parentNode
    }
    return !1
  }

  function C(e, t) {
    return -1 < e.indexOf(t)
  }

  function _(e) {
    return e.trim().split(/\s+/g)
  }

  function T(e, t, n) {
    if (e.indexOf && !n) return e.indexOf(t);
    for (var i = 0; i < e.length;) {
      if (n && e[i][n] == t || !n && e[i] === t) return i;
      i++
    }
    return -1
  }

  function $(e) {
    return Array.prototype.slice.call(e, 0)
  }

  function A(e, n, t) {
    for (var i = [], o = [], r = 0; r < e.length;) {
      var s = n ? e[r][n] : e[r];
      T(o, s) < 0 && i.push(e[r]), o[r] = s, r++
    }
    return t && (i = n ? i.sort(function(e, t) {
      return e[n] > t[n]
    }) : i.sort()), i
  }

  function j(e, t) {
    for (var n, i, o = t[0].toUpperCase() + t.slice(1), r = 0; r < l.length;) {
      if ((i = (n = l[r]) ? n + o : t) in e) return i;
      r++
    }
    return g
  }
  var P = 1;

  function E(e) {
    var t = e.ownerDocument || e;
    return t.defaultView || t.parentWindow || r
  }
  var D = "ontouchstart" in r,
    O = j(r, "PointerEvent") !== g,
    M = D && /mobile|tablet|ip(ad|hone|od)|android/i.test(navigator.userAgent),
    I = "touch",
    H = 25,
    L = 1,
    R = 4,
    N = 8,
    F = 1,
    B = 2,
    z = 4,
    q = 8,
    W = 16,
    Y = B | z,
    V = q | W,
    U = Y | V,
    G = ["x", "y"],
    X = ["clientX", "clientY"];

  function Q(t, e) {
    var n = this;
    this.manager = t, this.callback = e, this.element = t.element, this.target = t.options.inputTarget, this.domHandler = function(e) {
      b(t.options.enable, [t]) && n.handler(e)
    }, this.init()
  }

  function Z(e, t, n) {
    var i = n.pointers.length,
      o = n.changedPointers.length,
      r = t & L && i - o == 0,
      s = t & (R | N) && i - o == 0;
    n.isFirst = !!r, n.isFinal = !!s, r && (e.session = {}), n.eventType = t,
      function(e, t) {
        var n = e.session,
          i = t.pointers,
          o = i.length;
        n.firstInput || (n.firstInput = K(t));
        1 < o && !n.firstMultiple ? n.firstMultiple = K(t) : 1 === o && (n.firstMultiple = !1);
        var r = n.firstInput,
          s = n.firstMultiple,
          a = s ? s.center : r.center,
          l = t.center = J(i);
        t.timeStamp = v(), t.deltaTime = t.timeStamp - r.timeStamp, t.angle = ie(a, l), t.distance = ne(a, l),
          function(e, t) {
            var n = t.center,
              i = e.offsetDelta || {},
              o = e.prevDelta || {},
              r = e.prevInput || {};
            t.eventType !== L && r.eventType !== R || (o = e.prevDelta = {
              x: r.deltaX || 0,
              y: r.deltaY || 0
            }, i = e.offsetDelta = {
              x: n.x,
              y: n.y
            });
            t.deltaX = o.x + (n.x - i.x), t.deltaY = o.y + (n.y - i.y)
          }(n, t), t.offsetDirection = te(t.deltaX, t.deltaY);
        var c = ee(t.deltaTime, t.deltaX, t.deltaY);
        t.overallVelocityX = c.x, t.overallVelocityY = c.y, t.overallVelocity = m(c.x) > m(c.y) ? c.x : c.y, t.scale = s ? (f = s.pointers, h = i, ne(h[0], h[1], X) / ne(f[0], f[1], X)) : 1, t.rotation = s ? (u = s.pointers, d = i, ie(d[1], d[0], X) + ie(u[1], u[0], X)) : 0, t.maxPointers = n.prevInput ? t.pointers.length > n.prevInput.maxPointers ? t.pointers.length : n.prevInput.maxPointers : t.pointers.length,
          function(e, t) {
            var n, i, o, r, s = e.lastInterval || t,
              a = t.timeStamp - s.timeStamp;
            if (t.eventType != N && (H < a || s.velocity === g)) {
              var l = t.deltaX - s.deltaX,
                c = t.deltaY - s.deltaY,
                u = ee(a, l, c);
              i = u.x, o = u.y, n = m(u.x) > m(u.y) ? u.x : u.y, r = te(l, c), e.lastInterval = t
            } else n = s.velocity, i = s.velocityX, o = s.velocityY, r = s.direction;
            t.velocity = n, t.velocityX = i, t.velocityY = o, t.direction = r
          }(n, t);
        var u, d;
        var f, h;
        var p = e.element;
        k(t.srcEvent.target, p) && (p = t.srcEvent.target);
        t.target = p
      }(e, n), e.emit("hammer.input", n), e.recognize(n), e.session.prevInput = n
  }

  function K(e) {
    for (var t = [], n = 0; n < e.pointers.length;) t[n] = {
      clientX: c(e.pointers[n].clientX),
      clientY: c(e.pointers[n].clientY)
    }, n++;
    return {
      timeStamp: v(),
      pointers: t,
      center: J(t),
      deltaX: e.deltaX,
      deltaY: e.deltaY
    }
  }

  function J(e) {
    var t = e.length;
    if (1 === t) return {
      x: c(e[0].clientX),
      y: c(e[0].clientY)
    };
    for (var n = 0, i = 0, o = 0; o < t;) n += e[o].clientX, i += e[o].clientY, o++;
    return {
      x: c(n / t),
      y: c(i / t)
    }
  }

  function ee(e, t, n) {
    return {
      x: t / e || 0,
      y: n / e || 0
    }
  }

  function te(e, t) {
    return e === t ? F : m(e) >= m(t) ? e < 0 ? B : z : t < 0 ? q : W
  }

  function ne(e, t, n) {
    n || (n = G);
    var i = t[n[0]] - e[n[0]],
      o = t[n[1]] - e[n[1]];
    return Math.sqrt(i * i + o * o)
  }

  function ie(e, t, n) {
    n || (n = G);
    var i = t[n[0]] - e[n[0]],
      o = t[n[1]] - e[n[1]];
    return 180 * Math.atan2(o, i) / Math.PI
  }
  Q.prototype = {
    handler: function() {},
    init: function() {
      this.evEl && w(this.element, this.evEl, this.domHandler), this.evTarget && w(this.target, this.evTarget, this.domHandler), this.evWin && w(E(this.element), this.evWin, this.domHandler)
    },
    destroy: function() {
      this.evEl && S(this.element, this.evEl, this.domHandler), this.evTarget && S(this.target, this.evTarget, this.domHandler), this.evWin && S(E(this.element), this.evWin, this.domHandler)
    }
  };
  var oe = {
      mousedown: L,
      mousemove: 2,
      mouseup: R
    },
    re = "mousedown",
    se = "mousemove mouseup";

  function ae() {
    this.evEl = re, this.evWin = se, this.pressed = !1, Q.apply(this, arguments)
  }
  p(ae, Q, {
    handler: function(e) {
      var t = oe[e.type];
      t & L && 0 === e.button && (this.pressed = !0), 2 & t && 1 !== e.which && (t = R), this.pressed && (t & R && (this.pressed = !1), this.callback(this.manager, t, {
        pointers: [e],
        changedPointers: [e],
        pointerType: "mouse",
        srcEvent: e
      }))
    }
  });
  var le = {
      pointerdown: L,
      pointermove: 2,
      pointerup: R,
      pointercancel: N,
      pointerout: N
    },
    ce = {
      2: I,
      3: "pen",
      4: "mouse",
      5: "kinect"
    },
    ue = "pointerdown",
    de = "pointermove pointerup pointercancel";

  function fe() {
    this.evEl = ue, this.evWin = de, Q.apply(this, arguments), this.store = this.manager.session.pointerEvents = []
  }
  r.MSPointerEvent && !r.PointerEvent && (ue = "MSPointerDown", de = "MSPointerMove MSPointerUp MSPointerCancel"), p(fe, Q, {
    handler: function(e) {
      var t = this.store,
        n = !1,
        i = e.type.toLowerCase().replace("ms", ""),
        o = le[i],
        r = ce[e.pointerType] || e.pointerType,
        s = r == I,
        a = T(t, e.pointerId, "pointerId");
      o & L && (0 === e.button || s) ? a < 0 && (t.push(e), a = t.length - 1) : o & (R | N) && (n = !0), a < 0 || (t[a] = e, this.callback(this.manager, o, {
        pointers: t,
        changedPointers: [e],
        pointerType: r,
        srcEvent: e
      }), n && t.splice(a, 1))
    }
  });
  var he = {
    touchstart: L,
    touchmove: 2,
    touchend: R,
    touchcancel: N
  };

  function pe() {
    this.evTarget = "touchstart", this.evWin = "touchstart touchmove touchend touchcancel", this.started = !1, Q.apply(this, arguments)
  }
  p(pe, Q, {
    handler: function(e) {
      var t = he[e.type];
      if (t === L && (this.started = !0), this.started) {
        var n = function(e, t) {
          var n = $(e.touches),
            i = $(e.changedTouches);
          t & (R | N) && (n = A(n.concat(i), "identifier", !0));
          return [n, i]
        }.call(this, e, t);
        t & (R | N) && n[0].length - n[1].length == 0 && (this.started = !1), this.callback(this.manager, t, {
          pointers: n[0],
          changedPointers: n[1],
          pointerType: I,
          srcEvent: e
        })
      }
    }
  });
  var ge = {
      touchstart: L,
      touchmove: 2,
      touchend: R,
      touchcancel: N
    },
    me = "touchstart touchmove touchend touchcancel";

  function ve() {
    this.evTarget = me, this.targetIds = {}, Q.apply(this, arguments)
  }
  p(ve, Q, {
    handler: function(e) {
      var t = ge[e.type],
        n = function(e, t) {
          var n = $(e.touches),
            i = this.targetIds;
          if (t & (2 | L) && 1 === n.length) return i[n[0].identifier] = !0, [n, n];
          var o, r, s = $(e.changedTouches),
            a = [],
            l = this.target;
          if (r = n.filter(function(e) {
              return k(e.target, l)
            }), t === L)
            for (o = 0; o < r.length;) i[r[o].identifier] = !0, o++;
          o = 0;
          for (; o < s.length;) i[s[o].identifier] && a.push(s[o]), t & (R | N) && delete i[s[o].identifier], o++;
          if (!a.length) return;
          return [A(r.concat(a), "identifier", !0), a]
        }.call(this, e, t);
      n && this.callback(this.manager, t, {
        pointers: n[0],
        changedPointers: n[1],
        pointerType: I,
        srcEvent: e
      })
    }
  });
  var ye = 2500;

  function be() {
    Q.apply(this, arguments);
    var e = y(this.handler, this);
    this.touch = new ve(this.manager, e), this.mouse = new ae(this.manager, e), this.primaryTouch = null, this.lastTouches = []
  }

  function xe(e) {
    var t = e.changedPointers[0];
    if (t.identifier === this.primaryTouch) {
      var n = {
        x: t.clientX,
        y: t.clientY
      };
      this.lastTouches.push(n);
      var i = this.lastTouches;
      setTimeout(function() {
        var e = i.indexOf(n); - 1 < e && i.splice(e, 1)
      }, ye)
    }
  }
  p(be, Q, {
    handler: function(e, t, n) {
      var i = n.pointerType == I,
        o = "mouse" == n.pointerType;
      if (!(o && n.sourceCapabilities && n.sourceCapabilities.firesTouchEvents)) {
        if (i)(function(e, t) {
          e & L ? (this.primaryTouch = t.changedPointers[0].identifier, xe.call(this, t)) : e & (R | N) && xe.call(this, t)
        }).call(this, t, n);
        else if (o && function(e) {
            for (var t = e.srcEvent.clientX, n = e.srcEvent.clientY, i = 0; i < this.lastTouches.length; i++) {
              var o = this.lastTouches[i],
                r = Math.abs(t - o.x),
                s = Math.abs(n - o.y);
              if (r <= 25 && s <= 25) return !0
            }
            return !1
          }.call(this, n)) return;
        this.callback(e, t, n)
      }
    },
    destroy: function() {
      this.touch.destroy(), this.mouse.destroy()
    }
  });
  var we = j(t.style, "touchAction"),
    Se = we !== g,
    ke = "manipulation",
    Ce = "none",
    _e = "pan-x",
    Te = "pan-y",
    $e = function() {
      if (!Se) return !1;
      var t = {},
        n = r.CSS && r.CSS.supports;
      return ["auto", "manipulation", "pan-y", "pan-x", "pan-x pan-y", "none"].forEach(function(e) {
        t[e] = !n || r.CSS.supports("touch-action", e)
      }), t
    }();

  function Ae(e, t) {
    this.manager = e, this.set(t)
  }
  Ae.prototype = {
    set: function(e) {
      "compute" == e && (e = this.compute()), Se && this.manager.element.style && $e[e] && (this.manager.element.style[we] = e), this.actions = e.toLowerCase().trim()
    },
    update: function() {
      this.set(this.manager.options.touchAction)
    },
    compute: function() {
      var t = [];
      return d(this.manager.recognizers, function(e) {
          b(e.options.enable, [e]) && (t = t.concat(e.getTouchAction()))
        }),
        function(e) {
          if (C(e, Ce)) return Ce;
          var t = C(e, _e),
            n = C(e, Te);
          if (t && n) return Ce;
          if (t || n) return t ? _e : Te;
          if (C(e, ke)) return ke;
          return "auto"
        }(t.join(" "))
    },
    preventDefaults: function(e) {
      var t = e.srcEvent,
        n = e.offsetDirection;
      if (this.manager.session.prevented) t.preventDefault();
      else {
        var i = this.actions,
          o = C(i, Ce) && !$e.none,
          r = C(i, Te) && !$e[Te],
          s = C(i, _e) && !$e[_e];
        if (o) {
          var a = 1 === e.pointers.length,
            l = e.distance < 2,
            c = e.deltaTime < 250;
          if (a && l && c) return
        }
        if (!s || !r) return o || r && n & Y || s && n & V ? this.preventSrc(t) : void 0
      }
    },
    preventSrc: function(e) {
      this.manager.session.prevented = !0, e.preventDefault()
    }
  };
  var je = 1;

  function Pe(e) {
    this.options = s({}, this.defaults, e || {}), this.id = P++, this.manager = null, this.options.enable = x(this.options.enable, !0), this.state = je, this.simultaneous = {}, this.requireFail = []
  }

  function Ee(e) {
    return 16 & e ? "cancel" : 8 & e ? "end" : 4 & e ? "move" : 2 & e ? "start" : ""
  }

  function De(e) {
    return e == W ? "down" : e == q ? "up" : e == B ? "left" : e == z ? "right" : ""
  }

  function Oe(e, t) {
    var n = t.manager;
    return n ? n.get(e) : e
  }

  function Me() {
    Pe.apply(this, arguments)
  }

  function Ie() {
    Me.apply(this, arguments), this.pX = null, this.pY = null
  }

  function He() {
    Me.apply(this, arguments)
  }

  function Le() {
    Pe.apply(this, arguments), this._timer = null, this._input = null
  }

  function Re() {
    Me.apply(this, arguments)
  }

  function Ne() {
    Me.apply(this, arguments)
  }

  function Fe() {
    Pe.apply(this, arguments), this.pTime = !1, this.pCenter = !1, this._timer = null, this._input = null, this.count = 0
  }

  function Be(e, t) {
    return (t = t || {}).recognizers = x(t.recognizers, Be.defaults.preset), new ze(e, t)
  }
  Pe.prototype = {
    defaults: {},
    set: function(e) {
      return s(this.options, e), this.manager && this.manager.touchAction.update(), this
    },
    recognizeWith: function(e) {
      if (i(e, "recognizeWith", this)) return this;
      var t = this.simultaneous;
      return t[(e = Oe(e, this)).id] || (t[e.id] = e).recognizeWith(this), this
    },
    dropRecognizeWith: function(e) {
      return i(e, "dropRecognizeWith", this) || (e = Oe(e, this), delete this.simultaneous[e.id]), this
    },
    requireFailure: function(e) {
      if (i(e, "requireFailure", this)) return this;
      var t = this.requireFail;
      return -1 === T(t, e = Oe(e, this)) && (t.push(e), e.requireFailure(this)), this
    },
    dropRequireFailure: function(e) {
      if (i(e, "dropRequireFailure", this)) return this;
      e = Oe(e, this);
      var t = T(this.requireFail, e);
      return -1 < t && this.requireFail.splice(t, 1), this
    },
    hasRequireFailures: function() {
      return 0 < this.requireFail.length
    },
    canRecognizeWith: function(e) {
      return !!this.simultaneous[e.id]
    },
    emit: function(t) {
      var n = this,
        e = this.state;

      function i(e) {
        n.manager.emit(e, t)
      }
      e < 8 && i(n.options.event + Ee(e)), i(n.options.event), t.additionalEvent && i(t.additionalEvent), 8 <= e && i(n.options.event + Ee(e))
    },
    tryEmit: function(e) {
      if (this.canEmit()) return this.emit(e);
      this.state = 32
    },
    canEmit: function() {
      for (var e = 0; e < this.requireFail.length;) {
        if (!(this.requireFail[e].state & (32 | je))) return !1;
        e++
      }
      return !0
    },
    recognize: function(e) {
      var t = s({}, e);
      if (!b(this.options.enable, [this, t])) return this.reset(), void(this.state = 32);
      56 & this.state && (this.state = je), this.state = this.process(t), 30 & this.state && this.tryEmit(t)
    },
    process: function(e) {},
    getTouchAction: function() {},
    reset: function() {}
  }, p(Me, Pe, {
    defaults: {
      pointers: 1
    },
    attrTest: function(e) {
      var t = this.options.pointers;
      return 0 === t || e.pointers.length === t
    },
    process: function(e) {
      var t = this.state,
        n = e.eventType,
        i = 6 & t,
        o = this.attrTest(e);
      return i && (n & N || !o) ? 16 | t : i || o ? n & R ? 8 | t : 2 & t ? 4 | t : 2 : 32
    }
  }), p(Ie, Me, {
    defaults: {
      event: "pan",
      threshold: 10,
      pointers: 1,
      direction: U
    },
    getTouchAction: function() {
      var e = this.options.direction,
        t = [];
      return e & Y && t.push(Te), e & V && t.push(_e), t
    },
    directionTest: function(e) {
      var t = this.options,
        n = !0,
        i = e.distance,
        o = e.direction,
        r = e.deltaX,
        s = e.deltaY;
      return o & t.direction || (t.direction & Y ? (o = 0 === r ? F : r < 0 ? B : z, n = r != this.pX, i = Math.abs(e.deltaX)) : (o = 0 === s ? F : s < 0 ? q : W, n = s != this.pY, i = Math.abs(e.deltaY))), e.direction = o, n && i > t.threshold && o & t.direction
    },
    attrTest: function(e) {
      return Me.prototype.attrTest.call(this, e) && (2 & this.state || !(2 & this.state) && this.directionTest(e))
    },
    emit: function(e) {
      this.pX = e.deltaX, this.pY = e.deltaY;
      var t = De(e.direction);
      t && (e.additionalEvent = this.options.event + t), this._super.emit.call(this, e)
    }
  }), p(He, Me, {
    defaults: {
      event: "pinch",
      threshold: 0,
      pointers: 2
    },
    getTouchAction: function() {
      return [Ce]
    },
    attrTest: function(e) {
      return this._super.attrTest.call(this, e) && (Math.abs(e.scale - 1) > this.options.threshold || 2 & this.state)
    },
    emit: function(e) {
      if (1 !== e.scale) {
        var t = e.scale < 1 ? "in" : "out";
        e.additionalEvent = this.options.event + t
      }
      this._super.emit.call(this, e)
    }
  }), p(Le, Pe, {
    defaults: {
      event: "press",
      pointers: 1,
      time: 251,
      threshold: 9
    },
    getTouchAction: function() {
      return ["auto"]
    },
    process: function(e) {
      var t = this.options,
        n = e.pointers.length === t.pointers,
        i = e.distance < t.threshold,
        o = e.deltaTime > t.time;
      if (this._input = e, !i || !n || e.eventType & (R | N) && !o) this.reset();
      else if (e.eventType & L) this.reset(), this._timer = u(function() {
        this.state = 8, this.tryEmit()
      }, t.time, this);
      else if (e.eventType & R) return 8;
      return 32
    },
    reset: function() {
      clearTimeout(this._timer)
    },
    emit: function(e) {
      8 === this.state && (e && e.eventType & R ? this.manager.emit(this.options.event + "up", e) : (this._input.timeStamp = v(), this.manager.emit(this.options.event, this._input)))
    }
  }), p(Re, Me, {
    defaults: {
      event: "rotate",
      threshold: 0,
      pointers: 2
    },
    getTouchAction: function() {
      return [Ce]
    },
    attrTest: function(e) {
      return this._super.attrTest.call(this, e) && (Math.abs(e.rotation) > this.options.threshold || 2 & this.state)
    }
  }), p(Ne, Me, {
    defaults: {
      event: "swipe",
      threshold: 10,
      velocity: .3,
      direction: Y | V,
      pointers: 1
    },
    getTouchAction: function() {
      return Ie.prototype.getTouchAction.call(this)
    },
    attrTest: function(e) {
      var t, n = this.options.direction;
      return n & (Y | V) ? t = e.overallVelocity : n & Y ? t = e.overallVelocityX : n & V && (t = e.overallVelocityY), this._super.attrTest.call(this, e) && n & e.offsetDirection && e.distance > this.options.threshold && e.maxPointers == this.options.pointers && m(t) > this.options.velocity && e.eventType & R
    },
    emit: function(e) {
      var t = De(e.offsetDirection);
      t && this.manager.emit(this.options.event + t, e), this.manager.emit(this.options.event, e)
    }
  }), p(Fe, Pe, {
    defaults: {
      event: "tap",
      pointers: 1,
      taps: 1,
      interval: 300,
      time: 250,
      threshold: 9,
      posThreshold: 10
    },
    getTouchAction: function() {
      return [ke]
    },
    process: function(e) {
      var t = this.options,
        n = e.pointers.length === t.pointers,
        i = e.distance < t.threshold,
        o = e.deltaTime < t.time;
      if (this.reset(), e.eventType & L && 0 === this.count) return this.failTimeout();
      if (i && o && n) {
        if (e.eventType != R) return this.failTimeout();
        var r = !this.pTime || e.timeStamp - this.pTime < t.interval,
          s = !this.pCenter || ne(this.pCenter, e.center) < t.posThreshold;
        if (this.pTime = e.timeStamp, this.pCenter = e.center, s && r ? this.count += 1 : this.count = 1, this._input = e, 0 === this.count % t.taps) return this.hasRequireFailures() ? (this._timer = u(function() {
          this.state = 8, this.tryEmit()
        }, t.interval, this), 2) : 8
      }
      return 32
    },
    failTimeout: function() {
      return this._timer = u(function() {
        this.state = 32
      }, this.options.interval, this), 32
    },
    reset: function() {
      clearTimeout(this._timer)
    },
    emit: function() {
      8 == this.state && (this._input.tapCount = this.count, this.manager.emit(this.options.event, this._input))
    }
  }), Be.VERSION = "2.0.8", Be.defaults = {
    domEvents: !1,
    touchAction: "compute",
    enable: !0,
    inputTarget: null,
    inputClass: null,
    preset: [
      [Re, {
        enable: !1
      }],
      [He, {
          enable: !1
        },
        ["rotate"]
      ],
      [Ne, {
        direction: Y
      }],
      [Ie, {
          direction: Y
        },
        ["swipe"]
      ],
      [Fe],
      [Fe, {
          event: "doubletap",
          taps: 2
        },
        ["tap"]
      ],
      [Le]
    ],
    cssProps: {
      userSelect: "none",
      touchSelect: "none",
      touchCallout: "none",
      contentZooming: "none",
      userDrag: "none",
      tapHighlightColor: "rgba(0,0,0,0)"
    }
  };

  function ze(e, t) {
    var n;
    this.options = s({}, Be.defaults, t || {}), this.options.inputTarget = this.options.inputTarget || e, this.handlers = {}, this.session = {}, this.recognizers = [], this.oldCssProps = {}, this.element = e, this.input = new((n = this).options.inputClass || (O ? fe : M ? ve : D ? be : ae))(n, Z), this.touchAction = new Ae(this, this.options.touchAction), qe(this, !0), d(this.options.recognizers, function(e) {
      var t = this.add(new e[0](e[1]));
      e[2] && t.recognizeWith(e[2]), e[3] && t.requireFailure(e[3])
    }, this)
  }

  function qe(n, i) {
    var o, r = n.element;
    r.style && (d(n.options.cssProps, function(e, t) {
      o = j(r.style, t), i ? (n.oldCssProps[o] = r.style[o], r.style[o] = e) : r.style[o] = n.oldCssProps[o] || ""
    }), i || (n.oldCssProps = {}))
  }
  ze.prototype = {
    set: function(e) {
      return s(this.options, e), e.touchAction && this.touchAction.update(), e.inputTarget && (this.input.destroy(), this.input.target = e.inputTarget, this.input.init()), this
    },
    stop: function(e) {
      this.session.stopped = e ? 2 : 1
    },
    recognize: function(e) {
      var t = this.session;
      if (!t.stopped) {
        var n;
        this.touchAction.preventDefaults(e);
        var i = this.recognizers,
          o = t.curRecognizer;
        (!o || o && 8 & o.state) && (o = t.curRecognizer = null);
        for (var r = 0; r < i.length;) n = i[r], 2 === t.stopped || o && n != o && !n.canRecognizeWith(o) ? n.reset() : n.recognize(e), !o && 14 & n.state && (o = t.curRecognizer = n), r++
      }
    },
    get: function(e) {
      if (e instanceof Pe) return e;
      for (var t = this.recognizers, n = 0; n < t.length; n++)
        if (t[n].options.event == e) return t[n];
      return null
    },
    add: function(e) {
      if (i(e, "add", this)) return this;
      var t = this.get(e.options.event);
      return t && this.remove(t), this.recognizers.push(e), (e.manager = this).touchAction.update(), e
    },
    remove: function(e) {
      if (i(e, "remove", this)) return this;
      if (e = this.get(e)) {
        var t = this.recognizers,
          n = T(t, e); - 1 !== n && (t.splice(n, 1), this.touchAction.update())
      }
      return this
    },
    on: function(e, t) {
      if (e !== g && t !== g) {
        var n = this.handlers;
        return d(_(e), function(e) {
          n[e] = n[e] || [], n[e].push(t)
        }), this
      }
    },
    off: function(e, t) {
      if (e !== g) {
        var n = this.handlers;
        return d(_(e), function(e) {
          t ? n[e] && n[e].splice(T(n[e], t), 1) : delete n[e]
        }), this
      }
    },
    emit: function(e, t) {
      var n, i, o;
      this.options.domEvents && (n = e, i = t, (o = a.createEvent("Event")).initEvent(n, !0, !0), (o.gesture = i).target.dispatchEvent(o));
      var r = this.handlers[e] && this.handlers[e].slice();
      if (r && r.length) {
        t.type = e, t.preventDefault = function() {
          t.srcEvent.preventDefault()
        };
        for (var s = 0; s < r.length;) r[s](t), s++
      }
    },
    destroy: function() {
      this.element && qe(this, !1), this.handlers = {}, this.session = {}, this.input.destroy(), this.element = null
    }
  }, s(Be, {
    INPUT_START: L,
    INPUT_MOVE: 2,
    INPUT_END: R,
    INPUT_CANCEL: N,
    STATE_POSSIBLE: je,
    STATE_BEGAN: 2,
    STATE_CHANGED: 4,
    STATE_ENDED: 8,
    STATE_RECOGNIZED: 8,
    STATE_CANCELLED: 16,
    STATE_FAILED: 32,
    DIRECTION_NONE: F,
    DIRECTION_LEFT: B,
    DIRECTION_RIGHT: z,
    DIRECTION_UP: q,
    DIRECTION_DOWN: W,
    DIRECTION_HORIZONTAL: Y,
    DIRECTION_VERTICAL: V,
    DIRECTION_ALL: U,
    Manager: ze,
    Input: Q,
    TouchAction: Ae,
    TouchInput: ve,
    MouseInput: ae,
    PointerEventInput: fe,
    TouchMouseInput: be,
    SingleTouchInput: pe,
    Recognizer: Pe,
    AttrRecognizer: Me,
    Tap: Fe,
    Pan: Ie,
    Swipe: Ne,
    Pinch: He,
    Rotate: Re,
    Press: Le,
    on: w,
    off: S,
    each: d,
    merge: h,
    extend: f,
    assign: s,
    inherit: p,
    bindFn: y,
    prefixed: j
  }), (void 0 !== r ? r : "undefined" != typeof self ? self : {}).Hammer = Be, "function" == typeof define && define.amd ? define(function() {
    return Be
  }) : "undefined" != typeof module && module.exports ? module.exports = Be : r.Hammer = Be
}(window, document),
function(L) {
  "use strict";
  var e = {
    i18n: {
      bg: {
        months: ["Януари", "Февруари", "Март", "Април", "Май", "Юни", "Юли", "Август", "Септември", "Октомври", "Ноември", "Декември"],
        dayOfWeek: ["Нд", "Пн", "Вт", "Ср", "Чт", "Пт", "Сб"]
      },
      fa: {
        months: ["فروردین", "اردیبهشت", "خرداد", "تیر", "مرداد", "شهریور", "مهر", "آبان", "آذر", "دی", "بهمن", "اسفند"],
        dayOfWeek: ["یکشنبه", "دوشنبه", "سه شنبه", "چهارشنبه", "پنجشنبه", "جمعه", "شنبه"]
      },
      ru: {
        months: ["Январь", "Февраль", "Март", "Апрель", "Май", "Июнь", "Июль", "Август", "Сентябрь", "Октябрь", "Ноябрь", "Декабрь"],
        dayOfWeek: ["Вс", "Пн", "Вт", "Ср", "Чт", "Пт", "Сб"]
      },
      uk: {
        months: ["Січень", "Лютий", "Березень", "Квітень", "Травень", "Червень", "Липень", "Серпень", "Вересень", "Жовтень", "Листопад", "Грудень"],
        dayOfWeek: ["Ндл", "Пнд", "Втр", "Срд", "Чтв", "Птн", "Сбт"]
      },
      en: {
        months: ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"],
        dayOfWeek: ["Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"]
      },
      el: {
        months: ["Ιανουάριος", "Φεβρουάριος", "Μάρτιος", "Απρίλιος", "Μάιος", "Ιούνιος", "Ιούλιος", "Αύγουστος", "Σεπτέμβριος", "Οκτώβριος", "Νοέμβριος", "Δεκέμβριος"],
        dayOfWeek: ["Κυρ", "Δευ", "Τρι", "Τετ", "Πεμ", "Παρ", "Σαβ"]
      },
      de: {
        months: ["Januar", "Februar", "März", "April", "Mai", "Juni", "Juli", "August", "September", "Oktober", "November", "Dezember"],
        dayOfWeek: ["So", "Mo", "Di", "Mi", "Do", "Fr", "Sa"]
      },
      nl: {
        months: ["januari", "februari", "maart", "april", "mei", "juni", "juli", "augustus", "september", "oktober", "november", "december"],
        dayOfWeek: ["zo", "ma", "di", "wo", "do", "vr", "za"]
      },
      tr: {
        months: ["Ocak", "Şubat", "Mart", "Nisan", "Mayıs", "Haziran", "Temmuz", "Ağustos", "Eylül", "Ekim", "Kasım", "Aralık"],
        dayOfWeek: ["Paz", "Pts", "Sal", "Çar", "Per", "Cum", "Cts"]
      },
      fr: {
        months: ["Janvier", "Février", "Mars", "Avril", "Mai", "Juin", "Juillet", "Août", "Septembre", "Octobre", "Novembre", "Décembre"],
        dayOfWeek: ["Dim", "Lun", "Mar", "Mer", "Jeu", "Ven", "Sam"]
      },
      es: {
        months: ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"],
        dayOfWeek: ["Dom", "Lun", "Mar", "Mié", "Jue", "Vie", "Sáb"]
      },
      th: {
        months: ["มกราคม", "กุมภาพันธ์", "มีนาคม", "เมษายน", "พฤษภาคม", "มิถุนายน", "กรกฎาคม", "สิงหาคม", "กันยายน", "ตุลาคม", "พฤศจิกายน", "ธันวาคม"],
        dayOfWeek: ["อา.", "จ.", "อ.", "พ.", "พฤ.", "ศ.", "ส."]
      },
      pl: {
        months: ["styczeń", "luty", "marzec", "kwiecień", "maj", "czerwiec", "lipiec", "sierpień", "wrzesień", "październik", "listopad", "grudzień"],
        dayOfWeek: ["nd", "pn", "wt", "śr", "cz", "pt", "sb"]
      },
      pt: {
        months: ["Janeiro", "Fevereiro", "Março", "Abril", "Maio", "Junho", "Julho", "Agosto", "Setembro", "Outubro", "Novembro", "Dezembro"],
        dayOfWeek: ["Dom", "Seg", "Ter", "Qua", "Qui", "Sex", "Sab"]
      },
      ch: {
        months: ["一月", "二月", "三月", "四月", "五月", "六月", "七月", "八月", "九月", "十月", "十一月", "十二月"],
        dayOfWeek: ["日", "一", "二", "三", "四", "五", "六"]
      },
      se: {
        months: ["Januari", "Februari", "Mars", "April", "Maj", "Juni", "Juli", "Augusti", "September", "Oktober", "November", "December"],
        dayOfWeek: ["Sön", "Mån", "Tis", "Ons", "Tor", "Fre", "Lör"]
      },
      kr: {
        months: ["1월", "2월", "3월", "4월", "5월", "6월", "7월", "8월", "9월", "10월", "11월", "12월"],
        dayOfWeek: ["일", "월", "화", "수", "목", "금", "토"]
      },
      it: {
        months: ["Gennaio", "Febbraio", "Marzo", "Aprile", "Maggio", "Giugno", "Luglio", "Agosto", "Settembre", "Ottobre", "Novembre", "Dicembre"],
        dayOfWeek: ["Dom", "Lun", "Mar", "Mer", "Gio", "Ven", "Sab"]
      },
      da: {
        months: ["January", "Februar", "Marts", "April", "Maj", "Juni", "July", "August", "September", "Oktober", "November", "December"],
        dayOfWeek: ["Søn", "Man", "Tir", "Ons", "Tor", "Fre", "Lør"]
      },
      no: {
        months: ["Januar", "Februar", "Mars", "April", "Mai", "Juni", "Juli", "August", "September", "Oktober", "November", "Desember"],
        dayOfWeek: ["Søn", "Man", "Tir", "Ons", "Tor", "Fre", "Lør"]
      },
      ja: {
        months: ["1月", "2月", "3月", "4月", "5月", "6月", "7月", "8月", "9月", "10月", "11月", "12月"],
        dayOfWeek: ["日", "月", "火", "水", "木", "金", "土"]
      },
      vi: {
        months: ["Tháng 1", "Tháng 2", "Tháng 3", "Tháng 4", "Tháng 5", "Tháng 6", "Tháng 7", "Tháng 8", "Tháng 9", "Tháng 10", "Tháng 11", "Tháng 12"],
        dayOfWeek: ["CN", "T2", "T3", "T4", "T5", "T6", "T7"]
      },
      sl: {
        months: ["Januar", "Februar", "Marec", "April", "Maj", "Junij", "Julij", "Avgust", "September", "Oktober", "November", "December"],
        dayOfWeek: ["Ned", "Pon", "Tor", "Sre", "Čet", "Pet", "Sob"]
      },
      cs: {
        months: ["Leden", "Únor", "Březen", "Duben", "Květen", "Červen", "Červenec", "Srpen", "Září", "Říjen", "Listopad", "Prosinec"],
        dayOfWeek: ["Ne", "Po", "Út", "St", "Čt", "Pá", "So"]
      },
      hu: {
        months: ["Január", "Február", "Március", "Április", "Május", "Június", "Július", "Augusztus", "Szeptember", "Október", "November", "December"],
        dayOfWeek: ["Va", "Hé", "Ke", "Sze", "Cs", "Pé", "Szo"]
      }
    },
    value: "",
    lang: "en",
    format: "Y/m/d H:i",
    formatTime: "H:i",
    formatDate: "Y/m/d",
    startDate: !1,
    step: 60,
    monthChangeSpinner: !0,
    closeOnDateSelect: !1,
    closeOnWithoutClick: !0,
    closeOnInputClick: !0,
    timepicker: !0,
    datepicker: !0,
    weeks: !1,
    defaultTime: !1,
    defaultDate: !1,
    minDate: !1,
    maxDate: !1,
    minTime: !1,
    maxTime: !1,
    allowTimes: [],
    opened: !1,
    initTime: !0,
    inline: !1,
    onSelectDate: function() {},
    onSelectTime: function() {},
    onChangeMonth: function() {},
    onChangeDateTime: function() {},
    onShow: function() {},
    onClose: function() {},
    onGenerate: function() {},
    withoutCopyright: !0,
    inverseButton: !1,
    hours12: !1,
    next: "xdsoft_next",
    prev: "xdsoft_prev",
    dayOfWeekStart: 0,
    timeHeightInTimePicker: 25,
    timepickerScrollbar: !0,
    todayButton: !0,
    defaultSelect: !0,
    scrollMonth: !0,
    scrollTime: !0,
    scrollInput: !0,
    lazyInit: !1,
    mask: !1,
    validateOnBlur: !0,
    allowBlank: !0,
    yearStart: 1950,
    yearEnd: 2050,
    style: "",
    id: "",
    fixed: !1,
    roundTime: "round",
    className: "",
    weekends: [],
    yearOffset: 0,
    beforeShowDay: null
  };
  Array.prototype.indexOf || (Array.prototype.indexOf = function(e, t) {
    for (var n = t || 0, i = this.length; n < i; n++)
      if (this[n] === e) return n;
    return -1
  }), Date.prototype.countDaysInMonth = function() {
    return new Date(this.getFullYear(), this.getMonth() + 1, 0).getDate()
  }, L.fn.xdsoftScroller = function(f) {
    return this.each(function() {
      var r = L(this);
      if (!L(this).hasClass("xdsoft_scroller_box")) {
        var i = function(e) {
            var t = {
              x: 0,
              y: 0
            };
            if ("touchstart" == e.type || "touchmove" == e.type || "touchend" == e.type || "touchcancel" == e.type) {
              var n = e.originalEvent.touches[0] || e.originalEvent.changedTouches[0];
              t.x = n.pageX, t.y = n.pageY
            } else "mousedown" != e.type && "mouseup" != e.type && "mousemove" != e.type && "mouseover" != e.type && "mouseout" != e.type && "mouseenter" != e.type && "mouseleave" != e.type || (t.x = e.pageX, t.y = e.pageY);
            return t
          },
          t = 0,
          s = r.children().eq(0),
          a = r[0].clientHeight,
          l = s[0].offsetHeight,
          c = L('<div class="xdsoft_scrollbar"></div>'),
          u = L('<div class="xdsoft_scroller"></div>'),
          d = 100,
          o = !1;
        c.append(u), r.addClass("xdsoft_scroller_box").append(c), u.on("mousedown.xdsoft_scroller", function(e) {
          a || r.trigger("resize_scroll.xdsoft_scroller", [f]);
          var n = e.pageY,
            i = parseInt(u.css("margin-top")),
            o = c[0].offsetHeight;
          L(document.body).addClass("xdsoft_noselect"), L([document.body, window]).on("mouseup.xdsoft_scroller", function e() {
            L([document.body, window]).off("mouseup.xdsoft_scroller", e).off("mousemove.xdsoft_scroller", t).removeClass("xdsoft_noselect")
          }), L(document.body).on("mousemove.xdsoft_scroller", t = function(e) {
            var t = e.pageY - n + i;
            t < 0 && (t = 0), t + u[0].offsetHeight > o && (t = o - u[0].offsetHeight), r.trigger("scroll_element.xdsoft_scroller", [d ? t / d : 0])
          })
        }), r.on("scroll_element.xdsoft_scroller", function(e, t) {
          a || r.trigger("resize_scroll.xdsoft_scroller", [t, !0]), t = 1 < t ? 1 : t < 0 || isNaN(t) ? 0 : t, u.css("margin-top", d * t), s.css("marginTop", -parseInt((l - a) * t))
        }).on("resize_scroll.xdsoft_scroller", function(e, t, n) {
          a = r[0].clientHeight, l = s[0].offsetHeight;
          var i = a / l,
            o = i * c[0].offsetHeight;
          1 < i ? u.hide() : (u.show(), u.css("height", parseInt(10 < o ? o : 10)), d = c[0].offsetHeight - u[0].offsetHeight, !0 !== n && r.trigger("scroll_element.xdsoft_scroller", [t || Math.abs(parseInt(s.css("marginTop"))) / (l - a)]))
        }), r.mousewheel && r.mousewheel(function(e, t, n, i) {
          var o = Math.abs(parseInt(s.css("marginTop")));
          return r.trigger("scroll_element.xdsoft_scroller", [(o - 20 * t) / (l - a)]), e.stopPropagation(), !1
        }), r.on("touchstart", function(e) {
          o = i(e)
        }), r.on("touchmove", function(e) {
          if (o) {
            var t = i(e),
              n = Math.abs(parseInt(s.css("marginTop")));
            r.trigger("scroll_element.xdsoft_scroller", [(n - (t.y - o.y)) / (l - a)]), e.stopPropagation(), e.preventDefault(), o = i(e)
          }
        }), r.on("touchend touchcancel", function(e) {
          o = !1
        })
      }
      r.trigger("resize_scroll.xdsoft_scroller", [f])
    })
  }, L.fn.datetimepicker = function(o) {
    var p = 17,
      g = 13,
      m = 27,
      C = 37,
      _ = 38,
      T = 39,
      $ = 40,
      A = 9,
      j = 116,
      P = 65,
      E = 67,
      D = 86,
      O = 90,
      M = 89,
      I = !1,
      H = L.isPlainObject(o) || !o ? L.extend(!0, {}, e, o) : L.extend({}, e),
      r = 0,
      s = function(s) {
        var v = L("<div " + (H.id ? 'id="' + H.id + '"' : "") + " " + (H.style ? 'style="' + H.style + '"' : "") + ' class="xdsoft_datetimepicker xdsoft_noselect ' + (H.weeks ? " xdsoft_showweeks" : "") + H.className + '"></div>'),
          e = L('<div class="xdsoft_copyright"><a target="_blank" href="http://xdsoft.net/jqplugins/datetimepicker/">xdsoft.net</a></div>'),
          o = L('<div class="xdsoft_datepicker active"></div>'),
          y = L('<div class="xdsoft_mounthpicker"><button type="button" class="xdsoft_prev"></button><button type="button" class="xdsoft_today_button"></button><div class="xdsoft_label xdsoft_month"><span></span></div><div class="xdsoft_label xdsoft_year"><span></span></div><button type="button" class="xdsoft_next"></button></div>'),
          b = L('<div class="xdsoft_calendar"></div>'),
          t = L('<div class="xdsoft_timepicker active"><button type="button" class="xdsoft_prev"></button><div class="xdsoft_time_box"></div><button type="button" class="xdsoft_next"></button></div>'),
          c = t.find(".xdsoft_time_box").eq(0),
          x = L('<div class="xdsoft_time_variant"></div>'),
          n = L('<div class="xdsoft_scrollbar"></div>'),
          w = (L('<div class="xdsoft_scroller"></div>'), L('<div class="xdsoft_select xdsoft_monthselect"><div></div></div>')),
          S = L('<div class="xdsoft_select xdsoft_yearselect"><div></div></div>');
        y.find(".xdsoft_month span").after(w), y.find(".xdsoft_year span").after(S), y.find(".xdsoft_month,.xdsoft_year").on("mousedown.xdsoft", function(e) {
          y.find(".xdsoft_select").hide();
          var t = L(this).find(".xdsoft_select").eq(0),
            n = 0,
            i = 0;
          k.currentTime && (n = k.currentTime[L(this).hasClass("xdsoft_month") ? "getMonth" : "getFullYear"]()), t.show();
          for (var o = t.find("div.xdsoft_option"), r = 0; r < o.length && o.eq(r).data("value") != n; r++) i += o[0].offsetHeight;
          return t.xdsoftScroller(i / (t.children()[0].offsetHeight - t[0].clientHeight)), e.stopPropagation(), !1
        }), y.find(".xdsoft_select").xdsoftScroller().on("mousedown.xdsoft", function(e) {
          e.stopPropagation(), e.preventDefault()
        }).on("mousedown.xdsoft", ".xdsoft_option", function(e) {
          k && k.currentTime && k.currentTime[L(this).parent().parent().hasClass("xdsoft_monthselect") ? "setMonth" : "setFullYear"](L(this).data("value")), L(this).parent().parent().hide(), v.trigger("xchange.xdsoft"), H.onChangeMonth && H.onChangeMonth.call && H.onChangeMonth.call(v, k.currentTime, v.data("input"))
        }), v.setOptions = function(e) {
          if (H = L.extend(!0, {}, H, e), e.allowTimes && L.isArray(e.allowTimes) && e.allowTimes.length && (H.allowTimes = L.extend(!0, [], e.allowTimes)), e.weekends && L.isArray(e.weekends) && e.weekends.length && (H.weekends = L.extend(!0, [], e.weekends)), !H.open && !H.opened || H.inline || s.trigger("open.xdsoft"), H.inline && (l = !0, v.addClass("xdsoft_inline"), s.after(v).hide()), H.inverseButton && (H.next = "xdsoft_prev", H.prev = "xdsoft_next"), H.datepicker ? o.addClass("active") : o.removeClass("active"), H.timepicker ? t.addClass("active") : t.removeClass("active"), H.value && (s && s.val && s.val(H.value), k.setCurrentTime(H.value)), isNaN(H.dayOfWeekStart) ? H.dayOfWeekStart = 0 : H.dayOfWeekStart = parseInt(H.dayOfWeekStart) % 7, H.timepickerScrollbar || n.hide(), H.minDate && /^-(.*)$/.test(H.minDate) && (H.minDate = k.strToDateTime(H.minDate).dateFormat(H.formatDate)), H.maxDate && /^\+(.*)$/.test(H.maxDate) && (H.maxDate = k.strToDateTime(H.maxDate).dateFormat(H.formatDate)), y.find(".xdsoft_today_button").css("visibility", H.todayButton ? "visible" : "hidden"), H.mask) {
            var r = function(e, t) {
              var n = e.replace(/([\[\]\/\{\}\(\)\-\.\+]{1})/g, "\\$1").replace(/_/g, "{digit+}").replace(/([0-9]{1})/g, "{digit$1}").replace(/\{digit([0-9]{1})\}/g, "[0-$1_]{1}").replace(/\{digit[\+]\}/g, "[0-9_]{1}");
              return RegExp(n).test(t)
            };
            switch (s.off("keydown.xdsoft"), !0) {
              case !0 === H.mask:
                H.mask = H.format.replace(/Y/g, "9999").replace(/F/g, "9999").replace(/m/g, "19").replace(/d/g, "39").replace(/H/g, "29").replace(/i/g, "59").replace(/s/g, "59");
              case "string" == L.type(H.mask):
                r(H.mask, s.val()) || s.val(H.mask.replace(/[0-9]/g, "_")), s.on("keydown.xdsoft", function(e) {
                  var t = this.value,
                    n = e.which;
                  switch (!0) {
                    case 48 <= n && n <= 57 || 96 <= n && n <= 105 || 8 == n || 46 == n:
                      var i = function(e) {
                          try {
                            if (document.selection && document.selection.createRange) return document.selection.createRange().getBookmark().charCodeAt(2) - 2;
                            if (e.setSelectionRange) return e.selectionStart
                          } catch (e) {
                            return 0
                          }
                        }(this),
                        o = 8 != n && 46 != n ? String.fromCharCode(96 <= n && n <= 105 ? n - 48 : n) : "_";
                      for (8 != n && 46 != n || !i || (i--, o = "_");
                        /[^0-9_]/.test(H.mask.substr(i, 1)) && i < H.mask.length && 0 < i;) i += 8 == n || 46 == n ? -1 : 1;
                      if (t = t.substr(0, i) + o + t.substr(i + 1), "" == L.trim(t)) t = H.mask.replace(/[0-9]/g, "_");
                      else if (i == H.mask.length) break;
                      for (i += 8 == n || 46 == n ? 0 : 1;
                        /[^0-9_]/.test(H.mask.substr(i, 1)) && i < H.mask.length && 0 < i;) i += 8 == n || 46 == n ? -1 : 1;
                      r(H.mask, t) ? (this.value = t, function(e, t) {
                        if (!(e = "string" == typeof e || e instanceof String ? document.getElementById(e) : e)) return;
                        if (e.createTextRange) {
                          var n = e.createTextRange();
                          return n.collapse(!0), n.moveEnd(t), n.moveStart(t), n.select()
                        }!!e.setSelectionRange && e.setSelectionRange(t, t)
                      }(this, i)) : "" == L.trim(t) ? this.value = H.mask.replace(/[0-9]/g, "_") : s.trigger("error_input.xdsoft");
                      break;
                    case !!~[P, E, D, O, M].indexOf(n) && I:
                    case !!~[m, _, $, C, T, j, p, A, g].indexOf(n):
                      return !0
                  }
                  return e.preventDefault(), !1
                })
            }
          }
          H.validateOnBlur && s.off("blur.xdsoft").on("blur.xdsoft", function() {
            H.allowBlank && !L.trim(L(this).val()).length ? (L(this).val(null), v.data("xdsoft_datetime").empty()) : (Date.parseDate(L(this).val(), H.format) || L(this).val(k.now().dateFormat(H.format)), v.data("xdsoft_datetime").setCurrentTime(L(this).val())), v.trigger("changedatetime.xdsoft")
          }), H.dayOfWeekStartPrev = 0 == H.dayOfWeekStart ? 6 : H.dayOfWeekStart - 1, v.trigger("xchange.xdsoft").trigger("afterOpen.xdsoft")
        }, v.data("options", H).on("mousedown.xdsoft", function(e) {
          return e.stopPropagation(), e.preventDefault(), S.hide(), w.hide(), !1
        });
        var i = t.find(".xdsoft_time_box");
        i.append(x), i.xdsoftScroller(), v.on("afterOpen.xdsoft", function() {
          i.xdsoftScroller()
        }), v.append(o).append(t), !0 !== H.withoutCopyright && v.append(e), o.append(y).append(b), L("body").append(v);
        var k = new function() {
          var o = this;
          o.now = function(e) {
            var t = new Date;
            if (!e && H.defaultDate) {
              var n = o.strToDate(H.defaultDate);
              t.setFullYear(n.getFullYear()), t.setMonth(n.getMonth()), t.setDate(n.getDate())
            }
            if (H.yearOffset && t.setFullYear(t.getFullYear() + H.yearOffset), !e && H.defaultTime) {
              var i = o.strtotime(H.defaultTime);
              t.setHours(i.getHours()), t.setMinutes(i.getMinutes())
            }
            return t
          }, o.isValidDate = function(e) {
            return "[object Date]" === Object.prototype.toString.call(e) && !isNaN(e.getTime())
          }, o.setCurrentTime = function(e) {
            o.currentTime = "string" == typeof e ? o.strToDateTime(e) : o.isValidDate(e) ? e : o.now(), v.trigger("xchange.xdsoft")
          }, o.empty = function() {
            o.currentTime = null
          }, o.getCurrentTime = function(e) {
            return o.currentTime
          }, o.nextMonth = function() {
            var e = o.currentTime.getMonth() + 1;
            return 12 == e && (o.currentTime.setFullYear(o.currentTime.getFullYear() + 1), e = 0), o.currentTime.setDate(Math.min(Date.daysInMonth[e], o.currentTime.getDate())), o.currentTime.setMonth(e), H.onChangeMonth && H.onChangeMonth.call && H.onChangeMonth.call(v, k.currentTime, v.data("input")), v.trigger("xchange.xdsoft"), e
          }, o.prevMonth = function() {
            var e = o.currentTime.getMonth() - 1;
            return -1 == e && (o.currentTime.setFullYear(o.currentTime.getFullYear() - 1), e = 11), o.currentTime.setDate(Math.min(Date.daysInMonth[e], o.currentTime.getDate())), o.currentTime.setMonth(e), H.onChangeMonth && H.onChangeMonth.call && H.onChangeMonth.call(v, k.currentTime, v.data("input")), v.trigger("xchange.xdsoft"), e
          }, o.strToDateTime = function(e) {
            if (e && e instanceof Date && o.isValidDate(e)) return e;
            var t, n, i = [];
            return (i = /^(\+|\-)(.*)$/.exec(e)) && (i[2] = Date.parseDate(i[2], H.formatDate)) ? (t = i[2].getTime() - 6e4 * i[2].getTimezoneOffset(), n = new Date(k.now().getTime() + parseInt(i[1] + "1") * t)) : n = e ? Date.parseDate(e, H.format) : o.now(), o.isValidDate(n) || (n = o.now()), n
          }, o.strToDate = function(e) {
            if (e && e instanceof Date && o.isValidDate(e)) return e;
            var t = e ? Date.parseDate(e, H.formatDate) : o.now(!0);
            return o.isValidDate(t) || (t = o.now(!0)), t
          }, o.strtotime = function(e) {
            if (e && e instanceof Date && o.isValidDate(e)) return e;
            var t = e ? Date.parseDate(e, H.formatTime) : o.now();
            return o.isValidDate(t) || (t = o.now(!0)), t
          }, o.str = function() {
            return o.currentTime.dateFormat(H.format)
          }, o.currentTime = this.now()
        };
        y.find(".xdsoft_today_button").on("mousedown.xdsoft", function() {
          v.data("changed", !0), k.setCurrentTime(0), v.trigger("afterOpen.xdsoft")
        }).on("dblclick.xdsoft", function() {
          s.val(k.str()), v.trigger("close.xdsoft")
        }), y.find(".xdsoft_prev,.xdsoft_next").on("mousedown.xdsoft", function() {
          var n = L(this),
            i = 0,
            o = !1;
          ! function e(t) {
            k.currentTime.getMonth();
            n.hasClass(H.next) ? k.nextMonth() : n.hasClass(H.prev) && k.prevMonth(), H.monthChangeSpinner && !o && (i = setTimeout(e, t || 100))
          }(500), L([document.body, window]).on("mouseup.xdsoft", function e() {
            clearTimeout(i), o = !0, L([document.body, window]).off("mouseup.xdsoft", e)
          })
        }), t.find(".xdsoft_prev,.xdsoft_next").on("mousedown.xdsoft", function() {
          var r = L(this),
            s = 0,
            a = !1,
            l = 110;
          ! function e(t) {
            var n = c[0].clientHeight,
              i = x[0].offsetHeight,
              o = Math.abs(parseInt(x.css("marginTop")));
            r.hasClass(H.next) && i - n - H.timeHeightInTimePicker >= o ? x.css("marginTop", "-" + (o + H.timeHeightInTimePicker) + "px") : r.hasClass(H.prev) && 0 <= o - H.timeHeightInTimePicker && x.css("marginTop", "-" + (o - H.timeHeightInTimePicker) + "px"), c.trigger("scroll_element.xdsoft_scroller", [Math.abs(parseInt(x.css("marginTop")) / (i - n))]), l = 10 < l ? 10 : l - 10, !a && (s = setTimeout(e, t || l))
          }(500), L([document.body, window]).on("mouseup.xdsoft", function e() {
            clearTimeout(s), a = !0, L([document.body, window]).off("mouseup.xdsoft", e)
          })
        });
        var r = 0;
        v.on("xchange.xdsoft", function(e) {
          clearTimeout(r), r = setTimeout(function() {
            void 0 === k.currentTime && (k.currentTime = new Date);
            for (var e = "", t = new Date(k.currentTime.getFullYear(), k.currentTime.getMonth(), 1, 12, 0, 0), n = 0, i = k.now(); t.getDay() != H.dayOfWeekStart;) t.setDate(t.getDate() - 1);
            e += "<table><thead><tr>", H.weeks && (e += "<th></th>");
            for (var o = 0; o < 7; o++) e += "<th>" + H.i18n[H.lang].dayOfWeek[(o + H.dayOfWeekStart) % 7] + "</th>";
            e += "</tr></thead>", e += "<tbody>";
            var r = !1,
              s = !1;
            !1 !== H.maxDate && (r = k.strToDate(H.maxDate), r = new Date(r.getFullYear(), r.getMonth(), r.getDate(), 23, 59, 59, 999)), !1 !== H.minDate && (s = k.strToDate(H.minDate), s = new Date(s.getFullYear(), s.getMonth(), s.getDate()));
            for (var a, l, c, u, d = [], f = !0; n < k.currentTime.countDaysInMonth() || t.getDay() != H.dayOfWeekStart || k.currentTime.getMonth() == t.getMonth();) d = [], n++, a = t.getDate(), l = t.getFullYear(), p = t.getMonth(), c = t.getWeekOfYear(), d.push("xdsoft_date"), u = H.beforeShowDay && H.beforeShowDay.call ? H.beforeShowDay.call(v, t) : null, (!1 !== r && r < t || !1 !== s && t < s || u && !1 === u[0]) && d.push("xdsoft_disabled"), u && "" != u[1] && d.push(u[1]), k.currentTime.getMonth() != p && d.push("xdsoft_other_month"), (H.defaultSelect || v.data("changed")) && k.currentTime.dateFormat(H.formatDate) == t.dateFormat(H.formatDate) && d.push("xdsoft_current"), i.dateFormat(H.formatDate) == t.dateFormat(H.formatDate) && d.push("xdsoft_today"), (0 == t.getDay() || 6 == t.getDay() || ~H.weekends.indexOf(t.dateFormat(H.formatDate))) && d.push("xdsoft_weekend"), H.beforeShowDay && "function" == typeof H.beforeShowDay && d.push(H.beforeShowDay(t)), f && (e += "<tr>", f = !1, H.weeks && (e += "<th>" + c + "</th>")), e += '<td data-date="' + a + '" data-month="' + p + '" data-year="' + l + '" class="xdsoft_date xdsoft_day_of_week' + t.getDay() + " " + d.join(" ") + '"><div>' + a + "</div></td>", t.getDay() == H.dayOfWeekStartPrev && (e += "</tr>", f = !0), t.setDate(a + 1);
            e += "</tbody></table>", b.html(e), y.find(".xdsoft_label span").eq(0).text(H.i18n[H.lang].months[k.currentTime.getMonth()]), y.find(".xdsoft_label span").eq(1).text(k.currentTime.getFullYear());
            var h = "",
              p = "",
              g = function(e, t) {
                var n = k.now();
                n.setHours(e), e = parseInt(n.getHours()), n.setMinutes(t), t = parseInt(n.getMinutes()), (!(d = []) !== H.maxTime && k.strtotime(H.maxTime).getTime() < n.getTime() || !1 !== H.minTime && k.strtotime(H.minTime).getTime() > n.getTime()) && d.push("xdsoft_disabled"), (H.initTime || H.defaultSelect || v.data("changed")) && parseInt(k.currentTime.getHours()) == parseInt(e) && (59 < H.step || Math[H.roundTime](k.currentTime.getMinutes() / H.step) * H.step == parseInt(t)) && (H.defaultSelect || v.data("changed") ? d.push("xdsoft_current") : H.initTime && d.push("xdsoft_init_time")), parseInt(i.getHours()) == parseInt(e) && parseInt(i.getMinutes()) == parseInt(t) && d.push("xdsoft_today"), h += '<div class="xdsoft_time ' + d.join(" ") + '" data-hour="' + e + '" data-minute="' + t + '">' + n.dateFormat(H.formatTime) + "</div>"
              };
            if (H.allowTimes && L.isArray(H.allowTimes) && H.allowTimes.length)
              for (n = 0; n < H.allowTimes.length; n++) g(k.strtotime(H.allowTimes[n]).getHours(), p = k.strtotime(H.allowTimes[n]).getMinutes());
            else {
              var n = 0;
              for (o = 0; n < (H.hours12 ? 12 : 24); n++)
                for (o = 0; o < 60; o += H.step) g((n < 10 ? "0" : "") + n, p = (o < 10 ? "0" : "") + o)
            }
            x.html(h);
            var m = "";
            n = 0;
            for (n = parseInt(H.yearStart, 10) + H.yearOffset; n <= parseInt(H.yearEnd, 10) + H.yearOffset; n++) m += '<div class="xdsoft_option ' + (k.currentTime.getFullYear() == n ? "xdsoft_current" : "") + '" data-value="' + n + '">' + n + "</div>";
            for (S.children().eq(0).html(m), n = 0, m = ""; n <= 11; n++) m += '<div class="xdsoft_option ' + (k.currentTime.getMonth() == n ? "xdsoft_current" : "") + '" data-value="' + n + '">' + H.i18n[H.lang].months[n] + "</div>";
            w.children().eq(0).html(m), L(v).trigger("generate.xdsoft")
          }, 10), e.stopPropagation()
        }).on("afterOpen.xdsoft", function() {
          var e;
          if (H.timepicker)
            if (x.find(".xdsoft_current").length ? e = ".xdsoft_current" : x.find(".xdsoft_init_time").length && (e = ".xdsoft_init_time"), e) {
              var t = c[0].clientHeight,
                n = x[0].offsetHeight,
                i = x.find(e).index() * H.timeHeightInTimePicker + 1;
              n - t < i && (i = n - t), c.trigger("scroll_element.xdsoft_scroller", [parseInt(i) / (n - t)])
            } else c.trigger("scroll_element.xdsoft_scroller", [0])
        });
        var a = 0;
        b.on("click.xdsoft", "td", function(e) {
          e.stopPropagation(), a++;
          var t = L(this),
            n = k.currentTime;
          if (null == n && (k.currentTime = k.now(), n = k.currentTime), t.hasClass("xdsoft_disabled")) return !1;
          n.setDate(1), n.setFullYear(t.data("year")), n.setMonth(t.data("month")), n.setDate(t.data("date")), v.trigger("select.xdsoft", [n]), s.val(k.str()), (1 < a || !0 === H.closeOnDateSelect || 0 === H.closeOnDateSelect && !H.timepicker) && !H.inline && v.trigger("close.xdsoft"), H.onSelectDate && H.onSelectDate.call && H.onSelectDate.call(v, k.currentTime, v.data("input")), v.data("changed", !0), v.trigger("xchange.xdsoft"), v.trigger("changedatetime.xdsoft"), setTimeout(function() {
            a = 0
          }, 200)
        }), x.on("click.xdsoft", "div", function(e) {
          e.stopPropagation();
          var t = L(this),
            n = k.currentTime;
          if (null == n && (k.currentTime = k.now(), n = k.currentTime), t.hasClass("xdsoft_disabled")) return !1;
          n.setHours(t.data("hour")), n.setMinutes(t.data("minute")), v.trigger("select.xdsoft", [n]), v.data("input").val(k.str()), !H.inline && v.trigger("close.xdsoft"), H.onSelectTime && H.onSelectTime.call && H.onSelectTime.call(v, k.currentTime, v.data("input")), v.data("changed", !0), v.trigger("xchange.xdsoft"), v.trigger("changedatetime.xdsoft")
        }), v.mousewheel && o.mousewheel(function(e, t, n, i) {
          return !H.scrollMonth || (t < 0 ? k.nextMonth() : k.prevMonth(), !1)
        }), v.mousewheel && c.unmousewheel().mousewheel(function(e, t, n, i) {
          if (!H.scrollTime) return !0;
          var o = c[0].clientHeight,
            r = x[0].offsetHeight,
            s = Math.abs(parseInt(x.css("marginTop"))),
            a = !0;
          return t < 0 && r - o - H.timeHeightInTimePicker >= s ? (x.css("marginTop", "-" + (s + H.timeHeightInTimePicker) + "px"), a = !1) : 0 < t && 0 <= s - H.timeHeightInTimePicker && (x.css("marginTop", "-" + (s - H.timeHeightInTimePicker) + "px"), a = !1), c.trigger("scroll_element.xdsoft_scroller", [Math.abs(parseInt(x.css("marginTop")) / (r - o))]), e.stopPropagation(), a
        });
        var l = !1;
        v.on("changedatetime.xdsoft", function() {
          if (H.onChangeDateTime && H.onChangeDateTime.call) {
            var e = v.data("input");
            H.onChangeDateTime.call(v, k.currentTime, e), delete H.value, e.trigger("change")
          }
        }).on("generate.xdsoft", function() {
          H.onGenerate && H.onGenerate.call && H.onGenerate.call(v, k.currentTime, v.data("input")), l && (v.trigger("afterOpen.xdsoft"), l = !1)
        }).on("click.xdsoft", function(e) {
          e.stopPropagation()
        });
        var u = 0;
        s.mousewheel && s.mousewheel(function(e, t, n, i) {
          return !H.scrollInput || (!H.datepicker && H.timepicker ? (0 <= (u = x.find(".xdsoft_current").length ? x.find(".xdsoft_current").eq(0).index() : 0) + t && u + t < x.children().length && (u += t), x.children().eq(u).length && x.children().eq(u).trigger("mousedown"), !1) : H.datepicker && !H.timepicker ? (o.trigger(e, [t, n, i]), s.val && s.val(k.str()), v.trigger("changedatetime.xdsoft"), !1) : void 0)
        });
        var d = function() {
          var e = v.data("input").offset(),
            t = e.top + v.data("input")[0].offsetHeight - 1,
            n = e.left,
            i = "absolute";
          H.fixed ? (t -= L(window).scrollTop(), n -= L(window).scrollLeft(), i = "fixed") : (t + v[0].offsetHeight > L(window).height() + L(window).scrollTop() && (t = e.top - v[0].offsetHeight + 1), t < 0 && (t = 0), n + v[0].offsetWidth > L(window).width() && (n = e.left - v[0].offsetWidth + v.data("input")[0].offsetWidth)), v.css({
            left: n,
            top: t,
            position: i
          })
        };
        v.on("open.xdsoft", function() {
          var e = !0;
          H.onShow && H.onShow.call && (e = H.onShow.call(v, k.currentTime, v.data("input"))), !1 !== e && (v.show(), d(), L(window).off("resize.xdsoft", d).on("resize.xdsoft", d), H.closeOnWithoutClick && L([document.body, window]).on("mousedown.xdsoft", function e() {
            v.trigger("close.xdsoft"), L([document.body, window]).off("mousedown.xdsoft", e)
          }))
        }).on("close.xdsoft", function(e) {
          var t = !0;
          H.onClose && H.onClose.call && (t = H.onClose.call(v, k.currentTime, v.data("input"))), !1 === t || H.opened || H.inline || v.hide(), e.stopPropagation()
        }).data("input", s);
        var f = 0;

        function h() {
          var e = !1;
          return H.startDate ? e = k.strToDate(H.startDate) : (e = H.value ? H.value : s && s.val && s.val() ? s.val() : "") ? e = k.strToDateTime(e) : H.defaultDate && (e = k.strToDate(H.defaultDate)), e && k.isValidDate(e) ? v.data("changed", !0) : e = "", e || 0
        }
        v.data("xdsoft_datetime", k), v.setOptions(H), k.setCurrentTime(h()), s.data("xdsoft_datetimepicker", v).on("open.xdsoft focusin.xdsoft mousedown.xdsoft", function(e) {
          s.is(":disabled") || s.is(":hidden") || !s.is(":visible") || s.data("xdsoft_datetimepicker").is(":visible") && H.closeOnInputClick || (clearTimeout(f), f = setTimeout(function() {
            s.is(":disabled") || s.is(":hidden") || !s.is(":visible") || (l = !0, k.setCurrentTime(h()), v.trigger("open.xdsoft"))
          }, 100))
        }).on("keydown.xdsoft", function(e) {
          this.value;
          var t = e.which;
          switch (!0) {
            case !!~[g].indexOf(t):
              var n = L("input:visible,textarea:visible");
              return v.trigger("close.xdsoft"), n.eq(n.index(this) + 1).focus(), !1;
            case !!~[A].indexOf(t):
              return v.trigger("close.xdsoft"), !0
          }
        })
      };
    return L(document).off("keydown.xdsoftctrl keyup.xdsoftctrl").on("keydown.xdsoftctrl", function(e) {
      e.keyCode == p && (I = !0)
    }).on("keyup.xdsoftctrl", function(e) {
      e.keyCode == p && (I = !1)
    }), this.each(function() {
      var e, n, t, i;
      if (e = L(this).data("xdsoft_datetimepicker")) {
        if ("string" === L.type(o)) switch (o) {
          case "show":
            L(this).select().focus(), e.trigger("open.xdsoft");
            break;
          case "hide":
            e.trigger("close.xdsoft");
            break;
          case "destroy":
            t = L(this), (i = t.data("xdsoft_datetimepicker")) && (i.data("xdsoft_datetime", null), i.remove(), t.data("xdsoft_datetimepicker", null).off("open.xdsoft focusin.xdsoft focusout.xdsoft mousedown.xdsoft blur.xdsoft keydown.xdsoft"), L(window).off("resize.xdsoft"), L([window, document.body]).off("mousedown.xdsoft"), t.unmousewheel && t.unmousewheel());
            break;
          case "reset":
            this.value = this.defaultValue, this.value && e.data("xdsoft_datetime").isValidDate(Date.parseDate(this.value, H.format)) || e.data("changed", !1), e.data("xdsoft_datetime").setCurrentTime(this.value)
        } else e.setOptions(o);
        return 0
      }
      "string" !== L.type(o) && (!H.lazyInit || H.open || H.inline ? s(L(this)) : (n = L(this)).on("open.xdsoft focusin.xdsoft mousedown.xdsoft", function e(t) {
        n.is(":disabled") || n.is(":hidden") || !n.is(":visible") || n.data("xdsoft_datetimepicker") || (clearTimeout(r), r = setTimeout(function() {
          n.data("xdsoft_datetimepicker") || s(n), n.off("open.xdsoft focusin.xdsoft mousedown.xdsoft", e).trigger("open.xdsoft")
        }, 100))
      }))
    })
  }, L.fn.datetimepicker.defaults = e
}(jQuery),
function(e) {
  "function" == typeof define && define.amd ? define(["jquery"], e) : "object" == typeof exports ? module.exports = e : e(jQuery)
}(function(c) {
  var u, d, e = ["wheel", "mousewheel", "DOMMouseScroll", "MozMousePixelScroll"],
    t = "onwheel" in document || 9 <= document.documentMode ? ["wheel"] : ["mousewheel", "DomMouseScroll", "MozMousePixelScroll"];
  if (c.event.fixHooks)
    for (var n = e.length; n;) c.event.fixHooks[e[--n]] = c.event.mouseHooks;

  function i(e) {
    var t, n, i, o = e || window.event,
      r = [].slice.call(arguments, 1),
      s = 0,
      a = 0,
      l = 0;
    return (e = c.event.fix(o)).type = "mousewheel", o.wheelDelta && (s = o.wheelDelta), o.detail && (s = -1 * o.detail), o.deltaY && (s = l = -1 * o.deltaY), o.deltaX && (s = -1 * (a = o.deltaX)), void 0 !== o.wheelDeltaY && (l = o.wheelDeltaY), void 0 !== o.wheelDeltaX && (a = -1 * o.wheelDeltaX), t = Math.abs(s), (!u || t < u) && (u = t), n = Math.max(Math.abs(l), Math.abs(a)), (!d || n < d) && (d = n), i = 0 < s ? "floor" : "ceil", s = Math[i](s / u), a = Math[i](a / d), l = Math[i](l / d), r.unshift(e, s, a, l), (c.event.dispatch || c.event.handle).apply(this, r)
  }
  c.event.special.mousewheel = {
    setup: function() {
      if (this.addEventListener)
        for (var e = t.length; e;) this.addEventListener(t[--e], i, !1);
      else this.onmousewheel = i
    },
    teardown: function() {
      if (this.removeEventListener)
        for (var e = t.length; e;) this.removeEventListener(t[--e], i, !1);
      else this.onmousewheel = null
    }
  }, c.fn.extend({
    mousewheel: function(e) {
      return e ? this.bind("mousewheel", e) : this.trigger("mousewheel")
    },
    unmousewheel: function(e) {
      return this.unbind("mousewheel", e)
    }
  })
}), Date.parseFunctions = {
    count: 0
  }, Date.parseRegexes = [], Date.formatFunctions = {
    count: 0
  }, Date.prototype.dateFormat = function(e) {
    return "unixtime" == e ? parseInt(this.getTime() / 1e3) : (null == Date.formatFunctions[e] && Date.createNewFormat(e), this[Date.formatFunctions[e]]())
  }, Date.createNewFormat = function(format) {
    var funcName = "format" + Date.formatFunctions.count++;
    Date.formatFunctions[format] = funcName;
    for (var code = "Date.prototype." + funcName + " = function() {return ", special = !1, ch = "", i = 0; i < format.length; ++i) ch = format.charAt(i), special || "\\" != ch ? special ? (special = !1, code += "'" + String.escape(ch) + "' + ") : code += Date.getFormatCode(ch) : special = !0;
    eval(code.substring(0, code.length - 3) + ";}")
  }, Date.getFormatCode = function(e) {
    switch (e) {
      case "d":
        return "String.leftPad(this.getDate(), 2, '0') + ";
      case "D":
        return "Date.dayNames[this.getDay()].substring(0, 3) + ";
      case "j":
        return "this.getDate() + ";
      case "l":
        return "Date.dayNames[this.getDay()] + ";
      case "S":
        return "this.getSuffix() + ";
      case "w":
        return "this.getDay() + ";
      case "z":
        return "this.getDayOfYear() + ";
      case "W":
        return "this.getWeekOfYear() + ";
      case "F":
        return "Date.monthNames[this.getMonth()] + ";
      case "m":
        return "String.leftPad(this.getMonth() + 1, 2, '0') + ";
      case "M":
        return "Date.monthNames[this.getMonth()].substring(0, 3) + ";
      case "n":
        return "(this.getMonth() + 1) + ";
      case "t":
        return "this.getDaysInMonth() + ";
      case "L":
        return "(this.isLeapYear() ? 1 : 0) + ";
      case "Y":
        return "this.getFullYear() + ";
      case "y":
        return "('' + this.getFullYear()).substring(2, 4) + ";
      case "a":
        return "(this.getHours() < 12 ? 'am' : 'pm') + ";
      case "A":
        return "(this.getHours() < 12 ? 'AM' : 'PM') + ";
      case "g":
        return "((this.getHours() %12) ? this.getHours() % 12 : 12) + ";
      case "G":
        return "this.getHours() + ";
      case "h":
        return "String.leftPad((this.getHours() %12) ? this.getHours() % 12 : 12, 2, '0') + ";
      case "H":
        return "String.leftPad(this.getHours(), 2, '0') + ";
      case "i":
        return "String.leftPad(this.getMinutes(), 2, '0') + ";
      case "s":
        return "String.leftPad(this.getSeconds(), 2, '0') + ";
      case "O":
        return "this.getGMTOffset() + ";
      case "T":
        return "this.getTimezone() + ";
      case "Z":
        return "(this.getTimezoneOffset() * -60) + ";
      default:
        return "'" + String.escape(e) + "' + "
    }
  }, Date.parseDate = function(e, t) {
    if ("unixtime" == t) return new Date(isNaN(parseInt(e)) ? 0 : 1e3 * parseInt(e));
    null == Date.parseFunctions[t] && Date.createParser(t);
    var n = Date.parseFunctions[t];
    return Date[n](e)
  }, Date.createParser = function(format) {
    var funcName = "parse" + Date.parseFunctions.count++,
      regexNum = Date.parseRegexes.length,
      currentGroup = 1;
    Date.parseFunctions[format] = funcName;
    for (var code = "Date." + funcName + " = function(input) {\nvar y = -1, m = -1, d = -1, h = -1, i = -1, s = -1, z = -1;\nvar d = new Date();\ny = d.getFullYear();\nm = d.getMonth();\nd = d.getDate();\nvar results = input.match(Date.parseRegexes[" + regexNum + "]);\nif (results && results.length > 0) {", regex = "", special = !1, ch = "", i = 0; i < format.length; ++i) ch = format.charAt(i), special || "\\" != ch ? special ? (special = !1, regex += String.escape(ch)) : (obj = Date.formatCodeToRegex(ch, currentGroup), currentGroup += obj.g, regex += obj.s, obj.g && obj.c && (code += obj.c)) : special = !0;
    code += "if (y > 0 && z > 0){\nvar doyDate = new Date(y,0);\ndoyDate.setDate(z);\nm = doyDate.getMonth();\nd = doyDate.getDate();\n}", code += "if (y > 0 && m >= 0 && d > 0 && h >= 0 && i >= 0 && s >= 0)\n{return new Date(y, m, d, h, i, s);}\nelse if (y > 0 && m >= 0 && d > 0 && h >= 0 && i >= 0)\n{return new Date(y, m, d, h, i);}\nelse if (y > 0 && m >= 0 && d > 0 && h >= 0)\n{return new Date(y, m, d, h);}\nelse if (y > 0 && m >= 0 && d > 0)\n{return new Date(y, m, d);}\nelse if (y > 0 && m >= 0)\n{return new Date(y, m);}\nelse if (y > 0)\n{return new Date(y);}\n}return null;}", Date.parseRegexes[regexNum] = new RegExp("^" + regex + "$"), eval(code)
  }, Date.formatCodeToRegex = function(e, t) {
    switch (e) {
      case "D":
        return {
          g: 0, c: null, s: "(?:Sun|Mon|Tue|Wed|Thu|Fri|Sat)"
        };
      case "j":
      case "d":
        return {
          g: 1, c: "d = parseInt(results[" + t + "], 10);\n", s: "(\\d{1,2})"
        };
      case "l":
        return {
          g: 0, c: null, s: "(?:" + Date.dayNames.join("|") + ")"
        };
      case "S":
        return {
          g: 0, c: null, s: "(?:st|nd|rd|th)"
        };
      case "w":
        return {
          g: 0, c: null, s: "\\d"
        };
      case "z":
        return {
          g: 1, c: "z = parseInt(results[" + t + "], 10);\n", s: "(\\d{1,3})"
        };
      case "W":
        return {
          g: 0, c: null, s: "(?:\\d{2})"
        };
      case "F":
        return {
          g: 1, c: "m = parseInt(Date.monthNumbers[results[" + t + "].substring(0, 3)], 10);\n", s: "(" + Date.monthNames.join("|") + ")"
        };
      case "M":
        return {
          g: 1, c: "m = parseInt(Date.monthNumbers[results[" + t + "]], 10);\n", s: "(Jan|Feb|Mar|Apr|May|Jun|Jul|Aug|Sep|Oct|Nov|Dec)"
        };
      case "n":
      case "m":
        return {
          g: 1, c: "m = parseInt(results[" + t + "], 10) - 1;\n", s: "(\\d{1,2})"
        };
      case "t":
        return {
          g: 0, c: null, s: "\\d{1,2}"
        };
      case "L":
        return {
          g: 0, c: null, s: "(?:1|0)"
        };
      case "Y":
        return {
          g: 1, c: "y = parseInt(results[" + t + "], 10);\n", s: "(\\d{4})"
        };
      case "y":
        return {
          g: 1, c: "var ty = parseInt(results[" + t + "], 10);\ny = ty > Date.y2kYear ? 1900 + ty : 2000 + ty;\n", s: "(\\d{1,2})"
        };
      case "a":
        return {
          g: 1, c: "if (results[" + t + "] == 'am') {\nif (h == 12) { h = 0; }\n} else { if (h < 12) { h += 12; }}", s: "(am|pm)"
        };
      case "A":
        return {
          g: 1, c: "if (results[" + t + "] == 'AM') {\nif (h == 12) { h = 0; }\n} else { if (h < 12) { h += 12; }}", s: "(AM|PM)"
        };
      case "g":
      case "G":
      case "h":
      case "H":
        return {
          g: 1, c: "h = parseInt(results[" + t + "], 10);\n", s: "(\\d{1,2})"
        };
      case "i":
        return {
          g: 1, c: "i = parseInt(results[" + t + "], 10);\n", s: "(\\d{2})"
        };
      case "s":
        return {
          g: 1, c: "s = parseInt(results[" + t + "], 10);\n", s: "(\\d{2})"
        };
      case "O":
        return {
          g: 0, c: null, s: "[+-]\\d{4}"
        };
      case "T":
        return {
          g: 0, c: null, s: "[A-Z]{3}"
        };
      case "Z":
        return {
          g: 0, c: null, s: "[+-]\\d{1,5}"
        };
      default:
        return {
          g: 0, c: null, s: String.escape(e)
        }
    }
  }, Date.prototype.getTimezone = function() {
    return this.toString().replace(/^.*? ([A-Z]{3}) [0-9]{4}.*$/, "$1").replace(/^.*?\(([A-Z])[a-z]+ ([A-Z])[a-z]+ ([A-Z])[a-z]+\)$/, "$1$2$3")
  }, Date.prototype.getGMTOffset = function() {
    return (0 < this.getTimezoneOffset() ? "-" : "+") + String.leftPad(Math.floor(Math.abs(this.getTimezoneOffset()) / 60), 2, "0") + String.leftPad(Math.abs(this.getTimezoneOffset()) % 60, 2, "0")
  }, Date.prototype.getDayOfYear = function() {
    var e = 0;
    Date.daysInMonth[1] = this.isLeapYear() ? 29 : 28;
    for (var t = 0; t < this.getMonth(); ++t) e += Date.daysInMonth[t];
    return e + this.getDate()
  }, Date.prototype.getWeekOfYear = function() {
    var e = this.getDayOfYear() + (4 - this.getDay()),
      t = 7 - new Date(this.getFullYear(), 0, 1).getDay() + 4;
    return String.leftPad(Math.ceil((e - t) / 7) + 1, 2, "0")
  }, Date.prototype.isLeapYear = function() {
    var e = this.getFullYear();
    return 0 == (3 & e) && (e % 100 || e % 400 == 0 && e)
  }, Date.prototype.getFirstDayOfMonth = function() {
    var e = (this.getDay() - (this.getDate() - 1)) % 7;
    return e < 0 ? e + 7 : e
  }, Date.prototype.getLastDayOfMonth = function() {
    var e = (this.getDay() + (Date.daysInMonth[this.getMonth()] - this.getDate())) % 7;
    return e < 0 ? e + 7 : e
  }, Date.prototype.getDaysInMonth = function() {
    return Date.daysInMonth[1] = this.isLeapYear() ? 29 : 28, Date.daysInMonth[this.getMonth()]
  }, Date.prototype.getSuffix = function() {
    switch (this.getDate()) {
      case 1:
      case 21:
      case 31:
        return "st";
      case 2:
      case 22:
        return "nd";
      case 3:
      case 23:
        return "rd";
      default:
        return "th"
    }
  }, String.escape = function(e) {
    return e.replace(/('|\\)/g, "\\$1")
  }, String.leftPad = function(e, t, n) {
    var i = new String(e);
    for (null == n && (n = " "); i.length < t;) i = n + i;
    return i
  }, Date.daysInMonth = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31], Date.monthNames = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"], Date.dayNames = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"], Date.y2kYear = 50, Date.monthNumbers = {
    Jan: 0,
    Feb: 1,
    Mar: 2,
    Apr: 3,
    May: 4,
    Jun: 5,
    Jul: 6,
    Aug: 7,
    Sep: 8,
    Oct: 9,
    Nov: 10,
    Dec: 11
  }, Date.patterns = {
    ISO8601LongPattern: "Y-m-d H:i:s",
    ISO8601ShortPattern: "Y-m-d",
    ShortDatePattern: "n/j/Y",
    LongDatePattern: "l, F d, Y",
    FullDateTimePattern: "l, F d, Y g:i:s A",
    MonthDayPattern: "F d",
    ShortTimePattern: "g:i A",
    LongTimePattern: "g:i:s A",
    SortableDateTimePattern: "Y-m-d\\TH:i:s",
    UniversalSortableDateTimePattern: "Y-m-d H:i:sO",
    YearMonthPattern: "F, Y"
  },
  function(y) {
    var i, e, o = ((e = document.createElement("input")).setAttribute("onpaste", ""), ("function" == typeof e.onpaste ? "paste" : "input") + ".mask"),
      t = navigator.userAgent,
      b = /iphone/i.test(t),
      x = /android/i.test(t);
    y.mask = {
      definitions: {
        9: "[0-9]",
        a: "[A-Za-z]",
        "*": "[A-Za-z0-9]"
      },
      dataName: "rawMaskFn",
      placeholder: "_"
    }, y.fn.extend({
      caret: function(e, t) {
        var n;
        if (0 !== this.length && !this.is(":hidden")) return "number" == typeof e ? (t = "number" == typeof t ? t : e, this.each(function() {
          this.setSelectionRange ? this.setSelectionRange(e, t) : this.createTextRange && ((n = this.createTextRange()).collapse(!0), n.moveEnd("character", t), n.moveStart("character", e), n.select())
        })) : (this[0].setSelectionRange ? (e = this[0].selectionStart, t = this[0].selectionEnd) : document.selection && document.selection.createRange && (n = document.selection.createRange(), e = 0 - n.duplicate().moveStart("character", -1e5), t = e + n.text.length), {
          begin: e,
          end: t
        })
      },
      unmask: function() {
        return this.trigger("unmask")
      },
      mask: function(t, h) {
        var n, p, g, m, v;
        return !t && 0 < this.length ? y(this[0]).data(y.mask.dataName)() : (h = y.extend({
          placeholder: y.mask.placeholder,
          completed: null
        }, h), n = y.mask.definitions, p = [], g = v = t.length, m = null, y.each(t.split(""), function(e, t) {
          "?" == t ? (v--, g = e) : n[t] ? (p.push(new RegExp(n[t])), null === m && (m = p.length - 1)) : p.push(null)
        }), this.trigger("unmask").each(function() {
          var s = y(this),
            a = y.map(t.split(""), function(e, t) {
              if ("?" != e) return n[e] ? h.placeholder : e
            }),
            r = s.val();

          function l(e) {
            for (; ++e < v && !p[e];);
            return e
          }

          function c(e, t) {
            var n, i;
            if (!(e < 0)) {
              for (n = e, i = l(t); n < v; n++)
                if (p[n]) {
                  if (!(i < v && p[n].test(a[i]))) break;
                  a[n] = a[i], a[i] = h.placeholder, i = l(i)
                }
              d(), s.caret(Math.max(m, e))
            }
          }

          function u(e, t) {
            var n;
            for (n = e; n < t && n < v; n++) p[n] && (a[n] = h.placeholder)
          }

          function d() {
            s.val(a.join(""))
          }

          function f(e) {
            var t, n, i = s.val(),
              o = -1;
            for (t = 0, pos = 0; t < v; t++)
              if (p[t]) {
                for (a[t] = h.placeholder; pos++ < i.length;)
                  if (n = i.charAt(pos - 1), p[t].test(n)) {
                    a[t] = n, o = t;
                    break
                  }
                if (pos > i.length) break
              } else a[t] === i.charAt(pos) && t !== g && (pos++, o = t);
            return e ? d() : o + 1 < g ? (s.val(""), u(0, v)) : (d(), s.val(s.val().substring(0, o + 1))), g ? t : m
          }
          s.data(y.mask.dataName, function() {
            return y.map(a, function(e, t) {
              return p[t] && e != h.placeholder ? e : null
            }).join("")
          }), s.attr("readonly") || s.one("unmask", function() {
            s.unbind(".mask").removeData(y.mask.dataName)
          }).bind("focus.mask", function() {
            var e;
            clearTimeout(i), r = s.val(), e = f(), i = setTimeout(function() {
              d(), e == t.length ? s.caret(0, e) : s.caret(e)
            }, 10)
          }).bind("blur.mask", function() {
            f(), s.val() != r && s.change()
          }).bind("keydown.mask", function(e) {
            var t, n, i, o = e.which;
            8 === o || 46 === o || b && 127 === o ? (n = (t = s.caret()).begin, (i = t.end) - n == 0 && (n = 46 !== o ? function(e) {
              for (; 0 <= --e && !p[e];);
              return e
            }(n) : i = l(n - 1), i = 46 === o ? l(i) : i), u(n, i), c(n, i - 1), e.preventDefault()) : 27 == o && (s.val(r), s.caret(0, f()), e.preventDefault())
          }).bind("keypress.mask", function(e) {
            var t, n, i, o = e.which,
              r = s.caret();
            e.ctrlKey || e.altKey || e.metaKey || o < 32 || o && (r.end - r.begin != 0 && (u(r.begin, r.end), c(r.begin, r.end - 1)), (t = l(r.begin - 1)) < v && (n = String.fromCharCode(o), p[t].test(n) && (function(e) {
              var t, n, i, o;
              for (t = e, n = h.placeholder; t < v; t++)
                if (p[t]) {
                  if (i = l(t), o = a[t], a[t] = n, !(i < v && p[i].test(o))) break;
                  n = o
                }
            }(t), a[t] = n, d(), i = l(t), x ? setTimeout(y.proxy(y.fn.caret, s, i), 0) : s.caret(i), h.completed && v <= i && h.completed.call(s))), e.preventDefault())
          }).bind(o, function() {
            setTimeout(function() {
              var e = f(!0);
              s.caret(e), h.completed && e == s.val().length && h.completed.call(s)
            }, 0)
          }), f()
        }))
      }
    })
  }(jQuery),
  function(s, e) {
    s.fn.xval = function() {
      var e = arguments[0],
        n = arguments[1];
      if (this.is("select")) {
        if (arguments.length) {
          var t = this.val();
          return this.find("option").prop({
            selected: !1
          }).filter("[value='" + e + "']").eq(0).prop({
            selected: !0
          }), n || t == this.val() || this.trigger("change"), this
        }
        return this.find("option:selected").val()
      }
      if (this.is("input:radio")) {
        if (arguments.length) {
          t = this.filter(":checked").val();
          return this.prop({
            checked: !1
          }).filter("[value='" + e + "']").eq(0).prop({
            checked: !0
          }), n || t == this.filter(":checked").val() || this.filter(":checked").trigger("change"), this
        }
        return this.filter(":checked").val()
      }
      if (this.is("input:checkbox")) {
        if (arguments.length) {
          for (var i = e instanceof Array ? e : [e], o = 0; o < i.length; o++) i[o] = i[o].toString();
          return this.each(function(e) {
            var t = this.checked; - 1 == i.indexOf(this.value.toString()) || this.checked ? -1 == i.indexOf(this.value.toString()) && this.checked && (t = !1) : t = !0, t != this.checked && (this.checked = t, n || s(this).trigger("change"))
          }), this
        }
        var r = [];
        for (o = 0; o < this.length; o++) this[o].checked && r.push(this[o].value);
        return r
      }
      if (arguments.length) {
        t = this.val();
        return this.val(e), n || t == this.val() || this.trigger("change"), this
      }
      return this.val()
    }
  }(jQuery),
  function(s, e) {
    var a = {
      duration: 0,
      opacity: 1
    };

    function l(e) {
      (e = e || window.event).stopPropagation ? e.stopPropagation() : e && (e.cancelBubble = !0)
    }
    var c = 1e3;
    s.fn.extend({
      overlay: function(t, n) {
        var i = this;
        n = s.extend({}, a, n);
        for (var e = 0; e < this.length; e++) {
          var o = this.get(e);
          o.oldZIndex = this.css("zIndex"), this.css({
            zIndex: c + 1
          }).on("mousedown", l);
          o.$overlay = s(document.createElement("div")).css({
            position: "fixed",
            zIndex: c,
            left: 0,
            top: 0,
            width: "100%",
            height: "100%",
            opacity: 0
          }).addClass("jquery-overlay").insertBefore(this[e]).on("mousedown", function(e) {
            l(e), i.unoverlay(t, n)
          }), n.goInCssBefore && o.$overlay.css(n.goInCssBefore);
          var r = {};
          n.goInCssAfter && (r = n.goInCssAfter), r.opacity = n.opacity, o.$overlay.animate(r, {
            duration: n.duration
          }), c += 2
        }
        return this
      },
      unoverlay: function(e, t) {
        t = s.extend({}, a, t);
        for (var n = 0; n < this.length; n++) {
          var i = this.get(n);
          if (i.$overlay) {
            t.goOutCssBefore && i.$overlay.css(t.goOutCssBefore);
            var o = {};
            t.goOutCssAfter && (o = t.goOutCssAfter), o.opacity = 0, i.$overlay.animate(o, {
              duration: t.duration,
              complete: function() {
                i.$overlay.remove(), delete i.$overlay, i.style.zIndex = i.oldZIndex, delete i.oldZIndex
              }
            }), "function" == typeof e && e.call(i)
          }
        }
        return this.off("mousedown", l), this
      }
    })
  }(jQuery),
  function($, undef) {
    var UPLOAD_COUNTER = 0,
      defaultOptions = {
        uploadUrl: "/fileupload/",
        deleteUrl: "/fileupload/clear/",
        containerClass: "fileupload",
        linkClass: "fileupload__link",
        linkTarget: "_blank",
        deleteClass: "fileupload__delete",
        uploadClass: "fileupload__upload",
        deleteText: "удал.",
        uploadText: "загр.",
        deleteTitle: "Удалить файл",
        uploadTitle: "Загрузить файл",
        uploadOnChange: !0,
        deleteOnDelete: !1,
        beforeUpload: function() {},
        beforeDelete: function() {},
        afterUpload: function(e) {},
        afterDelete: function(e) {}
      },
      FileUploader = function(e, t) {
        this.input = e, this.opt = t, this.setup()
      };
    FileUploader.prototype = {
      setup: function() {
        var e = this;
        this.$input = $(this.input), this.default = this.$input.data();
        var t = UPLOAD_COUNTER++,
          n = "__upload_frame_" + t;
        this.$container = $(document.createElement("div")).addClass(this.opt.containerClass).insertAfter(this.$input), this.$link = $(document.createElement("a")).attr({
          href: "#",
          target: this.opt.linkTarget
        }).addClass(this.opt.linkClass).appendTo(this.$container), this.$delete = $(document.createElement("a")).attr({
          href: "#",
          title: this.opt.deleteTitle
        }).text(this.opt.deleteText).addClass(this.opt.deleteClass).appendTo(this.$container).click(function() {
          return e.delete.call(e), !1
        }), this.$upload = $(document.createElement("a")).attr({
          href: "#",
          title: this.opt.uploadTitle
        }).text(this.opt.uploadText).addClass(this.opt.uploadClass).appendTo(this.$container).click(function() {
          return e.upload.call(e), !1
        }), this.opt.uploadOnChange && this.$upload.hide(), this.$iframe = $(document.createElement("iframe")).attr({
          src: "",
          name: n
        }).load(function() {
          e.load.call(e, this)
        }).hide().appendTo(document.body), this.$form = $(document.createElement("form")).attr({
          method: "post",
          enctype: "multipart/form-data",
          target: n,
          action: this.opt.uploadUrl
        }).hide().appendTo(document.body), this.onUpload = function() {
          e.upload.call(e)
        }, this.onChange = function() {
          e.opt.uploadOnChange && e.upload.call(e)
        }, this.onSubmit = function() {
          e.$input.attr({
            disabled: !0
          })
        }, this.onReset = function() {
          e.$input.data(e.default).attr({
            disabled: !1
          }), e.$subinput.val(e.default.file_id), e.check()
        }, this.input.form && (this.defaultInputName = this.input.name, this.$subinput = $(document.createElement("input")).attr({
          type: "hidden",
          name: this.defaultInputName,
          value: this.default.file_id
        }).appendTo(this.input.form), this.input.name = "__upload_input_" + t, $(this.input.form).on("submit", this.onSubmit).on("reset", this.onReset)), this.$input.on("change", this.onChange).on("upload", this.onUpload), this.check()
      },
      check: function() {
        this.$input.val("");
        var e = this.$input.data("file_id");
        if (0 < e) {
          var t = this.$input.data("file_src") || "",
            n = this.$input.data("publ_name") || t.replace(/^.*?([^\/]+)$/, "$1");
          this.$link.text(n).attr({
            href: t
          }).show(), this.$delete.show(), this.$input.hide().prop({
            disabled: !0
          }), this.opt.uploadOnChange || this.$upload.hide()
        } else this.$link.attr({
          href: "#"
        }).empty().hide(), this.$delete.hide(), this.$input.show().prop({
          disabled: !1
        }), this.opt.uploadOnChange || this.$upload.show();
        this.input.form && this.$subinput.val(e)
      },
      upload: function() {
        if (this.input.value) {
          this.opt.beforeUpload.call(this.input);
          var e = this.$input,
            t = e.data();
          for (var n in this.$input = e.clone(!0).insertAfter(this.$input), this.input = this.$input.get(0), this.$form.empty().attr({
              action: this.opt.uploadUrl
            }).append(e.attr({
              name: "upload"
            })), t) "" != t[n] && "file_id" != n && "file_src" != n && "publ_name" != n && this.$form.append($(document.createElement("input")).attr({
            type: "hidden",
            name: n,
            value: t[n]
          }));
          this.$form.submit()
        } else alert("Вы не выбрали файл!")
      },
      delete: function() {
        if (this.opt.beforeDelete.call(this.input), this.justUploaded || this.opt.deleteOnDelete) {
          var e = this.$input.data("file_id");
          0 < e && (this.$form.empty().attr({
            action: this.opt.deleteUrl
          }), $(document.createElement("input")).attr({
            type: "hidden",
            name: "file_id",
            value: e
          }).appendTo(this.$form), this.$form.submit()), this.justUploaded = !1
        } else this.$input.data({
          file_id: "",
          file_src: "",
          publ_name: ""
        }), this.check()
      },
      load: function(iframe) {
        var self = this,
          d = null;
        if (iframe.contentDocument ? d = iframe.contentDocument : iframe.contentWindow && (d = iframe.contentWindow.document), d && "about:blank" != d.location.href && d) {
          var data = eval(d.body.innerHTML);
          if ("upload" == data.method)
            if (data.error) alert("Произошла ошибка при загрузке файла"), this.$input.data({
              file_id: null,
              file_src: null,
              publ_name: null
            });
            else {
              for (var p in self.justUploaded = !0, data) "" != data[p] && "method" != p && "ok" != p && self.$input.data(p, data[p]);
              self.opt.afterUpload.call(self.input, data)
            }
          else "clear" == data.method ? data.error ? alert("Произошла ошибка при удалении файла") : (this.$input.data({
            file_id: null,
            file_src: null,
            publ_name: null
          }), self.opt.afterDelete.call(self.input, data)) : alert("Неизвестный метод " + data.method);
          self.check()
        }
      },
      destroy: function() {
        if (this.justUploaded) {
          var e = this.$input.data("file_id");
          0 < e && (this.$form.empty().attr({
            action: this.opt.deleteUrl
          }), $(document.createElement("input")).attr({
            type: "hidden",
            name: "file_id",
            value: e
          }).appendTo(this.$form), this.$form.submit()), this.justUploaded = !1
        }
        this.$link.remove(), this.$delete.remove(), this.$upload.remove(), this.$container.remove(), this.$form.remove(), this.$iframe.remove(), this.$input.off("change", this.onChange).off("upload", this.onUpload).show(), this.input.form && (this.input.name = this.defaultInputName, this.$subinput.remove(), $(this.input.form).off("submit", this.onSubmit).off("reset", this.onReset))
      }
    }, $.fn.extend({
      fileupload: function(e) {
        for (var t = 0; t < this.length; t++) this[t].fileuploader || (this[t].fileuploader = new FileUploader(this.get(t), $.extend({}, defaultOptions, e)));
        return this
      },
      unfileupload: function() {
        for (var e = 0; e < this.length; e++) this[e].fileuploader && (this[e].fileuploader.destroy(), delete this[e].fileuploader)
      }
    })
  }(jQuery),
  function(i, e) {
    var o = {
        min: 0,
        max: 99,
        step: 1,
        prevClass: "prev",
        prevContent: "-",
        nextClass: "next",
        nextContent: "+",
        disabledClass: "disabled",
        comma: !1,
        coefGrad: 1.1,
        defaultGrad: 1,
        maxGrad: 10,
        allowEmpty: !1,
        checkValid: !0
      },
      r = function(e, t) {
        this.input = e, this.opt = t, this.setup()
      };
    r.prototype = {
      setup: function() {
        var t = this;
        this.$input = i(this.input), this.$prev = i(document.createElement("span")).addClass(this.opt.prevClass).html(this.opt.prevContent).insertBefore(this.$input), this.$next = i(document.createElement("span")).addClass(this.opt.nextClass).html(this.opt.nextContent).insertAfter(this.$input), this.$prev.mousedown(function(e) {
          t.startCounting(-1)
        }).mouseup(function(e) {
          t.started && (t.process ? t.stopCounting() : (t.clearCounting(), t.count(-1)))
        }).mouseleave(function(e) {
          t.process && t.stopCounting()
        }), this.$next.mousedown(function(e) {
          t.startCounting(1)
        }).mouseup(function(e) {
          t.started && (t.process ? t.stopCounting() : (t.clearCounting(), t.count(1)))
        }).mouseleave(function(e) {
          t.process && t.stopCounting()
        }), this.checkChange = function(e) {
          t.check()
        }, this.$input.on("change", this.checkChange), this.currentGrad = this.opt.defaultGrad, this.check()
      },
      count: function(e) {
        var t = this.getValue(),
          n = this.opt.min;
        if ("" !== t) n = t + e * this.opt.step;
        else if (e < 0 && this.opt.allowEmpty) return;
        n < this.opt.min ? n = this.opt.allowEmpty ? "" : this.opt.min : n > this.opt.max && (n = this.opt.max), n !== t && (this.setValue(n), this.$input.trigger("change"))
      },
      startCounting: function(e) {
        var t = this;
        this.started = !0, this.dtimer = setTimeout(function() {
          t.process = !0, t.oldValue = t.currentValue = t.getValue(), t.currentGrad = t.opt.defaultGrad, t.timer = setInterval(function() {
            t._count(e)
          }, 120)
        }, 300)
      },
      clearCounting: function() {
        this.started && (this.started = !1, clearTimeout(this.dtimer), this.dtimer = -1)
      },
      stopCounting: function() {
        this.clearCounting(), this.process && (this.process = !1, clearInterval(this.timer), this.timer = -1, this.getValue() !== this.oldValue && this.$input.trigger("change"))
      },
      _count: function(e) {
        if ("" !== this.currentValue) this.currentValue += e * this.opt.step * this.currentGrad;
        else {
          if (e < 0 && this.opt.allowEmpty) return;
          0 < e && (this.currentValue = this.opt.min)
        }
        var t = this.currentValue,
          n = !1;
        t < this.opt.min ? (t = this.opt.min, n = !0) : t > this.opt.max && (t = this.opt.max, n = !0), t !== this.getValue() && (this.setValue(t), this.check(t)), n ? this.stopCounting() : this._incGrad()
      },
      _incGrad: function() {
        this.currentGrad < this.opt.maxGrad && (this.currentGrad *= this.opt.coefGrad), this.currentGrad > this.opt.maxGrad && (this.currentGrad = this.opt.maxGrad)
      },
      getValue: function() {
        return "" === this.input.value ? "" : parseFloat(this.opt.comma ? this.input.value.replace(/,/, ".") : this.input.value)
      },
      setValue: function(e) {
        "" === e ? this.input.value = "" : (e = this.round(e), this.input.value = this.opt.comma ? e.toString().replace(/\./, ",") : e)
      },
      round: function(e) {
        return Math.round(e / this.opt.step) * this.opt.step
      },
      check: function() {
        var e = this.getValue();
        "" === e ? (this.$prev.addClass(this.opt.disabledClass), this.$next.removeClass(this.opt.disabledClass)) : (e <= this.opt.min ? (this.opt.allowEmpty ? this.$prev.removeClass(this.opt.disabledClass) : this.$prev.addClass(this.opt.disabledClass), this.opt.checkValid && e < this.opt.min && this.setValue(this.opt.min)) : this.$prev.removeClass(this.opt.disabledClass), e >= this.opt.max ? (this.$next.addClass(this.opt.disabledClass), this.opt.checkValid && e > this.opt.max && this.setValue(this.opt.max)) : this.$next.removeClass(this.opt.disabledClass))
      },
      destroy: function() {
        this.$input.off("change", this.checkChange), this.$prev.remove(), this.$next.remove()
      }
    }, i.fn.extend({
      inputarrow: function(e) {
        for (var t = 0; t < this.length; t++) {
          var n = {
            allowEmpty: !this.eq(t).attr("required")
          };
          void 0 !== this.eq(t).attr("min") && (n.min = parseFloat(this.eq(t).attr("min"))), void 0 !== this.eq(t).attr("max") && (n.max = parseFloat(this.eq(t).attr("max"))), void 0 !== this.eq(t).attr("step") && "any" !== this.eq(t).attr("step") && (n.step = parseFloat(this.eq(t).attr("step"))), this[t]._inputarrowcounter ? (i.extend(this[t]._inputarrowcounter.opt, n, e), this[t]._inputarrowcounter.check()) : this[t]._inputarrowcounter = new r(this.get(t), i.extend({}, o, n, e))
        }
        return this
      },
      uninputarrow: function() {
        for (var e = 0; e < this.length; e++) this[e]._inputarrowcounter && (this[e]._inputarrowcounter.destroy(), delete this[e]._inputarrowcounter);
        return this
      }
    })
  }(jQuery),
  function(e, t) {
    var n = function() {
      function n(e) {
        return 48 <= e && e <= 57
      }

      function i(e) {
        return 44 == e || 46 == e
      }

      function o(e) {
        return null == e.which ? e.keyCode < 32 ? 0 : e.keyCode : 0 != e.which && 0 != e.charCode ? e.which < 32 ? 0 : e.which : 0
      }
      return {
        int: function(e) {
          var t = o(e = e || window.event);
          return !(t && !n(t) && !i(t))
        },
        float: function(e) {
          var t = o(e = e || window.event);
          return !(t && !n(t) && !i(t))
        }
      }
    }();
    e.fn.extend({
      inputcheck: function(e) {
        return this.on("keypress", n[e])
      },
      uninputcheck: function(e) {
        return this.off("keypress", n[e])
      }
    })
  }(jQuery), $.fn.extend({
    serializeObject: function(e) {
      var t = {},
        n = this.serializeArray();
      return $.each(n, function() {
        t[this.name] ? (t[this.name].push || (t[this.name] = [t[this.name]]), t[this.name].push(this.value)) : t[this.name] = this.value
      }), t
    }
  }),
  function(t, e) {
    var n = {
        duration: 300,
        delay: 5e3,
        autoChange: !1,
        stopOnMouseOver: !1,
        loop: !1,
        activeClass: "is-active",
        disabledClass: "is-disabled",
        hideArrs: !1,
        hideNav: !1,
        swipeHandler: function(e, t) {
          return Math.abs(e) < Math.abs(t) || Math.abs(e) < 20 ? 0 : 0 < e ? -1 : 1
        },
        beforeChange: function(e, t, n) {},
        afterChange: function(e, t, n) {},
        goOutCssBefore: function(e, t, n) {
          return {
            opacity: 1
          }
        },
        goOutCssAfter: function(e, t, n) {
          return {
            opacity: 0
          }
        },
        goInCssBefore: function(e, t, n) {
          return {
            opacity: 0
          }
        },
        goInCssAfter: function(e, t, n) {
          return {
            opacity: 1
          }
        }
      },
      i = function(e, t) {
        return this.$items = e, this.opt = t, this.process = !1, this.idx = -1, this.timer = -1, this.setup(), this
      };
    i.prototype = {
      setup: function() {
        var r = this;
        if (this.$container = this.opt.container ? t(this.opt.container) : this.$items.eq(0).parent(), this.$nav = t(this.opt.nav), this.$prev = t(this.opt.prev), this.$next = t(this.opt.next), this.prevClick = function(e) {
            return (r.opt.loop || 0 < r.idx) && r.showPrev(), !1
          }, this.nextClick = function(e) {
            return (r.opt.loop || r.idx < r.$items.length - 1) && r.showNext(), !1
          }, this.navClick = function(e) {
            var t = r.$nav.index(this);
            return t != r.idx && r.show(t), !1
          }, this.touchStart = function(e) {
            var t = e.originalEvent.touches || e.originalEvent.targetTouches;
            if (t && 1 !== t.length || r.touch) return !0;
            r.$container.on("touchmove", r.touchMove).on("touchend", r.touchEnd).on("touchcancel", r.touchCancel), r.touch = {
              x: t[0].pageX,
              y: t[0].pageY
            }
          }, this.touchMove = function(e) {}, this.touchEnd = function(e) {
            if (!r.touch) return !0;
            var t = e.originalEvent.changedTouches,
              n = t[0].pageX - r.touch.x,
              i = t[0].pageY - r.touch.y,
              o = r.opt.swipeHandler(n, i);
            0 !== o && (o < 0 ? (r.opt.loop || 0 < r.idx) && r.showPrev() : (r.opt.loop || r.idx < r.$items.length - 1) && r.showNext()), r.$container.off("touchmove", r.touchMove).off("touchend", r.touchEnd).off("touchcancel", r.touchCancel), delete r.touch
          }, this.touchCancel = function(e) {
            r.$container.off("touchmove", r.touchMove).off("touchend", r.touchEnd).off("touchcancel", r.touchCancel), delete r.touch
          }, this.$prev.on("click", this.prevClick), this.$next.on("click", this.nextClick), this.$nav.on("click", this.navClick), this.$container.on("touchstart", this.touchStart), void 0 !== this.opt.idx) this.idx = this.opt.idx;
        else
          for (var e = this.idx = 0; e < this.$nav.length; e++)
            if (this.$nav.eq(e).hasClass(this.opt.activeClass)) {
              r.idx = e;
              break
            }
        this.$items.hide().eq(this.idx).show(), this.opt.afterChange.call(this.$items.get(this.idx), -1, this.idx, this.$items.length), this.$nav.removeClass(this.opt.activeClass).eq(this.idx).addClass(this.opt.activeClass), this.check(), this.opt.autoChange && 1 < this.$items.length && (this.resetTimer(), this.opt.stopOnMouseOver && (this.containerMouseOver = function(e) {
          r.clearTimer()
        }, this.containerMouseOut = function(e) {
          r.resetTimer()
        }, this.$container.on("mouseover", this.containerMouseOver).on("mouseout", this.containerMouseOut)))
      },
      resetTimer: function() {
        var e = this; - 1 < this.timer && this.clearTimer(), this.timer = setTimeout(function() {
          e.showNext()
        }, this.opt.delay)
      },
      clearTimer: function() {
        0 <= this.timer && (clearTimeout(this.timer), this.timer = -1)
      },
      showNext: function(e) {
        var t = this.idx + 1;
        t > this.$items.length - 1 && (t = 0), this.show(t, e)
      },
      showPrev: function(e) {
        var t = this.idx - 1;
        t < 0 && (t = this.$items.length - 1), this.show(t, e)
      },
      show: function(e, t) {
        if (e != this.idx) {
          var n = this;
          this.process = !0;
          var i = t ? 0 : this.opt.duration,
            o = this.$items.eq(this.idx),
            r = this.$items.eq(e),
            s = o.get(0),
            a = r.get(0),
            l = this.idx,
            c = this.$items.length;
          n.opt.beforeChange.call(s, l, e, c), o.stop(!0, !0).css(n.opt.goOutCssBefore.call(s, l, e, c)).animate(n.opt.goOutCssAfter.call(s, l, e, c), {
            duration: i,
            complete: function() {
              o.hide()
            }
          }), r.stop(!0, !0).css(n.opt.goInCssBefore.call(a, l, e, c)).show().animate(n.opt.goInCssAfter.call(a, l, e, c), {
            duration: i,
            complete: function() {
              n.process = !1, n.$nav.removeClass(n.opt.activeClass).eq(e).addClass(n.opt.activeClass), n.opt.afterChange.call(a, l, e, c)
            }
          }), this.idx = e, this.check(), -1 < this.timer && this.resetTimer()
        }
      },
      check: function() {
        this.$items.length <= 1 ? (this.opt.hideArrs && (this.$prev.hide(), this.$next.hide()), this.opt.hideNav && this.$nav.hide()) : (this.opt.hideArrs && (this.$prev.show(), this.$next.show()), this.opt.hideNav && this.$nav.show()), this.opt.loop || 0 != this.idx ? this.$prev.removeClass(this.opt.disabledClass) : this.$prev.addClass(this.opt.disabledClass), this.opt.loop || this.idx != this.$items.length - 1 ? this.$next.removeClass(this.opt.disabledClass) : this.$next.addClass(this.opt.disabledClass)
      },
      destroy: function() {
        this.clearTimer(), this.$prev.off("click", this.prevClick), this.$next.off("click", this.nextClick), this.$nav.off("click", this.navClick), this.$container.off("touchstart", this.touchStart), this.opt.autoChange && 1 < this.$items.length && this.opt.stopOnMouseOver && this.$container.off("mouseover", this.containerMouseOver).off("mouseout", this.containerMouseOut)
      }
    }, t.fn.extend({
      slider: function(e) {
        return this._slider ? this._slider.check() : this._slider = new i(this, t.extend({}, n, e)), this
      },
      unslider: function() {
        return this._slider && (this._slider.destroy(), delete this._slider), this
      }
    })
  }(jQuery),
  function(s, e) {
    var t = {
        disabledClass: "is-disabled",
        vertical: !1,
        duration: 300,
        delay: 5e3,
        autoScroll: !1,
        hideArrs: !1,
        coefGrad: 1.1,
        defaultGrad: 5,
        maxGrad: 30,
        swipeHandler: function(e, t) {
          return Math.abs(e) < Math.abs(t) || Math.abs(e) < 20 ? 0 : 0 < e ? -1 : 1
        }
      },
      n = function(e, t) {
        return this.$items = e, this.opt = t, this.process = !1, this.idx = -1, this.timer = -1, this.setup(), this
      };
    n.prototype = {
      setup: function() {
        var r = this;
        this.$container = this.opt.container ? s(this.opt.container) : this.$items.eq(0).parent(), this.container = this.$container.get(0), this.$prev = s(this.opt.prev), this.$next = s(this.opt.next), this.prevClick = function(e) {
          return !1
        }, this.prevMouseDown = function(e) {
          r.startScrolling(-1)
        }, this.prevMouseUp = function(e) {
          r.started && (r.process ? r.stopScrolling(-1) : (r.clearScrolling(), r.scroll(-1)))
        }, this.prevMouseLeave = function(e) {
          r.process && r.stopScrolling(-1)
        }, this.nextClick = function(e) {
          return !1
        }, this.nextMouseDown = function(e) {
          r.startScrolling(1)
        }, this.nextMouseUp = function(e) {
          r.started && (r.process ? r.stopScrolling(1) : (r.clearScrolling(), r.scroll(1)))
        }, this.nextMouseLeave = function(e) {
          r.process && r.stopScrolling(1)
        }, this.$prev.on("mousedown", this.prevMouseDown).on("mouseup", this.prevMouseUp).on("mouseleave", this.prevMouseLeave).on("click", this.prevClick), this.$next.on("mousedown", this.nextMouseDown).on("mouseup", this.nextMouseUp).on("mouseleave", this.nextMouseLeave).on("click", this.nextClick), this.touchStart = function(e) {
          var t = e.originalEvent.touches || e.originalEvent.targetTouches;
          if (t && 1 !== t.length || r.touch) return !0;
          r.$container.on("touchmove", r.touchMove).on("touchend", r.touchEnd).on("touchcancel", r.touchCancel), r.touch = {
            x: t[0].pageX,
            y: t[0].pageY
          }
        }, this.touchMove = function(e) {}, this.touchEnd = function(e) {
          if (!r.touch) return !0;
          var t = e.originalEvent.changedTouches,
            n = t[0].pageX - r.touch.x,
            i = t[0].pageY - r.touch.y,
            o = r.opt.swipeHandler(n, i);
          0 !== o && (o < 0 ? (r.opt.loop || 0 < r.idx) && r.scrollPrev() : (r.opt.loop || r.idx < r.$items.length - 1) && r.scrollNext()), r.$container.off("touchmove", r.touchMove).off("touchend", r.touchEnd).off("touchcancel", r.touchCancel), delete r.touch
        }, this.touchCancel = function(e) {
          r.$container.off("touchmove", r.touchMove).off("touchend", r.touchEnd).off("touchcancel", r.touchCancel), delete r.touch
        }, this.currentGrad = this.opt.defaultGrad, this.resizeHandler = function() {
          r.scrollTo(r.idx, !0)
        }, this.$container.on("touchstart", this.touchStart), this.$container.on("resize", this.resizeHandler), s(window).on("resize", this.resizeHandler), void 0 !== this.opt.idx ? this.scrollTo(this.opt.idx, !0) : (this.setIdx(), this.check()), this.opt.autoScroll && 1 < this.$items.length && (this.resetTimer(), s(this.container).mouseover(function() {
          r.clearTimer()
        }).mouseout(function() {
          r.resetTimer()
        }))
      },
      resetTimer: function() {
        var e = this; - 1 < this.stimer && this.clearTimer(), this.stimer = setTimeout(function() {
          e.scrollNext()
        }, this.opt.delay)
      },
      clearTimer: function() {
        -1 < this.stimer && (clearTimeout(this.stimer), this.stimer = -1)
      },
      scrollNext: function() {
        var e = this.idx + 1;
        e > this.$items.length - 1 && (e = 0);
        var t = e > this.idx ? 1 : -1;
        this.scroll(t)
      },
      scrollPrev: function() {
        var e = this.idx - 1;
        e < 0 && (e = this.$items.length - 1);
        var t = e < this.idx ? -1 : 1;
        this.scroll(t)
      },
      startScrolling: function(e) {
        var t = this;
        this.started = !0, this.dtimer = setTimeout(function() {
          t.process = !0, t.currentValue = t._getScrollPos(t.container), t.currentGrad = t.opt.defaultGrad, t.timer = setInterval(function() {
            t._scroll(e)
          }, 50)
        }, 300)
      },
      clearScrolling: function() {
        this.started && (this.started = !1, clearTimeout(this.dtimer), this.dtimer = -1)
      },
      stopScrolling: function(e) {
        this.clearScrolling(), this.process && (this.process = !1, clearInterval(this.timer), this.timer = -1, this.scroll(e))
      },
      _scroll: function(e) {
        this.currentValue += e * this.currentGrad;
        var t = this.currentValue,
          n = this._getScrollSize(this.container) - this._getClientSize(this.container),
          i = !1;
        t < 0 ? i = !(t = 0) : n < t && (t = n, i = !0), this._setScrollPos(this.container, t), this.check(t), i ? this.stopScrolling(e) : this._incGrad()
      },
      _incGrad: function() {
        this.currentGrad < this.opt.maxGrad && (this.currentGrad *= this.opt.coefGrad), this.currentGrad > this.opt.maxGrad && (this.currentGrad = this.opt.maxGrad)
      },
      scroll: function(e, t) {
        var n = t ? 0 : this.opt.duration,
          i = this._getScrollSize(this.container) - this._getClientSize(this.container),
          o = this._getScrollPos(this.container),
          r = this._getClientSize(this.container);
        if (e < 0) {
          for (var s = 1; s < this.$items.length; s++)
            if (this._getOffsetPos(this.$items.get(s)) >= o) {
              o = this._getOffsetPos(this.$items.get(s - 1));
              break
            }
        } else if (0 < e)
          for (s = this.$items.length - 2; - 1 < s; s--)
            if (this._getOffsetPos(this.$items.get(s)) + this._getOffsetSize(this.$items.get(s)) <= o + r) {
              o = this._getOffsetPos(this.$items.get(s + 1)) + this._getOffsetSize(this.$items.get(s + 1)) - r;
              break
            }
        o < 0 ? o = 0 : i < o && (o = i), this._setScrollPos(this.container, o, n), this.check(o), this.setIdx(o), -1 < this.stimer && this.resetTimer()
      },
      scrollTo: function(e, t) {
        var n = t ? 0 : this.opt.duration,
          i = this._getScrollSize(this.container) - this._getClientSize(this.container),
          o = (this._getClientSize(this.container), this._getOffsetPos(this.$items.get(e)));
        o < 0 ? o = 0 : i < o && (o = i), this._setScrollPos(this.container, o, n), this.check(o), this.setIdx(o), -1 < this.stimer && this.resetTimer()
      },
      check: function(e) {
        var t = this._getScrollSize(this.container),
          n = this._getClientSize(this.container);
        void 0 === e && (e = this._getScrollPos(this.container)), this.opt.hideArrs && (t <= n ? (this.$prev.hide(), this.$next.hide()) : (this.$prev.show(), this.$next.show())), e <= 0 ? this.$prev.addClass(this.opt.disabledClass) : this.$prev.removeClass(this.opt.disabledClass), t - n <= e ? this.$next.addClass(this.opt.disabledClass) : this.$next.removeClass(this.opt.disabledClass)
      },
      setIdx: function(e) {
        var t = -1;
        void 0 === e && (e = this._getScrollPos(this.container));
        for (var n = 0; n < this.$items.length - 1; n++)
          if (this._getOffsetPos(this.$items.get(n)) >= e) {
            t = n;
            break
          }
        this.idx = t
      },
      destroy: function() {
        this.$prev.off("mousedown", this.prevMouseDown).off("mouseup", this.prevMouseUp).off("mouseleave", this.prevMouseLeave).off("click", this.prevClick), this.$next.off("mousedown", this.nextMouseDown).off("mouseup", this.nextMouseUp).off("mouseleave", this.nextMouseLeave).off("click", this.nextClick), s(this.container).off("resize", this.resizeHandler), s(window).off("resize", this.resizeHandler)
      },
      _setScrollPos: function(e, t, n) {
        var i = this.opt.vertical ? "scrollTop" : "scrollLeft";
        if (0 < n) {
          var o = {};
          o[i] = t, s(e).animate(o, n)
        } else e[i] = t
      },
      _getScrollPos: function(e) {
        return e[this.opt.vertical ? "scrollTop" : "scrollLeft"]
      },
      _getOffsetPos: function(e) {
        return e[this.opt.vertical ? "offsetTop" : "offsetLeft"]
      },
      _getScrollSize: function(e) {
        return e[this.opt.vertical ? "scrollHeight" : "scrollWidth"]
      },
      _getOffsetSize: function(e) {
        return e[this.opt.vertical ? "offsetHeight" : "offsetWidth"]
      },
      _getClientSize: function(e) {
        return e[this.opt.vertical ? "clientHeight" : "clientWidth"]
      }
    }, s.fn.extend({
      scroller: function(e) {
        return this._scroller ? (s.extend(this._scroller.options, e), this._scroller.check()) : this._scroller = new n(this, s.extend({}, t, e)), this
      },
      unscroller: function() {
        return this._scroller && (this._scroller.destroy(), delete this._scroller), this
      }
    })
  }(jQuery),
  function(u, a) {
    var n = 0,
      l = Array.prototype.slice,
      i = u.cleanData;
    u.cleanData = function(e) {
      for (var t, n = 0; null != (t = e[n]); n++) try {
        u(t).triggerHandler("remove")
      } catch (e) {}
      i(e)
    }, u.widget = function(e, n, t) {
      var i, o, r, s, a = {},
        l = e.split(".")[0];
      e = e.split(".")[1], i = l + "-" + e, t || (t = n, n = u.Widget), u.expr[":"][i.toLowerCase()] = function(e) {
        return !!u.data(e, i)
      }, u[l] = u[l] || {}, o = u[l][e], r = u[l][e] = function(e, t) {
        if (!this._createWidget) return new r(e, t);
        arguments.length && this._createWidget(e, t)
      }, u.extend(r, o, {
        version: t.version,
        _proto: u.extend({}, t),
        _childConstructors: []
      }), (s = new n).options = u.widget.extend({}, s.options), u.each(t, function(t, i) {
        var o, r;
        u.isFunction(i) ? a[t] = (o = function() {
          return n.prototype[t].apply(this, arguments)
        }, r = function(e) {
          return n.prototype[t].apply(this, e)
        }, function() {
          var e, t = this._super,
            n = this._superApply;
          return this._super = o, this._superApply = r, e = i.apply(this, arguments), this._super = t, this._superApply = n, e
        }) : a[t] = i
      }), r.prototype = u.widget.extend(s, {
        widgetEventPrefix: o ? s.widgetEventPrefix : e
      }, a, {
        constructor: r,
        namespace: l,
        widgetName: e,
        widgetFullName: i
      }), o ? (u.each(o._childConstructors, function(e, t) {
        var n = t.prototype;
        u.widget(n.namespace + "." + n.widgetName, r, t._proto)
      }), delete o._childConstructors) : n._childConstructors.push(r), u.widget.bridge(e, r)
    }, u.widget.extend = function(e) {
      for (var t, n, i = l.call(arguments, 1), o = 0, r = i.length; o < r; o++)
        for (t in i[o]) n = i[o][t], i[o].hasOwnProperty(t) && n !== a && (u.isPlainObject(n) ? e[t] = u.isPlainObject(e[t]) ? u.widget.extend({}, e[t], n) : u.widget.extend({}, n) : e[t] = n);
      return e
    }, u.widget.bridge = function(r, t) {
      var s = t.prototype.widgetFullName || r;
      u.fn[r] = function(n) {
        var e = "string" == typeof n,
          i = l.call(arguments, 1),
          o = this;
        return n = !e && i.length ? u.widget.extend.apply(null, [n].concat(i)) : n, e ? this.each(function() {
          var e, t = u.data(this, s);
          return t ? u.isFunction(t[n]) && "_" !== n.charAt(0) ? (e = t[n].apply(t, i)) !== t && e !== a ? (o = e && e.jquery ? o.pushStack(e.get()) : e, !1) : void 0 : u.error("no such method '" + n + "' for " + r + " widget instance") : u.error("cannot call methods on " + r + " prior to initialization; attempted to call method '" + n + "'")
        }) : this.each(function() {
          var e = u.data(this, s);
          e ? e.option(n || {})._init() : u.data(this, s, new t(n, this))
        }), o
      }
    }, u.Widget = function() {}, u.Widget._childConstructors = [], u.Widget.prototype = {
      widgetName: "widget",
      widgetEventPrefix: "",
      defaultElement: "<div>",
      options: {
        disabled: !1,
        create: null
      },
      _createWidget: function(e, t) {
        t = u(t || this.defaultElement || this)[0], this.element = u(t), this.uuid = n++, this.eventNamespace = "." + this.widgetName + this.uuid, this.options = u.widget.extend({}, this.options, this._getCreateOptions(), e), this.bindings = u(), this.hoverable = u(), this.focusable = u(), t !== this && (u.data(t, this.widgetFullName, this), this._on(!0, this.element, {
          remove: function(e) {
            e.target === t && this.destroy()
          }
        }), this.document = u(t.style ? t.ownerDocument : t.document || t), this.window = u(this.document[0].defaultView || this.document[0].parentWindow)), this._create(), this._trigger("create", null, this._getCreateEventData()), this._init()
      },
      _getCreateOptions: u.noop,
      _getCreateEventData: u.noop,
      _create: u.noop,
      _init: u.noop,
      destroy: function() {
        this._destroy(), this.element.unbind(this.eventNamespace).removeData(this.widgetName).removeData(this.widgetFullName).removeData(u.camelCase(this.widgetFullName)), this.widget().unbind(this.eventNamespace).removeAttr("aria-disabled").removeClass(this.widgetFullName + "-disabled ui-state-disabled"), this.bindings.unbind(this.eventNamespace), this.hoverable.removeClass("ui-state-hover"), this.focusable.removeClass("ui-state-focus")
      },
      _destroy: u.noop,
      widget: function() {
        return this.element
      },
      option: function(e, t) {
        var n, i, o, r = e;
        if (0 === arguments.length) return u.widget.extend({}, this.options);
        if ("string" == typeof e)
          if (r = {}, e = (n = e.split(".")).shift(), n.length) {
            for (i = r[e] = u.widget.extend({}, this.options[e]), o = 0; o < n.length - 1; o++) i[n[o]] = i[n[o]] || {}, i = i[n[o]];
            if (e = n.pop(), t === a) return i[e] === a ? null : i[e];
            i[e] = t
          } else {
            if (t === a) return this.options[e] === a ? null : this.options[e];
            r[e] = t
          }
        return this._setOptions(r), this
      },
      _setOptions: function(e) {
        var t;
        for (t in e) this._setOption(t, e[t]);
        return this
      },
      _setOption: function(e, t) {
        return this.options[e] = t, "disabled" === e && (this.widget().toggleClass(this.widgetFullName + "-disabled ui-state-disabled", !!t).attr("aria-disabled", t), this.hoverable.removeClass("ui-state-hover"), this.focusable.removeClass("ui-state-focus")), this
      },
      enable: function() {
        return this._setOption("disabled", !1)
      },
      disable: function() {
        return this._setOption("disabled", !0)
      },
      _on: function(s, a, e) {
        var l, c = this;
        "boolean" != typeof s && (e = a, a = s, s = !1), e ? (a = l = u(a), this.bindings = this.bindings.add(a)) : (e = a, a = this.element, l = this.widget()), u.each(e, function(e, t) {
          function n() {
            if (s || !0 !== c.options.disabled && !u(this).hasClass("ui-state-disabled")) return ("string" == typeof t ? c[t] : t).apply(c, arguments)
          }
          "string" != typeof t && (n.guid = t.guid = t.guid || n.guid || u.guid++);
          var i = e.match(/^(\w+)\s*(.*)$/),
            o = i[1] + c.eventNamespace,
            r = i[2];
          r ? l.delegate(r, o, n) : a.bind(o, n)
        })
      },
      _off: function(e, t) {
        t = (t || "").split(" ").join(this.eventNamespace + " ") + this.eventNamespace, e.unbind(t).undelegate(t)
      },
      _delay: function(e, t) {
        var n = this;
        return setTimeout(function() {
          return ("string" == typeof e ? n[e] : e).apply(n, arguments)
        }, t || 0)
      },
      _hoverable: function(e) {
        this.hoverable = this.hoverable.add(e), this._on(e, {
          mouseenter: function(e) {
            u(e.currentTarget).addClass("ui-state-hover")
          },
          mouseleave: function(e) {
            u(e.currentTarget).removeClass("ui-state-hover")
          }
        })
      },
      _focusable: function(e) {
        this.focusable = this.focusable.add(e), this._on(e, {
          focusin: function(e) {
            u(e.currentTarget).addClass("ui-state-focus")
          },
          focusout: function(e) {
            u(e.currentTarget).removeClass("ui-state-focus")
          }
        })
      },
      _trigger: function(e, t, n) {
        var i, o, r = this.options[e];
        if (n = n || {}, (t = u.Event(t)).type = (e === this.widgetEventPrefix ? e : this.widgetEventPrefix + e).toLowerCase(), t.target = this.element[0], o = t.originalEvent)
          for (i in o) i in t || (t[i] = o[i]);
        return this.element.trigger(t, n), !(u.isFunction(r) && !1 === r.apply(this.element[0], [t].concat(n)) || t.isDefaultPrevented())
      }
    }, u.each({
      show: "fadeIn",
      hide: "fadeOut"
    }, function(r, s) {
      u.Widget.prototype["_" + r] = function(t, e, n) {
        "string" == typeof e && (e = {
          effect: e
        });
        var i, o = e ? !0 === e || "number" == typeof e ? s : e.effect || s : r;
        "number" == typeof(e = e || {}) && (e = {
          duration: e
        }), i = !u.isEmptyObject(e), e.complete = n, e.delay && t.delay(e.delay), i && u.effects && u.effects.effect[o] ? t[r](e) : o !== r && t[o] ? t[o](e.duration, e.easing, n) : t.queue(function(e) {
          u(this)[r](), n && n.call(t[0]), e()
        })
      }
    })
  }(jQuery),
  function(u, e) {
    var n, a = "ui-effects-";
    u.effects = {
        effect: {}
      },
      function(u, d) {
        var c, f = /^([\-+])=\s*(\d+\.?\d*)/,
          e = [{
            re: /rgba?\(\s*(\d{1,3})\s*,\s*(\d{1,3})\s*,\s*(\d{1,3})\s*(?:,\s*(\d?(?:\.\d+)?)\s*)?\)/,
            parse: function(e) {
              return [e[1], e[2], e[3], e[4]]
            }
          }, {
            re: /rgba?\(\s*(\d+(?:\.\d+)?)\%\s*,\s*(\d+(?:\.\d+)?)\%\s*,\s*(\d+(?:\.\d+)?)\%\s*(?:,\s*(\d?(?:\.\d+)?)\s*)?\)/,
            parse: function(e) {
              return [2.55 * e[1], 2.55 * e[2], 2.55 * e[3], e[4]]
            }
          }, {
            re: /#([a-f0-9]{2})([a-f0-9]{2})([a-f0-9]{2})/,
            parse: function(e) {
              return [parseInt(e[1], 16), parseInt(e[2], 16), parseInt(e[3], 16)]
            }
          }, {
            re: /#([a-f0-9])([a-f0-9])([a-f0-9])/,
            parse: function(e) {
              return [parseInt(e[1] + e[1], 16), parseInt(e[2] + e[2], 16), parseInt(e[3] + e[3], 16)]
            }
          }, {
            re: /hsla?\(\s*(\d+(?:\.\d+)?)\s*,\s*(\d+(?:\.\d+)?)\%\s*,\s*(\d+(?:\.\d+)?)\%\s*(?:,\s*(\d?(?:\.\d+)?)\s*)?\)/,
            space: "hsla",
            parse: function(e) {
              return [e[1], e[2] / 100, e[3] / 100, e[4]]
            }
          }],
          h = u.Color = function(e, t, n, i) {
            return new u.Color.fn.parse(e, t, n, i)
          },
          p = {
            rgba: {
              props: {
                red: {
                  idx: 0,
                  type: "byte"
                },
                green: {
                  idx: 1,
                  type: "byte"
                },
                blue: {
                  idx: 2,
                  type: "byte"
                }
              }
            },
            hsla: {
              props: {
                hue: {
                  idx: 0,
                  type: "degrees"
                },
                saturation: {
                  idx: 1,
                  type: "percent"
                },
                lightness: {
                  idx: 2,
                  type: "percent"
                }
              }
            }
          },
          g = {
            byte: {
              floor: !0,
              max: 255
            },
            percent: {
              max: 1
            },
            degrees: {
              mod: 360,
              floor: !0
            }
          },
          s = h.support = {},
          t = u("<p>")[0],
          m = u.each;

        function v(e, t, n) {
          var i = g[t.type] || {};
          return null == e ? n || !t.def ? null : t.def : (e = i.floor ? ~~e : parseFloat(e), isNaN(e) ? t.def : i.mod ? (e + i.mod) % i.mod : e < 0 ? 0 : i.max < e ? i.max : e)
        }

        function a(s) {
          var a = h(),
            l = a._rgba = [];
          return s = s.toLowerCase(), m(e, function(e, t) {
            var n, i = t.re.exec(s),
              o = i && t.parse(i),
              r = t.space || "rgba";
            if (o) return n = a[r](o), a[p[r].cache] = n[p[r].cache], l = a._rgba = n._rgba, !1
          }), l.length ? ("0,0,0,0" === l.join() && u.extend(l, c.transparent), a) : c[s]
        }

        function l(e, t, n) {
          return 6 * (n = (n + 1) % 1) < 1 ? e + (t - e) * n * 6 : 2 * n < 1 ? t : 3 * n < 2 ? e + (t - e) * (2 / 3 - n) * 6 : e
        }
        t.style.cssText = "background-color:rgba(1,1,1,.5)", s.rgba = -1 < t.style.backgroundColor.indexOf("rgba"), m(p, function(e, t) {
          t.cache = "_" + e, t.props.alpha = {
            idx: 3,
            type: "percent",
            def: 1
          }
        }), h.fn = u.extend(h.prototype, {
          parse: function(o, e, t, n) {
            if (o === d) return this._rgba = [null, null, null, null], this;
            (o.jquery || o.nodeType) && (o = u(o).css(e), e = d);
            var r = this,
              i = u.type(o),
              s = this._rgba = [];
            return e !== d && (o = [o, e, t, n], i = "array"), "string" === i ? this.parse(a(o) || c._default) : "array" === i ? (m(p.rgba.props, function(e, t) {
              s[t.idx] = v(o[t.idx], t)
            }), this) : "object" === i ? (m(p, o instanceof h ? function(e, t) {
              o[t.cache] && (r[t.cache] = o[t.cache].slice())
            } : function(e, n) {
              var i = n.cache;
              m(n.props, function(e, t) {
                if (!r[i] && n.to) {
                  if ("alpha" === e || null == o[e]) return;
                  r[i] = n.to(r._rgba)
                }
                r[i][t.idx] = v(o[e], t, !0)
              }), r[i] && u.inArray(null, r[i].slice(0, 3)) < 0 && (r[i][3] = 1, n.from && (r._rgba = n.from(r[i])))
            }), this) : void 0
          },
          is: function(e) {
            var o = h(e),
              r = !0,
              s = this;
            return m(p, function(e, t) {
              var n, i = o[t.cache];
              return i && (n = s[t.cache] || t.to && t.to(s._rgba) || [], m(t.props, function(e, t) {
                if (null != i[t.idx]) return r = i[t.idx] === n[t.idx]
              })), r
            }), r
          },
          _space: function() {
            var n = [],
              i = this;
            return m(p, function(e, t) {
              i[t.cache] && n.push(e)
            }), n.pop()
          },
          transition: function(e, s) {
            var a = h(e),
              t = a._space(),
              n = p[t],
              i = 0 === this.alpha() ? h("transparent") : this,
              l = i[n.cache] || n.to(i._rgba),
              c = l.slice();
            return a = a[n.cache], m(n.props, function(e, t) {
              var n = t.idx,
                i = l[n],
                o = a[n],
                r = g[t.type] || {};
              null !== o && (null === i ? c[n] = o : (r.mod && (o - i > r.mod / 2 ? i += r.mod : i - o > r.mod / 2 && (i -= r.mod)), c[n] = v((o - i) * s + i, t)))
            }), this[t](c)
          },
          blend: function(e) {
            if (1 === this._rgba[3]) return this;
            var t = this._rgba.slice(),
              n = t.pop(),
              i = h(e)._rgba;
            return h(u.map(t, function(e, t) {
              return (1 - n) * i[t] + n * e
            }))
          },
          toRgbaString: function() {
            var e = "rgba(",
              t = u.map(this._rgba, function(e, t) {
                return null == e ? 2 < t ? 1 : 0 : e
              });
            return 1 === t[3] && (t.pop(), e = "rgb("), e + t.join() + ")"
          },
          toHslaString: function() {
            var e = "hsla(",
              t = u.map(this.hsla(), function(e, t) {
                return null == e && (e = 2 < t ? 1 : 0), t && t < 3 && (e = Math.round(100 * e) + "%"), e
              });
            return 1 === t[3] && (t.pop(), e = "hsl("), e + t.join() + ")"
          },
          toHexString: function(e) {
            var t = this._rgba.slice(),
              n = t.pop();
            return e && t.push(~~(255 * n)), "#" + u.map(t, function(e) {
              return 1 === (e = (e || 0).toString(16)).length ? "0" + e : e
            }).join("")
          },
          toString: function() {
            return 0 === this._rgba[3] ? "transparent" : this.toRgbaString()
          }
        }), h.fn.parse.prototype = h.fn, p.hsla.to = function(e) {
          if (null == e[0] || null == e[1] || null == e[2]) return [null, null, null, e[3]];
          var t, n, i = e[0] / 255,
            o = e[1] / 255,
            r = e[2] / 255,
            s = e[3],
            a = Math.max(i, o, r),
            l = Math.min(i, o, r),
            c = a - l,
            u = a + l,
            d = .5 * u;
          return t = l === a ? 0 : i === a ? 60 * (o - r) / c + 360 : o === a ? 60 * (r - i) / c + 120 : 60 * (i - o) / c + 240, n = 0 === c ? 0 : d <= .5 ? c / u : c / (2 - u), [Math.round(t) % 360, n, d, null == s ? 1 : s]
        }, p.hsla.from = function(e) {
          if (null == e[0] || null == e[1] || null == e[2]) return [null, null, null, e[3]];
          var t = e[0] / 360,
            n = e[1],
            i = e[2],
            o = e[3],
            r = i <= .5 ? i * (1 + n) : i + n - i * n,
            s = 2 * i - r;
          return [Math.round(255 * l(s, r, t + 1 / 3)), Math.round(255 * l(s, r, t)), Math.round(255 * l(s, r, t - 1 / 3)), o]
        }, m(p, function(l, e) {
          var n = e.props,
            s = e.cache,
            a = e.to,
            c = e.from;
          h.fn[l] = function(e) {
            if (a && !this[s] && (this[s] = a(this._rgba)), e === d) return this[s].slice();
            var t, i = u.type(e),
              o = "array" === i || "object" === i ? e : arguments,
              r = this[s].slice();
            return m(n, function(e, t) {
              var n = o["object" === i ? e : t.idx];
              null == n && (n = r[t.idx]), r[t.idx] = v(n, t)
            }), c ? ((t = h(c(r)))[s] = r, t) : h(r)
          }, m(n, function(s, a) {
            h.fn[s] || (h.fn[s] = function(e) {
              var t, n = u.type(e),
                i = "alpha" === s ? this._hsla ? "hsla" : "rgba" : l,
                o = this[i](),
                r = o[a.idx];
              return "undefined" === n ? r : ("function" === n && (e = e.call(this, r), n = u.type(e)), null == e && a.empty ? this : ("string" === n && (t = f.exec(e)) && (e = r + parseFloat(t[2]) * ("+" === t[1] ? 1 : -1)), o[a.idx] = e, this[i](o)))
            })
          })
        }), h.hook = function(e) {
          var t = e.split(" ");
          m(t, function(e, r) {
            u.cssHooks[r] = {
              set: function(e, t) {
                var n, i, o = "";
                if ("transparent" !== t && ("string" !== u.type(t) || (n = a(t)))) {
                  if (t = h(n || t), !s.rgba && 1 !== t._rgba[3]) {
                    for (i = "backgroundColor" === r ? e.parentNode : e;
                      ("" === o || "transparent" === o) && i && i.style;) try {
                      o = u.css(i, "backgroundColor"), i = i.parentNode
                    } catch (e) {}
                    t = t.blend(o && "transparent" !== o ? o : "_default")
                  }
                  t = t.toRgbaString()
                }
                try {
                  e.style[r] = t
                } catch (e) {}
              }
            }, u.fx.step[r] = function(e) {
              e.colorInit || (e.start = h(e.elem, r), e.end = h(e.end), e.colorInit = !0), u.cssHooks[r].set(e.elem, e.start.transition(e.end, e.pos))
            }
          })
        }, h.hook("backgroundColor borderBottomColor borderLeftColor borderRightColor borderTopColor color columnRuleColor outlineColor textDecorationColor textEmphasisColor"), u.cssHooks.borderColor = {
          expand: function(n) {
            var i = {};
            return m(["Top", "Right", "Bottom", "Left"], function(e, t) {
              i["border" + t + "Color"] = n
            }), i
          }
        }, c = u.Color.names = {
          aqua: "#00ffff",
          black: "#000000",
          blue: "#0000ff",
          fuchsia: "#ff00ff",
          gray: "#808080",
          green: "#008000",
          lime: "#00ff00",
          maroon: "#800000",
          navy: "#000080",
          olive: "#808000",
          purple: "#800080",
          red: "#ff0000",
          silver: "#c0c0c0",
          teal: "#008080",
          white: "#ffffff",
          yellow: "#ffff00",
          transparent: [null, null, null, 0],
          _default: "#ffffff"
        }
      }(jQuery),
      function() {
        var r, o, s, a = ["add", "remove", "toggle"],
          l = {
            border: 1,
            borderBottom: 1,
            borderColor: 1,
            borderLeft: 1,
            borderRight: 1,
            borderTop: 1,
            borderWidth: 1,
            margin: 1,
            padding: 1
          };

        function c(e) {
          var t, n, i = e.ownerDocument.defaultView ? e.ownerDocument.defaultView.getComputedStyle(e, null) : e.currentStyle,
            o = {};
          if (i && i.length && i[0] && i[i[0]])
            for (n = i.length; n--;) "string" == typeof i[t = i[n]] && (o[u.camelCase(t)] = i[t]);
          else
            for (t in i) "string" == typeof i[t] && (o[t] = i[t]);
          return o
        }
        u.each(["borderLeftStyle", "borderRightStyle", "borderBottomStyle", "borderTopStyle"], function(e, t) {
          u.fx.step[t] = function(e) {
            ("none" !== e.end && !e.setAttr || 1 === e.pos && !e.setAttr) && (jQuery.style(e.elem, t, e.end), e.setAttr = !0)
          }
        }), u.fn.addBack || (u.fn.addBack = function(e) {
          return this.add(null == e ? this.prevObject : this.prevObject.filter(e))
        }), u.effects.animateClass = function(o, e, t, n) {
          var r = u.speed(e, t, n);
          return this.queue(function() {
            var e, n = u(this),
              t = n.attr("class") || "",
              i = r.children ? n.find("*").addBack() : n;
            i = i.map(function() {
              return {
                el: u(this),
                start: c(this)
              }
            }), (e = function() {
              u.each(a, function(e, t) {
                o[t] && n[t + "Class"](o[t])
              })
            })(), i = i.map(function() {
              return this.end = c(this.el[0]), this.diff = function(e, t) {
                var n, i, o = {};
                for (n in t) i = t[n], e[n] !== i && (l[n] || !u.fx.step[n] && isNaN(parseFloat(i)) || (o[n] = i));
                return o
              }(this.start, this.end), this
            }), n.attr("class", t), i = i.map(function() {
              var e = this,
                t = u.Deferred(),
                n = u.extend({}, r, {
                  queue: !1,
                  complete: function() {
                    t.resolve(e)
                  }
                });
              return this.el.animate(this.diff, n), t.promise()
            }), u.when.apply(u, i.get()).done(function() {
              e(), u.each(arguments, function() {
                var t = this.el;
                u.each(this.diff, function(e) {
                  t.css(e, "")
                })
              }), r.complete.call(n[0])
            })
          })
        }, u.fn.extend({
          addClass: (s = u.fn.addClass, function(e, t, n, i) {
            return t ? u.effects.animateClass.call(this, {
              add: e
            }, t, n, i) : s.apply(this, arguments)
          }),
          removeClass: (o = u.fn.removeClass, function(e, t, n, i) {
            return 1 < arguments.length ? u.effects.animateClass.call(this, {
              remove: e
            }, t, n, i) : o.apply(this, arguments)
          }),
          toggleClass: (r = u.fn.toggleClass, function(e, t, n, i, o) {
            return "boolean" == typeof t || void 0 === t ? n ? u.effects.animateClass.call(this, t ? {
              add: e
            } : {
              remove: e
            }, n, i, o) : r.apply(this, arguments) : u.effects.animateClass.call(this, {
              toggle: e
            }, t, n, i)
          }),
          switchClass: function(e, t, n, i, o) {
            return u.effects.animateClass.call(this, {
              add: t,
              remove: e
            }, n, i, o)
          }
        })
      }(),
      function() {
        function i(e, t, n, i) {
          return u.isPlainObject(e) && (e = (t = e).effect), e = {
            effect: e
          }, null == t && (t = {}), u.isFunction(t) && (i = t, n = null, t = {}), ("number" == typeof t || u.fx.speeds[t]) && (i = n, n = t, t = {}), u.isFunction(n) && (i = n, n = null), t && u.extend(e, t), n = n || t.duration, e.duration = u.fx.off ? 0 : "number" == typeof n ? n : n in u.fx.speeds ? u.fx.speeds[n] : u.fx.speeds._default, e.complete = i || t.complete, e
        }

        function n(e) {
          return !(e && "number" != typeof e && !u.fx.speeds[e]) || ("string" == typeof e && !u.effects.effect[e] || (!!u.isFunction(e) || "object" == typeof e && !e.effect))
        }
        var o, r, s;
        u.extend(u.effects, {
          version: "1.10.3",
          save: function(e, t) {
            for (var n = 0; n < t.length; n++) null !== t[n] && e.data(a + t[n], e[0].style[t[n]])
          },
          restore: function(e, t) {
            var n, i;
            for (i = 0; i < t.length; i++) null !== t[i] && (void 0 === (n = e.data(a + t[i])) && (n = ""), e.css(t[i], n))
          },
          setMode: function(e, t) {
            return "toggle" === t && (t = e.is(":hidden") ? "show" : "hide"), t
          },
          getBaseline: function(e, t) {
            var n, i;
            switch (e[0]) {
              case "top":
                n = 0;
                break;
              case "middle":
                n = .5;
                break;
              case "bottom":
                n = 1;
                break;
              default:
                n = e[0] / t.height
            }
            switch (e[1]) {
              case "left":
                i = 0;
                break;
              case "center":
                i = .5;
                break;
              case "right":
                i = 1;
                break;
              default:
                i = e[1] / t.width
            }
            return {
              x: i,
              y: n
            }
          },
          createWrapper: function(n) {
            if (n.parent().is(".ui-effects-wrapper")) return n.parent();
            var i = {
                width: n.outerWidth(!0),
                height: n.outerHeight(!0),
                float: n.css("float")
              },
              e = u("<div></div>").addClass("ui-effects-wrapper").css({
                fontSize: "100%",
                background: "transparent",
                border: "none",
                margin: 0,
                padding: 0
              }),
              t = {
                width: n.width(),
                height: n.height()
              },
              o = document.activeElement;
            try {
              o.id
            } catch (e) {
              o = document.body
            }
            return n.wrap(e), (n[0] === o || u.contains(n[0], o)) && u(o).focus(), e = n.parent(), "static" === n.css("position") ? (e.css({
              position: "relative"
            }), n.css({
              position: "relative"
            })) : (u.extend(i, {
              position: n.css("position"),
              zIndex: n.css("z-index")
            }), u.each(["top", "left", "bottom", "right"], function(e, t) {
              i[t] = n.css(t), isNaN(parseInt(i[t], 10)) && (i[t] = "auto")
            }), n.css({
              position: "relative",
              top: 0,
              left: 0,
              right: "auto",
              bottom: "auto"
            })), n.css(t), e.css(i).show()
          },
          removeWrapper: function(e) {
            var t = document.activeElement;
            return e.parent().is(".ui-effects-wrapper") && (e.parent().replaceWith(e), (e[0] === t || u.contains(e[0], t)) && u(t).focus()), e
          },
          setTransition: function(i, e, o, r) {
            return r = r || {}, u.each(e, function(e, t) {
              var n = i.cssUnit(t);
              0 < n[0] && (r[t] = n[0] * o + n[1])
            }), r
          }
        }), u.fn.extend({
          effect: function() {
            var r = i.apply(this, arguments),
              e = r.mode,
              t = r.queue,
              s = u.effects.effect[r.effect];
            if (u.fx.off || !s) return e ? this[e](r.duration, r.complete) : this.each(function() {
              r.complete && r.complete.call(this)
            });

            function n(e) {
              var t = u(this),
                n = r.complete,
                i = r.mode;

              function o() {
                u.isFunction(n) && n.call(t[0]), u.isFunction(e) && e()
              }(t.is(":hidden") ? "hide" === i : "show" === i) ? (t[i](), o()) : s.call(t[0], r, o)
            }
            return !1 === t ? this.each(n) : this.queue(t || "fx", n)
          },
          show: (s = u.fn.show, function(e) {
            if (n(e)) return s.apply(this, arguments);
            var t = i.apply(this, arguments);
            return t.mode = "show", this.effect.call(this, t)
          }),
          hide: (r = u.fn.hide, function(e) {
            if (n(e)) return r.apply(this, arguments);
            var t = i.apply(this, arguments);
            return t.mode = "hide", this.effect.call(this, t)
          }),
          toggle: (o = u.fn.toggle, function(e) {
            if (n(e) || "boolean" == typeof e) return o.apply(this, arguments);
            var t = i.apply(this, arguments);
            return t.mode = "toggle", this.effect.call(this, t)
          }),
          cssUnit: function(e) {
            var n = this.css(e),
              i = [];
            return u.each(["em", "px", "%", "pt"], function(e, t) {
              0 < n.indexOf(t) && (i = [parseFloat(n), t])
            }), i
          }
        })
      }(), n = {}, u.each(["Quad", "Cubic", "Quart", "Quint", "Expo"], function(t, e) {
        n[e] = function(e) {
          return Math.pow(e, t + 2)
        }
      }), u.extend(n, {
        Sine: function(e) {
          return 1 - Math.cos(e * Math.PI / 2)
        },
        Circ: function(e) {
          return 1 - Math.sqrt(1 - e * e)
        },
        Elastic: function(e) {
          return 0 === e || 1 === e ? e : -Math.pow(2, 8 * (e - 1)) * Math.sin((80 * (e - 1) - 7.5) * Math.PI / 15)
        },
        Back: function(e) {
          return e * e * (3 * e - 2)
        },
        Bounce: function(e) {
          for (var t, n = 4; e < ((t = Math.pow(2, --n)) - 1) / 11;);
          return 1 / Math.pow(4, 3 - n) - 7.5625 * Math.pow((3 * t - 2) / 22 - e, 2)
        }
      }), u.each(n, function(e, t) {
        u.easing["easeIn" + e] = t, u.easing["easeOut" + e] = function(e) {
          return 1 - t(1 - e)
        }, u.easing["easeInOut" + e] = function(e) {
          return e < .5 ? t(2 * e) / 2 : 1 - t(-2 * e + 2) / 2
        }
      })
  }(jQuery),
  function(e) {
    "function" == typeof define && define.amd ? define(["jquery"], e) : "object" == typeof exports ? module.exports = e : e(jQuery)
  }(function(c) {
    var u, d, e = ["wheel", "mousewheel", "DOMMouseScroll", "MozMousePixelScroll"],
      t = "onwheel" in document || 9 <= document.documentMode ? ["wheel"] : ["mousewheel", "DomMouseScroll", "MozMousePixelScroll"];
    if (c.event.fixHooks)
      for (var n = e.length; n;) c.event.fixHooks[e[--n]] = c.event.mouseHooks;

    function i(e) {
      var t, n, i, o = e || window.event,
        r = [].slice.call(arguments, 1),
        s = 0,
        a = 0,
        l = 0;
      return (e = c.event.fix(o)).type = "mousewheel", o.wheelDelta && (s = o.wheelDelta), o.detail && (s = -1 * o.detail), l = s, void 0 !== o.axis && o.axis === o.HORIZONTAL_AXIS && (l = 0, a = -1 * s), o.deltaY && (s = l = -1 * o.deltaY), o.deltaX && (s = -1 * (a = o.deltaX)), void 0 !== o.wheelDeltaY && (l = o.wheelDeltaY), void 0 !== o.wheelDeltaX && (a = -1 * o.wheelDeltaX), t = Math.abs(s), (!u || t < u) && (u = t), n = Math.max(Math.abs(l), Math.abs(a)), (!d || n < d) && (d = n), i = 0 < s ? "floor" : "ceil", s = Math[i](s / u), a = Math[i](a / d), l = Math[i](l / d), r.unshift(e, s, a, l), (c.event.dispatch || c.event.handle).apply(this, r)
    }
    c.event.special.mousewheel = {
      setup: function() {
        if (this.addEventListener)
          for (var e = t.length; e;) this.addEventListener(t[--e], i, !1);
        else this.onmousewheel = i
      },
      teardown: function() {
        if (this.removeEventListener)
          for (var e = t.length; e;) this.removeEventListener(t[--e], i, !1);
        else this.onmousewheel = null
      }
    }, c.fn.extend({
      mousewheel: function(e) {
        return e ? this.bind("mousewheel", e) : this.trigger("mousewheel")
      },
      unmousewheel: function(e) {
        return this.unbind("mousewheel", e)
      }
    })
  }),
  function(v) {
    "use strict";
    var y = {
        cursor: "move",
        decelerate: !0,
        triggerHardware: !1,
        y: !0,
        x: !0,
        slowdown: .9,
        maxvelocity: 40,
        throttleFPS: 60,
        movingClass: {
          up: "kinetic-moving-up",
          down: "kinetic-moving-down",
          left: "kinetic-moving-left",
          right: "kinetic-moving-right"
        },
        deceleratingClass: {
          up: "kinetic-decelerating-up",
          down: "kinetic-decelerating-down",
          left: "kinetic-decelerating-left",
          right: "kinetic-decelerating-right"
        }
      },
      b = "kinetic-settings",
      i = "kinetic-active";
    window.requestAnimationFrame || (window.requestAnimationFrame = window.webkitRequestAnimationFrame || window.mozRequestAnimationFrame || window.oRequestAnimationFrame || window.msRequestAnimationFrame || function(e, t) {
      window.setTimeout(e, 1e3 / 60)
    }), v.support = v.support || {}, v.extend(v.support, {
      touch: "ontouchend" in document
    });
    var o = function() {
        return !1
      },
      r = function(e, t) {
        return 0 === Math.floor(Math.abs(e)) ? 0 : e * t
      },
      x = function(e, t) {
        var n = e;
        return 0 < e ? t < e && (n = t) : e < 0 - t && (n = 0 - t), n
      },
      w = function(e, t) {
        this.removeClass(e.movingClass.up).removeClass(e.movingClass.down).removeClass(e.movingClass.left).removeClass(e.movingClass.right).removeClass(e.deceleratingClass.up).removeClass(e.deceleratingClass.down).removeClass(e.deceleratingClass.left).removeClass(e.deceleratingClass.right), 0 < e.velocity && this.addClass(t.right), e.velocity < 0 && this.addClass(t.left), 0 < e.velocityY && this.addClass(t.down), e.velocityY < 0 && this.addClass(t.up)
      },
      s = function(e, t) {
        t.velocity = 0, t.velocityY = 0, t.decelerate = !0, "function" == typeof t.stopped && t.stopped.call(e, t)
      },
      S = function(e, t) {
        var n = e[0];
        t.x && 0 < n.scrollWidth ? (n.scrollLeft = t.scrollLeft = n.scrollLeft + t.velocity, 0 < Math.abs(t.velocity) && (t.velocity = t.decelerate ? r(t.velocity, t.slowdown) : t.velocity)) : t.velocity = 0, t.y && 0 < n.scrollHeight ? (n.scrollTop = t.scrollTop = n.scrollTop + t.velocityY, 0 < Math.abs(t.velocityY) && (t.velocityY = t.decelerate ? r(t.velocityY, t.slowdown) : t.velocityY)) : t.velocityY = 0, w.call(e, t, t.deceleratingClass), "function" == typeof t.moved && t.moved.call(e, t), 0 < Math.abs(t.velocity) || 0 < Math.abs(t.velocityY) ? window.requestAnimationFrame(function() {
          S(e, t)
        }) : s(e, t)
      },
      k = function(e, t) {
        e[0];
        v.support.touch ? e.bind("touchstart", t.events.touchStart).bind("touchend", t.events.inputEnd).bind("touchmove", t.events.touchMove) : e.mousedown(t.events.inputDown).mouseup(t.events.inputEnd).mousemove(t.events.inputMove), e.click(t.events.inputClick).scroll(t.events.scroll).bind("selectstart", o).bind("dragstart", t.events.dragStart)
      },
      t = function(m) {
        this.addClass(i).each(function() {
          var t = this,
            n = v(this);
          if (!n.data(b)) {
            var i, o, r, s, a = v.extend({}, y, m),
              l = !1,
              c = !1,
              u = !1,
              d = 1e3 / a.throttleFPS;
            a.velocity = 0, a.velocityY = 0;
            var e = function() {
              u = o = i = !1
            };
            v(document).mouseup(e).click(e);
            var f = function() {
                a.velocity = x(l - i, a.maxvelocity), a.velocityY = x(c - o, a.maxvelocity)
              },
              h = function(e) {
                return !v.isFunction(a.filterTarget) || !1 !== a.filterTarget.call(t, e)
              },
              p = function(e, t) {
                u = !0, a.velocity = l = 0, a.velocityY = c = 0, i = e, o = t
              },
              g = function(e, t) {
                (!r || new Date > new Date(r.getTime() + d)) && (r = new Date, u && (i || o) && (s && (v(s).blur(), s = null, n.focus()), a.decelerate = !1, a.velocity = a.velocityY = 0, n[0].scrollLeft = a.scrollLeft = a.x ? n[0].scrollLeft - (e - i) : n[0].scrollLeft, n[0].scrollTop = a.scrollTop = a.y ? n[0].scrollTop - (t - o) : n[0].scrollTop, l = i, c = o, i = e, o = t, f(), w.call(n, a, a.movingClass), "function" == typeof a.moved && a.moved.call(n, a)))
              };
            a.events = {
              touchStart: function(e) {
                var t;
                h(e.target) && (t = e.originalEvent.touches[0], p(t.clientX, t.clientY), e.stopPropagation())
              },
              touchMove: function(e) {
                var t;
                u && (t = e.originalEvent.touches[0], g(t.clientX, t.clientY), e.preventDefault && e.preventDefault())
              },
              inputDown: function(e) {
                h(e.target) && (p(e.clientX, e.clientY), s = e.target, "IMG" === e.target.nodeName && e.preventDefault(), e.stopPropagation())
              },
              inputEnd: function(e) {
                i && l && !1 === a.decelerate && (a.decelerate = !0, f(), i = l = u = !1, S(n, a)), s = null, e.preventDefault && e.preventDefault()
              },
              inputMove: function(e) {
                u && (g(e.clientX, e.clientY), e.preventDefault && e.preventDefault())
              },
              scroll: function(e) {
                "function" == typeof a.moved && a.moved.call(n, a), e.preventDefault && e.preventDefault()
              },
              inputClick: function(e) {
                if (0 < Math.abs(a.velocity)) return e.preventDefault(), !1
              },
              dragStart: function(e) {
                if (s) return !1
              }
            }, k(n, a), n.data(b, a).css("cursor", a.cursor), a.triggerHardware && n.css({
              "-webkit-transform": "translate3d(0,0,0)",
              "-webkit-perspective": "1000",
              "-webkit-backface-visibility": "hidden"
            })
          }
        })
      };
    v.kinetic = {
      settingsKey: b,
      callMethods: {
        start: function(e, t) {
          var n = v(this);
          (e = v.extend(e, t)) && (e.decelerate = !1, S(n, e))
        },
        end: function(e, t) {
          v(this);
          e && (e.decelerate = !0)
        },
        stop: function(e, t) {
          var n = v(this);
          s(n, e)
        },
        detach: function(e, t) {
          var n = v(this);
          ! function(e, t) {
            e[0];
            v.support.touch ? e.unbind("touchstart", t.events.touchStart).unbind("touchend", t.events.inputEnd).unbind("touchmove", t.events.touchMove) : e.unbind("mousedown", t.events.inputDown).unbind("mouseup", t.events.inputEnd).unbind("mousemove", t.events.inputMove).unbind("scroll", t.events.scroll), e.unbind("click", t.events.inputClick).unbind("selectstart", o), e.unbind("dragstart", t.events.dragStart)
          }(n, e), n.removeClass(i).css("cursor", "")
        },
        attach: function(e, t) {
          var n = v(this);
          k(n, e), n.addClass(i).css("cursor", "move")
        }
      }
    }, v.fn.kinetic = function(e) {
      return "string" == typeof e ? function(e, t) {
        var n = v.kinetic.callMethods[e],
          i = Array.prototype.slice.call(arguments);
        n && this.each(function() {
          var e = i.slice(1),
            t = v(this).data(b);
          e.unshift(t), n.apply(this, e)
        })
      }.apply(this, arguments) : t.call(this, e), this
    }
  }(window.jQuery || window.Zepto),
  function(g) {
    g.widget("thomaskahn.smoothDivScroll", {
      options: {
        scrollingHotSpotLeftClass: "scrollingHotSpotLeft",
        scrollingHotSpotRightClass: "scrollingHotSpotRight",
        scrollingHotSpotLeftVisibleClass: "scrollingHotSpotLeftVisible",
        scrollingHotSpotRightVisibleClass: "scrollingHotSpotRightVisible",
        scrollableAreaClass: "scrollableArea",
        scrollWrapperClass: "scrollWrapper",
        hiddenOnStart: !1,
        getContentOnLoad: {},
        countOnlyClass: "",
        startAtElementId: "",
        hotSpotScrolling: !0,
        hotSpotScrollingStep: 15,
        hotSpotScrollingInterval: 10,
        hotSpotMouseDownSpeedBooster: 3,
        visibleHotSpotBackgrounds: "hover",
        hotSpotsVisibleTime: 5e3,
        easingAfterHotSpotScrolling: !0,
        easingAfterHotSpotScrollingDistance: 10,
        easingAfterHotSpotScrollingDuration: 300,
        easingAfterHotSpotScrollingFunction: "easeOutQuart",
        mousewheelScrolling: "",
        mousewheelScrollingStep: 70,
        easingAfterMouseWheelScrolling: !0,
        easingAfterMouseWheelScrollingDuration: 300,
        easingAfterMouseWheelScrollingFunction: "easeOutQuart",
        manualContinuousScrolling: !1,
        autoScrollingMode: "",
        autoScrollingDirection: "endlessLoopRight",
        autoScrollingStep: 1,
        autoScrollingInterval: 10,
        touchScrolling: !1,
        scrollToAnimationDuration: 1e3,
        scrollToEasingFunction: "easeOutQuart"
      },
      _create: function() {
        var r = this,
          s = this.options,
          a = this.element;
        a.data("scrollWrapper", a.find("." + s.scrollWrapperClass)), a.data("scrollingHotSpotRight", a.find("." + s.scrollingHotSpotRightClass)), a.data("scrollingHotSpotLeft", a.find("." + s.scrollingHotSpotLeftClass)), a.data("scrollableArea", a.find("." + s.scrollableAreaClass)), 0 < a.data("scrollingHotSpotRight").length && a.data("scrollingHotSpotRight").detach(), 0 < a.data("scrollingHotSpotLeft").length && a.data("scrollingHotSpotLeft").detach(), 0 === a.data("scrollableArea").length && 0 === a.data("scrollWrapper").length ? (a.wrapInner("<div class='" + s.scrollableAreaClass + "'>").wrapInner("<div class='" + s.scrollWrapperClass + "'>"), a.data("scrollWrapper", a.find("." + s.scrollWrapperClass)), a.data("scrollableArea", a.find("." + s.scrollableAreaClass))) : 0 === a.data("scrollWrapper").length ? (a.wrapInner("<div class='" + s.scrollWrapperClass + "'>"), a.data("scrollWrapper", a.find("." + s.scrollWrapperClass))) : 0 === a.data("scrollableArea").length && (a.data("scrollWrapper").wrapInner("<div class='" + s.scrollableAreaClass + "'>"), a.data("scrollableArea", a.find("." + s.scrollableAreaClass))), 0 === a.data("scrollingHotSpotRight").length ? (a.prepend("<div class='" + s.scrollingHotSpotRightClass + "'></div>"), a.data("scrollingHotSpotRight", a.find("." + s.scrollingHotSpotRightClass))) : a.prepend(a.data("scrollingHotSpotRight")), 0 === a.data("scrollingHotSpotLeft").length ? (a.prepend("<div class='" + s.scrollingHotSpotLeftClass + "'></div>"), a.data("scrollingHotSpotLeft", a.find("." + s.scrollingHotSpotLeftClass))) : a.prepend(a.data("scrollingHotSpotLeft")), a.data("speedBooster", 1), a.data("scrollXPos", 0), a.data("hotSpotWidth", a.data("scrollingHotSpotLeft").innerWidth()), a.data("scrollableAreaWidth", 0), a.data("startingPosition", 0), a.data("rightScrollingInterval", null), a.data("leftScrollingInterval", null), a.data("autoScrollingInterval", null), a.data("hideHotSpotBackgroundsInterval", null), a.data("previousScrollLeft", 0), a.data("pingPongDirection", "right"), a.data("getNextElementWidth", !0), a.data("swapAt", null), a.data("startAtElementHasNotPassed", !0), a.data("swappedElement", null), a.data("originalElements", a.data("scrollableArea").children(s.countOnlyClass)), a.data("visible", !0), a.data("enabled", !0), a.data("scrollableAreaHeight", a.data("scrollableArea").height()), a.data("scrollerOffset", a.offset()), s.touchScrolling && a.data("enabled") && a.data("scrollWrapper").kinetic({
          y: !1,
          moved: function(e) {
            s.manualContinuousScrolling && (a.data("scrollWrapper").scrollLeft() <= 0 ? r._checkContinuousSwapLeft() : r._checkContinuousSwapRight()), r._trigger("touchMoved")
          },
          stopped: function(e) {
            a.data("scrollWrapper").stop(!0, !1), r.stopAutoScrolling(), r._trigger("touchStopped")
          }
        }), a.data("scrollingHotSpotRight").bind("mousemove", function(e) {
          if (s.hotSpotScrolling) {
            var t = e.pageX - g(this).offset().left;
            a.data("scrollXPos", Math.round(t / a.data("hotSpotWidth") * s.hotSpotScrollingStep)), (a.data("scrollXPos") === 1 / 0 || a.data("scrollXPos") < 1) && a.data("scrollXPos", 1)
          }
        }), a.data("scrollingHotSpotRight").bind("mouseover", function() {
          s.hotSpotScrolling && (a.data("scrollWrapper").stop(!0, !1), r.stopAutoScrolling(), a.data("rightScrollingInterval", setInterval(function() {
            0 < a.data("scrollXPos") && a.data("enabled") && (a.data("scrollWrapper").scrollLeft(a.data("scrollWrapper").scrollLeft() + a.data("scrollXPos") * a.data("speedBooster")), s.manualContinuousScrolling && r._checkContinuousSwapRight(), r._showHideHotSpots())
          }, s.hotSpotScrollingInterval)), r._trigger("mouseOverRightHotSpot"))
        }), a.data("scrollingHotSpotRight").bind("mouseout", function() {
          s.hotSpotScrolling && (clearInterval(a.data("rightScrollingInterval")), a.data("scrollXPos", 0), s.easingAfterHotSpotScrolling && a.data("enabled") && a.data("scrollWrapper").animate({
            scrollLeft: a.data("scrollWrapper").scrollLeft() + s.easingAfterHotSpotScrollingDistance
          }, {
            duration: s.easingAfterHotSpotScrollingDuration,
            easing: s.easingAfterHotSpotScrollingFunction
          }))
        }), a.data("scrollingHotSpotRight").bind("mousedown", function() {
          a.data("speedBooster", s.hotSpotMouseDownSpeedBooster)
        }), g("body").bind("mouseup", function() {
          a.data("speedBooster", 1)
        }), a.data("scrollingHotSpotLeft").bind("mousemove", function(e) {
          if (s.hotSpotScrolling) {
            var t = a.data("hotSpotWidth") - (e.pageX - g(this).offset().left);
            a.data("scrollXPos", Math.round(t / a.data("hotSpotWidth") * s.hotSpotScrollingStep)), (a.data("scrollXPos") === 1 / 0 || a.data("scrollXPos") < 1) && a.data("scrollXPos", 1)
          }
        }), a.data("scrollingHotSpotLeft").bind("mouseover", function() {
          s.hotSpotScrolling && (a.data("scrollWrapper").stop(!0, !1), r.stopAutoScrolling(), a.data("leftScrollingInterval", setInterval(function() {
            0 < a.data("scrollXPos") && a.data("enabled") && (a.data("scrollWrapper").scrollLeft(a.data("scrollWrapper").scrollLeft() - a.data("scrollXPos") * a.data("speedBooster")), s.manualContinuousScrolling && r._checkContinuousSwapLeft(), r._showHideHotSpots())
          }, s.hotSpotScrollingInterval)), r._trigger("mouseOverLeftHotSpot"))
        }), a.data("scrollingHotSpotLeft").bind("mouseout", function() {
          s.hotSpotScrolling && (clearInterval(a.data("leftScrollingInterval")), a.data("scrollXPos", 0), s.easingAfterHotSpotScrolling && a.data("enabled") && a.data("scrollWrapper").animate({
            scrollLeft: a.data("scrollWrapper").scrollLeft() - s.easingAfterHotSpotScrollingDistance
          }, {
            duration: s.easingAfterHotSpotScrollingDuration,
            easing: s.easingAfterHotSpotScrollingFunction
          }))
        }), a.data("scrollingHotSpotLeft").bind("mousedown", function() {
          a.data("speedBooster", s.hotSpotMouseDownSpeedBooster)
        }), a.data("scrollableArea").mousewheel(function(e, t, n, i) {
          var o;
          a.data("enabled") && 0 < s.mousewheelScrolling.length && ("vertical" === s.mousewheelScrolling && 0 !== i ? (r.stopAutoScrolling(), e.preventDefault(), o = Math.round(s.mousewheelScrollingStep * i * -1), r.move(o)) : "horizontal" === s.mousewheelScrolling && 0 !== n ? (r.stopAutoScrolling(), e.preventDefault(), o = Math.round(s.mousewheelScrollingStep * n * -1), r.move(o)) : "allDirections" === s.mousewheelScrolling && (r.stopAutoScrolling(), e.preventDefault(), o = Math.round(s.mousewheelScrollingStep * t * -1), r.move(o)))
        }), s.mousewheelScrolling && a.data("scrollingHotSpotLeft").add(a.data("scrollingHotSpotRight")).mousewheel(function(e) {
          e.preventDefault()
        }), g(window).bind("resize", function() {
          r._showHideHotSpots(), r._trigger("windowResized")
        }), jQuery.isEmptyObject(s.getContentOnLoad) || r[s.getContentOnLoad.method](s.getContentOnLoad.content, s.getContentOnLoad.manipulationMethod, s.getContentOnLoad.addWhere, s.getContentOnLoad.filterTag), s.hiddenOnStart && r.hide(), g(window).load(function() {
          if (s.hiddenOnStart || r.recalculateScrollableArea(), 0 < s.autoScrollingMode.length && !s.hiddenOnStart && r.startAutoScrolling(), "always" !== s.autoScrollingMode) switch (s.visibleHotSpotBackgrounds) {
            case "always":
              r.showHotSpotBackgrounds();
              break;
            case "onStart":
              r.showHotSpotBackgrounds(), a.data("hideHotSpotBackgroundsInterval", setTimeout(function() {
                r.hideHotSpotBackgrounds(250)
              }, s.hotSpotsVisibleTime));
              break;
            case "hover":
              a.mouseenter(function(e) {
                s.hotSpotScrolling && (e.stopPropagation(), r.showHotSpotBackgrounds(250))
              }).mouseleave(function(e) {
                s.hotSpotScrolling && (e.stopPropagation(), r.hideHotSpotBackgrounds(250))
              })
          }
          r._showHideHotSpots(), r._trigger("setupComplete")
        })
      },
      _init: function() {
        this.element;
        this.recalculateScrollableArea(), this._showHideHotSpots(), this._trigger("initializationComplete")
      },
      _setOption: function(e, t) {
        var n = this.options,
          i = this.element;
        n[e] = t, "hotSpotScrolling" === e ? !0 === t ? this._showHideHotSpots() : (i.data("scrollingHotSpotLeft").hide(), i.data("scrollingHotSpotRight").hide()) : "autoScrollingStep" === e || "easingAfterHotSpotScrollingDistance" === e || "easingAfterHotSpotScrollingDuration" === e || "easingAfterMouseWheelScrollingDuration" === e ? n[e] = parseInt(t, 10) : "autoScrollingInterval" === e && (n[e] = parseInt(t, 10), this.startAutoScrolling())
      },
      showHotSpotBackgrounds: function(e) {
        var t = this.element,
          n = this.options;
        void 0 !== e ? (t.data("scrollingHotSpotLeft").addClass(n.scrollingHotSpotLeftVisibleClass), t.data("scrollingHotSpotRight").addClass(n.scrollingHotSpotRightVisibleClass), t.data("scrollingHotSpotLeft").add(t.data("scrollingHotSpotRight")).fadeTo(e, .35)) : (t.data("scrollingHotSpotLeft").addClass(n.scrollingHotSpotLeftVisibleClass), t.data("scrollingHotSpotLeft").removeAttr("style"), t.data("scrollingHotSpotRight").addClass(n.scrollingHotSpotRightVisibleClass), t.data("scrollingHotSpotRight").removeAttr("style")), this._showHideHotSpots()
      },
      hideHotSpotBackgrounds: function(e) {
        var t = this.element,
          n = this.options;
        void 0 !== e ? (t.data("scrollingHotSpotLeft").fadeTo(e, 0, function() {
          t.data("scrollingHotSpotLeft").removeClass(n.scrollingHotSpotLeftVisibleClass)
        }), t.data("scrollingHotSpotRight").fadeTo(e, 0, function() {
          t.data("scrollingHotSpotRight").removeClass(n.scrollingHotSpotRightVisibleClass)
        })) : (t.data("scrollingHotSpotLeft").removeClass(n.scrollingHotSpotLeftVisibleClass).removeAttr("style"), t.data("scrollingHotSpotRight").removeClass(n.scrollingHotSpotRightVisibleClass).removeAttr("style"))
      },
      _showHideHotSpots: function() {
        var e = this.element,
          t = this.options;
        t.hotSpotScrolling ? t.hotSpotScrolling && "always" !== t.autoScrollingMode && null !== e.data("autoScrollingInterval") ? (e.data("scrollingHotSpotLeft").show(), e.data("scrollingHotSpotRight").show()) : "always" !== t.autoScrollingMode && t.hotSpotScrolling ? e.data("scrollableAreaWidth") <= e.data("scrollWrapper").innerWidth() ? (e.data("scrollingHotSpotLeft").hide(), e.data("scrollingHotSpotRight").hide()) : 0 === e.data("scrollWrapper").scrollLeft() ? (e.data("scrollingHotSpotLeft").hide(), e.data("scrollingHotSpotRight").show(), this._trigger("scrollerLeftLimitReached"), clearInterval(e.data("leftScrollingInterval")), e.data("leftScrollingInterval", null)) : e.data("scrollableAreaWidth") <= e.data("scrollWrapper").innerWidth() + e.data("scrollWrapper").scrollLeft() ? (e.data("scrollingHotSpotLeft").show(), e.data("scrollingHotSpotRight").hide(), this._trigger("scrollerRightLimitReached"), clearInterval(e.data("rightScrollingInterval")), e.data("rightScrollingInterval", null)) : (e.data("scrollingHotSpotLeft").show(), e.data("scrollingHotSpotRight").show()) : (e.data("scrollingHotSpotLeft").hide(), e.data("scrollingHotSpotRight").hide()) : (e.data("scrollingHotSpotLeft").hide(), e.data("scrollingHotSpotRight").hide())
      },
      _setElementScrollPosition: function(e, t) {
        var n = this.element,
          i = this.options,
          o = 0;
        switch (e) {
          case "first":
            return n.data("scrollXPos", 0), !0;
          case "start":
            return !("" === i.startAtElementId || !n.data("scrollableArea").has("#" + i.startAtElementId)) && (o = g("#" + i.startAtElementId).position().left, n.data("scrollXPos", o), !0);
          case "last":
            return n.data("scrollXPos", n.data("scrollableAreaWidth") - n.data("scrollWrapper").innerWidth()), !0;
          case "number":
            return !isNaN(t) && (o = n.data("scrollableArea").children(i.countOnlyClass).eq(t - 1).position().left, n.data("scrollXPos", o), !0);
          case "id":
            return !!(0 < t.length && n.data("scrollableArea").has("#" + t)) && (o = g("#" + t).position().left, n.data("scrollXPos", o), !0);
          default:
            return !1
        }
      },
      jumpToElement: function(e, t) {
        var n = this.element;
        if (n.data("enabled") && this._setElementScrollPosition(e, t)) switch (n.data("scrollWrapper").scrollLeft(n.data("scrollXPos")), this._showHideHotSpots(), e) {
          case "first":
            this._trigger("jumpedToFirstElement");
            break;
          case "start":
            this._trigger("jumpedToStartElement");
            break;
          case "last":
            this._trigger("jumpedToLastElement");
            break;
          case "number":
            this._trigger("jumpedToElementNumber", null, {
              elementNumber: t
            });
            break;
          case "id":
            this._trigger("jumpedToElementId", null, {
              elementId: t
            })
        }
      },
      scrollToElement: function(e, t) {
        var n = this,
          i = this.element,
          o = this.options,
          r = !1;
        i.data("enabled") && n._setElementScrollPosition(e, t) && (null !== i.data("autoScrollingInterval") && (n.stopAutoScrolling(), r = !0), i.data("scrollWrapper").stop(!0, !1), i.data("scrollWrapper").animate({
          scrollLeft: i.data("scrollXPos")
        }, {
          duration: o.scrollToAnimationDuration,
          easing: o.scrollToEasingFunction,
          complete: function() {
            switch (r && n.startAutoScrolling(), n._showHideHotSpots(), e) {
              case "first":
                n._trigger("scrolledToFirstElement");
                break;
              case "start":
                n._trigger("scrolledToStartElement");
                break;
              case "last":
                n._trigger("scrolledToLastElement");
                break;
              case "number":
                n._trigger("scrolledToElementNumber", null, {
                  elementNumber: t
                });
                break;
              case "id":
                n._trigger("scrolledToElementId", null, {
                  elementId: t
                })
            }
          }
        }))
      },
      move: function(e) {
        var t = this,
          n = this.element,
          i = this.options;
        if (n.data("scrollWrapper").stop(!0, !0), e < 0 && 0 < n.data("scrollWrapper").scrollLeft() || 0 < e && n.data("scrollableAreaWidth") > n.data("scrollWrapper").innerWidth() + n.data("scrollWrapper").scrollLeft() || i.manualContinuousScrolling) {
          var o = n.data("scrollableArea").width() - n.data("scrollWrapper").width(),
            r = n.data("scrollWrapper").scrollLeft() + e;
          if (r < 0)
            for (; r < 0;) n.data("swappedElement", n.data("scrollableArea").children(":last").detach()), n.data("scrollableArea").prepend(n.data("swappedElement")), n.data("scrollWrapper").scrollLeft(n.data("scrollWrapper").scrollLeft() + n.data("swappedElement").outerWidth(!0)), r = n.data("scrollableArea").children(":first").outerWidth(!0) + r;
          else if (0 < r - o)
            for (var s = function() {
                n.data("swappedElement", n.data("scrollableArea").children(":first").detach()), n.data("scrollableArea").append(n.data("swappedElement"));
                var e = n.data("scrollWrapper").scrollLeft();
                n.data("scrollWrapper").scrollLeft(e - n.data("swappedElement").outerWidth(!0))
              }; 0 < r - o;) s(), r -= n.data("scrollableArea").children(":last").outerWidth(!0);
          i.easingAfterMouseWheelScrolling ? n.data("scrollWrapper").animate({
            scrollLeft: n.data("scrollWrapper").scrollLeft() + e
          }, {
            duration: i.easingAfterMouseWheelScrollingDuration,
            easing: i.easingAfterMouseWheelFunction,
            complete: function() {
              t._showHideHotSpots(), i.manualContinuousScrolling && (0 < e ? t._checkContinuousSwapRight() : t._checkContinuousSwapLeft())
            }
          }) : (n.data("scrollWrapper").scrollLeft(n.data("scrollWrapper").scrollLeft() + e), t._showHideHotSpots(), i.manualContinuousScrolling && (0 < e ? t._checkContinuousSwapRight() : t._checkContinuousSwapLeft()))
        }
      },
      getFlickrContent: function(e, f) {
        var h = this,
          p = this.element;
        g.getJSON(e, function(e) {
          var n, s = [{
              size: "small square",
              pixels: 75,
              letter: "_s"
            }, {
              size: "thumbnail",
              pixels: 100,
              letter: "_t"
            }, {
              size: "small",
              pixels: 240,
              letter: "_m"
            }, {
              size: "medium",
              pixels: 500,
              letter: ""
            }, {
              size: "medium 640",
              pixels: 640,
              letter: "_z"
            }, {
              size: "large",
              pixels: 1024,
              letter: "_b"
            }],
            a = [],
            l = [],
            c = e.items.length,
            u = 0;

          function d(e) {
            var t = p.data("scrollableAreaHeight") / e.height,
              n = Math.round(e.width * t),
              i = g(e).attr("src").split("/");
            i = i[i.length - 1].split("."), g(e).attr("id", i[0]), g(e).css({
              height: p.data("scrollableAreaHeight"),
              width: n
            }), l.push(i[0]), a.push(e), u++
          }
          n = p.data("scrollableAreaHeight") <= 75 ? 0 : p.data("scrollableAreaHeight") <= 100 ? 1 : p.data("scrollableAreaHeight") <= 240 ? 2 : p.data("scrollableAreaHeight") <= 500 ? 3 : p.data("scrollableAreaHeight") <= 640 ? 4 : 5, g.each(e.items, function(e, t) {
            ! function e(t, n) {
              var i = t.media.m;
              var o = i.replace("_m", s[n].letter);
              var r = g("<img />").attr("src", o);
              r.load(function() {
                if (this.height < p.data("scrollableAreaHeight") && n + 1 < s.length ? e(t, n + 1) : d(this), u === c) {
                  switch (f) {
                    case "addFirst":
                      p.data("scrollableArea").children(":first").before(a);
                      break;
                    case "addLast":
                      p.data("scrollableArea").children(":last").after(a);
                      break;
                    default:
                      p.data("scrollableArea").html(a)
                  }
                  h.recalculateScrollableArea(), h._showHideHotSpots(), h._trigger("addedFlickrContent", null, {
                    addedElementIds: l
                  })
                }
              })
            }(t, n)
          })
        })
      },
      getAjaxContent: function(n, i, o) {
        var r = this,
          s = this.element;
        g.ajaxSetup({
          cache: !1
        }), g.get(n, function(e) {
          var t;
          switch (t = void 0 !== o ? 0 < o.length ? g("<div>").html(e).find(o) : n : e, i) {
            case "addFirst":
              s.data("scrollableArea").children(":first").before(t);
              break;
            case "addLast":
              s.data("scrollableArea").children(":last").after(t);
              break;
            default:
              s.data("scrollableArea").html(t)
          }
          r.recalculateScrollableArea(), r._showHideHotSpots(), r._trigger("addedAjaxContent")
        })
      },
      getHtmlContent: function(e, t, n) {
        var i, o = this.element;
        switch (i = void 0 !== n && 0 < n.length ? g("<div>").html(e).find(n) : e, t) {
          case "addFirst":
            o.data("scrollableArea").children(":first").before(i);
            break;
          case "addLast":
            o.data("scrollableArea").children(":last").after(i);
            break;
          default:
            o.data("scrollableArea").html(i)
        }
        this.recalculateScrollableArea(), this._showHideHotSpots(), this._trigger("addedHtmlContent")
      },
      recalculateScrollableArea: function() {
        var e = 0,
          t = !1,
          n = this.options,
          i = this.element;
        i.data("scrollableArea").children(n.countOnlyClass).each(function() {
          0 < n.startAtElementId.length && g(this).attr("id") === n.startAtElementId && (i.data("startingPosition", e), t = !0), e += g(this).outerWidth(!0)
        }), t || i.data("startAtElementId", ""), i.data("scrollableAreaWidth", e), i.data("scrollableArea").width(i.data("scrollableAreaWidth")), i.data("scrollWrapper").scrollLeft(i.data("startingPosition")), i.data("scrollXPos", i.data("startingPosition"))
      },
      getScrollerOffset: function() {
        return this.element.data("scrollWrapper").scrollLeft()
      },
      stopAutoScrolling: function() {
        var e = this.element;
        null !== e.data("autoScrollingInterval") && (clearInterval(e.data("autoScrollingInterval")), e.data("autoScrollingInterval", null), this._showHideHotSpots(), this._trigger("autoScrollingStopped"))
      },
      startAutoScrolling: function() {
        var e = this,
          t = this.element,
          n = this.options;
        t.data("enabled") && (e._showHideHotSpots(), clearInterval(t.data("autoScrollingInterval")), t.data("autoScrollingInterval", null), e._trigger("autoScrollingStarted"), t.data("autoScrollingInterval", setInterval(function() {
          if (!t.data("visible") || t.data("scrollableAreaWidth") <= t.data("scrollWrapper").innerWidth()) clearInterval(t.data("autoScrollingInterval")), t.data("autoScrollingInterval", null);
          else switch (t.data("previousScrollLeft", t.data("scrollWrapper").scrollLeft()), n.autoScrollingDirection) {
            case "right":
              t.data("scrollWrapper").scrollLeft(t.data("scrollWrapper").scrollLeft() + n.autoScrollingStep), t.data("previousScrollLeft") === t.data("scrollWrapper").scrollLeft() && (e._trigger("autoScrollingRightLimitReached"), e.stopAutoScrolling());
              break;
            case "left":
              t.data("scrollWrapper").scrollLeft(t.data("scrollWrapper").scrollLeft() - n.autoScrollingStep), t.data("previousScrollLeft") === t.data("scrollWrapper").scrollLeft() && (e._trigger("autoScrollingLeftLimitReached"), e.stopAutoScrolling());
              break;
            case "backAndForth":
              "right" === t.data("pingPongDirection") ? t.data("scrollWrapper").scrollLeft(t.data("scrollWrapper").scrollLeft() + n.autoScrollingStep) : t.data("scrollWrapper").scrollLeft(t.data("scrollWrapper").scrollLeft() - n.autoScrollingStep), t.data("previousScrollLeft") === t.data("scrollWrapper").scrollLeft() && ("right" === t.data("pingPongDirection") ? (t.data("pingPongDirection", "left"), e._trigger("autoScrollingRightLimitReached")) : (t.data("pingPongDirection", "right"), e._trigger("autoScrollingLeftLimitReached")));
              break;
            case "endlessLoopRight":
              t.data("scrollWrapper").scrollLeft(t.data("scrollWrapper").scrollLeft() + n.autoScrollingStep), e._checkContinuousSwapRight();
              break;
            case "endlessLoopLeft":
              t.data("scrollWrapper").scrollLeft(t.data("scrollWrapper").scrollLeft() - n.autoScrollingStep), e._checkContinuousSwapLeft()
          }
        }, n.autoScrollingInterval)))
      },
      _checkContinuousSwapRight: function() {
        var e = this.element,
          t = this.options;
        if (e.data("getNextElementWidth") && (0 < t.startAtElementId.length && e.data("startAtElementHasNotPassed") ? (e.data("swapAt", g("#" + t.startAtElementId).outerWidth(!0)), e.data("startAtElementHasNotPassed", !1)) : e.data("swapAt", e.data("scrollableArea").children(":first").outerWidth(!0)), e.data("getNextElementWidth", !1)), e.data("swapAt") <= e.data("scrollWrapper").scrollLeft()) {
          e.data("swappedElement", e.data("scrollableArea").children(":first").detach()), e.data("scrollableArea").append(e.data("swappedElement"));
          var n = e.data("scrollWrapper").scrollLeft();
          e.data("scrollWrapper").scrollLeft(n - e.data("swappedElement").outerWidth(!0)), e.data("getNextElementWidth", !0)
        }
      },
      _checkContinuousSwapLeft: function() {
        var e = this.element,
          t = this.options;
        e.data("getNextElementWidth") && (0 < t.startAtElementId.length && e.data("startAtElementHasNotPassed") ? (e.data("swapAt", g("#" + t.startAtElementId).outerWidth(!0)), e.data("startAtElementHasNotPassed", !1)) : e.data("swapAt", e.data("scrollableArea").children(":first").outerWidth(!0)), e.data("getNextElementWidth", !1)), 0 === e.data("scrollWrapper").scrollLeft() && (e.data("swappedElement", e.data("scrollableArea").children(":last").detach()), e.data("scrollableArea").prepend(e.data("swappedElement")), e.data("scrollWrapper").scrollLeft(e.data("scrollWrapper").scrollLeft() + e.data("swappedElement").outerWidth(!0)), e.data("getNextElementWidth", !0))
      },
      restoreOriginalElements: function() {
        var e = this.element;
        e.data("scrollableArea").html(e.data("originalElements")), this.recalculateScrollableArea(), this.jumpToElement("first")
      },
      show: function() {
        var e = this.element;
        e.data("visible", !0), e.show()
      },
      hide: function() {
        var e = this.element;
        e.data("visible", !1), e.hide()
      },
      enable: function() {
        var e = this.element;
        this.options.touchScrolling && e.data("scrollWrapper").kinetic("attach"), e.data("enabled", !0)
      },
      disable: function() {
        var e = this.element;
        this.stopAutoScrolling(), clearInterval(e.data("rightScrollingInterval")), clearInterval(e.data("leftScrollingInterval")), clearInterval(e.data("hideHotSpotBackgroundsInterval")), this.options.touchScrolling && e.data("scrollWrapper").kinetic("detach"), e.data("enabled", !1)
      },
      destroy: function() {
        var e = this.element;
        this.stopAutoScrolling(), clearInterval(e.data("rightScrollingInterval")), clearInterval(e.data("leftScrollingInterval")), clearInterval(e.data("hideHotSpotBackgroundsInterval")), e.data("scrollingHotSpotRight").unbind("mouseover"), e.data("scrollingHotSpotRight").unbind("mouseout"), e.data("scrollingHotSpotRight").unbind("mousedown"), e.data("scrollingHotSpotLeft").unbind("mouseover"), e.data("scrollingHotSpotLeft").unbind("mouseout"), e.data("scrollingHotSpotLeft").unbind("mousedown"), e.unbind("mousenter"), e.unbind("mouseleave"), e.data("scrollingHotSpotRight").remove(), e.data("scrollingHotSpotLeft").remove(), e.data("scrollableArea").remove(), e.data("scrollWrapper").remove(), e.html(e.data("originalElements")), g.Widget.prototype.destroy.apply(this, arguments)
      }
    })
  }(jQuery),
  function r(s, a, l) {
    function c(n, e) {
      if (!a[n]) {
        if (!s[n]) {
          var t = "function" == typeof require && require;
          if (!e && t) return t(n, !0);
          if (u) return u(n, !0);
          var i = new Error("Cannot find module '" + n + "'");
          throw i.code = "MODULE_NOT_FOUND", i
        }
        var o = a[n] = {
          exports: {}
        };
        s[n][0].call(o.exports, function(e) {
          var t = s[n][1][e];
          return c(t || e)
        }, o, o.exports, r, s, a, l)
      }
      return a[n].exports
    }
    for (var u = "function" == typeof require && require, e = 0; e < l.length; e++) c(l[e]);
    return c
  }({
    1: [function(V, e, U) {
      (function(e) {
        "use strict";
        Object.defineProperty(U, "__esModule", {
          value: !0
        }), U.Proj = void 0;
        var t = V("./utils/params"),
          n = V("./utils/ajax"),
          i = V("./utils/browser"),
          o = V("./utils/cookies"),
          r = V("./proj/header-slider"),
          s = V("./proj/sticky-header"),
          a = V("./proj/sticky-aside"),
          l = V("./proj/main-menu"),
          c = V("./proj/clinic-block"),
          u = V("./proj/helper"),
          d = V("./proj/gallery"),
          f = V("./proj/slider"),
          h = V("./proj/scroller"),
          p = V("./proj/carousel"),
          g = V("./proj/video"),
          m = V("./proj/popup-video"),
          v = V("./proj/tabs"),
          y = V("./proj/types"),
          b = V("./proj/services"),
          x = V("./proj/reviews"),
          w = V("./proj/faqs"),
          S = V("./proj/form"),
          k = V("./proj/content"),
          C = V("./proj/overflow"),
          _ = V("./proj/callback"),
          T = V("./proj/map"),
          $ = V("./proj/scroll"),
          A = V("./proj/anchor"),
          j = V("./proj/responsive"),
          P = V("./proj/support"),
          E = V("./proj/service-group"),
          D = V("./proj/group-advantage"),
          O = V("./proj/type-advantage"),
          M = V("./proj/ajax-objects"),
          I = V("./proj/ajax-specialists"),
          H = V("./proj/ajax-reviews"),
          L = V("./proj/ajax-faqs"),
          R = V("./proj/specialist-cat"),
          N = V("./proj/review-cat"),
          F = V("./proj/program-cat"),
          B = V("./proj/contact-page"),
          z = V("./proj/price-page"),
          q = V("./proj/festival-page"),
          W = V("./proj/targets"),
          Y = function() {};
        Y.prototype.interfaceName = t.interfaceName, Y.prototype.interfaceMark = t.interfaceMark, Y.prototype.getAjax = n.getAjax, Y.prototype.browser = i.browser, Y.prototype.cookies = o.cookies, Y.prototype.HeaderSlider = r.HeaderSlider, Y.prototype.StickyHeader = s.StickyHeader, Y.prototype.StickyAside = a.StickyAside, Y.prototype.MainMenu = l.MainMenu, Y.prototype.ClinicBlock = c.ClinicBlock, Y.prototype.Helper = u.Helper, Y.prototype.Gallery = d.Gallery, Y.prototype.Slider = f.Slider, Y.prototype.Scroller = h.Scroller, Y.prototype.Carousel = p.Carousel, Y.prototype.Video = g.Video, Y.prototype.PopupVideo = m.PopupVideo, Y.prototype.Tabs = v.Tabs, Y.prototype.Types = y.Types, Y.prototype.Services = b.Services, Y.prototype.Reviews = x.Reviews, Y.prototype.FAQs = w.FAQs, Y.prototype.Form = S.Form, Y.prototype.Content = k.Content, Y.prototype.Overflow = C.Overflow, Y.prototype.Callback = _.Callback, Y.prototype.Map = T.Map, Y.prototype.Scroll = $.Scroll, Y.prototype.Anchor = A.Anchor, Y.prototype.Responsive = j.Responsive, Y.prototype.Support = P.Support, Y.prototype.ServiceGroup = E.ServiceGroup, Y.prototype.GroupAdvantage = D.GroupAdvantage, Y.prototype.TypeAdvantage = O.TypeAdvantage, Y.prototype.AjaxObjects = M.AjaxObjects, Y.prototype.AjaxSpecialists = I.AjaxSpecialists, Y.prototype.AjaxReviews = H.AjaxReviews, Y.prototype.AjaxFaqs = L.AjaxFaqs, Y.prototype.SpecialistCat = R.SpecialistCat, Y.prototype.ReviewCat = N.ReviewCat, Y.prototype.ProgramCat = F.ProgramCat, Y.prototype.ContactPage = B.ContactPage, Y.prototype.PricePage = z.PricePage, Y.prototype.FestivalPage = q.FestivalPage, Y.prototype.Targets = W.Targets, e.Proj = Y, U.Proj = Y
      }).call(this, "undefined" != typeof global ? global : "undefined" != typeof self ? self : "undefined" != typeof window ? window : {})
    }, {
      "./proj/ajax-faqs": 3,
      "./proj/ajax-objects": 4,
      "./proj/ajax-reviews": 5,
      "./proj/ajax-specialists": 6,
      "./proj/anchor": 7,
      "./proj/callback": 8,
      "./proj/carousel": 9,
      "./proj/clinic-block": 10,
      "./proj/contact-page": 11,
      "./proj/content": 12,
      "./proj/faqs": 13,
      "./proj/festival-page": 14,
      "./proj/form": 15,
      "./proj/gallery": 16,
      "./proj/group-advantage": 17,
      "./proj/header-slider": 18,
      "./proj/helper": 19,
      "./proj/main-menu": 20,
      "./proj/map": 21,
      "./proj/overflow": 22,
      "./proj/popup-video": 23,
      "./proj/price-page": 24,
      "./proj/program-cat": 25,
      "./proj/responsive": 26,
      "./proj/review-cat": 27,
      "./proj/reviews": 28,
      "./proj/scroll": 29,
      "./proj/scroller": 30,
      "./proj/service-group": 31,
      "./proj/services": 32,
      "./proj/slider": 33,
      "./proj/specialist-cat": 34,
      "./proj/sticky-aside": 35,
      "./proj/sticky-header": 36,
      "./proj/support": 37,
      "./proj/tabs": 38,
      "./proj/targets": 39,
      "./proj/type-advantage": 40,
      "./proj/types": 41,
      "./proj/video": 42,
      "./utils/ajax": 43,
      "./utils/browser": 44,
      "./utils/cookies": 45,
      "./utils/params": 47
    }],
    2: [function(e, n, i) {
      (function(c) {
        "use strict";
        var e, t, u = "function" == typeof Symbol && "symbol" == typeof Symbol.iterator ? function(e) {
          return typeof e
        } : function(e) {
          return e && "function" == typeof Symbol && e.constructor === Symbol && e !== Symbol.prototype ? "symbol" : typeof e
        };
        t = function() {
          return function(n) {
            var i = {};

            function o(e) {
              if (i[e]) return i[e].exports;
              var t = i[e] = {
                exports: {},
                id: e,
                loaded: !1
              };
              return n[e].call(t.exports, t, t.exports, o), t.loaded = !0, t.exports
            }
            return o.m = n, o.c = i, o.p = "", o(0)
          }([function(e, t, n) {
            e.exports = n(1)
          }, function(e, t, n) {
            var i = n(2),
              o = n(3),
              r = n(4);

            function s(e) {
              var t = new r(e),
                n = o(r.prototype.request, t);
              return i.extend(n, r.prototype, t), i.extend(n, t), n
            }
            var a = s();
            a.Axios = r, a.create = function(e) {
              return s(e)
            }, a.Cancel = n(22), a.CancelToken = n(23), a.isCancel = n(19), a.all = function(e) {
              return Promise.all(e)
            }, a.spread = n(24), e.exports = a, e.exports.default = a
          }, function(e, t, n) {
            var o = n(3),
              i = Object.prototype.toString;

            function r(e) {
              return "[object Array]" === i.call(e)
            }

            function s(e) {
              return null !== e && "object" === (void 0 === e ? "undefined" : u(e))
            }

            function a(e) {
              return "[object Function]" === i.call(e)
            }

            function l(e, t) {
              if (null != e)
                if ("object" === (void 0 === e ? "undefined" : u(e)) || r(e) || (e = [e]), r(e))
                  for (var n = 0, i = e.length; n < i; n++) t.call(null, e[n], n, e);
                else
                  for (var o in e) Object.prototype.hasOwnProperty.call(e, o) && t.call(null, e[o], o, e)
            }
            e.exports = {
              isArray: r,
              isArrayBuffer: function(e) {
                return "[object ArrayBuffer]" === i.call(e)
              },
              isFormData: function(e) {
                return "undefined" != typeof FormData && e instanceof FormData
              },
              isArrayBufferView: function(e) {
                return "undefined" != typeof ArrayBuffer && ArrayBuffer.isView ? ArrayBuffer.isView(e) : e && e.buffer && e.buffer instanceof ArrayBuffer
              },
              isString: function(e) {
                return "string" == typeof e
              },
              isNumber: function(e) {
                return "number" == typeof e
              },
              isObject: s,
              isUndefined: function(e) {
                return void 0 === e
              },
              isDate: function(e) {
                return "[object Date]" === i.call(e)
              },
              isFile: function(e) {
                return "[object File]" === i.call(e)
              },
              isBlob: function(e) {
                return "[object Blob]" === i.call(e)
              },
              isFunction: a,
              isStream: function(e) {
                return s(e) && a(e.pipe)
              },
              isURLSearchParams: function(e) {
                return void 0 !== URLSearchParams && e instanceof URLSearchParams
              },
              isStandardBrowserEnv: function() {
                return "undefined" != typeof window && "undefined" != typeof document && "function" == typeof document.createElement
              },
              forEach: l,
              merge: function n() {
                var i = {};

                function e(e, t) {
                  "object" === u(i[t]) && "object" === (void 0 === e ? "undefined" : u(e)) ? i[t] = n(i[t], e) : i[t] = e
                }
                for (var t = 0, o = arguments.length; t < o; t++) l(arguments[t], e);
                return i
              },
              extend: function(n, e, i) {
                return l(e, function(e, t) {
                  n[t] = i && "function" == typeof e ? o(e, i) : e
                }), n
              },
              trim: function(e) {
                return e.replace(/^\s*/, "").replace(/\s*$/, "")
              }
            }
          }, function(e, t) {
            e.exports = function(n, i) {
              return function() {
                for (var e = new Array(arguments.length), t = 0; t < e.length; t++) e[t] = arguments[t];
                return n.apply(i, e)
              }
            }
          }, function(e, t, n) {
            var i = n(5),
              o = n(2),
              r = n(16),
              s = n(17),
              a = n(20),
              l = n(21);

            function c(e) {
              this.defaults = o.merge(i, e), this.interceptors = {
                request: new r,
                response: new r
              }
            }
            c.prototype.request = function(e) {
              "string" == typeof e && (e = o.merge({
                url: arguments[0]
              }, arguments[1])), (e = o.merge(i, this.defaults, {
                method: "get"
              }, e)).baseURL && !a(e.url) && (e.url = l(e.baseURL, e.url));
              var t = [s, void 0],
                n = Promise.resolve(e);
              for (this.interceptors.request.forEach(function(e) {
                  t.unshift(e.fulfilled, e.rejected)
                }), this.interceptors.response.forEach(function(e) {
                  t.push(e.fulfilled, e.rejected)
                }); t.length;) n = n.then(t.shift(), t.shift());
              return n
            }, o.forEach(["delete", "get", "head"], function(n) {
              c.prototype[n] = function(e, t) {
                return this.request(o.merge(t || {}, {
                  method: n,
                  url: e
                }))
              }
            }), o.forEach(["post", "put", "patch"], function(i) {
              c.prototype[i] = function(e, t, n) {
                return this.request(o.merge(n || {}, {
                  method: i,
                  url: e,
                  data: t
                }))
              }
            }), e.exports = c
          }, function(e, t, n) {
            var i, o = n(2),
              r = n(6),
              s = /^\)\]\}',?\n/,
              a = {
                "Content-Type": "application/x-www-form-urlencoded"
              };

            function l(e, t) {
              !o.isUndefined(e) && o.isUndefined(e["Content-Type"]) && (e["Content-Type"] = t)
            }
            e.exports = {
              adapter: ("undefined" != typeof XMLHttpRequest ? i = n(7) : void 0 !== c && (i = n(7)), i),
              transformRequest: [function(e, t) {
                return r(t, "Content-Type"), o.isFormData(e) || o.isArrayBuffer(e) || o.isStream(e) || o.isFile(e) || o.isBlob(e) ? e : o.isArrayBufferView(e) ? e.buffer : o.isURLSearchParams(e) ? (l(t, "application/x-www-form-urlencoded;charset=utf-8"), e.toString()) : o.isObject(e) ? (l(t, "application/json;charset=utf-8"), JSON.stringify(e)) : e
              }],
              transformResponse: [function(e) {
                if ("string" == typeof e) {
                  e = e.replace(s, "");
                  try {
                    e = JSON.parse(e)
                  } catch (e) {}
                }
                return e
              }],
              headers: {
                common: {
                  Accept: "application/json, text/plain, */*"
                },
                patch: o.merge(a),
                post: o.merge(a),
                put: o.merge(a)
              },
              timeout: 0,
              xsrfCookieName: "XSRF-TOKEN",
              xsrfHeaderName: "X-XSRF-TOKEN",
              maxContentLength: -1,
              validateStatus: function(e) {
                return 200 <= e && e < 300
              }
            }
          }, function(e, t, n) {
            var o = n(2);
            e.exports = function(n, i) {
              o.forEach(n, function(e, t) {
                t !== i && t.toUpperCase() === i.toUpperCase() && (n[i] = e, delete n[t])
              })
            }
          }, function(e, t, f) {
            var h = f(2),
              p = f(8),
              g = f(11),
              m = f(12),
              v = f(13),
              y = f(9),
              b = "undefined" != typeof window && window.btoa || f(14);
            e.exports = function(d) {
              return new Promise(function(n, i) {
                var o = d.data,
                  r = d.headers;
                h.isFormData(o) && delete r["Content-Type"];
                var s = new XMLHttpRequest,
                  e = "onreadystatechange",
                  a = !1;
                if ("undefined" == typeof window || !window.XDomainRequest || "withCredentials" in s || v(d.url) || (s = new window.XDomainRequest, e = "onload", a = !0, s.onprogress = function() {}, s.ontimeout = function() {}), d.auth) {
                  var t = d.auth.username || "",
                    l = d.auth.password || "";
                  r.Authorization = "Basic " + b(t + ":" + l)
                }
                if (s.open(d.method.toUpperCase(), g(d.url, d.params, d.paramsSerializer), !0), s.timeout = d.timeout, s[e] = function() {
                    if (s && (4 === s.readyState || a) && (0 !== s.status || s.responseURL && 0 === s.responseURL.indexOf("file:"))) {
                      var e = "getAllResponseHeaders" in s ? m(s.getAllResponseHeaders()) : null,
                        t = {
                          data: d.responseType && "text" !== d.responseType ? s.response : s.responseText,
                          status: 1223 === s.status ? 204 : s.status,
                          statusText: 1223 === s.status ? "No Content" : s.statusText,
                          headers: e,
                          config: d,
                          request: s
                        };
                      p(n, i, t), s = null
                    }
                  }, s.onerror = function() {
                    i(y("Network Error", d)), s = null
                  }, s.ontimeout = function() {
                    i(y("timeout of " + d.timeout + "ms exceeded", d, "ECONNABORTED")), s = null
                  }, h.isStandardBrowserEnv()) {
                  var c = f(15),
                    u = (d.withCredentials || v(d.url)) && d.xsrfCookieName ? c.read(d.xsrfCookieName) : void 0;
                  u && (r[d.xsrfHeaderName] = u)
                }
                if ("setRequestHeader" in s && h.forEach(r, function(e, t) {
                    void 0 === o && "content-type" === t.toLowerCase() ? delete r[t] : s.setRequestHeader(t, e)
                  }), d.withCredentials && (s.withCredentials = !0), d.responseType) try {
                  s.responseType = d.responseType
                } catch (e) {
                  if ("json" !== s.responseType) throw e
                }
                "function" == typeof d.onDownloadProgress && s.addEventListener("progress", d.onDownloadProgress), "function" == typeof d.onUploadProgress && s.upload && s.upload.addEventListener("progress", d.onUploadProgress), d.cancelToken && d.cancelToken.promise.then(function(e) {
                  s && (s.abort(), i(e), s = null)
                }), void 0 === o && (o = null), s.send(o)
              })
            }
          }, function(e, t, n) {
            var o = n(9);
            e.exports = function(e, t, n) {
              var i = n.config.validateStatus;
              n.status && i && !i(n.status) ? t(o("Request failed with status code " + n.status, n.config, null, n)) : e(n)
            }
          }, function(e, t, n) {
            var r = n(10);
            e.exports = function(e, t, n, i) {
              var o = new Error(e);
              return r(o, t, n, i)
            }
          }, function(e, t) {
            e.exports = function(e, t, n, i) {
              return e.config = t, n && (e.code = n), e.response = i, e
            }
          }, function(e, t, n) {
            var r = n(2);

            function s(e) {
              return encodeURIComponent(e).replace(/%40/gi, "@").replace(/%3A/gi, ":").replace(/%24/g, "$").replace(/%2C/gi, ",").replace(/%20/g, "+").replace(/%5B/gi, "[").replace(/%5D/gi, "]")
            }
            e.exports = function(e, t, n) {
              if (!t) return e;
              var i;
              if (n) i = n(t);
              else if (r.isURLSearchParams(t)) i = t.toString();
              else {
                var o = [];
                r.forEach(t, function(e, t) {
                  null != e && (r.isArray(e) && (t += "[]"), r.isArray(e) || (e = [e]), r.forEach(e, function(e) {
                    r.isDate(e) ? e = e.toISOString() : r.isObject(e) && (e = JSON.stringify(e)), o.push(s(t) + "=" + s(e))
                  }))
                }), i = o.join("&")
              }
              return i && (e += (-1 === e.indexOf("?") ? "?" : "&") + i), e
            }
          }, function(e, t, n) {
            var r = n(2);
            e.exports = function(e) {
              var t, n, i, o = {};
              return e && r.forEach(e.split("\n"), function(e) {
                i = e.indexOf(":"), t = r.trim(e.substr(0, i)).toLowerCase(), n = r.trim(e.substr(i + 1)), t && (o[t] = o[t] ? o[t] + ", " + n : n)
              }), o
            }
          }, function(e, t, n) {
            var s = n(2);
            e.exports = s.isStandardBrowserEnv() ? function() {
              var n, i = /(msie|trident)/i.test(navigator.userAgent),
                o = document.createElement("a");

              function r(e) {
                var t = e;
                return i && (o.setAttribute("href", t), t = o.href), o.setAttribute("href", t), {
                  href: o.href,
                  protocol: o.protocol ? o.protocol.replace(/:$/, "") : "",
                  host: o.host,
                  search: o.search ? o.search.replace(/^\?/, "") : "",
                  hash: o.hash ? o.hash.replace(/^#/, "") : "",
                  hostname: o.hostname,
                  port: o.port,
                  pathname: "/" === o.pathname.charAt(0) ? o.pathname : "/" + o.pathname
                }
              }
              return n = r(window.location.href),
                function(e) {
                  var t = s.isString(e) ? r(e) : e;
                  return t.protocol === n.protocol && t.host === n.host
                }
            }() : function() {
              return !0
            }
          }, function(e, t) {
            function a() {
              this.message = "String contains an invalid character"
            }(a.prototype = new Error).code = 5, a.prototype.name = "InvalidCharacterError", e.exports = function(e) {
              for (var t, n, i = String(e), o = "", r = 0, s = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/="; i.charAt(0 | r) || (s = "=", r % 1); o += s.charAt(63 & t >> 8 - r % 1 * 8)) {
                if (255 < (n = i.charCodeAt(r += .75))) throw new a;
                t = t << 8 | n
              }
              return o
            }
          }, function(e, t, n) {
            var a = n(2);
            e.exports = a.isStandardBrowserEnv() ? {
              write: function(e, t, n, i, o, r) {
                var s = [];
                s.push(e + "=" + encodeURIComponent(t)), a.isNumber(n) && s.push("expires=" + new Date(n).toGMTString()), a.isString(i) && s.push("path=" + i), a.isString(o) && s.push("domain=" + o), !0 === r && s.push("secure"), document.cookie = s.join("; ")
              },
              read: function(e) {
                var t = document.cookie.match(new RegExp("(^|;\\s*)(" + e + ")=([^;]*)"));
                return t ? decodeURIComponent(t[3]) : null
              },
              remove: function(e) {
                this.write(e, "", Date.now() - 864e5)
              }
            } : {
              write: function() {},
              read: function() {
                return null
              },
              remove: function() {}
            }
          }, function(e, t, n) {
            var i = n(2);

            function o() {
              this.handlers = []
            }
            o.prototype.use = function(e, t) {
              return this.handlers.push({
                fulfilled: e,
                rejected: t
              }), this.handlers.length - 1
            }, o.prototype.eject = function(e) {
              this.handlers[e] && (this.handlers[e] = null)
            }, o.prototype.forEach = function(t) {
              i.forEach(this.handlers, function(e) {
                null !== e && t(e)
              })
            }, e.exports = o
          }, function(e, t, n) {
            var i = n(2),
              o = n(18),
              r = n(19),
              s = n(5);

            function a(e) {
              e.cancelToken && e.cancelToken.throwIfRequested()
            }
            e.exports = function(t) {
              return a(t), t.headers = t.headers || {}, t.data = o(t.data, t.headers, t.transformRequest), t.headers = i.merge(t.headers.common || {}, t.headers[t.method] || {}, t.headers || {}), i.forEach(["delete", "get", "head", "post", "put", "patch", "common"], function(e) {
                delete t.headers[e]
              }), (t.adapter || s.adapter)(t).then(function(e) {
                return a(t), e.data = o(e.data, e.headers, t.transformResponse), e
              }, function(e) {
                return r(e) || (a(t), e && e.response && (e.response.data = o(e.response.data, e.response.headers, t.transformResponse))), Promise.reject(e)
              })
            }
          }, function(e, t, n) {
            var i = n(2);
            e.exports = function(t, n, e) {
              return i.forEach(e, function(e) {
                t = e(t, n)
              }), t
            }
          }, function(e, t) {
            e.exports = function(e) {
              return !(!e || !e.__CANCEL__)
            }
          }, function(e, t) {
            e.exports = function(e) {
              return /^([a-z][a-z\d\+\-\.]*:)?\/\//i.test(e)
            }
          }, function(e, t) {
            e.exports = function(e, t) {
              return e.replace(/\/+$/, "") + "/" + t.replace(/^\/+/, "")
            }
          }, function(e, t) {
            function n(e) {
              this.message = e
            }
            n.prototype.toString = function() {
              return "Cancel" + (this.message ? ": " + this.message : "")
            }, n.prototype.__CANCEL__ = !0, e.exports = n
          }, function(e, t, n) {
            var i = n(22);

            function o(e) {
              if ("function" != typeof e) throw new TypeError("executor must be a function.");
              var t;
              this.promise = new Promise(function(e) {
                t = e
              });
              var n = this;
              e(function(e) {
                n.reason || (n.reason = new i(e), t(n.reason))
              })
            }
            o.prototype.throwIfRequested = function() {
              if (this.reason) throw this.reason
            }, o.source = function() {
              var t;
              return {
                token: new o(function(e) {
                  t = e
                }),
                cancel: t
              }
            }, e.exports = o
          }, function(e, t) {
            e.exports = function(t) {
              return function(e) {
                return t.apply(null, e)
              }
            }
          }])
        }, "object" === ((e = void 0) === i ? "undefined" : u(i)) && "object" === (void 0 === n ? "undefined" : u(n)) ? n.exports = t() : "function" == typeof define && define.amd ? define([], t) : "object" === (void 0 === i ? "undefined" : u(i)) ? i.axios = t() : e.axios = t()
      }).call(this, e("_process"))
    }, {
      _process: 48
    }],
    3: [function(e, t, n) {
      "use strict";
      Object.defineProperty(n, "__esModule", {
        value: !0
      }), n.AjaxFaqs = void 0;
      var s = e("./ajax-objects"),
        a = e("./faqs");
      n.AjaxFaqs = function(e) {
        var t = $.extend({
          selector: ".js-ajax-faqs.js-ajax",
          block: ".js-ajax-block",
          navbar: ".js-ajax-navbar",
          navbarLink: ".js-ajax-navbar-link",
          link: ".js-ajax-link",
          faq: ".js-faq"
        }, e);
        $(t.selector).each(function() {
          ! function(i) {
            var e = $(i),
              o = (e.find(t.block), e.find(t.navbar).find(t.navbarLink), e.find(t.link), e.data("object_id"));

            function r() {
              e.find(t.faq).each(function() {
                (0, a.InitFAQ)(this)
              })
            }
            e.on("click", ".js-ajax-navbar-link", function(e) {
              e.preventDefault();
              var t = $(this).data("page"),
                n = {
                  act: "get_faqs",
                  el: i,
                  options: {
                    page: t,
                    object_id: o
                  }
                };
              (0, s.AjaxObjects)(n, r)
            })
          }(this)
        })
      }
    }, {
      "./ajax-objects": 4,
      "./faqs": 13
    }],
    4: [function(e, t, n) {
      "use strict";
      Object.defineProperty(n, "__esModule", {
        value: !0
      }), n.AjaxObjects = void 0;
      var o = e("../utils/ajax");
      n.AjaxObjects = function(e, n) {
        var t = $.extend({}, e),
          i = $(t.el);
        (0, o.getAjax)(t.act, t.options).then(function(e) {
          var t = e.data;
          t.ok && (i.html(t.content), n())
        }).catch(function(e) {
          console.log(e)
        })
      }
    }, {
      "../utils/ajax": 43
    }],
    5: [function(e, t, n) {
      "use strict";
      Object.defineProperty(n, "__esModule", {
        value: !0
      }), n.AjaxReviews = void 0;
      var s = e("./ajax-objects"),
        a = e("./reviews");
      n.AjaxReviews = function(e) {
        var t = $.extend({
          selector: ".js-ajax-reviews.js-ajax",
          block: ".js-ajax-block",
          navbar: ".js-ajax-navbar",
          navbarLink: ".js-ajax-navbar-link",
          link: ".js-ajax-link",
          review: ".js-review"
        }, e);
        $(t.selector).each(function() {
          ! function(i) {
            var e = $(i),
              o = (e.find(t.block), e.find(t.navbar).find(t.navbarLink), e.find(t.link), e.data("object_id"));

            function r() {
              e.find(t.review).each(function() {
                (0, a.InitReview)(this)
              })
            }
            e.on("click", ".js-ajax-navbar-link", function(e) {
              e.preventDefault();
              var t = $(this).data("page"),
                n = {
                  act: "get_reviews",
                  el: i,
                  options: {
                    page: t,
                    object_id: o
                  }
                };
              (0, s.AjaxObjects)(n, r)
            })
          }(this)
        })
      }
    }, {
      "./ajax-objects": 4,
      "./reviews": 28
    }],
    6: [function(e, t, n) {
      "use strict";
      Object.defineProperty(n, "__esModule", {
        value: !0
      }), n.AjaxSpecialists = void 0;
      var r = e("./ajax-objects");
      n.AjaxSpecialists = function(e) {
        var t = $.extend({
          selector: ".js-ajax-specialists.js-ajax",
          block: ".js-ajax-block",
          navbar: ".js-ajax-navbar",
          navbarLink: ".js-ajax-navbar-link",
          link: ".js-ajax-link"
        }, e);
        $(t.selector).each(function() {
          var i, e, o;
          i = this, (e = $(i)).find(t.block), e.find(t.navbar).find(t.navbarLink), e.find(t.link), o = e.data("object_id"), e.on("click", ".js-ajax-navbar-link", function(e) {
            e.preventDefault();
            var t = $(this).data("page"),
              n = {
                act: "get_specialists",
                el: i,
                options: {
                  page: t,
                  object_id: o
                }
              };
            (0, r.AjaxObjects)(n)
          })
        })
      }
    }, {
      "./ajax-objects": 4
    }],
    7: [function(e, t, n) {
      "use strict";
      Object.defineProperty(n, "__esModule", {
        value: !0
      }), n.Anchor = void 0;
      var s = e("../utils/logger"),
        a = e("../utils/browser");
      n.Anchor = function(e) {
        s.logger.debug("Anchor", e);
        var i = $.extend({
            selector: ".js-anchor-link",
            offset: 85,
            duration: 600,
            delay: 200
          }, e),
          o = $(a.browser.getScrollBody());

        function r(e) {
          s.logger.debug("Anchor/gotoAnchor", e);
          var t = $("#" + e + ', a[name="' + e + '"]');
          if (t.length) {
            var n = t.offset().top;
            n -= i.offset, o.animate({
              scrollTop: n
            }, {
              duration: i.duration
            })
          }
        }
        $(document.body).on("click", i.selector, function(e) {
          var t = document.location.href.replace(/\#.*$/, ""),
            n = this.href.replace(/\#.*$/, ""),
            i = this.href.replace(/^.*?\#(.*)$/, "$1");
          t === n && (e.preventDefault(), r(i), document.location.hash = i)
        });
        var t = document.location.hash.replace(/^\#/, "");
        "" !== t && (window.scrollTo(0, 0), setTimeout(function() {
          r(t)
        }, i.delay))
      }
    }, {
      "../utils/browser": 44,
      "../utils/logger": 46
    }],
    8: [function(e, t, n) {
      "use strict";
      Object.defineProperty(n, "__esModule", {
        value: !0
      }), n.Callback = function(e) {
        var t = $.extend({
          formSelector: ".js-form_ambulance"
        }, e);
        $(document.body).on("submit", t.formSelector, function(e) {
          var t = $(this).serializeObject().phone.replace(/\s/g, "").replace(/\(|\)/g, "");
          window.PozvonimcomWidget.api.call({
            phone: t
          })
        })
      }
    }, {}],
    9: [function(e, t, n) {
      "use strict";
      Object.defineProperty(n, "__esModule", {
        value: !0
      }), n.Carousel = function(e) {
        var n = $.extend({
            selector: ".js-carousel",
            list: ".js-carousel-list",
            item: ".js-carousel-item",
            prevSelector: ".js-carousel-prev",
            nextSelector: ".js-carousel-next",
            params: {
              duration: 300,
              delay: 5e3
            }
          }, e),
          i = $(window);
        768 <= i.width() ? $(n.list).each(function() {
          var e = $(this),
            t = e.data("direction");
          e.smoothDivScroll({
            manualContinuousScrolling: !0,
            hotSpotScrolling: !0,
            autoScrollingDirection: t,
            autoScrollingStep: 2,
            autoScrollingInterval: 10
          }), e.bind("mouseover", function() {
            $(this).smoothDivScroll("startAutoScrolling")
          }).bind("mouseout", function() {
            $(this).smoothDivScroll("stopAutoScrolling")
          }), i.width() < 768 && e.smoothDivScroll("destroy")
        }) : $(n.selector).each(function() {
          var e = $(this),
            t = $.extend(!0, {}, {
              prev: e.find(n.prevSelector),
              next: e.find(n.nextSelector)
            }, n.params, e.data());
          $(n.item, this).scroller(t)
        })
      }
    }, {}],
    10: [function(e, t, n) {
      "use strict";
      Object.defineProperty(n, "__esModule", {
        value: !0
      }), n.ClinicBlock = void 0;
      var p = e("../utils/params"),
        g = e("../utils/logger"),
        m = e("../utils/browser");
      n.ClinicBlock = function(e) {
        g.logger.debug("ClinicBlock", e);
        for (var o = $.extend({
            selector: ".js-clinic",
            itemSelector: ".js-clinic-item",
            linkSelector: ".js-clinic-link",
            mapSelector: ".js-clinic-map",
            mapOptions: {
              zoom: 15,
              controls: ["default"]
            }
          }, e), t = $(window), n = $(o.selector), r = n.find(o.itemSelector), i = n.find(o.mapSelector), s = [], a = void 0, l = !1, c = !1, u = function(t) {
            var e = r.eq(t),
              n = e.find(o.linkSelector),
              i = e.data();
            i.coords = i.coords.split(/[^0-9.]+/), n.on("click", function(e) {
              e.preventDefault(), h(t)
            }), s.push(i)
          }, d = 0; d < r.length; d++) u(d);

        function f() {
          if (l && !c) {
            g.logger.debug("ClinicBlock/initMap");
            var e = $.extend({}, o.mapOptions, {
              center: [55.1508, 61.4577]
            });
            a = new ymaps.Map(i.get(0), e), c = !0, a.behaviors.disable('scrollZoom')
          }
        }

        function h(e) {
          if (l) {
            c || f(), g.logger.debug("ClinicBlock/showItem", e);
            var t = s[e];
            if (!t.placemark) {
              var n = new ymaps.Placemark(t.coords, {
                balloonContentHeader: t.header,
                balloonContentBody: t.content.replace(/\n/g, "<br />")
              });
              t.placemark = n
            }
            a.setCenter(t.coords), a.setZoom(o.mapOptions.zoom), a.geoObjects.removeAll(), a.geoObjects.add(t.placemark), i.addClass("is-active"), r.removeClass("is-active").eq(e).addClass("is-active"), p.isMobile && $(m.browser.getScrollBody()).animate({
              scrollTop: r.eq(e).offset().top
            })
          }
        }
        ymaps.ready( function() {
          g.logger.debug("ClinicBlock/ymapsLoad"), t.trigger("ClinicBlock.ymapsLoad")
        }), t.on("ClinicBlock.ymapsLoad", function() {
          l = !0, (f(), h(0))
        })
      }
    }, {
      "../utils/browser": 44,
      "../utils/logger": 46,
      "../utils/params": 47
    }],
    11: [function(e, t, n) {
      "use strict";
      Object.defineProperty(n, "__esModule", {
        value: !0
      }), n.ContactPage = void 0;
      var c = e("../utils/logger");
      n.ContactPage = function(e) {
        c.logger.debug("ContactPage", e);
        var s = $.extend({
            selector: ".js-contact",
            linkSelector: ".js-contact-link",
            itemSelector: ".js-contact-item",
            mapSelector: ".js-contact-map",
            mapOptions: {
              zoom: 15,
              controls: ["default"]
            }
          }, e),
          a = !1,
          l = $(window);

        function t(e) {
          c.logger.debug("ContactPage/initBlock", e);
          var t = $(s.linkSelector, e),
            n = $(s.itemSelector, e),
            i = !1,
            r = !1;

          function o(e) {
            c.logger.debug("ContactPage/showItem", e), t.removeClass("is-active").eq(e).addClass("is-active"), n.hide().eq(e).show(), n.eq(e).is(s.mapSelector) && !r ? function(e) {
              var o = $(e);

              function t() {
                var e = o.data();
                e.coords = e.coords.split(/[^0-9.]+/);
                var t = $.extend({}, s.mapOptions, {
                    center: e.coords
                  }),
                  n = new ymaps.Map(o.get(0), t),
                  i = new ymaps.Placemark(e.coords, {
                    balloonContentHeader: e.header,
                    balloonContentBody: e.content.replace(/\n/g, "<br />")
                  });
                n.geoObjects.add(i), r = !0
              }
              a ? t() : l.on("ContactPage.ymapsLoad", t)
            }(n.get(e)) : n.eq(e).is(s.sliderSelector) && !i && (n.get(e), i = !0)
          }
          t.on("click", function(e) {
            e.preventDefault(), o(t.index(this))
          }), o(0)
        }
        ymaps.ready(function() {
          c.logger.debug("ContactPage/ymapsLoad"), l.trigger("ContactPage.ymapsLoad")
        }), $(s.selector).each(function() {
          t(this)
        }), l.on("ContactPage.ymapsLoad", function() {
          a = !0
        })
      }
    }, {
      "../utils/logger": 46
    }],
    12: [function(e, t, n) {
      "use strict";
      Object.defineProperty(n, "__esModule", {
        value: !0
      }), n.Content = void 0;
      var r = e("../utils/ajax"),
        s = e("../utils/params"),
        a = e("../utils/logger");
      n.Content = function(i) {
        function o(e, o) {
          a.logger.debug("Content/getContent", e, o), (0, r.getAjax)("get_content", e).then(function(e) {
            var t, n, i = e.data;
            i.ok ? (t = i.content, n = o, a.logger.debug("Content/showContent", n), $.fancybox($.extend(!0, {}, s.fancybox, n, {
              type: "html",
              content: t
            }))) : alert("Не удалось получить страницу")
          }).catch(function(e) {
            a.logger.error("Form/GetContent", e), alert("При получении страницы произошла ошибка")
          })
        }
        a.logger.debug("Content", i);
        var e = $.extend(!0, {}, {
          linkSelector: ".js-content-link"
        }, i);
        $(document.body).on("click", e.linkSelector, function(e) {
          e.preventDefault();
          var t = $(this),
            n = this.getAttribute("href");
          o($.extend({}, t.data(), {
            href: n
          }), i)
        })
      }
    }, {
      "../utils/ajax": 43,
      "../utils/logger": 46,
      "../utils/params": 47
    }],
    13: [function(e, t, n) {
      "use strict";
      Object.defineProperty(n, "__esModule", {
        value: !0
      }), n.InitFAQ = n.FAQs = void 0;
      var r = e("../utils/logger"),
        i = {
          blockSelector: ".js-faq",
          answerSelector: ".js-faq-answer",
          linkSelector: ".js-faq-link",
          duration: 400
        };

      function o(e, t) {
        r.logger.debug("FAQs/initBlock", e);
        var n = $(t.answerSelector, e),
          i = $(t.linkSelector, e);

        function o() {
          n.hasClass("is-opened") ? (r.logger.debug("FAQs/hideAnswer", e), n.slideUp(t.duration, function() {
            n.removeClass("is-opened"), i.text(i.data("show"))
          })) : (r.logger.debug("FAQs/showAnswer", e), n.slideDown(t.duration, function() {
            n.addClass("is-opened"), i.text(i.data("hide"))
          }))
        }
        n.hide(), i.on("click", function(e) {
          e.preventDefault(), o()
        })
      }
      n.FAQs = function(e) {
        r.logger.debug("FAQs", e);
        var t = $.extend({}, i, e);
        $(t.blockSelector).each(function() {
          o($(this), t)
        })
      }, n.InitFAQ = function(e, t) {
        var n = $.extend({}, i, t);
        o($(e), n)
      }
    }, {
      "../utils/logger": 46
    }],
    14: [function(e, t, n) {
      "use strict";
      Object.defineProperty(n, "__esModule", {
        value: !0
      }), n.FestivalPage = function(e) {
        var o = $.extend({
            headerMenu: ".js-main-menu",
            stickyMenu: ".js-sticky-block"
          }, e),
          r = $(window);
        ! function() {
          var e = $(o.headerMenu),
            t = void 0;

          function n() {
            t = r.scrollTop(), r.width() < 768 && (0 < t ? e.hasClass("is-fixed") || e.addClass("is-fixed") : e.hasClass("is-fixed") && e.removeClass("is-fixed"))
          }

          function i() {
            r.width() < 768 ? n() : e.hasClass("is-fixed") && e.removeClass("is-fixed")
          }
          r.on("resize", i).on("scroll", n), i()
        }(),
        function() {
          var t = $(o.stickyMenu),
            n = t.offset().top;

          function e() {
            var e = $(document).scrollTop();
            768 <= r.width() && (e < n ? t.css({
              position: "absolute",
              top: "-40px"
            }) : t.css({
              position: "fixed",
              top: "40px"
            }))
          }

          function i() {
            n = t.offset().top, e()
          }
          r.on("resize", i).on("scroll", e), i()
        }()
      }
    }, {}],
    15: [function(e, t, n) {
      "use strict";
      Object.defineProperty(n, "__esModule", {
        value: !0
      }), n.Form = void 0;
      var l = e("../utils/ajax"),
        s = e("../utils/params"),
        f = e("../utils/logger"),
        a = e("./targets"),
        h = {
          formSelector: ".js-form",
          linkSelector: ".js-form-link",
          fieldSelector: ".js-form-field",
          inputSelector: "input, select, textarea",
          errorClass: "is-error"
        };

      function c(e, t) {
        f.logger.debug("Form/InitForm", e, t);
        var a = $(e),
          o = $.extend(!0, {}, s.form, t);
        $('input[type="tel"]', e).mask("+7 (999) 999 99 99"), $("input.js-input-int", e).inputcheck("int"), $("input.js-input-float", e).inputcheck("float"), $("input.js-input-file", e).fileupload(o.fileuploadOptions), $("input.js-input-date, input.js-input-time, input.js-input-datetime", e).each(function() {
          var e = $(this),
            t = $.extend({}, e.data());
          for (var n in t) /^\{|\[/.test(t[n]) && (t[n] = JSON.parse(t[n]));
          var i = {};
          i = e.hasClass("js-input-date") ? o.dateOptions : e.hasClass("js-input-time") ? o.timeOptions : o.dateTimeOptions, e.prop("readonly", !0), $.extend(i, t), e.datetimepicker(i)
        });
        var d = a.find(h.fieldSelector);
        d.filter(".js-has-dependent").on("change", h.inputSelector, function() {
          var u = $(this).parents(h.fieldSelector).data("dependent").split(/\s+/),
            e = !0,
            t = !1,
            n = void 0;
          try {
            for (var i, o = u[Symbol.iterator](); !(e = (i = o.next()).done); e = !0) {
              var r = i.value;
              d.filter(".m--name-" + r).find(h.inputSelector).addClass("is-loading")
            }
          } catch (e) {
            t = !0, n = e
          } finally {
            try {
              !e && o.return && o.return()
            } finally {
              if (t) throw n
            }
          }
          var s = a.serializeObject()
        }), d.filter(".m--name-captcha").remove()
      }
      n.Form = function(r) {
        f.logger.debug("Form", r);
        var e = $.extend(!0, {}, h, r);
        $(document.body).on("click", e.linkSelector, function(e) {
          e.preventDefault();
          var t, n, i = $(this),
            o = this.getAttribute("href");
          t = $.extend({}, i.data(), {
            href: o
          }), n = r, f.logger.debug("Form/GetForm", t, n), (0, l.getAjax)("get_form", t).then(function(e) {
            var t = e.data;
            f.logger.debug("Form/GetForm/then", t), t.ok ? function(e, t) {
              f.logger.debug("Form/ShowForm", t);
              var n = $.extend({}, h, t);
              $.fancybox($.extend(!0, {}, s.fancybox, t, {
                type: "html",
                content: e,
                afterShow: function() {
                  this.inner.find(n.formSelector).each(function() {
                    c(this)
                  })
                }
              }))
            }(t.content, n) : alert("Не удалось получить форму")
          }).catch(function(e) {
            f.logger.error("Form/GetForm/catch", e), alert("При получении формы произошла ошибка")
          })
        })
      }
    }, {
      "../utils/ajax": 43,
      "../utils/logger": 46,
      "../utils/params": 47,
      "./targets": 39
    }],
    16: [function(e, t, n) {
      "use strict";
      Object.defineProperty(n, "__esModule", {
        value: !0
      }), n.Gallery = void 0;
      var i = e("../utils/params"),
        o = e("../utils/logger");
      n.Gallery = function(e) {
        o.logger.debug("Gallery", e);
        var t = $.extend({
          selector: '.js-fancy, a[rel="lightbox"]'
        }, e);
        $(t.selector).fancybox($.extend(!0, {}, i.fancybox, t.fancybox))
      }
    }, {
      "../utils/logger": 46,
      "../utils/params": 47
    }],
    17: [function(e, t, n) {
      "use strict";
      Object.defineProperty(n, "__esModule", {
        value: !0
      }), n.GroupAdvantage = function(e) {
        var n = $.extend({
          selector: ".js-group-advantage",
          more: ".js-group-advantage-more",
          link: ".js-group-advantage-more-link"
        }, e);
        $(n.selector).each(function() {
          var t = $(this);
          t.find(n.more).find(n.link).on("click", function(e) {
            e.preventDefault(), t.toggleClass("is-active")
          })
        })
      }
    }, {}],
    18: [function(e, t, n) {
      "use strict";
      Object.defineProperty(n, "__esModule", {
        value: !0
      }), n.HeaderSlider = void 0;
      var i = e("../utils/logger");
      n.HeaderSlider = function(e) {
        i.logger.debug("HeaderSlider", e);
        var r = $.extend({
            selector: ".js-header-slider",
            itemSelector: ".js-header-slider-item",
            prevSelector: ".js-header-slider-prev",
            nextSelector: ".js-header-slider-next",
            bgSelector: ".js-header-slider-bg",
            numSelector: ".js-header-slider-num",
            params: {
              autoChange: !0,
              stopOnMouseOver: !0,
              loop: !0,
              duration: 300,
              delay: 5e3,
              goOutCssBefore: function() {
                return {
                  left: "0%"
                }
              },
              goOutCssAfter: function(e, t, n) {
                return 0 == e && t == n - 1 ? {
                  left: "100%"
                } : e == n - 1 && 0 == t ? {
                  left: "-100%"
                } : {
                  left: e < t ? "-100%" : "100%"
                }
              },
              goInCssBefore: function(e, t, n) {
                return 0 == e && t == n - 1 ? {
                  left: "-100%"
                } : e == n - 1 && 0 == t ? {
                  left: "100%"
                } : {
                  left: e < t ? "100%" : "-100%"
                }
              },
              goInCssAfter: function() {
                return {
                  left: "0%"
                }
              }
            }
          }, e),
          t = $(r.selector),
          s = $(r.bgSelector),
          n = !1;
        $(window).on("Responsive.on.m Responsive.on.l Responsive.on.xl", function() {
          n || (i.logger.debug("HeaderSlider/initSlider"), t.each(function() {
            var n = $(this),
              i = n.find(r.numSelector),
              o = n.find(r.itemSelector),
              e = $.extend(!0, {}, {
                prev: $(r.prevSelector, this),
                next: $(r.nextSelector, this),
                beforeChange: function(e, t) {
                  s.eq(e).animate({
                    opacity: 0
                  }, {
                    duration: r.params.duration,
                    complete: function() {
                      s.eq(e).css({
                        display: "none",
                        opacity: ""
                      })
                    }
                  }), s.eq(t).css({
                    display: "block",
                    opacity: 0
                  }).animate({
                    opacity: 1
                  }, {
                    duration: r.params.duration,
                    complete: function() {
                      s.eq(t).css({
                        opacity: ""
                      })
                    }
                  }), o.eq(t).data("publ_slider") ? n.show() : n.hide()
                },
                afterChange: function(e, t) {
                  i.text(t + 1)
                }
              }, r.params, n.data());
            n.data({
              items: o
            }), o.slider(e)
          }), n = !0)
        }), $(window).on("Responsive.on.s Responsive.on.xs", function() {
          n && (i.logger.debug("HeaderSlider/destroySlider"), t.each(function() {
            var e = $(this);
            e.data("items").unslider(), e.removeData("items")
          }), n = !1)
        })
      }
    }, {
      "../utils/logger": 46
    }],
    19: [function(e, t, n) {
      "use strict";
      Object.defineProperty(n, "__esModule", {
        value: !0
      }), n.Helper = void 0;
      var a = e("../utils/logger"),
        l = e("../utils/cookies"),
        c = e("../utils/ajax");
      n.Helper = function(e) {
        a.logger.debug("Helper", e);
        var n = $.extend({
            selector: ".js-helper",
            closeSelector: ".js-helper-close",
            delay: 12e4
          }, e),
          i = void 0;

        function t() {
          a.logger.debug("Helper/getHelper"), (0, c.getAjax)("get_helper", {}).then(function(e) {
            var t = e.data;
            a.logger.debug("Helper/getHelper/then", t), $(t.content).appendTo(document.body), i = $(n.selector), $(document.body).on("click", n.closeSelector, function(e) {
              e.preventDefault(), l.cookies.set("Helper.isClosed", "true", "+1d", "/"), a.logger.debug("Helper/hideHelper"), i.removeClass("is-active")
            }), a.logger.debug("Helper/showHelper"), i.addClass("is-active")
          }).catch(function(e) {
            a.logger.error("Helper/getHelper/catch", e)
          })
        }
        if (!l.cookies.get("Helper.isClosed")) {
          var o = 0,
            r = (new Date).getTime();
          l.cookies.get("Helper.start") ? o = parseInt(l.cookies.get("Helper.start")) : (o = r, l.cookies.set("Helper.start", o, "+1h", "/"));
          var s = o - r + n.delay;
          0 < s ? setTimeout(function() {
            t()
          }, s) : t()
        }
      }
    }, {
      "../utils/ajax": 43,
      "../utils/cookies": 45,
      "../utils/logger": 46
    }],
    20: [function(e, t, n) {
      "use strict";
      Object.defineProperty(n, "__esModule", {
        value: !0
      }), n.MainMenu = void 0;
      var p = e("../utils/params"),
        g = e("../utils/logger");
      n.MainMenu = function(e) {
        g.logger.debug("MainMenu", e);
        var d = $.extend({
            selector: ".js-main-menu",
            listSelector: ".js-main-menu-list",
            hamburgerSelector: ".js-main-menu-hamburger",
            hideSelector: ".js-main-menu-hide",
            itemSelector: ".js-main-menu-item",
            linkSelector: ".js-main-menu-link",
            submenuSelector: ".js-main-menu-submenu"
          }, e),
          f = $(window),
          h = !1;

        function t(e) {
          var n = $(e),
            i = n.find(d.listSelector),
            t = n.find(d.hamburgerSelector),
            o = n.find(d.hideSelector),
            r = i.find(d.itemSelector);

          function s() {
            g.logger.debug("MainMenu/rebuildMenu"), n.removeClass("is-collapsed"), r.css({
              display: ""
            });
            for (var e = r.length - 1, t = !1; i.prop("scrollWidth") > i.prop("clientWidth");) t || (n.addClass("is-collapsed"), t = !0), r.eq(e).css({
              display: "none"
            }), e--
          }

          function a() {
            if (g.logger.debug("MainMenu/fitMenu"), h) {
              var e = void 0;
              e = "festival_page" === p.interfaceName ? f.height() : f.height() - n.outerHeight(), i.css({
                height: e
              })
            } else i.css({
              height: ""
            })
          }

          function l() {
            g.logger.debug("MainMenu/hideMenu"), n.removeClass("is-opened").unoverlay(), f.trigger("MainMenu.hideMenu"), f.off("resize", a), i.css({
              height: ""
            }), p.isDesktop || r.filter(".m--submenu.is-hover").each(function() {
              u(this), $(this).removeClass("is-hover")
            })
          }

          function c(e) {
            g.logger.debug("MainMenu/showSubmenu", e);
            var n = $(e);
            if (!n.data("submenu")) {
              var i = n.find(d.submenuSelector);
              n.data({
                submenu: i,
                fitSubmenu: function() {
                  g.logger.debug("MainMenu/fitSubmenu");
                  var e = n.offset().left,
                    t = i.offset().left;
                  i.css({
                    paddingLeft: e - t
                  })
                }
              })
            }
            var t = n.data("fitSubmenu");
            f.on("resize MainMenu.showSubmenu", t), t()
          }

          function u(e) {
            g.logger.debug("MainMenu/hideSubmenu", e);
            var t = $(this).data("fitSubmenu");
            f.off("resize MainMenu.showSubmenu", t)
          }
          s(), f.on("resize StickyHeader.show StickyHeader.hide MainMenu.showMenu MainMenu.hideMenu", s), t.on("click", function(e) {
            e.preventDefault(), n.hasClass("is-opened") ? l() : (g.logger.debug("MainMenu/showMenu"), n.addClass("is-opened").overlay(l), f.trigger("MainMenu.showMenu"), f.on("resize", a), a())
          }), o.on("click", function(e) {
            e.preventDefault(), l()
          }), p.isDesktop ? r.filter(".m--submenu").on("mouseover", function() {
            c(this)
          }).on("mouseout", function() {
            u(this)
          }) : (r.filter(".m--submenu").each(function() {
            var t = this,
              n = $(this);
            $(d.linkSelector, this).on("click", function(e) {
              e.preventDefault(), n.hasClass("is-hover") ? (u(t), n.removeClass("is-hover")) : (n.addClass("is-hover"), c(t))
            })
          }), f.on("StickyHeader.show StickyHeader.hide", function() {
            r.filter(".m--submenu.is-hover").each(function() {
              u(this), $(this).removeClass("is-hover")
            })
          })), f.on("StickyHeader.show StickyHeader.hide", function() {
            l()
          })
        }
        $(d.selector).each(function() {
          t(this)
        }), f.on("Responsive.on.s Responsive.on.xs", function() {
          h = !0
        }), f.on("Responsive.on.m Responsive.on.m Responsive.on.xl", function() {
          h = !1
        })
      }
    }, {
      "../utils/logger": 46,
      "../utils/params": 47
    }],
    21: [function(e, t, n) {
      "use strict";
      Object.defineProperty(n, "__esModule", {
        value: !0
      }), n.Map = function(e) {
        var r = $.extend({
          selector: ".js-map",
          mapOptions: {
            zoom: 15,
            controls: ["default"]
          }
        }, e);
        ymaps.ready(function() {
          $(r.selector).each(function() {
            var o;
            o = $(this),
              function() {
                var e = o.data();
                e.coords = e.coords.split(/[^0-9.]+/);
                var t = $.extend({}, r.mapOptions, {
                    center: e.coords
                  }),
                  n = new ymaps.Map(o.get(0), t),
                  i = new ymaps.Placemark(e.coords, {}, {
                    iconLayout: "default#image",
                    iconImageHref: e.logo,
                    iconImageSize: [142, 42],
                    iconImageOffset: [-71, 0]
                  });
                n.geoObjects.add(i)
              }()
          })
        })
      }
    }, {}],
    22: [function(e, t, n) {
      "use strict";
      Object.defineProperty(n, "__esModule", {
        value: !0
      }), n.Overflow = void 0;
      var d = e("../utils/logger"),
        f = e("../utils/params");
      n.Overflow = function(e) {
        d.logger.debug("Overflow", e);
        var c = $.extend({
            selector: ".js-overflow",
            blockSelector: ".js-overflow-block",
            moreSelector: ".js-overflow-more",
            linkSelector: ".js-overflow-link",
            height: "auto",
            duration: 400
          }, e),
          u = $(window);

        function t(t) {
          d.logger.debug("Overflow/initBlock", t);
          var n = $(t),
            i = n.find(c.blockSelector),
            o = n.find(c.moreSelector),
            e = n.find(c.linkSelector);

          function r() {
            var e = i.data("height");
            d.logger.debug("Overflow/rebuildBlock", t, e), i.css({
              height: ""
            }).data({
              scrollHeight: i.prop("scrollHeight")
            }), "auto" === e || i.height() <= parseInt(e) ? (n.addClass("is-opened"), n.addClass("m--auto-height"), o.hide()) : (n.removeClass("is-opened"), i.css({
              height: e
            }), o.show())
          }

          function s() {
            n.hasClass("is-opened") ? (d.logger.debug("Overflow/hideBlock", t), i.animate({
              height: i.data("height")
            }, {
              duration: c.duration,
              complete: function() {
                n.removeClass("is-opened"), e.each(function() {
                  var e = $(this);
                  e.data("show") && e.text(e.data("show"))
                })
              }
            })) : (d.logger.debug("Overflow/showBlock", t), i.animate({
              height: i.data("scrollHeight")
            }, {
              duration: c.duration,
              complete: function() {
                n.addClass("is-opened"), i.css({
                  height: ""
                }), e.each(function() {
                  var e = $(this);
                  e.data("hide") && e.text(e.data("hide"))
                })
              }
            }))
          }
          e.on("click", function(e) {
            e.preventDefault(), s()
          });
          var a = function(e) {
            var t = "height" + (e.charAt(0).toUpperCase() + e.slice(1));
            i.data(t) && u.on("Responsive.on." + e, function() {
              i.data("height", i.data(t)), r()
            })
          };
          for (var l in f.breakpoints) a(l);
          i.data("height") || i.data("height", c.height), r()
        }
        $(c.selector).each(function() {
          t(this)
        })
      }
    }, {
      "../utils/logger": 46,
      "../utils/params": 47
    }],
    23: [function(e, t, n) {
      "use strict";
      Object.defineProperty(n, "__esModule", {
        value: !0
      }), n.PopupVideo = void 0;
      var o = e("../utils/logger"),
        r = e("../utils/params"),
        s = e("../utils/ajax"),
        a = e("../utils/cookies"),
        l = e("./video");
      n.PopupVideo = function(n) {
        o.logger.debug("PopupVideo", n);
        var i = $.extend({
          params: {},
          videoSelector: ".js-video",
          videoOptions: {}
        }, n);
        a.cookies.get("popupVideoShown") && !a.cookies.get("PopupVideo.isShown") && (a.cookies.set("PopupVideo.isShown", a.cookies.get("popupVideoShown"), "+1y", "/"), a.cookies.set("popupVideoShown", "", "+1s", "")), r.isDesktop && !a.cookies.get("PopupVideo.isShown") && (0, s.getAjax)("get_video", i.params).then(function(e) {
          var t = e.data;
          o.logger.debug("PopupVideo/then", t), t.ok && $.fancybox($.extend(!0, {}, r.fancybox, n, i.fancybox, {
            type: "html",
            content: t.content,
            afterShow: function() {
              this.inner.find(i.videoSelector).each(function() {
                (0, l.InitVideo)(this, i.videoOptions), a.cookies.set("PopupVideo.isShown", "true", "+1y", "/")
              })
            }
          }))
        }).catch(function(e) {
          o.logger.error("PopupVideo/catch", e)
        })
      }
    }, {
      "../utils/ajax": 43,
      "../utils/cookies": 45,
      "../utils/logger": 46,
      "../utils/params": 47,
      "./video": 42
    }],
    24: [function(e, t, n) {
      "use strict";
      Object.defineProperty(n, "__esModule", {
        value: !0
      }), n.PricePage = void 0;
      var o = e("../utils/logger");
      n.PricePage = function(e) {
        o.logger.debug("PricePage", e);
        var i = $.extend({
          selector: ".js-price",
          linkSelector: ".js-price-link",
          listSelector: ".js-price-list",
          duration: 300
        }, e);
        $(i.selector).each(function() {
          var t = $(this),
            e = t.find(i.linkSelector),
            n = t.find(i.listSelector);
          n.hide(), e.on("click", function(e) {
            e.preventDefault(), t.hasClass("is-opened") ? (t.removeClass("is-opened"), n.slideUp(i.duration)) : (t.addClass("is-opened"), n.slideDown(i.duration))
          })
        })
      }
    }, {
      "../utils/logger": 46
    }],
    25: [function(e, t, n) {
      "use strict";
      Object.defineProperty(n, "__esModule", {
        value: !0
      }), n.ProgramCat = void 0;
      var a = e("../utils/logger");
      n.ProgramCat = function(e) {
        a.logger.debug("ProgramCat", e);
        var n = $.extend({
          selector: ".js-program-cat",
          formSelector: ".js-program-form",
          groupSelector: ".js-program-group"
        }, e);
        $(n.selector).each(function() {
          var r = $(n.formSelector),
            s = $(n.groupSelector);

          function t() {
            var e = r.serializeObject();
            a.logger.debug("ProgramCat/submitForm", e);
            for (var t = 0; t < s.length; t++) {
              var n = s.eq(t),
                i = !0;
              for (var o in e)
                if (e[o] && n.data(o) != e[o]) {
                  i = !1;
                  break
                }
              i ? n.show() : n.hide()
            }
          }
          r.on("change", "select", function() {
            t()
          }).on("submit", function(e) {
            e.preventDefault(), t()
          }), t()
        })
      }
    }, {
      "../utils/logger": 46
    }],
    26: [function(e, t, n) {
      "use strict";
      Object.defineProperty(n, "__esModule", {
        value: !0
      }), n.Responsive = void 0;
      var r = e("../utils/params"),
        s = e("../utils/logger");
      n.Responsive = function(e) {
        s.logger.debug("Responsive", e);
        var n = $.extend(!0, {}, {
            breakpoints: r.breakpoints
          }, e),
          i = $(window),
          o = void 0;

        function t() {
          var e = i.width(),
            t = "xs";
          e > n.breakpoints.l ? t = "xl" : e > n.breakpoints.m ? t = "l" : e > n.breakpoints.s ? t = "m" : e > n.breakpoints.xs && (t = "s"), o && t == o || (o && (s.logger.debug("Responsive/watchSize/Responsive.off." + o), i.trigger("Responsive.off." + o)), s.logger.debug("Responsive/watchSize/Responsive.on." + t), i.trigger("Responsive.on." + t), o = t)
        }
        i.on("resize", t), t()
      }
    }, {
      "../utils/logger": 46,
      "../utils/params": 47
    }],
    27: [function(e, t, n) {
      "use strict";
      Object.defineProperty(n, "__esModule", {
        value: !0
      }), n.ReviewCat = void 0;
      var i = e("../utils/logger");
      n.ReviewCat = function(e) {
        i.logger.debug("ReviewCat", e);
        var t = $.extend({
            formSelector: ".js-review-filter"
          }, e),
          n = $(t.formSelector);
        n.each(function() {
          n.on("change", "input, select, textarea", function() {
            n.submit()
          })
        })
      }
    }, {
      "../utils/logger": 46
    }],
    28: [function(e, t, n) {
      "use strict";
      Object.defineProperty(n, "__esModule", {
        value: !0
      }), n.InitReview = n.Reviews = void 0;
      var a = e("../utils/logger"),
        i = {
          blockSelector: ".js-review",
          contentSelector: ".js-review-content",
          moreSelector: ".js-review-more",
          linkSelector: ".js-review-link",
          maxHeight: 84,
          duration: 400
        },
        l = $(window);

      function o(e, t) {
        a.logger.debug("Reviews/initBlock", e);
        var n = $(t.contentSelector, e),
          i = $(t.moreSelector, e),
          o = $(t.linkSelector, e);

        function r() {
          a.logger.debug("Reviews/rebuildContent", e), n.hasClass("is-opened") || (n.css({
            height: ""
          }), n.data({
            scrollHeight: n.prop("scrollHeight")
          }), n.data("scrollHeight") > t.maxHeight ? (n.css({
            height: t.maxHeight
          }), i.show()) : i.hide())
        }

        function s() {
          n.hasClass("is-opened") ? (a.logger.debug("Reviews/hideContent", e), n.animate({
            height: t.maxHeight
          }, {
            duration: t.duration,
            complete: function() {
              n.removeClass("is-opened"), o.text(o.data("show"))
            }
          })) : (a.logger.debug("Reviews/showContent", e), n.animate({
            height: n.data("scrollHeight")
          }, {
            duration: t.duration,
            complete: function() {
              n.addClass("is-opened"), o.text(o.data("hide"))
            }
          }))
        }
        o.on("click", function(e) {
          e.preventDefault(), s()
        }), l.on("resize Tabs.show", r), r()
      }
      n.Reviews = function(e) {
        a.logger.debug("Reviews", e);
        var t = $.extend({}, i, e);
        $(t.blockSelector).each(function() {
          o($(this), t)
        })
      }, n.InitReview = function(e, t) {
        var n = $.extend({}, i, t);
        o($(e), n)
      }
    }, {
      "../utils/logger": 46
    }],
    29: [function(e, t, n) {
      "use strict";
      Object.defineProperty(n, "__esModule", {
        value: !0
      }), n.Scroll = void 0;
      e("../utils/params");
      n.Scroll = function(e) {
        var d = $.extend({
          selector: ".js-scroll",
          block: ".js-scroll-block",
          blockWrapper: ".js-scroll-block-wrapper",
          top: ".js-scroll-top",
          bottom: ".js-scroll-bottom"
        }, e);
        $(d.selector).each(function() {
          ! function(e) {
            var t = $(e),
              n = t.find(d.block),
              i = n.find(d.blockWrapper),
              o = t.find(d.top),
              r = t.find(d.bottom),
              s = void 0,
              a = void 0,
              l = void 0,
              c = $(window);

            function u() {
              s = i[0].scrollHeight, a = n.outerHeight(), l = n[0].scrollHeight, s <= a ? (o.addClass("is-hidden"), r.addClass("is-hidden")) : (o.removeClass("is-hidden"), r.removeClass("is-hidden"))
            }
            o.addClass("is-disabled"), o.on({
              "mousedown touchstart": function(e) {
                e.preventDefault(), n.animate({
                  scrollTop: 0
                }, {
                  duration: 500,
                  step: function() {
                    0 == n.scrollTop() && o.addClass("is-disabled")
                  }
                }), r.hasClass("is-disabled") && r.removeClass("is-disabled")
              },
              "mouseup touchend": function() {
                n.stop(!0)
              }
            }), r.on({
              "mousedown touchstart": function(e) {
                e.preventDefault(), n.animate({
                  scrollTop: l
                }, {
                  duration: 1e3,
                  step: function() {
                    n.scrollTop() + a == s && r.addClass("is-disabled")
                  }
                }), o.hasClass("is-disabled") && o.removeClass("is-disabled")
              },
              "mouseup touchend": function() {
                n.stop(!0)
              }
            }), c.resize(u), u()
          }(this)
        })
      }
    }, {
      "../utils/params": 47
    }],
    30: [function(e, t, n) {
      "use strict";
      Object.defineProperty(n, "__esModule", {
        value: !0
      }), n.Scroller = void 0;
      var i = e("../utils/logger");
      n.Scroller = function(e) {
        i.logger.debug("Scroller", e);
        var n = $.extend(!0, {
          selector: ".js-scroller",
          itemSelector: ".js-scroller-item",
          prevSelector: ".js-scroller-prev",
          nextSelector: ".js-scroller-next",
          params: {
            duration: 300,
            delay: 5e3
          }
        }, e);
        $(n.selector).each(function() {
          var e = $(this),
            t = $.extend(!0, {}, {
              prev: e.find(n.prevSelector),
              next: e.find(n.nextSelector)
            }, n.params, e.data());
          $(n.itemSelector, this).scroller(t)
        })
      }
    }, {
      "../utils/logger": 46
    }],
    31: [function(e, t, n) {
      "use strict";
      Object.defineProperty(n, "__esModule", {
        value: !0
      }), n.ServiceGroup = void 0;
      var i = e("./ajax-specialists"),
        o = e("./ajax-reviews"),
        r = e("./ajax-faqs");
      n.ServiceGroup = function(e) {
        $.extend({}, e), (0, i.AjaxSpecialists)(), (0, o.AjaxReviews)(), (0, r.AjaxFaqs)()
      }
    }, {
      "./ajax-faqs": 3,
      "./ajax-reviews": 5,
      "./ajax-specialists": 6
    }],
    32: [function(e, t, n) {
      "use strict";
      Object.defineProperty(n, "__esModule", {
        value: !0
      }), n.Services = void 0;
      var i = e("../utils/logger");
      n.Services = function(e) {
        i.logger.debug("Services", e);
        var r = $.extend({
          selector: ".js-service",
          linkSelector: ".js-service-link",
          contentSelector: ".js-service-content",
          closeSelector: ".js-service-close",
          duration: 400
        }, e);
        $(r.selector).each(function() {
          var e = $(r.linkSelector, this),
            t = $(r.contentSelector, this),
            n = $(r.closeSelector, this);

          function i() {
            t.slideUp(r.duration, function() {
              e.each(function() {
                var e = $(this);
                e.data("show") && e.text(e.data("show"))
              })
            })
          }

          function o() {
            t.is(":visible") ? i() : t.slideDown(r.duration, function() {
              e.each(function() {
                var e = $(this);
                e.data("hide") && e.text(e.data("hide"))
              })
            })
          }
          t.hide(), e.on("click", function(e) {
            e.preventDefault(), o()
          }), n.on("click", function(e) {
            e.preventDefault(), i()
          })
        })
      }
    }, {
      "../utils/logger": 46
    }],
    33: [function(e, t, n) {
      "use strict";
      Object.defineProperty(n, "__esModule", {
        value: !0
      }), n.Slider = void 0;
      var o = e("../utils/logger");
      n.Slider = function(e) {
        o.logger.debug("Slider", e);
        var i = $.extend({
          selector: ".js-slider",
          itemSelector: ".js-slider-item",
          prevSelector: ".js-slider-prev",
          nextSelector: ".js-slider-next",
          numSelector: ".js-slider-num",
          navSelector: ".js-slider-nav",
          params: {
            duration: 300,
            delay: 5e3,
            autoChange: !0,
            stopOnMouseOver: !0,
            loop: !0,
            goOutCssBefore: function() {
              return {
                left: "0%"
              }
            },
            goOutCssAfter: function(e, t, n) {
              return 0 == e && t == n - 1 ? {
                left: "100%"
              } : e == n - 1 && 0 == t ? {
                left: "-100%"
              } : {
                left: e < t ? "-100%" : "100%"
              }
            },
            goInCssBefore: function(e, t, n) {
              return 0 == e && t == n - 1 ? {
                left: "-100%"
              } : e == n - 1 && 0 == t ? {
                left: "100%"
              } : {
                left: e < t ? "100%" : "-100%"
              }
            },
            goInCssAfter: function() {
              return {
                left: "0%"
              }
            }
          }
        }, e);
        $(i.selector).each(function() {
          var e = $(this),
            n = e.find(i.numSelector),
            t = $.extend(!0, {}, {
              prev: e.find(i.prevSelector),
              next: e.find(i.nextSelector),
              nav: e.find(i.navSelector),
              afterChange: function(e, t) {
                n.text(t + 1)
              }
            }, i.params, e.data());
          $(i.itemSelector, this).slider(t)
        })
      }
    }, {
      "../utils/logger": 46
    }],
    34: [function(e, t, n) {
      "use strict";
      Object.defineProperty(n, "__esModule", {
        value: !0
      }), n.SpecialistCat = void 0;
      var m = e("../utils/logger");
      n.SpecialistCat = function(e) {
        m.logger.debug("SpecialistCat", e);
        var s = $.extend({
          selector: ".js-specialist-cat",
          formSelector: ".js-specialist-filter",
          linkSelector: ".js-specialist-link",
          groupSelector: ".js-specialist-group",
          itemSelector: ".js-specialist-item",
          tabsSelector: ".js-specialist-tabs",
          delay: 300
        }, e);
        $(s.selector).each(function() {
          for (var f = $(s.formSelector), h = $(s.tabsSelector), p = $(s.linkSelector), g = $(s.groupSelector), e = 0; e < g.length; e++) {
            for (var t = g.eq(e), n = t.find(s.itemSelector), i = 0; i < n.length; i++) {
              var o = n.eq(i);
              o.data({
                service: o.data("service") ? o.data("service").toString().split(/\s+/) : [],
                clinic: o.data("clinic") ? o.data("clinic").toString().split(/\s+/) : []
              })
            }
            n.length || (t.addClass("is-disabled"), p.eq(e).addClass("is-disabled")), t.data({
              items: n
            })
          }

          function r() {
            var e = f.serializeObject();
            m.logger.debug("SpecialistCat/submitForm", e), e.name = new RegExp(e.name, "i");
            for (var t = !1, n = 0; n < g.length; n++) {
              var i = g.eq(n),
                o = p.eq(n),
                r = i.data("items"),
                s = 0;
              r.removeClass("m--1 m--2 m--3 m--0");
              for (var a = 0; a < r.length; a++) {
                var l = r.eq(a),
                  c = !0;
                for (var u in e)
                  if (e[u])
                    if ("name" === u) {
                      if (!e.name.test(l.data("name"))) {
                        c = !1;
                        break
                      }
                    } else if (-1 == l.data(u).indexOf(e[u])) {
                  c = !1;
                  break
                }
                c ? (l.show().addClass("m--" + (s + 1) % 4), s++) : l.hide()
              }
              s ? (i.removeClass("is-disabled"), o.removeClass("is-disabled")) : (i.addClass("is-disabled"), o.addClass("is-disabled"), o.hasClass("is-active") && (t = !0)), t || p.filter(".is-active").length || (t = !0)
            }
            if (t) {
              var d = p.filter(":not(.is-disabled)");
              h.trigger("Tabs.showItem", {
                idx: p.index(d.get(0))
              })
            }
          }
          f.on("change", "select", function() {
            r()
          }).on("submit", function(e) {
            e.preventDefault(), r()
          }), f.on("keyup", "input, textarea", function() {
            this.timer && clearTimeout(this.timer), this.timer = setTimeout(r, s.delay)
          }), r()
        })
      }
    }, {
      "../utils/logger": 46
    }],
    35: [function(e, t, n) {
      "use strict";
      Object.defineProperty(n, "__esModule", {
        value: !0
      }), n.StickyAside = void 0;
      var r = e("../utils/params"),
        i = e("../utils/logger");
      n.StickyAside = function(e) {
        i.logger.debug("StickyAside", e);
        var t = $.extend({
            selector: ".js-sticky-aside",
            searchSelector: ".js-sticky-aside-search",
            inputSelector: ".js-sticky-aside-input"
          }, e),
          n = $(t.selector),
          o = $(t.inputSelector),
          s = $(t.searchSelector);
        o.on("focus", function() {
          s.addClass("is-focus")
        }).on("blur", function() {
          s.removeClass("is-focus")
        }), r.isDesktop || function() {
          function e(e) {
            e.stopPropagation ? e.stopPropagation() : e.cancelBubble = !0
          }

          function t() {
            n.addClass("is-hover").on("touchstart", e), o.on("touchstart", r)
          }

          function r() {
            n.removeClass("is-hover").off("touchstart", e), o.off("touchstart", r)
          }
          var o = $(document.body);
          n.on("click", function(e) {
            n.hasClass("is-hover") || (e.preventDefault(), n.addClass("is-hover"))
          }), $(document).click(function(e) {
            e.target == n[0] || n.has(e.target).length || n.removeClass("is-hover")
          }), new Hammer(document.body, {
            threshold: 50
          }).on("swiperight", function(e) {
            t()
          }).on("swipeleft", function(e) {
            r()
          })
        }()
      }
    }, {
      "../utils/logger": 46,
      "../utils/params": 47
    }],
    36: [function(e, t, n) {
      "use strict";
      Object.defineProperty(n, "__esModule", {
        value: !0
      }), n.StickyHeader = void 0;
      var c = e("../utils/browser"),
        u = e("../utils/logger");
      n.StickyHeader = function(e) {
        u.logger.debug("StickyHeader", e);
        var t = $.extend({
            selector: ".js-sticky-header",
            menuSelector: ".js-header .js-main-menu"
          }, e),
          n = $(t.selector),
          i = $(t.menuSelector),
          o = $(window),
          r = ($(c.browser.getScrollBody()), !1),
          s = 0;

        function a() {
          s = i.offset().top + i.outerHeight(), l()
        }

        function l() {
          var e = o.scrollTop();
          s < e ? (r || (n.addClass("is-active"), r = !0, u.logger.debug("StickyHeader/show"), o.trigger("StickyHeader.show")), n.css({
            left: -o.scrollLeft()
          })) : r && (n.removeClass("is-active").css({
            left: ""
          }), r = !1, u.logger.debug("StickyHeader/hide"), o.trigger("StickyHeader.hide"))
        }
        o.on("resize", a).on("scroll", l), a()
      }
    }, {
      "../utils/browser": 44,
      "../utils/logger": 46
    }],
    37: [function(e, t, n) {
      "use strict";
      Object.defineProperty(n, "__esModule", {
        value: !0
      }), n.Support = void 0;
      var i = e("../utils/logger");
      n.Support = function(e) {
        i.logger.debug("Support", e);
        var n = $.extend({
          advantageSelector: ".js-support-advantage",
          advantageLinkSelector: ".js-support-advantage-link"
        }, e);
        $(n.advantageSelector).each(function() {
          var t = $(this);
          $(n.advantageLinkSelector, this).on("click", function(e) {
            e.preventDefault(), t.toggleClass("is-active")
          })
        })
      }
    }, {
      "../utils/logger": 46
    }],
    38: [function(e, t, n) {
      "use strict";
      Object.defineProperty(n, "__esModule", {
        value: !0
      }), n.Tabs = void 0;
      var g = e("../utils/logger"),
        m = e("../utils/browser");
      n.Tabs = function(e) {
        g.logger.debug("Tabs", e);
        var c = $.extend({
            selector: ".js-tabs",
            linkSelector: ".js-tabs-link",
            itemSelector: ".js-tabs-item",
            headerSelector: ".js-tabs-header",
            contentSelector: ".js-tabs-content",
            stickyHeaderSelector: ".js-sticky-header",
            duration: 600
          }, e),
          u = document.location.hash,
          d = !1,
          f = !1,
          h = $(window),
          p = $(c.stickyHeaderSelector),
          t = $(c.selector);
        t.each(function() {
          var e = $(this),
            i = $(c.linkSelector, this),
            o = $(c.itemSelector, this),
            r = $(c.headerSelector, this),
            s = $(c.contentSelector, this),
            a = -1;

          function n(e) {
            g.logger.debug("Tabs/showItem", e), a = e, t(), h.trigger("Tabs.show resize")
          }

          function t(e) {
            if (g.logger.debug("Tabs/rebuildBlock", e, d), i.removeClass("is-active"), r.removeClass("is-active"), -1 !== a && (i.eq(a).addClass("is-active"), r.eq(a).addClass("is-active")), d) {
              if (o.show(), e) s.hide(), -1 !== a && s.eq(a).show();
              else {
                var t = 0;
                if (-1 !== a) {
                  var n = o.filter(".is-active").find(c.contentSelector).hide();
                  s.eq(a).show(), t = o.eq(a).offset().top, f && (t -= p.outerHeight()), n.show(), s.eq(a).hide()
                }
                s.slideUp(c.duration), -1 !== a && (s.eq(a).slideDown(c.duration), $(m.browser.getScrollBody()).animate({
                  scrollTop: t
                }))
              }
              o.removeClass("is-active"), -1 !== a && o.eq(a).addClass("is-active")
            } else s.show(), o.removeClass("is-active").hide(), -1 !== a && o.eq(a).show().addClass("is-active")
          }
          i.on("click", function(e) {
            if (e.preventDefault(), !$(this).hasClass("is-disabled")) {
              var t = i.index(this);
              t !== a ? n(t) : d && n(-1)
            }
          }), r.on("click", function(e) {
            if (e.preventDefault(), !$(this).parents(c.itemSelector).hasClass("is-disabled")) {
              var t = r.index(this);
              t !== a ? n(t) : d && n(-1)
            }
          });
          var l = i.filter('[href="' + u + '"]');
          l.length && (a = i.index(l.get(0))), e.on("Tabs.rebuildBlock", function() {
            -1 === a && (a = 0), t(!0)
          }).on("Tabs.showItem", function(e, t) {
            g.logger.debug("Tabs/onShowItem", t), n(t.idx)
          })
        }), h.on("Responsive.on.xl Responsive.on.l Responsive.on.m", function() {
          d = !1, t.trigger("Tabs.rebuildBlock")
        }), h.on("Responsive.on.s Responsive.on.xs", function() {
          d = !0, t.trigger("Tabs.rebuildBlock")
        }), h.on("StickyHeader.show", function() {
          f = !0
        }), h.on("StickyHeader.hide", function() {
          f = !1
        })
      }
    }, {
      "../utils/browser": 44,
      "../utils/logger": 46
    }],
    39: [function(e, t, n) {
      "use strict";
      Object.defineProperty(n, "__esModule", {
        value: !0
      }), n.Targets = void 0;
      var h = e("../utils/logger"),
        i = function() {
          var c = void 0,
            u = {},
            d = {
              targets: []
            };

          function e(e) {
            if (h.logger.debug("Targets/off", e), e in u) {
              var t = u[e];
              t.event && t.selector && (f(t.selector) ? c.off(t.event, t.selector, t.handler) : $(t.selector).off(t.event, t.handler)), delete u[e]
            } else h.logger.warn("Targets/remove", e, "Target is not added")
          }

          function f(e) {
            return -1 === [window, document, document.documentElement, document.body, "html", "body"].indexOf(e)
          }
          return {
            init: function(e) {
              h.logger.debug("Targets/init", e), $.extend(d, e), c = $(document.body);
              var t = !0,
                n = !1,
                i = void 0;
              try {
                for (var o, r = d.targets[Symbol.iterator](); !(t = (o = r.next()).done); t = !0) {
                  var s = o.value;
                  a = s.id, l = s, h.logger.debug("Targets/on", a, l), a in u ? h.logger.warn("Targets/on", a, "Target is already added") : (u[a] = l).event && l.selector && (f(l.selector) ? c.on(l.event, l.selector, l.handler) : $(l.selector).on(l.event, l.handler))
                }
              } catch (e) {
                n = !0, i = e
              } finally {
                try {
                  !t && r.return && r.return()
                } finally {
                  if (n) throw i
                }
              }
              var a, l
            },
            on: e,
            off: e,
            trigger: function(e, t) {
              h.logger.debug("Targets/trigger", e, t), e in u ? u[e].handler.call(window, t) : h.logger.warn("Targets/trigger", e, "Target is not added")
            }
          }
        }();
      n.Targets = i
    }, {
      "../utils/logger": 46
    }],
    40: [function(e, t, n) {
      "use strict";
      Object.defineProperty(n, "__esModule", {
        value: !0
      }), n.TypeAdvantage = function(e) {
        var g = $.extend({
            selector: ".js-type-advantage",
            block: ".js-type-advantage-block",
            ann: ".js-type-advantage-ann",
            content: ".js-type-advantage-content",
            wrapper: ".js-type-advantage-wrapper",
            more: ".js-type-advantage-more",
            link: ".js-type-advantage-more-link"
          }, e),
          m = $(window);

        function t(e) {
          var t = $(e),
            n = t.find(g.block),
            i = n.find(g.ann),
            o = n.find(g.content),
            r = o.find(g.wrapper),
            s = n.find(g.more),
            a = s.find(g.link),
            l = o.data("height"),
            c = void 0,
            u = void 0,
            d = void 0,
            f = void 0,
            h = !1;

          function p() {
            a.each(function() {
              u = $(this).data("default"), d = $(this).data("open"), f = $(this).data("close"), 980 < m.width() ? $(this).text(u) : $(this).text(d)
            }), 980 < m.width() ? (o.hide(), i.show(), s.show(), n.hasClass("is-active") && n.removeClass("is-active"), h = !1, t.css({
              height: ""
            }), n.eq(1).css({
              height: ""
            }), o.css({
              height: ""
            })) : (i.hide(), o.show(), n.hasClass("is-active") && n.removeClass("is-active"), c = r[0].scrollHeight, l < c ? (s.show(), o.css({
              height: l
            })) : s.hide())
          }
          m.resize(p), p(), a.on("click", function(e) {
            e.preventDefault(), 980 < m.width() ? (n.toggleClass("is-active"), n.hasClass("is-active") ? i.fadeOut(300, function() {
              o.fadeIn(300)
            }) : o.fadeOut(300, function() {
              i.fadeIn(300)
            })) : h ? (o.animate({
              height: l
            }, {
              duration: 400,
              complete: function() {
                t.css({
                  height: ""
                }), n.eq(1).css({
                  height: ""
                }), a.text(d)
              }
            }), o.animate({
              height: l
            }), h = !1) : (t.css({
              height: "auto"
            }), n.eq(1).css({
              height: "auto"
            }), o.animate({
              height: c
            }, {
              duration: 400,
              complete: function() {
                a.text(f)
              }
            }), h = !0)
          })
        }
        $(g.selector).each(function() {
          t(this)
        })
      }
    }, {}],
    41: [function(e, t, n) {
      "use strict";
      Object.defineProperty(n, "__esModule", {
        value: !0
      }), n.Types = void 0;
      var i = e("../utils/logger");
      n.Types = function(e) {
        i.logger.debug("Types", e);
        var r = $.extend({
          selector: ".js-type",
          linkSelector: ".js-type-link",
          contentSelector: ".js-type-content",
          closeSelector: ".js-type-close",
          duration: 400
        }, e);
        $(r.selector).each(function() {
          var e = $(r.linkSelector, this),
            t = $(r.contentSelector, this),
            n = $(r.closeSelector, this);

          function i() {
            t.slideUp(r.duration, function() {
              e.each(function() {
                var e = $(this);
                e.data("show") && e.text(e.data("show"))
              })
            })
          }

          function o() {
            t.is(":visible") ? i() : t.slideDown(r.duration, function() {
              e.each(function() {
                var e = $(this);
                e.data("hide") && e.text(e.data("hide"))
              })
            })
          }
          t.hide(), e.on("click", function(e) {
            e.preventDefault(), o()
          }), n.on("click", function(e) {
            e.preventDefault(), i()
          })
        })
      }
    }, {
      "../utils/logger": 46
    }],
    42: [function(e, t, n) {
      "use strict";
      Object.defineProperty(n, "__esModule", {
        value: !0
      }), n.InitVideo = n.Video = void 0;
      var v = e("../utils/logger"),
        y = e("../utils/browser");

      function i(e, t) {
        var n = $(e);
        v.logger.debug("Video/InitVideo", e, t);
        var i = $.extend({
            selector: ".js-video",
            objectSelector: ".js-video-object",
            playSelector: ".js-video-play",
            scaleSelector: ".js-video-scale",
            playScaleSelector: ".js-video-scale-play",
            wrapSelector: ".js-video-wrap",
            playedTime: 1e4
          }, t),
          o = $(window),
          r = n.find(i.objectSelector),
          s = n.find(i.playerSelector),
          a = n.find(i.scaleSelector),
          l = n.find(i.playScaleSelector),
          c = n.find(i.wrapSelector),
          u = r.get(0),
          d = !1,
          f = -1,
          h = !1;

        function p() {
          return !u.paused
        }

        function g() {
          v.logger.debug("Video/InitVideo/play"), d || (a.on("click", function(e) {
            e.stopPropagation();
            var t = y.browser.getMousePosition(e).left,
              n = a.offset().left,
              i = a.width(),
              o = (t - n) / i;
            u.currentTime = Math.round(u.duration * o), p() || g()
          }), r.on("play", function(e) {
            v.logger.debug("Video/InitVideo/onplay", e), n.addClass("is-playing"), h || (f = setTimeout(function() {
              o.trigger("Video.played", {
                video: u
              }), h = !0
            }, i.playedTime))
          }).on("pause", function(e) {
            v.logger.debug("Video/InitVideo/onpause", e), n.removeClass("is-playing"), -1 !== f && (clearTimeout(f), f = -1)
          }).on("timeupdate", function(e) {
            v.logger.debug("Video/InitVideo/ontimeupdate", e);
            var t = this.currentTime / this.duration * 100;
            l.css({
              left: t + "%"
            })
          }), d = !0), u.play()
        }

        function m() {
          v.logger.debug("Video/InitVideo/toggle"), p() ? (v.logger.debug("Video/InitVideo/pause"), u.pause()) : g()
        }
        $(window).resize(function() {
          i.festival && ($(this).width() < 1e3 ? p() && u.pause() : u.play())
        }), $(window).resize(), s.on("click", function(e) {
          e.preventDefault(), e.stopPropagation(), m()
        }), c.on("click", function(e) {
          e.preventDefault(), e.stopPropagation(), m()
        })
      }
      n.Video = function(e) {
        v.logger.debug("Video", e);
        var t = $.extend({
          selector: ".js-video"
        }, e);
        $(t.selector).each(function() {
          i(this, e)
        })
      }, n.InitVideo = i
    }, {
      "../utils/browser": 44,
      "../utils/logger": 46
    }],
    43: [function(e, t, n) {
      "use strict";
      Object.defineProperty(n, "__esModule", {
        value: !0
      }), n.getAjax = void 0;
      var i, o = e("../lib/axios"),
        r = (i = o) && i.__esModule ? i : {
          default: i
        },
        s = e("./params");
      n.getAjax = function(e, t, n) {
        s.ajax.SESSION_ID && (t.SESS_ID = s.ajax.SESSION_ID);
        var i = s.ajax.URI,
          o = s.ajax.METHOD;
        return n && (n.url && (i = n.url, delete n.url), n.method && (o = n.method, delete n.method)), i = i + e + "/", "post" === o ? r.default.post(i, function(e) {
          var t = new URLSearchParams;
          if (e instanceof Array) {
            var n = !0,
              i = !1,
              o = void 0;
            try {
              for (var r, s = e[Symbol.iterator](); !(n = (r = s.next()).done); n = !0) {
                var a = r.value;
                t.append(a.name, a.value)
              }
            } catch (e) {
              i = !0, o = e
            } finally {
              try {
                !n && s.return && s.return()
              } finally {
                if (i) throw o
              }
            }
          } else
            for (var l in e) t.append(l, e[l]);
          return t
        }(t), n) : r.default.get(i, {
          params: t
        }, n)
      }
    }, {
      "../lib/axios": 2,
      "./params": 47
    }],
    44: [function(e, t, n) {
      "use strict";
      Object.defineProperty(n, "__esModule", {
        value: !0
      });
      var i = function() {
        var e = navigator.userAgent.toLowerCase(),
          t = /msie/.test(e) && !/opera/.test(e),
          n = /opera/.test(e),
          i = /webkit/.test(e);

        function o() {
          return i ? document.body : document.documentElement
        }
        return {
          isOpera: n,
          isMSIE: t,
          isWebKit: i,
          supportsAttribute: function(e, t) {
            return "string" == typeof(t = t || "div") && (t = document.createElement(t)), e in t
          },
          supportsStyle: function(e, t) {
            return "string" == typeof(t = t || "div") && (t = document.createElement(t)), void 0 !== t.style[e]
          },
          getScrollBody: o,
          getMousePosition: function(e) {
            e = e || window.event;
            var t = o();
            return {
              left: e.pageX || e.clientX + t.scrollLeft - (document.documentElement.clientLeft || 0),
              top: e.pageY || e.clientY + t.scrollTop - (document.documentElement.clientTop || 0)
            }
          }
        }
      }();
      n.browser = i
    }, {}],
    45: [function(e, t, n) {
      "use strict";
      Object.defineProperty(n, "__esModule", {
        value: !0
      });
      var l, i = (l = /\+(\d+)(i|h|d|m|y)/, {
        set: function(e, t, n, i, o, r) {
          if (n && (n = n.toString().match(l)), n) {
            var s = new Date,
              a = parseInt(n[1]);
            switch (n[2]) {
              case "y":
                s.setFullYear(s.getFullYear() + a);
                break;
              case "m":
                s.setMonth(s.getMonth() + a);
                break;
              case "d":
                s.setDate(s.getDate() + a);
                break;
              case "h":
                s.setHours(s.getHours() + a);
                break;
              case "i":
                s.setMinutes(s.getMinutes() + a);
              case "s":
                s.setSeconds(s.getSeconds() + a)
            }
            n = s.toUTCString()
          }
          document.cookie = e + "=" + escape(t) + (n ? "; expires=" + n : "") + (i ? "; path=" + i : "") + (o ? "; domain=" + o : "") + (r ? "; secure" : "")
        },
        get: function(e) {
          var t = " " + document.cookie,
            n = " " + e + "=",
            i = null,
            o = 0,
            r = 0;
          return 0 < t.length && -1 != (o = t.indexOf(n)) && (o += n.length, -1 == (r = t.indexOf(";", o)) && (r = t.length), i = unescape(t.substring(o, r))), i
        }
      });
      n.cookies = i
    }, {}],
    46: [function(e, t, n) {
      "use strict";
      Object.defineProperty(n, "__esModule", {
        value: !0
      }), n.logger = void 0;
      var s = e("../utils/params"),
        i = function() {
          for (var e = $.extend({}, {
              level: "info"
            }, s.logging), t = ["none", "error", "warn", "info", "debug"], n = {}, i = 0; i < t.length; i++) {
            var o = t[i];
            if (n[o] = !0, o === e.level) break
          }

          function r() {}
          return /(?:\b(MS)?IE\s+|\bTrident\/7\.0;.*\s+rv:|\bEdge\/)(\d+)/.test(navigator.userAgent) ? {
            debug: function() {
              if (n.debug) try {
                console.log.apply(window, arguments)
              } catch (e) {}
            },
            info: function() {
              if (n.info) try {
                console.info.apply(window, arguments)
              } catch (e) {}
            },
            warn: function() {
              if (n.warn) try {
                console.warn.apply(window, arguments)
              } catch (e) {}
            },
            error: function() {
              if (n.error) try {
                console.error.apply(window, arguments)
              } catch (e) {}
            }
          } : {
            debug: n.debug ? console.log : r,
            info: n.info ? console.info : r,
            warn: n.warn ? console.warn : r,
            error: n.error ? console.error : r
          }
        }();
      n.logger = i
    }, {
      "../utils/params": 47
    }],
    47: [function(e, t, n) {
      "use strict";
      Object.defineProperty(n, "__esModule", {
        value: !0
      });
      var i, o = {
          DATA_TYPE: "json",
          URI: "/json/",
          METHOD: "post",
          TIMEOUT: 5e3,
          SESSION_ID: (i = "", window.location.toString().match(/SESS_ID=(\d+)/) && (i = RegExp.$1), i)
        },
        r = $(document.body),
        s = r.data("interfaceName"),
        a = r.data("interfaceMark"),
        l = !!parseInt(r.data("isTablet")),
        c = !!parseInt(r.data("isMobile")),
        u = !c && !l,
        d = {
          xl: 1 / 0,
          l: 1280,
          m: 980,
          s: 760,
          xs: 480
        };
      n.interfaceName = s, n.interfaceMark = a, n.isTablet = l, n.isMobile = c, n.isDesktop = u, n.breakpoints = d, n.ajax = o, n.form = {
        dateOptions: {
          lang: "ru",
          format: "Y-m-d",
          formatDate: "d.m.Y",
          formatTime: "H:i",
          timepicker: !1,
          dayOfWeekStart: 1,
          closeOnDateSelect: !0
        },
        timeOptions: {
          lang: "ru",
          format: "H:i",
          formatDate: "d.m.Y",
          formatTime: "H:i",
          datepicker: !1,
          dayOfWeekStart: 1,
          closeOnDateSelect: !0
        },
        dateTimeOptions: {
          lang: "ru",
          format: "Y-m-d H:i",
          formatDate: "d.m.Y",
          formatTime: "H:i",
          dayOfWeekStart: 1,
          closeOnDateSelect: !0
        },
        fileuploadOptions: {}
      }, n.fancybox = {
        padding: 10,
        helpers: {
          title: {
            type: "inside"
          }
        }
      }, n.logging = {
        level: "warning"
      }
    }, {}],
    48: [function(e, t, n) {
      var i, o, r = t.exports = {};

      function s() {
        throw new Error("setTimeout has not been defined")
      }

      function a() {
        throw new Error("clearTimeout has not been defined")
      }

      function l(t) {
        if (i === setTimeout) return setTimeout(t, 0);
        if ((i === s || !i) && setTimeout) return i = setTimeout, setTimeout(t, 0);
        try {
          return i(t, 0)
        } catch (e) {
          try {
            return i.call(null, t, 0)
          } catch (e) {
            return i.call(this, t, 0)
          }
        }
      }! function() {
        try {
          i = "function" == typeof setTimeout ? setTimeout : s
        } catch (e) {
          i = s
        }
        try {
          o = "function" == typeof clearTimeout ? clearTimeout : a
        } catch (e) {
          o = a
        }
      }();
      var c, u = [],
        d = !1,
        f = -1;

      function h() {
        d && c && (d = !1, c.length ? u = c.concat(u) : f = -1, u.length && p())
      }

      function p() {
        if (!d) {
          var e = l(h);
          d = !0;
          for (var t = u.length; t;) {
            for (c = u, u = []; ++f < t;) c && c[f].run();
            f = -1, t = u.length
          }
          c = null, d = !1,
            function(t) {
              if (o === clearTimeout) return clearTimeout(t);
              if ((o === a || !o) && clearTimeout) return o = clearTimeout, clearTimeout(t);
              try {
                o(t)
              } catch (e) {
                try {
                  return o.call(null, t)
                } catch (e) {
                  return o.call(this, t)
                }
              }
            }(e)
        }
      }

      function g(e, t) {
        this.fun = e, this.array = t
      }

      function m() {}
      r.nextTick = function(e) {
        var t = new Array(arguments.length - 1);
        if (1 < arguments.length)
          for (var n = 1; n < arguments.length; n++) t[n - 1] = arguments[n];
        u.push(new g(e, t)), 1 !== u.length || d || l(p)
      }, g.prototype.run = function() {
        this.fun.apply(null, this.array)
      }, r.title = "browser", r.browser = !0, r.env = {}, r.argv = [], r.version = "", r.versions = {}, r.on = m, r.addListener = m, r.once = m, r.off = m, r.removeListener = m, r.removeAllListeners = m, r.emit = m, r.binding = function(e) {
        throw new Error("process.binding is not supported")
      }, r.cwd = function() {
        return "/"
      }, r.chdir = function(e) {
        throw new Error("process.chdir is not supported")
      }, r.umask = function() {
        return 0
      }
    }, {}]
  }, {}, [1]), $(function() {
    "use strict";
    objectFitImages();
    var e = new Proj;
    e.StickyHeader(), "home_page" !== e.interfaceName && e.StickyAside(), e.MainMenu(), "festival_page" !== e.interfaceName && e.Helper(), 1 != ["contact_page", "festival_page"].indexOf(e.interfaceName) && e.ClinicBlock(), "home_page" === e.interfaceName && e.HeaderSlider(), e.Gallery(), e.Slider(), e.Scroller(), e.Video(), e.Tabs(), e.Services(), e.Form(), e.Content(), e.Overflow(), e.Callback(), e.Scroll(), "festival_page" === e.interfaceName && (e.Video({
      autoplay: !0,
      festival: !0
    }), e.Carousel(), e.Map(), e.FestivalPage()), "home_page" === e.interfaceName && e.PopupVideo(), -1 != ["ambulance", "support", "test_group", "service_group"].indexOf(e.interfaceName) && e.Types(), -1 != ["about_page", "review_cat", "medicine", "ambulance", "cosmetology", "insurance", "support", "specialist", "test_cat", "service_group", "service_type", "landing"].indexOf(e.interfaceName) && e.Reviews(), -1 != ["faq_cat", "cosmetology", "service_group", "service_type"].indexOf(e.interfaceName) && e.FAQs(), "support" === e.interfaceName ? e.Support() : "specialist_cat" === e.interfaceName ? e.SpecialistCat() : "review_cat" === e.interfaceName ? e.ReviewCat() : "program_cat" === e.interfaceName ? e.ProgramCat() : "contact_page" === e.interfaceName ? e.ContactPage() : "price_page" === e.interfaceName ? e.PricePage() : "service_group" === e.interfaceName ? (e.ServiceGroup(), e.GroupAdvantage()) : "service_type" === e.interfaceName && (e.ServiceGroup(), e.TypeAdvantage()), e.Anchor(), e.Responsive(), e.Targets.init({
      targets: [{
        id: "goal-link",
        event: "click",
        selector: ".js-goal-link",
        handler: function() {
          var e = $(this).data("goal");
          if (e) {
            try {
              yaCounter39984760.reachGoal(e)
            } catch (e) {
              console.error(e)
            }
            try {
              ga("send", "event", e)
            } catch (e) {
              console.error(e)
            }
          }
        }
      }, {
        id: "goal-form",
        event: "submit",
        selector: ".js-goal-form",
        handler: function() {
          var e = $(this).data("goal");
          if (e) {
            try {
              yaCounter39984760.reachGoal(e)
            } catch (e) {
              console.error(e)
            }
            try {
              ga("send", "event", e)
            } catch (e) {
              console.error(e)
            }
          }
        }
      }, {
        id: "send-form",
        handler: function(e) {
          if (e) {
            try {
              yaCounter39984760.reachGoal(e)
            } catch (e) {
              console.error(e)
            }
            try {
              ga("send", "event", e)
            } catch (e) {
              console.error(e)
            }
          }
        }
      }, {
        id: "video-play",
        event: "Video.played",
        selector: window,
        handler: function() {
          try {
            yaCounter39984760.reachGoal("video")
          } catch (e) {
            console.error(e)
          }
          try {
            ga("send", "event", "video")
          } catch (e) {
            console.error(e)
          }
        }
      }]
    })
  }), $(document).ready(function() {
    $('.prices__element .prices__link').each(function() {
      if ($(this).attr('href') === document.location.href) {
        $(this).parent().addClass('active')
      }
    });
    window.ist = {}, window.ist.animator = {
      showBlock: function(e, t, n, i) {
        void 0 === t && (t = "btt"), void 0 === n && (n = 500), void 0 === i && (i = 0), setTimeout(function() {
          switch (t) {
            case "rtl":
              e.animate({
                right: 0,
                opacity: 1
              }, n, "easeInOutCubic", function() {
                $(this).addClass("visible")
              });
              break;
            case "ltr":
              e.animate({
                left: 0,
                opacity: 1
              }, n, "easeInOutCubic", function() {
                $(this).addClass("visible")
              });
              break;
            case "ttb":
              e.animate({
                top: 0,
                opacity: 1
              }, n, "easeInOutCubic", function() {
                $(this).addClass("visible")
              });
              break;
            case "btt":
              e.animate({
                bottom: 0,
                opacity: 1
              }, n, "easeInOutCubic", function() {
                $(this).find(".veg").length && !$(this).hasClass("visible") && $(this).find(".veg-feat__text span").each(function(e) {
                  var t = $(this),
                    n = $(this).data("digits");
                  setTimeout(function() {
                    t.animateNumber({
                      number: n
                    }, 5e3, function() {
                      t.addClass("complete")
                    })
                  }, 500 * (e + 1))
                }), $(this).addClass("visible")
              });
              break;
            case "fi":
              e.animate({
                opacity: 1
              }, n, "easeInOutCubic", function() {
                $(this).addClass("visible")
              })
          }
        }, i)
      },
      startCheckVis: function() {
        var e = $(window).scrollTop() + $(window).height();
        $(".animator").each(function() {
          $(this).offset().top >= e && $(this).removeClass("visible")
        })
      },
      checkVisibility: function(e) {
        var t = $(window).scrollTop() + $(window).height();
        return e.offset().top < t
      },
      scrollVis: function() {
        var e = this;
        $(".animator").each(function() {
          !$(this).hasClass("visible") && e.checkVisibility($(this)) && e.showBlock($(this), $(this).data("direction"), $(this).data("duration"), $(this).data("pause"))
        })
      },
      init: function() {
        var e = this;
        return e.startCheckVis(), $(window).scroll(function() {
          e.scrollVis()
        }).trigger("scroll"), this
      }
    }.init(), $('.js-open-text').click(function() {
      var destination = $(this).parent().offset().top;
      if ($(this).hasClass('open')) {
        $(this).removeClass('open');
        $(this).text('Читать далее');
        $(this).prev().find('.hide-text').slideToggle("slow");
        $("body").animate({
          scrollTop: destination
        }, 500)
        $(this).prev('.doctor__desc').addClass("doctor__desc--hide")
      } else {
        $(this).addClass('open');
        $(this).text('Скрыть');
        $(this).prev().find('.hide-text').slideToggle("slow");
        $(this).prev('.doctor__desc').removeClass("doctor__desc--hide")
      }
    }), $('.js-more-btn').click(function() {
      var btn_text = $(this).attr('data-text');
      var destination = $(this).parent().offset().top;
      if ($(this).hasClass('open')) {
        $(this).removeClass('open');
        $(this).text(btn_text);
        $(this).prev('.hidden-elements').slideToggle('slow');
        $("body").animate({
          scrollTop: destination
        }, 500)
        $(this).prev('.hidden-elements').addClass("hidden")
      } else {
        $(this).addClass('open');
        $(this).text('Скрыть');
        $(this).prev('.hidden-elements').slideToggle("slow");
        $(this).prev('.hidden-elements').removeClass("hidden");
        if (this_is_mobile == !0) {
          $('.hidden-elements .reviews__text__inner').each(function() {
            $(this).readmore({
              moreLink: '<a href="#" class="js-open-hide-btn">Читать полностью</a>',
              lessLink: '<a href="#" class="js-open-hide-btn">Скрыть</a>',
              collapsedHeight: 95,
              heightMargin: 0
            })
          })
        }
      }
    }), $('.js-tab-link, .js-tab-sublink').click(function() {
      var _th = $(this);
      var tab_id = _th.attr('data-tab');
      if (!_th.hasClass('js-tab-sublink')) {
        $('.js-sublist-link').removeClass('current');
        $('.prices__sublist').slideUp('slow')
      }
      $('.js-tab-link, .js-tab-sublink').removeClass('current');
      $('.prices__elements').removeClass('current');
      _th.addClass('current');
      $("#" + tab_id).addClass('current')
    }), $('.js-sublist-link').click(function() {
      if ($(this).hasClass('current')) {
        $(this).removeClass('current');
        $(this).parent().find('.prices__sublist').slideToggle('slow')
      } else {
        $('.js-tab-link').removeClass('current');
        $(this).addClass('current');
        $(this).parent().find('.prices__sublist').slideToggle('slow')
      }
    }), $(".js-reviews-star").each(function() {
      var e = $(this).data("rating");
      e *= 20, $(this).find("span").css("width", e + "%")
    }), 1139 < $(window).width() && $(".faq__sidebar").stickySidebar({
      resizeSensor: !0,
      topSpacing: 60,
      bottomSpacing: 60
    }), $("[data-fancybox]").fancybox({
      lang: "ru",
      touch: !1,
      i18n: {
        ru: {
          CLOSE: "Закрыть",
          NEXT: "Следующий",
          PREV: "Предыдущий",
          ERROR: "Запрошенный контент не может быть загружен. <br> Повторите попытку позже.",
          PLAY_START: "Начать слайд-шоу",
          PLAY_STOP: "Остановить слайд-шоу",
          FULL_SCREEN: "Полный экран",
          THUMBS: "Эскизы",
          DOWNLOAD: "Скачать",
          SHARE: "Поделиться",
          ZOOM: "Увеличить"
        }
      },
      buttons: ["close"]
    });
    var e = $(".js-pluses-slider").find(".owl-carousel");
    e.owlCarousel({
      items: 1,
      slideBy: 1,
      dots: !1,
      nav: !0,
      loop: !1,
      margin: 0,
      autoplay: !1,
      autoplayTimeout: 5e3,
      autoplayHoverPause: !0,
      onInitialized: function(e) {
        $(".owl-count").text("1 / " + this.items().length)
      }
    }), e.on("changed.owl.carousel", function(e) {
      if ($(".owl-count").text(Math.round(++e.item.index) + " / " + e.item.count), e.item) {
        var t = e.item.index,
          n = e.item.count;
        n < t && (t -= n), t <= 0 && (t += n), $(".js-carousel-text.current").removeClass("current").fadeOut(0), $(".js-carousel-text").each(function() {
          t == $(this).data("item") && $(this).fadeIn(200).addClass("current")
        })
      }
      setTimeout(() => {
        if (!$('.pluses__left.current .pluses__paragraph').attr('data-readmore')) {
          $('.pluses__left.current .pluses__paragraph').readmore({
            moreLink: '<a href="#" class="js-open-hide-btn">Показать еще</a>',
            lessLink: '<a href="#" class="js-open-hide-btn">Скрыть</a>',
            maxHeight: 200,
            heightMargin: 16
          })
        }
      }, 300)
    }), $(".js-articles-slider").find(".owl-carousel").owlCarousel({
      items: 4,
      dots: !1,
      nav: !0,
      loop: !1,
      margin: 0,
      onInitialized: function(e) {
        $(".articles__paragraph").length && $(".articles__paragraph").dotdotdot({
          height: 130
        })
        $(".articles__slide-header a").length && $(".articles__slide-header a").dotdotdot({
          height: 80
        })
      },
      responsiveClass: !0,
      responsive: {
        0: {
          items: 1
        },
        600: {
          items: 2
        },
        1300: {
          items: 3,
          center: !0
        },
        1301: {
          items: 4,
          center: !1
        }
      }
    }), $("select:not(.specialist-cat__filter-select)").styler({
      selectSmartPositioning: !1
    }), /Mobi/.test(navigator.userAgent) && $("table:not([class])").wrap('<div class="table-responsive"></div>'), $("body").on("click", ".anchors__item a, .totop", function(e) {
      e.preventDefault();
      var t = $(this).attr("href"),
        n = $(t).offset().top;
      $("html, body").animate({
        scrollTop: n - $(".sticky-header__wrap").height() - 40
      }, 1500)
    }), $(window).on("scroll", function() {
      $(this).scrollTop() > $(window).innerHeight() ? $(".totop").css("opacity", "1") : $(this).scrollTop() < $(window).innerHeight() && $(".totop").css("opacity", "0")
    }).trigger("scroll"), $('.doctor__desc').readmore({
      moreLink: '<a href="#" class="js-open-hide-btn">Показать еще</a>',
      lessLink: '<a href="#" class="js-open-hide-btn">Скрыть</a>',
      collapsedHeight: 108,
      heightMargin: 16
    }), $(".jq-selectbox__dropdown ul").mCustomScrollbar({
      theme: "dark"
    }), $(window).on("scroll", function() {
      if (/Mobi/.test(navigator.userAgent)) {
        $(this).scrollTop() > $('body').height() - $(window).innerHeight() - 200 ? $(".free-admission").css("opacity", "0") : $(this).scrollTop() < $('body').height() - $(window).innerHeight() - 200 && $(".free-admission").css("opacity", "1")
      }
    }).trigger("scroll"), (function() {
      var a, b, c, d, e, f, g, h, i, j, k, l, m, n, o, p, q, r, s, t, u, v, w, x, y, z, A, B, C, D, E, F, G, H, I, J, K, L, M, N, O, P, Q, R, S, T, U, V, W, X = [].slice,
        Y = {}.hasOwnProperty,
        Z = function(a, b) {
          function c() {
            this.constructor = a
          }
          for (var d in b) Y.call(b, d) && (a[d] = b[d]);
          return c.prototype = b.prototype, a.prototype = new c, a.__super__ = b.prototype, a
        },
        $ = [].indexOf || function(a) {
          for (var b = 0, c = this.length; c > b; b++)
            if (b in this && this[b] === a) return b;
          return -1
        };
      for (u = {
          catchupTime: 100,
          initialRate: .03,
          minTime: 250,
          ghostTime: 100,
          maxProgressPerFrame: 20,
          easeFactor: 1.25,
          startOnPageLoad: !0,
          restartOnPushState: !0,
          restartOnRequestAfter: 500,
          target: "body",
          elements: {
            checkInterval: 100,
            selectors: ["body"]
          },
          eventLag: {
            minSamples: 10,
            sampleCount: 3,
            lagThreshold: 3
          },
          ajax: {
            trackMethods: ["GET"],
            trackWebSockets: !0,
            ignoreURLs: []
          }
        }, C = function() {
          var a;
          return null != (a = "undefined" != typeof performance && null !== performance && "function" == typeof performance.now ? performance.now() : void 0) ? a : +new Date
        }, E = window.requestAnimationFrame || window.mozRequestAnimationFrame || window.webkitRequestAnimationFrame || window.msRequestAnimationFrame, t = window.cancelAnimationFrame || window.mozCancelAnimationFrame, null == E && (E = function(a) {
          return setTimeout(a, 50)
        }, t = function(a) {
          return clearTimeout(a)
        }), G = function(a) {
          var b, c;
          return b = C(), (c = function() {
            var d;
            return d = C() - b, d >= 33 ? (b = C(), a(d, function() {
              return E(c)
            })) : setTimeout(c, 33 - d)
          })()
        }, F = function() {
          var a, b, c;
          return c = arguments[0], b = arguments[1], a = 3 <= arguments.length ? X.call(arguments, 2) : [], "function" == typeof c[b] ? c[b].apply(c, a) : c[b]
        }, v = function() {
          var a, b, c, d, e, f, g;
          for (b = arguments[0], d = 2 <= arguments.length ? X.call(arguments, 1) : [], f = 0, g = d.length; g > f; f++)
            if (c = d[f])
              for (a in c) Y.call(c, a) && (e = c[a], null != b[a] && "object" == typeof b[a] && null != e && "object" == typeof e ? v(b[a], e) : b[a] = e);
          return b
        }, q = function(a) {
          var b, c, d, e, f;
          for (c = b = 0, e = 0, f = a.length; f > e; e++) d = a[e], c += Math.abs(d), b++;
          return c / b
        }, x = function(a, b) {
          var c, d, e;
          if (null == a && (a = "options"), null == b && (b = !0), e = document.querySelector("[data-pace-" + a + "]")) {
            if (c = e.getAttribute("data-pace-" + a), !b) return c;
            try {
              return JSON.parse(c)
            } catch (f) {
              return d = f, "undefined" != typeof console && null !== console ? console.error("Error parsing inline pace options", d) : void 0
            }
          }
        }, g = function() {
          function a() {}
          return a.prototype.on = function(a, b, c, d) {
            var e;
            return null == d && (d = !1), null == this.bindings && (this.bindings = {}), null == (e = this.bindings)[a] && (e[a] = []), this.bindings[a].push({
              handler: b,
              ctx: c,
              once: d
            })
          }, a.prototype.once = function(a, b, c) {
            return this.on(a, b, c, !0)
          }, a.prototype.off = function(a, b) {
            var c, d, e;
            if (null != (null != (d = this.bindings) ? d[a] : void 0)) {
              if (null == b) return delete this.bindings[a];
              for (c = 0, e = []; c < this.bindings[a].length;) e.push(this.bindings[a][c].handler === b ? this.bindings[a].splice(c, 1) : c++);
              return e
            }
          }, a.prototype.trigger = function() {
            var a, b, c, d, e, f, g, h, i;
            if (c = arguments[0], a = 2 <= arguments.length ? X.call(arguments, 1) : [], null != (g = this.bindings) ? g[c] : void 0) {
              for (e = 0, i = []; e < this.bindings[c].length;) h = this.bindings[c][e], d = h.handler, b = h.ctx, f = h.once, d.apply(null != b ? b : this, a), i.push(f ? this.bindings[c].splice(e, 1) : e++);
              return i
            }
          }, a
        }(), j = window.Pace || {}, window.Pace = j, v(j, g.prototype), D = j.options = v({}, u, window.paceOptions, x()), U = ["ajax", "document", "eventLag", "elements"], Q = 0, S = U.length; S > Q; Q++) K = U[Q], D[K] === !0 && (D[K] = u[K]);
      i = function(a) {
        function b() {
          return V = b.__super__.constructor.apply(this, arguments)
        }
        return Z(b, a), b
      }(Error), b = function() {
        function a() {
          this.progress = 0
        }
        return a.prototype.getElement = function() {
          var a;
          if (null == this.el) {
            if (a = document.querySelector(D.target), !a) throw new i;
            this.el = document.createElement("div"), this.el.className = "pace pace-active", document.body.className = document.body.className.replace(/pace-done/g, ""), document.body.className += " pace-running", this.el.innerHTML = '<div class="pace-progress">\n  <div class="pace-progress-inner"></div>\n</div>\n<div class="pace-activity"></div>', null != a.firstChild ? a.insertBefore(this.el, a.firstChild) : a.appendChild(this.el)
          }
          return this.el
        }, a.prototype.finish = function() {
          var a;
          return a = this.getElement(), a.className = a.className.replace("pace-active", ""), a.className += " pace-inactive", document.body.className = document.body.className.replace("pace-running", ""), document.body.className += " pace-done"
        }, a.prototype.update = function(a) {
          return this.progress = a, this.render()
        }, a.prototype.destroy = function() {
          try {
            this.getElement().parentNode.removeChild(this.getElement())
          } catch (a) {
            i = a
          }
          return this.el = void 0
        }, a.prototype.render = function() {
          var a, b, c, d, e, f, g;
          if (null == document.querySelector(D.target)) return !1;
          for (a = this.getElement(), d = "translate3d(" + this.progress + "%, 0, 0)", g = ["webkitTransform", "msTransform", "transform"], e = 0, f = g.length; f > e; e++) b = g[e], a.children[0].style[b] = d;
          return (!this.lastRenderedProgress || this.lastRenderedProgress | 0 !== this.progress | 0) && (a.children[0].setAttribute("data-progress-text", "" + (0 | this.progress) + "%"), this.progress >= 100 ? c = "99" : (c = this.progress < 10 ? "0" : "", c += 0 | this.progress), a.children[0].setAttribute("data-progress", "" + c)), this.lastRenderedProgress = this.progress
        }, a.prototype.done = function() {
          return this.progress >= 100
        }, a
      }(), h = function() {
        function a() {
          this.bindings = {}
        }
        return a.prototype.trigger = function(a, b) {
          var c, d, e, f, g;
          if (null != this.bindings[a]) {
            for (f = this.bindings[a], g = [], d = 0, e = f.length; e > d; d++) c = f[d], g.push(c.call(this, b));
            return g
          }
        }, a.prototype.on = function(a, b) {
          var c;
          return null == (c = this.bindings)[a] && (c[a] = []), this.bindings[a].push(b)
        }, a
      }(), P = window.XMLHttpRequest, O = window.XDomainRequest, N = window.WebSocket, w = function(a, b) {
        var c, d, e, f;
        f = [];
        for (d in b.prototype) try {
          e = b.prototype[d], f.push(null == a[d] && "function" != typeof e ? a[d] = e : void 0)
        } catch (g) {
          c = g
        }
        return f
      }, A = [], j.ignore = function() {
        var a, b, c;
        return b = arguments[0], a = 2 <= arguments.length ? X.call(arguments, 1) : [], A.unshift("ignore"), c = b.apply(null, a), A.shift(), c
      }, j.track = function() {
        var a, b, c;
        return b = arguments[0], a = 2 <= arguments.length ? X.call(arguments, 1) : [], A.unshift("track"), c = b.apply(null, a), A.shift(), c
      }, J = function(a) {
        var b;
        if (null == a && (a = "GET"), "track" === A[0]) return "force";
        if (!A.length && D.ajax) {
          if ("socket" === a && D.ajax.trackWebSockets) return !0;
          if (b = a.toUpperCase(), $.call(D.ajax.trackMethods, b) >= 0) return !0
        }
        return !1
      }, k = function(a) {
        function b() {
          var a, c = this;
          b.__super__.constructor.apply(this, arguments), a = function(a) {
            var b;
            return b = a.open, a.open = function(d, e) {
              return J(d) && c.trigger("request", {
                type: d,
                url: e,
                request: a
              }), b.apply(a, arguments)
            }
          }, window.XMLHttpRequest = function(b) {
            var c;
            return c = new P(b), a(c), c
          };
          try {
            w(window.XMLHttpRequest, P)
          } catch (d) {}
          if (null != O) {
            window.XDomainRequest = function() {
              var b;
              return b = new O, a(b), b
            };
            try {
              w(window.XDomainRequest, O)
            } catch (d) {}
          }
          if (null != N && D.ajax.trackWebSockets) {
            window.WebSocket = function(a, b) {
              var d;
              return d = null != b ? new N(a, b) : new N(a), J("socket") && c.trigger("request", {
                type: "socket",
                url: a,
                protocols: b,
                request: d
              }), d
            };
            try {
              w(window.WebSocket, N)
            } catch (d) {}
          }
        }
        return Z(b, a), b
      }(h), R = null, y = function() {
        return null == R && (R = new k), R
      }, I = function(a) {
        var b, c, d, e;
        for (e = D.ajax.ignoreURLs, c = 0, d = e.length; d > c; c++)
          if (b = e[c], "string" == typeof b) {
            if (-1 !== a.indexOf(b)) return !0
          } else if (b.test(a)) return !0;
        return !1
      }, y().on("request", function(b) {
        var c, d, e, f, g;
        return f = b.type, e = b.request, g = b.url, I(g) ? void 0 : j.running || D.restartOnRequestAfter === !1 && "force" !== J(f) ? void 0 : (d = arguments, c = D.restartOnRequestAfter || 0, "boolean" == typeof c && (c = 0), setTimeout(function() {
          var b, c, g, h, i, k;
          if (b = "socket" === f ? e.readyState < 2 : 0 < (h = e.readyState) && 4 > h) {
            for (j.restart(), i = j.sources, k = [], c = 0, g = i.length; g > c; c++) {
              if (K = i[c], K instanceof a) {
                K.watch.apply(K, d);
                break
              }
              k.push(void 0)
            }
            return k
          }
        }, c))
      }), a = function() {
        function a() {
          var a = this;
          this.elements = [], y().on("request", function() {
            return a.watch.apply(a, arguments)
          })
        }
        return a.prototype.watch = function(a) {
          var b, c, d, e;
          return d = a.type, b = a.request, e = a.url, I(e) ? void 0 : (c = "socket" === d ? new n(b) : new o(b), this.elements.push(c))
        }, a
      }(), o = function() {
        function a(a) {
          var b, c, d, e, f, g, h = this;
          if (this.progress = 0, null != window.ProgressEvent)
            for (c = null, a.addEventListener("progress", function(a) {
                return h.progress = a.lengthComputable ? 100 * a.loaded / a.total : h.progress + (100 - h.progress) / 2
              }, !1), g = ["load", "abort", "timeout", "error"], d = 0, e = g.length; e > d; d++) b = g[d], a.addEventListener(b, function() {
              return h.progress = 100
            }, !1);
          else f = a.onreadystatechange, a.onreadystatechange = function() {
            var b;
            return 0 === (b = a.readyState) || 4 === b ? h.progress = 100 : 3 === a.readyState && (h.progress = 50), "function" == typeof f ? f.apply(null, arguments) : void 0
          }
        }
        return a
      }(), n = function() {
        function a(a) {
          var b, c, d, e, f = this;
          for (this.progress = 0, e = ["error", "open"], c = 0, d = e.length; d > c; c++) b = e[c], a.addEventListener(b, function() {
            return f.progress = 100
          }, !1)
        }
        return a
      }(), d = function() {
        function a(a) {
          var b, c, d, f;
          for (null == a && (a = {}), this.elements = [], null == a.selectors && (a.selectors = []), f = a.selectors, c = 0, d = f.length; d > c; c++) b = f[c], this.elements.push(new e(b))
        }
        return a
      }(), e = function() {
        function a(a) {
          this.selector = a, this.progress = 0, this.check()
        }
        return a.prototype.check = function() {
          var a = this;
          return document.querySelector(this.selector) ? this.done() : setTimeout(function() {
            return a.check()
          }, D.elements.checkInterval)
        }, a.prototype.done = function() {
          return this.progress = 100
        }, a
      }(), c = function() {
        function a() {
          var a, b, c = this;
          this.progress = null != (b = this.states[document.readyState]) ? b : 100, a = document.onreadystatechange, document.onreadystatechange = function() {
            return null != c.states[document.readyState] && (c.progress = c.states[document.readyState]), "function" == typeof a ? a.apply(null, arguments) : void 0
          }
        }
        return a.prototype.states = {
          loading: 0,
          interactive: 50,
          complete: 100
        }, a
      }(), f = function() {
        function a() {
          var a, b, c, d, e, f = this;
          this.progress = 0, a = 0, e = [], d = 0, c = C(), b = setInterval(function() {
            var g;
            return g = C() - c - 50, c = C(), e.push(g), e.length > D.eventLag.sampleCount && e.shift(), a = q(e), ++d >= D.eventLag.minSamples && a < D.eventLag.lagThreshold ? (f.progress = 100, clearInterval(b)) : f.progress = 100 * (3 / (a + 3))
          }, 50)
        }
        return a
      }(), m = function() {
        function a(a) {
          this.source = a, this.last = this.sinceLastUpdate = 0, this.rate = D.initialRate, this.catchup = 0, this.progress = this.lastProgress = 0, null != this.source && (this.progress = F(this.source, "progress"))
        }
        return a.prototype.tick = function(a, b) {
          var c;
          return null == b && (b = F(this.source, "progress")), b >= 100 && (this.done = !0), b === this.last ? this.sinceLastUpdate += a : (this.sinceLastUpdate && (this.rate = (b - this.last) / this.sinceLastUpdate), this.catchup = (b - this.progress) / D.catchupTime, this.sinceLastUpdate = 0, this.last = b), b > this.progress && (this.progress += this.catchup * a), c = 1 - Math.pow(this.progress / 100, D.easeFactor), this.progress += c * this.rate * a, this.progress = Math.min(this.lastProgress + D.maxProgressPerFrame, this.progress), this.progress = Math.max(0, this.progress), this.progress = Math.min(100, this.progress), this.lastProgress = this.progress, this.progress
        }, a
      }(), L = null, H = null, r = null, M = null, p = null, s = null, j.running = !1, z = function() {
        return D.restartOnPushState ? j.restart() : void 0
      }, null != window.history.pushState && (T = window.history.pushState, window.history.pushState = function() {
        return z(), T.apply(window.history, arguments)
      }), null != window.history.replaceState && (W = window.history.replaceState, window.history.replaceState = function() {
        return z(), W.apply(window.history, arguments)
      }), l = {
        ajax: a,
        elements: d,
        document: c,
        eventLag: f
      }, (B = function() {
        var a, c, d, e, f, g, h, i;
        for (j.sources = L = [], g = ["ajax", "elements", "document", "eventLag"], c = 0, e = g.length; e > c; c++) a = g[c], D[a] !== !1 && L.push(new l[a](D[a]));
        for (i = null != (h = D.extraSources) ? h : [], d = 0, f = i.length; f > d; d++) K = i[d], L.push(new K(D));
        return j.bar = r = new b, H = [], M = new m
      })(), j.stop = function() {
        return j.trigger("stop"), j.running = !1, r.destroy(), s = !0, null != p && ("function" == typeof t && t(p), p = null), B()
      }, j.restart = function() {
        return j.trigger("restart"), j.stop(), j.start()
      }, j.go = function() {
        var a;
        return j.running = !0, r.render(), a = C(), s = !1, p = G(function(b, c) {
          var d, e, f, g, h, i, k, l, n, o, p, q, t, u, v, w;
          for (l = 100 - r.progress, e = p = 0, f = !0, i = q = 0, u = L.length; u > q; i = ++q)
            for (K = L[i], o = null != H[i] ? H[i] : H[i] = [], h = null != (w = K.elements) ? w : [K], k = t = 0, v = h.length; v > t; k = ++t) g = h[k], n = null != o[k] ? o[k] : o[k] = new m(g), f &= n.done, n.done || (e++, p += n.tick(b));
          return d = p / e, r.update(M.tick(b, d)), r.done() || f || s ? (r.update(100), j.trigger("done"), setTimeout(function() {
            return r.finish(), j.running = !1, j.trigger("hide")
          }, Math.max(D.ghostTime, Math.max(D.minTime - (C() - a), 0)))) : c()
        })
      }, j.start = function(a) {
        v(D, a), j.running = !0;
        try {
          r.render()
        } catch (b) {
          i = b
        }
        return document.querySelector(".pace") ? (j.trigger("start"), j.go()) : setTimeout(j.start, 50)
      }, "function" == typeof define && define.amd ? define(function() {
        return j
      }) : "object" == typeof exports ? module.exports = j : D.startOnPageLoad && j.start()
    }).call(this), $('.pluses__left .pluses__paragraph, .pluses__right .pluses__paragraph').readmore({
      moreLink: '<a href="#" class="js-open-hide-btn">Показать еще</a>',
      lessLink: '<a href="#" class="js-open-hide-btn">Скрыть</a>',
      collapsedHeight: 200,
      heightMargin: 16
    });
    $('.introduction__paragraph').readmore({
      moreLink: '<a href="#" class="js-open-hide-btn">Показать еще</a>',
      lessLink: '<a href="#" class="js-open-hide-btn">Скрыть</a>',
      collapsedHeight: 90,
      heightMargin: 16
    });
    if ($('.mobile-only').css('display') == 'block') {
      var this_is_mobile = !0
    } else {
      var this_is_mobile = !1
    }
    if (this_is_mobile == !0) {
      $('.reviews__text__inner').each(function() {
        $(this).readmore({
          moreLink: '<a href="#" class="js-open-hide-btn">Читать полностью</a>',
          lessLink: '<a href="#" class="js-open-hide-btn">Скрыть</a>',
          collapsedHeight: 95,
          heightMargin: 0
        })
      });
      $(".mobile-only .specialist__list").addClass("owl-carousel").owlCarousel({
        items: 1,
        slideBy: 1,
        dots: 1,
        nav: 0,
        loop: 1,
        margin: 0
      })
    }
    var medicineBlock = $(".medicine-block__list").addClass("owl-carousel")
    medicineBlock.owlCarousel({
      items: 3,
      slideBy: 1,
      dots: !1,
      nav: !0,
      loop: !1,
      margin: 0,
      responsiveClass: !0,
      responsive: {
        0: {
          items: 1
        },
        600: {
          items: 2
        },
        1024: {
          items: 3
        }
      }
    })
  });
$(document).ready(function() {
  let videoBlock = $('.video__container');
  videoBlock.addClass('video-styling');
  videoBlock.on('click', function() {
    $(this).removeClass('video-styling')
  })
});
(function($) {
  $(document).ready(function() {
    if ($('div').hasClass('feedback_formsz')) {
      $('.medicine-block').css('display', 'none');
      $('.mobile-only').css('display', 'none')
    }
    var block_show = null;
    var iteration = !1;

    function scrollTracking() {
      var wt = $(window).scrollTop();
      var wh = $(window).height();
      var et = $('.menu-block').offset().top;
      var eh = $('.menu-block').outerHeight();
      if (et >= wt && et + eh <= wh + wt) {
        if (block_show == null || block_show == !1) {
          if (iteration == !1) {
            var script = document.createElement("script");
            script.type = "text/javascript";
            script.src = "https://api-maps.yandex.ru/services/constructor/1.0/js/?um=constructor%3A6013f0c7f0dcf19447307f4d203afdcc659be9e6c064d7e7f3720ac9256eceda&amp;width=100%25&amp;height=400&amp;lang=ru_RU&amp;scroll=true";
            document.getElementById("YaMaps").appendChild(script);
            iteration = !0
          }
        }
        block_show = !0
      } else {
        if (block_show == null || block_show == !0) {}
        block_show = !1
      }
    }
    $(window).scroll(function() {
      scrollTracking()
    });
    $(document).ready(function() {
      scrollTracking()
    })

    // edited 6.10.21
    $('.reviews-carousel').owlCarousel({
      items: 1,
      nav: true,
      dots: false,
      autoplay: true,
      onInitialized: counter,
      onChanged: counter,
    })
    function counter(event) {
      if (!event.namespace) {
        return;
      }
      var slides = event.relatedTarget;
      $('.reviews__list-item-counter').text(slides.relative(slides.current()) + 1 + '/' + slides.items().length);
    };
    var specialistsCarousel = null;
    if($(window).width() < 768) {
      specialistsCarousel = $('.specialist__list.owl-carousel').owlCarousel({
        items: 1,
        nav: false,
        dots: false,
        autoplay: true,
        onInitialized: counterSpecialists,
        onChanged: counterSpecialists,
      })
    }
    function counterSpecialists(event) {
      if (!event.namespace) {
        return;
      };
      if($(event.target).hasClass('owl-hidden')) {
        $(event.target).removeClass('owl-hidden');
      }
      var slides = event.relatedTarget;
      $('.specialist__count-item').text(slides.relative(slides.current()) + 1 + '/' + slides.items().length);
    };
    $(window).on('resize', function(e) {
      setTimeout(function() {
        if($(window).width() < 768 && !specialistsCarousel) {
          specialistsCarousel = $('.specialist__list.owl-carousel').owlCarousel({
            items: 1,
            nav: false,
            dots: false,
            autoplay: true,
            onInitialized: counterSpecialists,
            onChanged: counterSpecialists
          })
        } else if($(window).width() > 767 && specialistsCarousel) {
          specialistsCarousel.trigger('destroy.owl.carousel');
          specialistsCarousel = null;
        }
      }, 1000)
    })
    $('.sertificates-carousel').owlCarousel({
      items: 4,
      margin: 30,
      nav: true,
      dots: false,
      autoplay: true,
      responsive: {
        0: {
          items: 1,
          margin: 0
        },
        576: {
          items: 2,
          margin: 10
        },
        768: {
          items: 3,
          margin: 20
        },
        1200: {
          items: 4,
          margin: 30
        },
      },
      onInitialized: counterSecondary,
      onChanged: counterSecondary,
    });
    function counterSecondary(event) {
      if (!event.namespace) {
        return;
      }
      var slides = event.relatedTarget;
      $('.sertificates__carousel-item-count').text(slides.relative(slides.current()) + 1 + '/' + slides.items().length);
    };
    $('.sertificates-carousel__item').magnificPopup({
      type: 'image',
      gallery:{
        enabled:true
      }
    });
  })
})(jQuery)